<?php
include 'funciones.php';
$evento=$_GET['xevento'];

$cmdsql="select * FROM 	evento WHERE id_evento = ".$evento;
$resultado=mysqli_query(conectar(), $cmdsql);
if (!$resultado) {
    echo "<br>Error de procedimiento";
}

while ($row=mysqli_fetch_assoc($resultado)) {
    $id_evento =$row['id_evento'];
    $nombre_evento =$row['nombre_evento'];
    $id_universidad =$row['id_universidad'];
    $fechainic_evento =$row['fechainic_evento'];
    $descripcion_evento =$row['descripcion_evento'];
    $link_evento =$row['link_evento'];
    $foto_adm_eventos =$row['foto_adm_eventos'];
    
?>
    <div class="modal-header">
        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
        <h4 class="modal-title">Modificar Evento</h4>
    </div>
    <div class="modal-body modbod-exp">
         <form class="form-horizontal form-datoper" role="form" action="" method="post" enctype="multipart/form-data">
             <div class="row form-datper-left">
                <div class="col-xs-12">
                  <div class="form-group" style="display:none">
                    <label class="control-label col-sm-4" for="nombre-evento">Id de evento:</label>
                    <div class="col-sm-8">
                      <input type="number" class="form-control" id="id-evento-mod" name="id-evento-mod" placeholder="Ingrese el nombre del evento" value="<?php echo $id_evento; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="nombre-evento">Nombre de evento:</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="nombre-evento-mod" name="nombre-evento-mod" placeholder="Ingrese el nombre del evento" value="<?php echo $nombre_evento; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="nombre-evento">Departamento:</label>
                    <div class="col-sm-8">
                        <select class="form-control sel-unibydel" name="departamento-mod" id="departamento-mod">
                          <option class="sel-dep" value="Departamento">Departamento</option>
                          <option class="sel-dep" value="Amazonas">Amazonas</option>
                            <option class="sel-dep" value="Áncash">Ancash</option>
                            <option class="sel-dep" value="Apurímac">Apurímac</option>
                            <option class="sel-dep" value="Arequipa">Arequipa</option>
                            <option class="sel-dep" value="Ayacucho">Ayacucho</option>
                            <option class="sel-dep" value="Cajamarca">Cajamarca</option>
                            <option class="sel-dep" value="Callao">Callao</option>
                            <option class="sel-dep" value="Cusco">Cusco</option>
                            <option class="sel-dep" value="Huancavelica">Huancavelica</option>
                            <option class="sel-dep" value="Huánuco">Huánuco</option>
                            <option class="sel-dep" value="Ica">Ica</option>
                            <option class="sel-dep" value="Junín">Junín</option>
                            <option class="sel-dep" value="La Libertad">La Libertad</option>
                            <option class="sel-dep" value="Lambayeque">Lambayeque</option>
                            <option class="sel-dep" value="Lima">Lima</option>
                            <option class="sel-dep" value="Loreto">Loreto</option>
                            <option class="sel-dep" value="Madre de Dios">Madre de Dios</option>
                            <option class="sel-dep" value="Moquegua">Moquegua</option>
                            <option class="sel-dep" value="Pasco">Pasco</option>
                            <option class="sel-dep" value="Piura">Piura</option>
                            <option class="sel-dep" value="Puno">Puno</option>
                            <option class="sel-dep" value="San Martín">San Martín</option>
                            <option class="sel-dep" value="Tacna">Tacna</option>
                            <option class="sel-dep" value="Tumbes">Tumbes</option>
                            <option class="sel-dep" value="Ucayali">Ucayali</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group" id="row-univ">
                    <label class="control-label col-sm-4">Universidad:</label>
                    <div class="col-sm-8">
                        <select class="form-control sel-unibydel" name="univ-dep-mod" id="univ-dep-mod">
                          <option class="sel-prov" value="universidad">Seleccione el departamento</option>
                        </select>
                    </div>
                  </div>
                  <div class="form-group" style="display:none">
                    <label class="control-label col-sm-4">Universidad antigua:</label>
                    <div class="col-sm-8">
                        <input type="number" class="form-control" id="id-univant-mod" name="id-univant-mod" placeholder="Ingrese el nombre del evento" value="<?php echo $id_universidad; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="inicio-evento">Inicio de evento<br>(XX de Mes):</label>
                    <div class="col-sm-8">
                        <div class='input-group date' style="display:block" id='datetimepicker8'>
                            <input type="text" class="form-control inp-sevolunt" id="inicio-evento-mod" name="inicio-evento-mod" placeholder="Inicio de evento" required style="border-radius:4px;" value="<?php echo $fechainic_evento; ?>">
                            <span class="input-group-addon" id="btn-dp-3" style="visibility: hidden;position: relative;top: -60px;">
                                <span class="fa fa-calendar" id="btn-dp-4" >
                                </span>
                            </span>
                        </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="dscr-evento-mod">Descripción de evento:</label>
                    <div class="col-sm-8">
                        <textarea rows="2" style="width:100%" class="form-control" name="dscr-evento-mod" id="dscr-evento-mod"><?php echo $descripcion_evento; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="link-evento-mod">Link de evento (Facebook):</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="link-evento-mod" name="link-evento-mod" placeholder="Ingrese el link del evento" value="<?php echo $link_evento; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4" >Imagen de evento:</label>
                    <div class="col-sm-8">
                          <label for="foto-evento-mod"><a class="btn btn-md form-control input-sm selec-arch-mod">Selecciona archivo</a></label>
                        <div id="nombrefoto"></div>
                          <input type="file" class="archivo-subir" id="foto-evento-mod" name="foto-evento-mod" style="display:none" data-id="2">
                        <span class="span-foto-estado" id="estado-cambio-2"></span>
                    </div>
                  </div>

                </div>
             </div>
             <div class="row">
                 <div class="col-xs-12 datpersbut-wrap">
                      <div class="datpersbut-mod">
                        <button type="submit" class="btn btn-datper-mod btn-sm" name="evento-mod-sub" >Guardar</button>
                        <button type="button" class="btn btn-datper-canc btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                     </div>
                 </div>
             </div>
        </form>
    </div>
<?php
    }
?>


<script>
$(function () {
    $('#datetimepicker8').datetimepicker({
        viewMode: 'years',
        format:'YYYY-MM-DD',
        locale: 'es'
    });
});
$( "#inicio-evento-mod" ).unbind().on( "focus", function() {
  $('#btn-dp-3').click();
});
$("#departamento-mod").change(function() {
    var temp=0;
    var departamento= this.value;
    var prov=departamentos[departamento];
    $("#univ-dep-mod").html(prov);
    //console.log(prov);
});
    
</script>