<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    echo "<script> window.location.href='index.php';</script>";
}



$universidadeslista = listaruniversidades();

$amazonas   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ancash     = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$apurimac   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$arequipa   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ayacucho   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$cajamarca  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$callao     = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$cusco      = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$huancavelica = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$huanuco = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ica = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$junin = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$lalibertad = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$lambayeque = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$lima  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$loreto  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$madrededios = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$moquegua   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$pasco   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$piura  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$puno   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$sanmartin   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$tumbes   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ucayali   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";

$universidadesarr = array();

while ($row=mysqli_fetch_assoc($universidadeslista)) {
    $id_universidad=$row['id_universidad'];
    $nombre_universidad=$row['nombre_universidad'];
    $departamento=$row['departamento'];

    switch ($departamento) {
        case "Amazonas":
            $amazonas   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Áncash":
            $ancash     .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Apurímac":
            $apurimac   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Arequipa":
            $arequipa   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Ayacucho":
            $ayacucho   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Cajamarca":
            $cajamarca  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Callao":
            $callao     .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Cusco":
            $cusco      .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Huancavelica":
            $huancavelica .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Huánuco":
            $huanuco .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Ica":
            $ica .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Junín":
            $junin .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "La Libertad":
            $lalibertad .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Lambayeque":
            $lambayeque .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Lima":
            $lima  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Loreto":
            $loreto  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Madre de Dios":
            $madrededios .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Moquegua":
            $moquegua   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Pasco":
            $pasco   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Piura":
            $piura  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Puno":
            $puno   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "San Martín":
            $sanmartin   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Tumbes":
            $tumbes   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Ucayali":
            $ucayali   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
    }


}
mysqli_free_result($universidadeslista);

$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();


?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Eventos</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body style="min-width:450px;height:100%">
<script type="text/javascript" src="js/jquery.min.js"></script>
    
<div id="header"></div>
    
<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Generar Códigos</span>
    </div>
</div>
<div class="container">
    <h1 class="h2">
        <span>Paso 1</span>
    </h1>
    <h1 class="h4">
        <span>Seleccione universidad sede</span>
    </h1>
</div>

<div class="container cont-eventos" style="min-height: 100%;margin: -472px auto 0;">
    <div class="row" style="padding-top: 472px;">
        <div class="col-xs-6">
            <div class="tit-sel-univdel">Departamento:</div>
            <select class="form-control sel-unibydel" name="departamento" id="departamento">
              <option class="sel-dep" value="Departamento">Departamento</option>
              <option class="sel-dep" value="Amazonas">Amazonas</option>
                <option class="sel-dep" value="Áncash">Ancash</option>
                <option class="sel-dep" value="Apurímac">Apurímac</option>
                <option class="sel-dep" value="Arequipa">Arequipa</option>
                <option class="sel-dep" value="Ayacucho">Ayacucho</option>
                <option class="sel-dep" value="Cajamarca">Cajamarca</option>
                <option class="sel-dep" value="Callao">Callao</option>
                <option class="sel-dep" value="Cusco">Cusco</option>
                <option class="sel-dep" value="Huancavelica">Huancavelica</option>
                <option class="sel-dep" value="Huánuco">Huánuco</option>
                <option class="sel-dep" value="Ica">Ica</option>
                <option class="sel-dep" value="Junín">Junín</option>
                <option class="sel-dep" value="La Libertad">La Libertad</option>
                <option class="sel-dep" value="Lambayeque">Lambayeque</option>
                <option class="sel-dep" value="Lima">Lima</option>
                <option class="sel-dep" value="Loreto">Loreto</option>
                <option class="sel-dep" value="Madre de Dios">Madre de Dios</option>
                <option class="sel-dep" value="Moquegua">Moquegua</option>
                <option class="sel-dep" value="Pasco">Pasco</option>
                <option class="sel-dep" value="Piura">Piura</option>
                <option class="sel-dep" value="Puno">Puno</option>
                <option class="sel-dep" value="San Martín">San Martín</option>
                <option class="sel-dep" value="Tacna">Tacna</option>
                <option class="sel-dep" value="Tumbes">Tumbes</option>
                <option class="sel-dep" value="Ucayali">Ucayali</option>
            </select>
        </div>
        <div class="col-xs-6">
            <div class="tit-sel-univdel">Universidad:</div>
            <select class="form-control sel-unibydel" name="univ-dep" id="univ-dep">
              <option class="sel-prov" value="universidad">Seleccione el departamento</option>
            </select>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-8 col-lg-9" style="padding-top:20px;">
            <a class="btn btn-md btn-info" href="#andir-univ-mod" data-target="#andir-univ-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin">Agregar Nueva Universidad</a>
            <a class="btn btn-md btn-info" id="link-sgte" href="#">Siguiente</a>
        </div>
    </div>
</div>

<div id="footer"></div>
    
<?php if($sesioninic){ ?>
    
<div id="andir-univ-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Añadir Universidad</h4>
            </div>
            <div class="modal-body modbod-exp">
                 <form class="form-horizontal form-datoper" role="form" action="" method="post" enctype="multipart/form-data">
                     <div class="row form-datper-left">
                        <div class="col-xs-12">
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="nombre-evento">Departamento:</label>
                            <div class="col-sm-8">
                                <select class="form-control sel-unibydel" name="departamento" id="departamento">
                                  <option class="sel-dep" value="Departamento">Departamento</option>
                                  <option class="sel-dep" value="Amazonas">Amazonas</option>
                                    <option class="sel-dep" value="Áncash">Ancash</option>
                                    <option class="sel-dep" value="Apurímac">Apurímac</option>
                                    <option class="sel-dep" value="Arequipa">Arequipa</option>
                                    <option class="sel-dep" value="Ayacucho">Ayacucho</option>
                                    <option class="sel-dep" value="Cajamarca">Cajamarca</option>
                                    <option class="sel-dep" value="Callao">Callao</option>
                                    <option class="sel-dep" value="Cusco">Cusco</option>
                                    <option class="sel-dep" value="Huancavelica">Huancavelica</option>
                                    <option class="sel-dep" value="Huánuco">Huánuco</option>
                                    <option class="sel-dep" value="Ica">Ica</option>
                                    <option class="sel-dep" value="Junín">Junín</option>
                                    <option class="sel-dep" value="La Libertad">La Libertad</option>
                                    <option class="sel-dep" value="Lambayeque">Lambayeque</option>
                                    <option class="sel-dep" value="Lima">Lima</option>
                                    <option class="sel-dep" value="Loreto">Loreto</option>
                                    <option class="sel-dep" value="Madre de Dios">Madre de Dios</option>
                                    <option class="sel-dep" value="Moquegua">Moquegua</option>
                                    <option class="sel-dep" value="Pasco">Pasco</option>
                                    <option class="sel-dep" value="Piura">Piura</option>
                                    <option class="sel-dep" value="Puno">Puno</option>
                                    <option class="sel-dep" value="San Martín">San Martín</option>
                                    <option class="sel-dep" value="Tacna">Tacna</option>
                                    <option class="sel-dep" value="Tumbes">Tumbes</option>
                                    <option class="sel-dep" value="Ucayali">Ucayali</option>
                                </select>
                            </div>
                          </div>
                            <div class="form-group row">
                              <label for="univag-dep" class="col-sm-4 col-form-label" style="text-align: right;">Agregar universidad</label>
                              <div class="col-sm-8">
                                <input class="form-control" type="text" placeholder="Ingrese el nombre de la universidad a agregar." name="univag-dep" id="univag-dep">
                                <span>No se olvide de también seleccionar el departamento donde se encuentra la universidad.<br> Al ingresar el nombre especifique si es sede. Por por ejemplo: "Universidad del Perú - Sede Lima"</span>
                              </div>
                            </div>

                        </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-12 datpersbut-wrap">
                              <div class="datpersbut-mod">
                                <button type="submit" class="btn btn-datper-mod btn-sm" name="univ-adm-sub" >Guardar</button>
                                <button type="button" class="btn btn-datper-canc btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                             </div>
                         </div>
                     </div>
                </form>
            </div>

        </div>
    </div>
</div>
    
<?php } ?>
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script src="js/jquery.carouFredSel.js"></script>
<script src="js/moment-with-locales.js"></script>
<script src="js/bootstrap-datetimepicker.js"></script>
<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 2, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<script>

$("#univ-dep").change(function() {
    if($(this).val() != 0){
        var linksgte= "generarcodigos2.php?xiduniv="+$(this).val();
    }else{
        var linksgte= "#";
    }

    $("#link-sgte").attr("href",linksgte);
});
    
</script>
    
    
<script>

var departamentos ={

    "Departamento":'<option class="sel-prov" value="universidad">Seleccione el departamento</option>',
    "Amazonas": "<?php echo $amazonas; ?>",
    "Áncash": "<?php echo $ancash; ?>",
    "Apurímac": "<?php echo $apurimac; ?>",
    "Arequipa": "<?php echo $arequipa; ?>",
    "Ayacucho": "<?php echo $ayacucho; ?>",
    "Cajamarca": "<?php echo $cajamarca; ?>",
    "Callao": "<?php echo $callao; ?>",
    "Cusco": "<?php echo $cusco; ?>",
    "Huancavelica": "<?php echo $huancavelica; ?>",
    "Huánuco": "<?php echo $huanuco; ?>",
    "Ica": "<?php echo $ica; ?>",
    "Junín": "<?php echo $junin; ?>",
    "La Libertad": "<?php echo $lalibertad; ?>",
    "Lambayeque": "<?php echo $lambayeque; ?>",
    "Lima": "<?php echo $lima; ?>",
    "Loreto": "<?php echo $loreto; ?>",
    "Madre de Dios": "<?php echo $madrededios; ?>",
    "Moquegua": "<?php echo $moquegua; ?>",
    "Pasco": "<?php echo $pasco; ?>",
    "Piura": "<?php echo $piura; ?>",
    "Puno": "<?php echo $puno; ?>",
    "San Martín": "<?php echo $sanmartin; ?>",
    "Tumbes": "<?php echo $tumbes; ?>",
    "Ucayali": "<?php echo $ucayali; ?>",
    
};
/*
$("#departamento").change(function() {
    var temp=0;
    var departamento= this.value;
    var prov=departamentos[departamento];
    var provarr = prov.split(",");
    var startlista="<ul>";

    var midlista="";
    for(provinc in provarr){
        midlista+="<option class='sel-prov' value='"+provarr[temp]+"'>"+provarr[temp]+"</option>";
        temp++;
    }
    var listaprov=startlista+midlista;
    $("#univ-dep").html(listaprov);
});
*/
$("#departamento").change(function() {
    var temp=0;
    var departamento= this.value;
    var prov=departamentos[departamento];
    $("#univ-dep").html(prov);
    //console.log(prov);
});
    
$("#univ-dep").change(function() {
    console.log("asd");
    var id_universidad=this.value;

    $(".cont-unidele").css({
        "opacity": "0.5",
        "pointer-events":"none"
    });
    $(".univdel-card-wrapp").html("Cargando...");

    $.ajax({url:"apost_append_delegados.php?xid_universidad="+id_universidad,cache:false,success:function(result){
        $(".univdel-card-wrapp").html(result);
                $(".cont-unidele").css({
                    "opacity": "1",
                    "pointer-events":"inherit"
                });
    }});
    
});
</script>
    
<?php

if (isset($_POST['univ-adm-sub'])){

    if(!empty($_POST['departamento']) && !empty($_POST['univag-dep'])){
        
        $departamento=$_POST['departamento'];
        $nombre_universidad=$_POST['univag-dep'];
        $estado_universidad = 1;
        $id_universidad= anadiruniversidad($nombre_universidad, $departamento, $estado_universidad);
        
        if($id_universidad =="Error"){
            echo "<script>alert('Hubo un error al añadir la universidad. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script> window.location.href='generarcodigos.php';</script>";
        }
    }else{

        echo "<script>alert('Ingrese ambos datos.')</script>";
    }

}

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}   
?>

</body>
</html>