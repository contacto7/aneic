<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Comité</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
</head>
<body>
<script type="text/javascript" src="js/jquery.min.js"></script>
<nav class="navbar navbar-nav navbar-fixed-top" role="navigation">
    <div class="nav-top-wrap">
        <div class="nav-top-inn">
            <img src="foto_index/1.jpg" class="nav-top-img" alt="foto de cabecera">
            <div class="nav-top-cuad"></div>
        </div>
    </div>
	<div class="container cont-nav">
		<div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar,#navbar-dwn" aria-expanded="false" aria-controls="navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>
				<a class="navbar-brand" href="index.php"><img src="img/logoaneicv1.png" class="nav-img-logo" alt="logo"></a>
		</div>
		<div id="navbar-dwn" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-middle">
 				<li class="active">
                    <a href="univydelegados.php">Universidades y delegados</a>
                </li>
 				<li class="active">
                    <a href="eventos.php">Eventos</a>
                </li>
 				<li class="active">
                    <a href="nosotros.php">Nosotros</a>
                </li>
 				<li class="active-nav-a">
                    <a href="comite.php">Comité</a>
                </li>
            </ul>
		</div>
	</div>
</nav>
<div class="bot-nav-wrap" style="top:180px;">
    <div class=" container bot-nav-inn">
        <div class="bot-nav-ap">
            <span class="glyphicon glyphicon-time"></span>7:00 17:00 &nbsp; 
        </div>
        <div class="bot-nav-tel">
             <span class="glyphicon glyphicon-earphone"></span>999596296 &nbsp;
        </div>
    </div>
</div>

<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Registro de delegados</span>
    </div>
</div>
<div class="container cont-comite">
    <div class="row row-diagrama">
        <div class="diagrama-organizacional">
            <div class="diagrama-org-ext">

                <div class="diagrama-org-wrapp diagrama-org-1">
                    <div class="diagrama-org-nombre">
                        <span>León Reyes, Heser Harold</span>
                    </div>
                    <div class="diagrama-org-puesto">
                        Presidente
                    </div>
                    <a class="diagrama-org-img toscroll" href="#descr-comite-1">
                        <img alt="Imagen de integrante de comite" class="img-circle" src="fotos_comite/wenceslao.jpg">
                    </a>
                    <div class="diagr-lin-cent"></div>
                </div>
                <div class="diagrama-org-wrapp diagrama-org-2">
                    <div class="diagrama-org-nombre">
                        <span>León Reyes, Heser Harold</span>
                    </div>
                    <div class="diagrama-org-puesto">
                        Secretario General
                    </div>
                    <a class="diagrama-org-img toscroll" href="#descr-comite-2">
                        <img alt="Imagen de integrante de comite" class="img-circle" src="fotos_comite/yiyo.jpg">
                    </a>
                    <div class="diagr-lin-cent"></div>
                </div>
                <div class="diagrama-org-wrapp diagrama-org-3">
                    <div class="diagrama-org-nombre">
                        <span>León Reyes, Heser Harold</span>
                    </div>
                    <div class="diagrama-org-puesto">
                        Secretario de Imagen Institucional
                    </div>
                    <a class="diagrama-org-img toscroll" href="#descr-comite-3" >
                        <img alt="Imagen de integrante de comite" class="img-circle" src="fotos_comite/heser.jpg">
                    </a>
                    <div class="diagr-lin-cent"></div>
                </div>
                <div class="diagrama-org-wrapp diagrama-org-4">
                    <div class="diagrama-org-nombre">
                        <span>León Reyes, Heser Harold</span>
                    </div>
                    <div class="diagrama-org-puesto">
                        Tesorero
                    </div>
                    <a class="diagrama-org-img toscroll" href="#descr-comite-4">
                        <img alt="Imagen de integrante de comite" class="img-circle" src="fotos_comite/hugo.jpg">
                    </a>
                </div>

            </div>
        </div>

    </div>
    <div class="row row-comite-descr">
        <div class="descr-comite-wrapp">
            <div class="descr-comite-inn" id="descr-comite-1">
                <div class="descr-comite-nombr">
                    León Reyes, Heser Harold
                </div>
                <div class="descr-comite-redes">
                    <a class="img-comite-red" href="#" target="_blank">
                        <img alt="imagen logo de red social" class="img-circle" src="img/fb-comite.png">
                    </a>
                    <a class="img-comite-red" href="#" target="_blank">
                        <img alt="imagen logo de red social" class="img-circle" src="img/linkedin-comite.png">
                    </a>
                </div>
                <div class="descr-comite-descr">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                    
                </div>
            </div>
            
            <div class="descr-comite-inn" id="descr-comite-2">
                <div class="descr-comite-nombr">
                    León Reyes, Heser Harold
                </div>
                <div class="descr-comite-redes">
                    <a class="img-comite-red" href="#" target="_blank">
                        <img alt="imagen logo de red social" class="img-circle" src="img/fb-comite.png">
                    </a>
                    <a class="img-comite-red" href="#" target="_blank">
                        <img alt="imagen logo de red social" class="img-circle" src="img/linkedin-comite.png">
                    </a>
                </div>
                <div class="descr-comite-descr">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                </div>
            </div>
            
            <div class="descr-comite-inn" id="descr-comite-3">
                <div class="descr-comite-nombr">
                    León Reyes, Heser Harold
                </div>
                <div class="descr-comite-redes">
                    <a class="img-comite-red" href="#" target="_blank">
                        <img alt="imagen logo de red social" class="img-circle" src="img/fb-comite.png">
                    </a>
                    <a class="img-comite-red" href="#" target="_blank">
                        <img alt="imagen logo de red social" class="img-circle" src="img/linkedin-comite.png">
                    </a>
                </div>
                <div class="descr-comite-descr">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                </div>
            </div>
            
            <div class="descr-comite-inn" id="descr-comite-4">
                <div class="descr-comite-nombr">
                    León Reyes, Heser Harold
                </div>
                <div class="descr-comite-redes">
                    <a class="img-comite-red" href="#" target="_blank">
                        <img alt="imagen logo de red social" class="img-circle" src="img/fb-comite.png">
                    </a>
                    <a class="img-comite-red" href="#" target="_blank">
                        <img alt="imagen logo de red social" class="img-circle" src="img/linkedin-comite.png">
                    </a>
                </div>
                <div class="descr-comite-descr">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                </div>
            </div>
        </div>
    
    </div>
</div>

<footer class="foot-wrap">
    <div class="footer-inner">
        <div class="footer-izq">
            <div class="aneic-footer">&copy; Aneic Perú 2017</div>
            <div class="div-footer-2"> - </div>
            <div class="copr-aqm"><span>Desarrollado por </span>
                <a href="http://www.inoloop.com" target="_blank" class="logo-foot-wrap">    <img class="logo-foot" src="img/logo-ino.png"> 
                </a>
            </div>
        </div>
        <div class="footer-der">
            <div class="fb-footer"><span>Síguenos en: </span><a href="https://www.facebook.com/aneicp" class="fb-link" target="_blank"><img src="img/fb-logo.png" class="logo-fb-img" alt="logo de facebook"></a></div>
        </div>
        <div class="footr-dwn">
            <a class="busq-cert-wrap" href="buscarcert.php" target="_blank">
                <div class="busq-cer-icon"><span class="glyphicon glyphicon-search"></span></div>
                <div class="busq-cer-lin">Buscar</div>
                <div class="busq-cer-lin">Certificado</div>
            </a>
        </div>
    </div>
</footer>

<div id="login-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title">Login</h4>
            </div>
            <div class="modal-body">
                 <form class="form-inline" role="form" action="" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="correo">Correo</label>
                        <input type="email" class="form-control input-sm" name="correo" id="user_mail" placeholder="Correo" required>
                    </div>     
                    <div class="form-group">
                        <label class="sr-only" for="contrasena">Contraseña</label>
                        <input type="password" class="form-control input-sm" name="contrasena" id="password" placeholder="Contraseña" required>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox"><small>Recordarme</small></label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm" name="ingresar" >Entrar</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script src="js/jquery.carouFredSel.js"></script>
<script>
$("a.toscroll").on('click',function() {
    var url =$(this).attr("href");
    //console.log(url);
    var hash = url.substring(url.indexOf("#")+1);
    $('html, body').animate({
        scrollTop: $('#'+hash).offset().top -200
    }, 500);
    return false;
});
</script>
</body>
</html>