<?php
//error_reporting(E_ALL);
//ini_set('display_errors','on');
if(!($_SERVER['HTTP_ORIGIN'] == "http://aneicperu.com" || $_SERVER['HTTP_ORIGIN'] == "https://aneicperu.com" || $_SERVER['HTTP_ORIGIN'] == "http://www.aneicperu.com" || $_SERVER['HTTP_ORIGIN'] == "https://www.aneicperu.com")) {
    echo "Por favor ingrese del dominio de Aneic";
}else{
    
    include 'funciones.php';

    $link = conectar();
    
    if (!isset($_POST['x_i1'])) exit;

    $id_evento = $_POST['x_i1'];
    $cant_descargables = $_POST['x_i2'];
    $temp_inicio = $_POST['x_i3'];
    
    $cmdsql="select participante_certificado.*,asistente.*,evento.*,universidad.* from participante_certificado JOIN asistente ON participante_certificado.id_asistente=asistente.id_asistente JOIN evento ON participante_certificado.id_evento=evento.id_evento JOIN universidad ON participante_certificado.id_universidad=universidad.id_universidad  WHERE evento.id_evento=".$id_evento." ORDER BY participante_certificado.id_participante_certificado ASC LIMIT $temp_inicio,$cant_descargables  ";

    //echo $cmdsql;
    $resultado = mysqli_query($link, $cmdsql);

    $error_votacion_code = mysqli_errno($link);
    
    if ($error_votacion_code > 0) {
        echo "Ocurrió algún error al actualizar la base de datos. Si el problema persiste, contáctese con servicio técnico.";
    }else{

        $temp=0;
        $width_canvas = 870*2;
        $height_canvas = 620*2;
        while ($row=mysqli_fetch_assoc($resultado)) {

            $id_participante_certificado=$row['id_participante_certificado'];

            $nombre_asistente=$row['nombre_asistente'];
            $calidad_participante_certificado=$row['calidad_participante_certificado'];
            $nombre_evento=$row['nombre_evento'];
            $nombre_universidad=$row['nombre_universidad'];
            $fecemi_participante_certificado=$row['fecemi_participante_certificado'];
            
            //VARIABLES DE NOMBRE
            $left_nombre = $row['left_nombre'];
            $top_nombre = $row['top_nombre'];
            $tamano_nombre = $row['tamano_nombre'];
            $estilo_nombre = $row['estilo_nombre'];
            $fuente_nombre = $row['fuente_nombre'];
            $color_nombre = $row['color_nombre'];

            //VARIABLES DE CALIDAD
            $left_calidad = $row['left_calidad'];
            $top_calidad = $row['top_calidad'];
            $tamano_calidad = $row['tamano_calidad'];
            $estilo_calidad = $row['estilo_calidad'];
            $fuente_calidad = $row['fuente_calidad'];
            $color_calidad = $row['color_calidad'];
            
            //VARIABLES DE QR
            $left_qr = $row['left_qr'];
            $top_qr = $row['top_qr'];
            $tamano_qr = $row['tamano_qr'];
            
            //EVENTO
            $foto_certificado = $row['foto_certificado'];

            //INLINE CSS NOMBRE
            $font_nombre="";

            $left_nombre_css = $left_nombre."px";
            $top_nombre_css = $top_nombre."px";
            $tamano_nombre_css = $tamano_nombre."px";

            $font_nombre.="$tamano_nombre_css ";

            if($estilo_nombre == 1){
                $estilo_nombrestyle_css = "normal";
                $estilo_nombreweight_css = "100";

                $font_nombre.="";
            }elseif($estilo_nombre == 2){
                $estilo_nombrestyle_css = "italic";
                $estilo_nombreweight_css = "100";

                $font_nombre.="italic";
            }elseif($estilo_nombre == 3){
                $estilo_nombrestyle_css = "normal";
                $estilo_nombreweight_css = "bold";

                $font_nombre.="bold";
            }elseif($estilo_nombre == 4){
                $estilo_nombrestyle_css = "italic";
                $estilo_nombreweight_css = "bold";

                $font_nombre.="bold";
            }
            $fuente_nombre_css = $fuente_nombre;
            $color_nombre_css = $color_nombre;

            //INLINE CSS CALIDAD
            $left_calidad_css = $left_calidad."px";
            $top_calidad_css = $top_calidad."px";
            $tamano_calidad_css = $tamano_calidad."px";

            if($estilo_calidad == 1){
                $estilo_calidadstyle_css = "normal";
                $estilo_calidadweight_css = "100";
            }elseif($estilo_calidad == 2){
                $estilo_calidadstyle_css = "italic";
                $estilo_calidadweight_css = "100";
            }elseif($estilo_calidad == 3){
                $estilo_calidadstyle_css = "normal";
                $estilo_calidadweight_css = "bold";
            }elseif($estilo_calidad == 4){
                $estilo_calidadstyle_css = "italic";
                $estilo_calidadweight_css = "bold";
            }
            $fuente_calidad_css = $fuente_calidad;
            $color_calidad_css = $color_calidad;


            //INLINE CSS QR
            $left_qr_css = $left_qr."px";
            $top_qr_css = $top_qr."px";
            $tamano_qr_css = $tamano_qr."px";
            
?>
            
            
            
            <div class="fila-certificado-canvas" id="canvas-<?php echo $temp; ?>">
            
                <div class="tit-cert-canvas">Certificado:</div>
                <div class="canvas-cert-inn" id="canvas-dwn-<?php echo $temp; ?>">
                    <div class="elemento-drag nombr-participante" style="<?php echo "left:$left_nombre_css; top:$top_nombre_css; font-size:$tamano_nombre_css; font-style: $estilo_nombrestyle_css; font-weight: $estilo_nombreweight_css; font-family: $fuente_nombre_css; color: $color_nombre_css;"; ?>text-align:center;">
                        <span style="left:-50%;position:relative"><?php echo $nombre_asistente; ?></span>
                    </div>                    
                    <div class="elemento-drag calidad-participante" style="<?php echo "left:$left_calidad_css; top:$top_calidad_css; font-size:$tamano_calidad_css; font-style: $estilo_calidadstyle_css; font-weight: $estilo_calidadweight_css; font-family: $fuente_calidad_css; color: $color_calidad_css;"; ?>text-align:center;">
                        <span style="left:-50%;position:relative"><?php echo $calidad_participante_certificado; ?></span>
                    </div>
                    <div id="demo-<?php echo $temp ?>" class="qr-canvas-cert" style="position:absolute; left: <?php echo $left_qr_css ?>; top: <?php echo $top_qr_css ?>;"></div>
                    <canvas id="canvas_certificado-<?php echo $temp ?>" width="<?php echo $width_canvas; ?>" height="<?php echo $height_canvas; ?>" class="canvas-adm-cert"></canvas>
                </div>
            </div>
            <script>

                var myCanvas<?php echo $temp ?> = document.getElementById('canvas_certificado-<?php echo $temp ?>');
                var ctx<?php echo $temp ?> = myCanvas<?php echo $temp ?>.getContext('2d');
                                
                var img<?php echo $temp ?> = new Image;
                
                 img<?php echo $temp ?>.onload = function(){
                    ctx<?php echo $temp ?>.drawImage(img<?php echo $temp ?>, 0, 0, img<?php echo $temp ?>.width,    img<?php echo $temp ?>.height,     // source rectangle
                                       0, 0, myCanvas<?php echo $temp ?>.width, myCanvas<?php echo $temp ?>.height); 
                 };
                
                img<?php echo $temp ?>.src = "<?php echo $foto_certificado; ?>";
                
                var linkarch="http://www.aneicperu.com/certificado.php?xid="+<?php echo $id_participante_certificado ?>;

                jQuery("#demo-"+<?php echo $temp ?>).qrcode({
                    //render:"table"
                    width: <?php echo $tamano_qr ?>,
                    height: <?php echo $tamano_qr ?>,
                    text: linkarch
                });
                
                
            </script>
            
            
            
  <?php          
            $temp++;
        }
        
    }

}

?>