<?php 
//xph es el tipo de pagina
//xhs es para ver si es administrador

//1 : Delegados
//2 : Certificados
//3 : Asistencia
//4 : Votacion
//5 : Usuario

//validando las variables de sesion
include 'funciones.php';

//Inicializamos variables
$sesioninic = 0;
$tipo_pagina = 0;
$img_logo_1 = "";
$img_logo_2 = "";

$img_logo_1  = $_POST['xdf1'] ;
$img_logo_2  = $_POST['xdf2'] ;
$sesioninic  = $_POST['xhs'] ;

//CLASES DE LOS LINKS
$a_class_1= "";
$a_class_2= "";
$a_class_3= "";
$a_class_4= "";
$a_class_5= "";

if (isset($_POST['xph'])){
    
    $tipo_pagina = $_POST['xph'] ;
    switch ($tipo_pagina) {
        case 1:
            $a_class_1= "active-nav-a";
            break;
        case 2:
            $a_class_2= "active-nav-a";
            break;
        case 3:
            $a_class_3= "active-nav-a";
            break;
        case 4:
            $a_class_4= "active-nav-a";
            break;
        case 5:
            $a_class_5= "active-nav-a";
            break;
    }

}
?>

<nav class="navbar navbar-nav navbar-fixed-top" role="navigation">
<?php if($sesioninic==1){ ?>
		<div id="navbar" class="" style="position: fixed; z-index:100; left : 50px; top: 50px">
            <div class="container">
                <ul class="nav navbar-nav">
				    <li style="display:inline-block">
                        <button class="btn btn-sm btn-info" onclick="location.href='adm-regdel.php'" type="button"> ADMINISTRAR </button>
                    </li>
                    <li style="display:inline-block"><form class="form-inline" role="form"    action="" method="get"><button type="submit" class="btn btn-sm btn-warning" name="cerrar-sesion">Cerrar Sesión</button>
                        </form></li>

                </ul>
            </div>

		</div>
 <?php } ?>
	<div class="container cont-nav">
		<div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-dwn" aria-expanded="false" aria-controls="navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>
				<a class="navbar-brand" href="index.php"><img src="<?php echo $img_logo_2; ?>" class="nav-img-logo" alt="logo"></a>
		</div>
		<div id="navbar-dwn" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-middle">

                <li class="dropdown <?php echo $a_class_1; ?>">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Delegados <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li class="active-nav-a"><a href="adm-regdel.php">Acreditación</a></li>
                    <li class=""><a href="adm_gen_delegados.php">Descargar base de delegados</a></li>
                    <li class=""><a href="adm_elim_delegados.php">Depurar delegados</a></li>
                  </ul>
                </li>
                <li class="dropdown <?php echo $a_class_2; ?>">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Certificados <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li class=""><a href="generarcodigos.php">Generar códigos qr</a></li>
                    <li class=""><a href="generarcertificados.php">Generar certificados</a></li>
                    <li class=""><a href="adm_elim_certificados.php">Depurar certificados</a></li>
                  </ul>
                </li>
 				<li class="active <?php echo $a_class_3; ?>">
                    <a href="adm_asistencia_lista.php">Asistencia</a>
                </li>
 				<li class="active <?php echo $a_class_4; ?>">
                    <a href="adm_votacion_lista.php">Votación</a>
                </li>
 				<li class="active <?php echo $a_class_5; ?>">
                    <a href="adm_cambiar_clave.php">Usuario</a>
                </li>
            </ul>
		</div>
	</div>
</nav>