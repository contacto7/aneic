<?php 
session_start();
$nombres="";
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    echo "<script> window.location.href='index.php';</script>";
}
if(!isset($_GET['xiduniv']) && !isset($_GET['xidevento']) && !isset($_GET['xprimer_id']) && !isset($_GET['xprimer_id_cert'])){
    echo "<script> window.location.href='generarcodigos.php';</script>";
}

$id_universidad=$_GET['xiduniv'];
$id_evento=$_GET['xidevento'];
$primer_idcert_insertado = $_GET['xprimer_id_cert'];
$primer_id_insertado = $_GET['xprimer_id'];

$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();

?>
<!DOCTYPE html>
<html ng-app="">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jquery.raty.css">
    <link href="css/princ.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery.min.older.js"></script>
</head>
<body style="background-color:#eee;">
<script type="text/javascript" src="js/jquery.min.js"></script>
    
<div id="header"></div>

<div class="container" style="padding-top:110px;">

    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal form-qr-settings" style="">
              <div class="form-group">
                <label for="altpapel">Altura de papel (Referencia: A4 = 842)</label>
                <input type="number" class="form-control" id="altpapel" >
              </div>
              <div class="form-group">
                <label for="anchpapel">Ancho de papel (Referencia: A4 = 595)</label>
                <input type="number" class="form-control" id="anchpapel" >
              </div>
              <div class="form-group">
                <label for="tamqr">Tamaño de Qr</label>
                <input type="number" class="form-control" id="tamqr" >
              </div>
              <div class="form-group">
                <label for="grosorln">Grosor de línea de borde</label>
                <input type="number" class="form-control" id="grosorln">
              </div>
              <div class="form-group">
                <label for="espaciadoqr">Espaciado entre Qr y borde</label>
                <input type="number" class="form-control" id="espaciadoqr">
              </div>
              <div class="form-group">
                <label for="distarr">Distancia a arriba</label>
                <input type="number" class="form-control" id="distarr">
              </div>
              <div class="form-group">
                <label for="distizq">Distancia a izquierda</label>
                <input type="number" class="form-control" id="distizq">
              </div>
              <div class="form-group">
                <a class="btn btn-info" id="genqr" onclick="dataqr()">Generar QR </a> 
                  <a class="btn btn-info" id="download" data-nombre="cert-h-e1.png" data-padreid="#demo" onclick="descargarqrs()">Descargar </a>
              </div>

                <a class="btn btn-info" id="download-verd" style="display:none">Descargar </a>
            </form>
        </div>

        <div class="canvas-ant" style="display:none"></div>
        <div id="demo"></div>
<?php 

$result=listarcertificados($id_evento , $primer_idcert_insertado);
$temp=0;

while ($row=mysqli_fetch_assoc($result)) {
    
    $id_participante_certificado=$row['id_participante_certificado'];
    $id_evento=$row['id_evento'];
    $id_certificado=$row['id_certificado'];
    $nombre_asistente=$row['nombre_asistente'];
    $calidad_participante_certificado=$row['calidad_participante_certificado'];

?>
        <div id="demo-<?php echo $temp; ?>" class="qr-canvas" data-idevento="<?php echo $id_evento; ?>" data-idcert="<?php echo $id_certificado; ?>" data-id="<?php echo $id_participante_certificado; ?>"  data-nombre="<?php echo $nombre_asistente; ?>" data-calidad="<?php echo $calidad_participante_certificado; ?>"></div>
        
<?php 
$temp++;
}

?>
    </div>
</div>

<div id="footer"></div>

<div id="login-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title">Login</h4>
            </div>
            <div class="modal-body">
                 <form class="form-inline" role="form" action="" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="correo">Correo</label>
                        <input type="email" class="form-control input-sm" name="correo" id="user_mail" placeholder="Correo" required>
                    </div>     
                    <div class="form-group">
                        <label class="sr-only" for="contrasena">Contraseña</label>
                        <input type="password" class="form-control input-sm" name="contrasena" id="password" placeholder="Contraseña" required>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox"><small>Recordarme</small></label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm" name="ingresar" >Entrar</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script type="text/javascript" src="js/jquery.qrcode.min.js"></script>
<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 2, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
<script type="text/javascript">
$(document).ready(function() {

    
});
</script>
<?php
 
if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}  
    
?>
</body>
</html>