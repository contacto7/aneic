<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    echo "<script> window.location.href='index.php';</script>";
}

$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();

?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Eventos</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body style="min-width:450px;height:100%">
<script type="text/javascript" src="js/jquery.min.js"></script>

<div id="header"></div>
  
<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Generar certificados</span>
    </div>
</div>
<div class="container">
    <h1 class="h2">
        <span>Paso 1</span>
    </h1>
    <h1 class="h4">
        <span>Seleccione el evento</span>
    </h1>
</div>

<div class="container cont-eventos" style="min-height: 100%;margin: -472px auto 0;">
    <div class="row row-eventos" style="padding-top: 472px;">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <select class="form-control sel-unibydel" name="id-evento" id="id-evento">
              <option class="sel-prov" value="0">Seleccione el evento</option>
            
<?php 
    $eventos_res=listareventosadm();
            
    $nombre_adm_eventos="";
    $inicio_adm_eventos="";
    $dscr_adm_eventos="";
    $link_adm_eventos="";
    $foto_adm_eventos="";
    
                                    
    while ($row=mysqli_fetch_assoc($eventos_res)){

        $id_adm_eventos=$row['id_evento'];
        $nombre_adm_eventos=$row['nombre_evento'];
        $inicio_adm_eventos=$row['fechainic_evento'];
        $dscr_adm_eventos=$row['descripcion_evento'];
        $link_adm_eventos=$row['link_evento'];
        $foto_adm_eventos=$row['foto_adm_eventos'];

        setlocale(LC_TIME, "");
        setlocale(LC_ALL,"es_ES.UTF8");
        
        //$fechain= date('m/Y', strtotime($fechain));
        $inicio_adm_eventos = DateTime::createFromFormat("Y-m-d", $inicio_adm_eventos);
        $inicio_adm_eventos = strftime("%d de %B",$inicio_adm_eventos->getTimestamp());
        
?>
              <option class="sel-prov" value="<?php echo $id_adm_eventos ?>"><?php echo $nombre_adm_eventos ?></option>
            
<?php 

    };
?>
            </select>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-8 col-lg-9">
            <a class="btn btn-md btn-info" id="link-sgte" href="#">Siguiente</a>
        </div>
    </div>
</div>

<div id="footer"></div>
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script src="js/jquery.carouFredSel.js"></script>
<script src="js/moment-with-locales.js"></script>
<script src="js/bootstrap-datetimepicker.js"></script>

<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 2, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<script>

$(function () {
    $('#datetimepicker7').datetimepicker({
        viewMode: 'years',
        format:'YYYY-MM-DD',
        locale: 'es'
    });
});
$("#id-evento").change(function() {
    if($(this).val() != 0){
        var linksgte= "generarcertificados2.php?&xidevento="+$(this).val();
    }else{
        var linksgte= "#";
    }

    $("#link-sgte").attr("href",linksgte);
});
    
</script>
    
<?php


if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}  
?>
    
</body>
</html>