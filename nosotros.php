<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    //echo "<script> window.location.href='index.php';</script>";
}

$nosotros_res= listarnosotrosadm();

while ($row=mysqli_fetch_assoc($nosotros_res)) {
    $nosotros_adm_nosotros =$row['nosotros_adm_nosotros'];
    $historia_adm_nosotros =$row['historia_adm_nosotros'];
    $mision_adm_nosotros =$row['mision_adm_nosotros'];
    $vision_adm_nosotros =$row['vision_adm_nosotros'];
    $url_adm_nosotros =$row['url_adm_nosotros'];
}
mysqli_free_result($nosotros_res);
desconectar();


$foto_res= listarfotonosotrosadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_res)) {
    $url_adm_fotos =$row['url_adm_fotosnosotros'];
    $url_fotos_arr[] = $url_adm_fotos;
}
$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();

$foto_sup_res= listarfotoadm();

$url_fotos_arrin = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotosin =$row['url_adm_fotos'];
    $url_fotos_arrin[] = $url_adm_fotosin;
}
?>

<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Nosotros</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
</head>
<body style="min-height:inherit;">
<script type="text/javascript" src="js/jquery.min.js"></script>

<div id="header"></div>
    
<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Nosotros</span>
    </div>
</div>
<?php if($sesioninic){ ?>
            <div class="btn-foto-navmed" href="#modi-nst-mod" data-target="#modi-nst-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin">
                <button>Modificar textos y video</button>
            </div>
<?php } ?>
<div class="container cont-nosotros">
    <div class="row row-nosotros">
        <div class="col-xs-12">
            <div class="cont-nosotros-wrapp">
                <div class="cont-flow-wrapp">
                    <div class="cont-flow-inn">
                        <div class="cont-flow-tit">
                            <span>Nosotros</span>
                        </div>
                        <div class="cont-flow-contenido">
                            <p>
                                <?php echo $nosotros_adm_nosotros; ?>
                            </p>
                        </div>
                    </div>
                    <div class="cont-flow-inn">
                        <div class="cont-flow-tit">
                            <span>Historia</span>
                        </div>
                        <div class="cont-flow-contenido">
                            <p>
                                <?php echo $historia_adm_nosotros; ?>
                            </p>
                        </div>
                    </div>
                    <div class="cont-flow-inn">
                        <div class="cont-flow-tit">
                            <span>Misión</span>
                        </div>
                        <div class="cont-flow-contenido">
                            <p>
                                <?php echo $mision_adm_nosotros; ?>
                            </p>
                        </div>
                    </div>
                    <div class="cont-flow-inn">
                        <div class="cont-flow-tit">
                            <span>Visión</span>
                        </div>
                        <div class="cont-flow-contenido">
                            <p>
                                <?php echo $vision_adm_nosotros; ?>
                            </p>
                        </div>
                    </div>
                    <div class="cont-flow-inn">
                        <div class="cont-flow-tit">
                            <span>Video de presentación</span>
                        </div>
                        <div class="cont-flow-contenido">
                            <div class="embed-responsive embed-responsive-4by3">
                                <div id="iframe-wrapp">
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="cont-imagenes-nos">
                    <div class="cont-imagenes-nos-inn">
                        <div class="img-nos-wrap">
<?php if($sesioninic){ ?>
            <div class="btn-foto-navmed" href="#camb-fotonos-mod" data-target="#camb-fotonos-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" onclick="cambtipo(1)">
                <button>Editar Foto</button>
            </div>
<?php } ?>
                            <img src="<?php echo $url_fotos_arr[0]; ?>" alt="Imagen de nosotros">
                        </div>
                        <div class="img-nos-wrap">
<?php if($sesioninic){ ?>
            <div class="btn-foto-navmed" href="#camb-fotonos-mod" data-target="#camb-fotonos-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" onclick="cambtipo(2)">
                <button>Editar Foto</button>
            </div>
<?php } ?>
                            <img src="<?php echo $url_fotos_arr[1]; ?>" alt="Imagen de nosotros">
                        </div>
                        <div class="img-nos-wrap">
<?php if($sesioninic){ ?>
            <div class="btn-foto-navmed" href="#camb-fotonos-mod" data-target="#camb-fotonos-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" onclick="cambtipo(3)">
                <button>Editar Foto</button>
            </div>
<?php } ?>
                            <img src="<?php echo $url_fotos_arr[2]; ?>" alt="Imagen de nosotros">
                        </div>
                        <div class="img-nos-wrap">
<?php if($sesioninic){ ?>
            <div class="btn-foto-navmed" href="#camb-fotonos-mod" data-target="#camb-fotonos-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" onclick="cambtipo(4)">
                <button>Editar Foto</button>
            </div>
<?php } ?>
                            <img src="<?php echo $url_fotos_arr[3]; ?>" alt="Imagen de nosotros">
                        </div>
                        <div class="img-nos-wrap">
<?php if($sesioninic){ ?>
            <div class="btn-foto-navmed" href="#camb-fotonos-mod" data-target="#camb-fotonos-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" onclick="cambtipo(5)">
                <button>Editar Foto</button>
            </div>
<?php } ?>
                            <img src="<?php echo $url_fotos_arr[4]; ?>" alt="Imagen de nosotros">
                        </div>
                        <div class="img-nos-wrap">
<?php if($sesioninic){ ?>
            <div class="btn-foto-navmed" href="#camb-fotonos-mod" data-target="#camb-fotonos-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" onclick="cambtipo(6)">
                <button>Editar Foto</button>
            </div>
<?php } ?>
                            <img src="<?php echo $url_fotos_arr[5]; ?>" alt="Imagen de nosotros">
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

<div class="container cont-nosotros-docu">
    <div class="row row-nosotros">
        <div class="col-xs-12">
            <div class="docu-wrapper">
                <div class="tit-docu">Documentos de la asociación</div>
<?php if($sesioninic){ ?>
            <div class="btn-foto-navmed" href="#anad-docu-mod" data-target="#anad-docu-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin">
                <button>Añadir documento</button>
            </div>
<?php } ?>
              <table class="table table-docu">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th style="display:none">Fecha de subida</th>
                  </tr>
                </thead>
                <tbody>
                    
<?php 
$nosotros_res = listardocumentosadm();
    
while ($row=mysqli_fetch_assoc($nosotros_res)) {
    $id_adm_documentos =$row['id_adm_documentos'];
    $nombre_adm_documentos =$row['nombre_adm_documentos'];
    $url_adm_documentos =$row['url_adm_documentos'];
    $fechareg_adm_documentos =$row['fechareg_adm_documentos'];
    
    //$fechain= date('m/Y', strtotime($fechain));
    $fechareg_adm_documentos = DateTime::createFromFormat("Y-m-d", $fechareg_adm_documentos);
    $fechareg_adm_documentos = strftime("%d/%m/%Y",$fechareg_adm_documentos->getTimestamp());

?>
                  <tr>
                    <td><div class="link-docu"><a href="<?php echo $url_adm_documentos; ?>" dowload><img alt="imagen de archivo" class="img-docu-aneic" src="img/file.png"> <?php echo $nombre_adm_documentos; ?></a></div></td>
                    <td style="display:none"><?php echo $fechareg_adm_documentos; ?></td>
                  </tr>
<?php 
                    }
?>
                </tbody>
              </table>
            </div>
        </div>
    </div>
</div>

<div id="footer"></div>
    
<?php if($sesioninic){ ?>
    
<div id="camb-fotonos-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Cambiar Fotografía</h4>
            </div>
            <div class="modal-body modbod-exp">
                 <form class="form-horizontal form-datoper" role="form" action="" method="post" enctype="multipart/form-data">
                     <div class="row form-datper-left">
                        <div class="col-xs-12">
                          <input type="number" class="archivo-subir" id="tipo-foto" name="tipo-foto" style="display:none" value="0">
                        </div>
                        <div class="col-xs-12">
                            <label class="lbl-login-mod">Cambiar foto</label>
                            <label for="foto-adm"><a class="btn btn-md form-control input-sm selec-arch-mod">Selecciona archivo</a></label>
                            <div id="nombrefoto"></div>
                              <input type="file" class="archivo-subir" id="foto-adm" name="foto-adm" style="display:none" data-id="1">
                            <span class="span-foto-estado" id="estado-cambio-1"></span>
                        </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-12 datpersbut-wrap">
                              <div class="datpersbut-mod">
                                <button type="submit" class="btn btn-datper-mod btn-sm" name="foto-adm-sub" >Guardar</button>
                                <button type="button" class="btn btn-datper-canc btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                             </div>
                         </div>
                     </div>
                </form>
            </div>

        </div>
    </div>
</div>
    
    
<div id="modi-nst-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Cambiar textos y video</h4>
            </div>
            <div class="modal-body modbod-exp">
                 <form class="form-horizontal form-datoper" role="form" action="" method="post" enctype="multipart/form-data">
                     <div class="row form-datper-left">
                        <div class="col-xs-12"><br>
                          <div class="form-group" style="margin-top:-25px;">
                            <label class="control-label col-sm-12" for="nosotros-nosotros" style="text-align:left; font-weight:100">Máximo 2000 caracteres por texto</label>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-2" for="nosotros-nosotros">Nosotros:</label>
                            <div class="col-sm-10">
                                <textarea rows="3" style="width:100%" class="form-control" name="nosotros-nosotros"><?php echo $nosotros_adm_nosotros; ?></textarea>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-2" for="historia-nosotros">Historia:</label>
                            <div class="col-sm-10">
                                <textarea rows="3" style="width:100%" class="form-control" name="historia-nosotros"><?php echo $historia_adm_nosotros; ?></textarea>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-2" for="mision-nosotros">Misión:</label>
                            <div class="col-sm-10">
                                <textarea rows="3" style="width:100%" class="form-control" name="mision-nosotros"><?php echo $historia_adm_nosotros; ?></textarea>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-2" for="vision-nosotros">Visión:</label>
                            <div class="col-sm-10">
                                <textarea rows="3" style="width:100%" class="form-control" name="vision-nosotros"><?php echo $vision_adm_nosotros; ?></textarea>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-2" for="video-nosotros">Video de presentación:</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="video-nosotros" name="video-nosotros" placeholder="Ingrese el link del video de presentación" value="<?php echo $url_adm_nosotros; ?>">
                            </div>
                          </div>

                        </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-12 datpersbut-wrap">
                              <div class="datpersbut-mod">
                                <button type="submit" class="btn btn-datper-mod btn-sm" name="nosotros-adm-sub" >Guardar</button>
                                <button type="button" class="btn btn-datper-canc btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                             </div>
                         </div>
                     </div>
                </form>
            </div>

        </div>
    </div>
</div>
    
    
<div id="anad-docu-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Añadir documento</h4>
            </div>
            <div class="modal-body modbod-exp">
                 <form class="form-horizontal form-datoper" role="form" action="" method="post" enctype="multipart/form-data">
                     <div class="row form-datper-left">
                        <div class="col-xs-12">
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="nombre-documento">Nombre de documento:</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="nombre-documento" name="nombre-documento" placeholder="Ingrese el nombre del documento">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="link-evento">Documento:</label>
                            <div class="col-sm-8">
                                  <label for="documento-anadir"><a class="btn btn-md form-control input-sm selec-arch-mod">Selecciona archivo</a></label>
                                <div id="nombrefoto"></div>
                                  <input type="file" class="archivo-subir" id="documento-anadir" name="documento-anadir" style="display:none" data-id="2">
                                <span class="span-foto-estado" id="estado-cambio-2"></span>
                            </div>
                          </div>

                        </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-12 datpersbut-wrap">
                              <div class="datpersbut-mod">
                                <button type="submit" class="btn btn-datper-mod btn-sm" name="anad-docu-sub" >Guardar</button>
                                <button type="button" class="btn btn-datper-canc btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                             </div>
                         </div>
                     </div>
                </form>
            </div>

        </div>
    </div>
</div>
    
<?php } ?>
    
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script src="js/jquery.carouFredSel.js"></script>


<script>
$(function(){
    
   $("#header").load("header.php", {
       xph: 5, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arrin[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arrin[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arrin[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<script>
   
var videoId = getId('<?php echo $url_adm_nosotros; ?>');

var iframeMarkup = '<iframe width="560" height="315" src="//www.youtube.com/embed/' 
    + videoId + '" frameborder="0" allowfullscreen></iframe>';

$("#iframe-wrapp").html(iframeMarkup);
    
</script>
    
<?php

if (isset($_POST['foto-adm-sub'])){

    if($image_size=$_FILES['foto-adm']['size']>0 || !empty($_POST['tipo-foto']) ){

        $allowed =  array('gif','png' ,'jpg','GIF','PNG' ,'JPG');
        $filename = $_FILES['foto-adm']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!in_array($ext,$allowed) ) {
            echo "<script>alert('Por favor selecciona una imagen')</script>";
            exit();
        }else{
            $nuevo_nombre=randomalfanume(25);
            $nuevo_nombre= $nuevo_nombre.".".$ext;
            $url_foto= "adm_nosotros/".$nuevo_nombre;
            
            $image_temp_name=$_FILES['foto-adm']['tmp_name'];
            if(move_uploaded_file($image_temp_name,$url_foto) && $_POST['tipo-foto'] > 0){
        
                $pagdireccion = "nosotros.php";

                $tipo_foto_mod=$_POST['tipo-foto'];

                modificarfotonosotrosadm($tipo_foto_mod, $url_foto, $pagdireccion);

                
            }else{
                echo "<script>alert('Hubo un error al cargar la imagen. Por favor intenta más tarde.')</script>";
                exit();
            }
            
            
        }

    }

}

if (isset($_POST['anad-docu-sub'])){

    if($image_size=$_FILES['documento-anadir']['size']>0 || !empty($_POST['nombre-documento']) ){

        //$allowed =  array('gif','png' ,'jpg','GIF','PNG' ,'JPG');
        $filename = $_FILES['documento-anadir']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if( 0) {//Todos los formatos son permitidos
            echo "<script>alert('Por favor selecciona un archivo')</script>";
            exit();
        }else{
            $nuevo_nombre=randomalfanume(25);
            $nuevo_nombre= $nuevo_nombre.".".$ext;
            $url_foto= "documentos_aneic/".$nuevo_nombre;
            
            $image_temp_name=$_FILES['documento-anadir']['tmp_name'];
            if(move_uploaded_file($image_temp_name,$url_foto) ){
        
                $pagdireccion = "nosotros.php";

                $nombre_documento_mod=$_POST['nombre-documento'];

                anadirdocumentoadm($nombre_documento_mod, $url_foto, $pagdireccion);

                
            }else{
                echo "<script>alert('Hubo un error al cargar la imagen. Por favor intenta más tarde.')</script>";
                exit();
            }
            
            
        }

    }

}
    
if (isset($_POST['nosotros-adm-sub'])){

    if(!empty($_POST['nosotros-nosotros']) || !empty($_POST['historia-nosotros']) || !empty($_POST['mision-nosotros']) || !empty($_POST['vision-nosotros']) || !empty($_POST['video-nosotros'])  ){

        $nosotros_nosotros_mod=$_POST['nosotros-nosotros'];
        $historia_nosotros_mod=$_POST['historia-nosotros'];
        $mision_nosotros_mod=$_POST['mision-nosotros'];
        $vision_nosotros_mod=$_POST['vision-nosotros'];
        $video_nosotros_mod=$_POST['video-nosotros'];
        
        $pagdireccion = "nosotros.php";

        modificarnosotrosadm($nosotros_nosotros_mod, $historia_nosotros_mod, $mision_nosotros_mod, $vision_nosotros_mod, $video_nosotros_mod, $pagdireccion);

    }

}

if(isset($_POST['ver-votdel-btn'])){

    if(!empty($_POST['dni-votar']) && !empty($_POST['cod-votar'])){
        
        $dni_delegado_votar = $_POST['dni-votar'];
        $cod_delegado_votar = $_POST['cod-votar'];

        $info_delegado = listarinfodelegadodni($dni_delegado_votar);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_delegado)) {
            $id_delegado =$row['id_delegado'];
            $puesto_delegado =$row['puesto_delegado'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){
            
            $codigo_delegado_tmp=$dni_delegado_votar*7*13*37;//para codificar el código

            $codigo_delegado_tmp = substr($codigo_delegado_tmp, -4);

            if($cod_delegado_votar == $codigo_delegado_tmp){
                
                unset($_SESSION['id_delegado']);
                unset($_SESSION['puesto_delegado']);
                
                $_SESSION["id_delegado"]=$id_delegado;
                $_SESSION["puesto_delegado"]=$puesto_delegado;
                
                $id_delegado_encriptado = doEncrypt($id_delegado);
                $puesto_delegado_encriptado = doEncrypt($puesto_delegado);
                                
                //echo "<script>window.location.href='lista_delegado_votacion.php?xi_1=$id_delegado_encriptado&xi_2=$puesto_delegado_encriptado';</script>";
                echo "<script>window.location.href='lista_delegado_votacion.php';</script>";

            }else{
                echo "<script> alert('Código de votación incorrecto.');</script>";
            }
            
        }else{
            echo "<script> alert('No se encuentra un delegado con el DNI brindado.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}

if(isset($_POST['ver-asisdel-btn'])){

    if(!empty($_POST['dni-asist']) && !empty($_POST['cod-asist'])){
        
        $dni_delegado_asist = $_POST['dni-asist'];
        $cod_delegado_asist = $_POST['cod-asist'];

        $info_asistencia = listarinfoasis($dni_delegado_asist, $cod_delegado_asist);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_asistencia)) {
            $id_delegado_asis =$row['id_delegado'];
            $id_asistencia_asis =$row['id_asistencia'];
            $estado_asistencia_asis =$row['estado_asistencia'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){

            if($estado_asistencia_asis > 0){

                $resultado_asi_agr = registrarasistenciadel($id_delegado_asis, $id_asistencia_asis);
                $cuenta_tmp = 0;

                if($resultado_asi_agr){
                    echo "<script> alert('Se registró la asistencia.');</script>";
                }else{
                    echo "<script> alert('Ha ocurrido un problema al registrar la asistencia. Inténtelo de nuevo, o comuníquese con algún representante de Aneic para solucionar el problema.');</script>";
                }

            }else{
                echo "<script> alert('La asistencia ya se ha cerrado.');</script>";
            }
            
        }else{
            echo "<script> alert('La asistencia no se ha abierto para usted, o el código brindado es incorrecto.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}



if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}


if (isset($_POST['codigo-recu'])){
	if(!empty($_POST['dni-recu'])){
		$dni_delegado_rec=$_POST['dni-recu'];

		$result=recuperarcontradel($dni_delegado_rec);

	}else{
        echo "<script> alert('Ingrese un DNI.');</script>";
    }
}
if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}    
    
?>
</body>
</html>