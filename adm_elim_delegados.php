<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    echo "<script> window.location.href='index.php';</script>";
}


$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();


$universidadeslista = listaruniversidades();

$amazonas   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ancash     = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$apurimac   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$arequipa   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ayacucho   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$cajamarca  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$callao     = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$cusco      = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$huancavelica = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$huanuco = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ica = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$junin = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$lalibertad = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$lambayeque = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$lima  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$loreto  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$madrededios = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$moquegua   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$pasco   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$piura  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$puno   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$sanmartin   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$tumbes   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ucayali   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";

$universidadesarr = array();

while ($row=mysqli_fetch_assoc($universidadeslista)) {
    $id_universidad=$row['id_universidad'];
    $nombre_universidad=$row['nombre_universidad'];
    $departamento=$row['departamento'];

    switch ($departamento) {
        case "Amazonas":
            $amazonas   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Áncash":
            $ancash     .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Apurímac":
            $apurimac   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Arequipa":
            $arequipa   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Ayacucho":
            $ayacucho   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Cajamarca":
            $cajamarca  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Callao":
            $callao     .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Cusco":
            $cusco      .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Huancavelica":
            $huancavelica .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Huánuco":
            $huanuco .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Ica":
            $ica .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Junín":
            $junin .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "La Libertad":
            $lalibertad .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Lambayeque":
            $lambayeque .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Lima":
            $lima  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Loreto":
            $loreto  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Madre de Dios":
            $madrededios .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Moquegua":
            $moquegua   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Pasco":
            $pasco   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Piura":
            $piura  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Puno":
            $puno   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "San Martín":
            $sanmartin   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Tumbes":
            $tumbes   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Ucayali":
            $ucayali   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
    }


}
mysqli_free_result($universidadeslista);
?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Eventos</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body style="min-width:450px;height:100%">
<script type="text/javascript" src="js/jquery.min.js"></script>

<div id="header"></div>
    
<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Eliminar delegados</span>
    </div>
</div>
<div class="container cont-eventos" style="min-height: 100%;margin: -472px auto 0;">
    <div class="row" style="padding-top: 472px;">
        <div class="col-xs-12">
          <table class="table">
            <tbody>
              <tr>
                <td>Por DNI</td>
                <td><input class="form-control" type="text" placeholder="Ingrese el DNI del delegado a eliminar" name="dnielim-del" id="dnielim-del"></td>
                <td><button class='btn btn-sm btn-danger dnielim-del-btnmod'   href='#elimdel-dni-mod' data-target='#elimdel-dni-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin'>Eliminar</button></td>
                  <td></td>
              </tr>
              <tr>
                <td>Por universidad</td>
                <td>
                    <select class="form-control sel-unibydel" name="departamento" id="departamento">
                      <option class="sel-dep" value="Departamento">Departamento</option>
                      <option class="sel-dep" value="Amazonas">Amazonas</option>
                        <option class="sel-dep" value="Áncash">Ancash</option>
                        <option class="sel-dep" value="Apurímac">Apurímac</option>
                        <option class="sel-dep" value="Arequipa">Arequipa</option>
                        <option class="sel-dep" value="Ayacucho">Ayacucho</option>
                        <option class="sel-dep" value="Cajamarca">Cajamarca</option>
                        <option class="sel-dep" value="Callao">Callao</option>
                        <option class="sel-dep" value="Cusco">Cusco</option>
                        <option class="sel-dep" value="Huancavelica">Huancavelica</option>
                        <option class="sel-dep" value="Huánuco">Huánuco</option>
                        <option class="sel-dep" value="Ica">Ica</option>
                        <option class="sel-dep" value="Junín">Junín</option>
                        <option class="sel-dep" value="La Libertad">La Libertad</option>
                        <option class="sel-dep" value="Lambayeque">Lambayeque</option>
                        <option class="sel-dep" value="Lima">Lima</option>
                        <option class="sel-dep" value="Loreto">Loreto</option>
                        <option class="sel-dep" value="Madre de Dios">Madre de Dios</option>
                        <option class="sel-dep" value="Moquegua">Moquegua</option>
                        <option class="sel-dep" value="Pasco">Pasco</option>
                        <option class="sel-dep" value="Piura">Piura</option>
                        <option class="sel-dep" value="Puno">Puno</option>
                        <option class="sel-dep" value="San Martín">San Martín</option>
                        <option class="sel-dep" value="Tacna">Tacna</option>
                        <option class="sel-dep" value="Tumbes">Tumbes</option>
                        <option class="sel-dep" value="Ucayali">Ucayali</option>
                    </select>
                </td>
                <td>
                    <select class="form-control sel-unibydel" name="univ-dep" id="univ-dep">
                      <option class="sel-prov" value="universidad">Seleccione el departamento</option>
                    </select>                      
                </td>
                <td>
                    <button class='btn btn-sm btn-danger cerrar-vota'   href='#elimdel-univ-mod' data-target='#elimdel-univ-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin'>Eliminar</button>
                </td>
              </tr>
              <tr>
                <td>A todos</td>
                <td><button class='btn btn-sm btn-danger' href='#elimdel-todos-mod' data-target='#elimdel-todos-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin'>Eliminar</button></td>
                <td></td>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
    </div>
</div>

<div id="footer"></div>
    
<div id="elimdel-dni-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Eliminar delegado</h4>
            </div>
            <div class="modal-body elimdeldni-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea eliminar el delegado: <span class="elimdel-nomb"></span> con DNI <span class="elimdel-dni"></span>?</div>
                <div class="mensaje-vota-cerrar"></div>
                <div class="vota-modbod-btn">
                    <button type="button" class="btn btn-sm btn-warning cerrar-vota-btn" data-id="0">Si</button>
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                </div>
            </div>

        </div>
    </div>
</div> 
    
    
<div id="elimdel-univ-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Eliminar delegados</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea eliminar los delegados de la: <span class="elimuniv-dscr"></span>?</div>
                <div class="mensaje-deledni-elim"></div>
                <div class="vota-modbod-btn">
                    <button type="button" class="btn btn-sm btn-warning elimdel-univ-btn" data-id="0">Si</button>
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                </div>
            </div>

        </div>
    </div>
</div> 
    

    
<div id="elimdel-todos-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Eliminar delegados</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea eliminar a todos los delegados?</div>
                <div class="vota-modbod-btn">
                    <form class="form-inline" role="form"  action="" method="post">
                        <button type="submit" class="btn btn-sm btn-warning elimdel-todos-btn" name="elimdel-todos-btn">Si</button>
                        <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div> 
    
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>

<script>

var departamentos ={

    "Departamento":'<option class="sel-prov" value="universidad">Seleccione el departamento</option>',
    "Amazonas": "<?php echo $amazonas; ?>",
    "Áncash": "<?php echo $ancash; ?>",
    "Apurímac": "<?php echo $apurimac; ?>",
    "Arequipa": "<?php echo $arequipa; ?>",
    "Ayacucho": "<?php echo $ayacucho; ?>",
    "Cajamarca": "<?php echo $cajamarca; ?>",
    "Callao": "<?php echo $callao; ?>",
    "Cusco": "<?php echo $cusco; ?>",
    "Huancavelica": "<?php echo $huancavelica; ?>",
    "Huánuco": "<?php echo $huanuco; ?>",
    "Ica": "<?php echo $ica; ?>",
    "Junín": "<?php echo $junin; ?>",
    "La Libertad": "<?php echo $lalibertad; ?>",
    "Lambayeque": "<?php echo $lambayeque; ?>",
    "Lima": "<?php echo $lima; ?>",
    "Loreto": "<?php echo $loreto; ?>",
    "Madre de Dios": "<?php echo $madrededios; ?>",
    "Moquegua": "<?php echo $moquegua; ?>",
    "Pasco": "<?php echo $pasco; ?>",
    "Piura": "<?php echo $piura; ?>",
    "Puno": "<?php echo $puno; ?>",
    "San Martín": "<?php echo $sanmartin; ?>",
    "Tumbes": "<?php echo $tumbes; ?>",
    "Ucayali": "<?php echo $ucayali; ?>",
    
};

$("#departamento").change(function() {
    var temp=0;
    var departamento= this.value;
    var prov=departamentos[departamento];
    $("#univ-dep").html(prov);
    //console.log(prov);
});
    
$("#univ-dep").change(function() {
    var id_universidad=this.value;
    var nombre_universidad = $(this).find("option:selected").text();
    
    $(".elimuniv-dscr").html(nombre_universidad);
    $(".elimdel-univ-btn").attr('data-id',id_universidad);

});
    
    
/*Funciones AJAX*/
    
    
$(document).on("click", '.elimdel-univ-btn',function(){
    
    var i1_1 = $(this).attr('data-id');
    
    $(".mensaje-deleuniv-elim").html("Eliminando...");
    
    $.ajax({
        url:'apost_eliminardel_universidad.php',
        type:'post',
        data:{
            'x_i1': i1_1
        },
        success: function (result) {
            $('#elimdel-univ-mod').modal('hide');
            if(result == 1){
                //console.log(result);
                alert("Ocurrió algún problema al eliminar los delegados. Si este inconveniente persiste, comuníquese con servicio técnico.");
            }else if(result == 2){
                alert("Se eliminaron los delegados correctamente.");
            }else if(result == 3){
                alert("El sistema de votación no ha identificado el dominio como Aneic. Por favor, contáctese con servicio técnico.");
            }else{

            }
            $(".mensaje-deleuniv-elim").html("");
            
        }
    });


});
    
    
$(document).on("click", '.dnielim-del-btnmod',function(){
    
    var i1_1 = $("#dnielim-del").val();
    
    $(".elimdeldni-mod-bod").html("Cargando...");
    
    $.ajax({
        url:'apost_eliminardel_dni.php',
        type:'post',
        data:{
            'x_i1': i1_1
        },
        success: function (result) {
            $(".elimdeldni-mod-bod").html(result);
        }
    });


});
    
    
</script>
    
<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 1, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<?php


if (isset($_POST['elimdel-dni-btn'])){

    if(!empty($_POST['iddel-elim-inp'])){
        
        $id_delegado_eliminar = $_POST['iddel-elim-inp'];

        $id_votacion = eliminardelegado($id_delegado_eliminar);
        
        if($id_votacion =="Error"){
            echo "<script>alert('Hubo un error al eliminar el delegado. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Se eliminó al delegado con éxito.')</script>";
            echo "<script> window.location.href='adm_elim_delegados.php';</script>";
        }
    }else{

        echo "<script>alert('No se cargó el id del delegado. Si el problema persiste comuníquese con servicio técnico.')</script>";
    }

}
  
if (isset($_POST['elimdel-todos-btn'])){

    $estado_elim = eliminartodosdelegados();

    if($estado_elim =="Error"){
        echo "<script>alert('Hubo un error al eliminar los delegados. Por favor intenta más tarde.')</script>";
        exit();
    }else{
        echo "<script>alert('Se eliminaron los delegados con éxito.')</script>";
        echo "<script> window.location.href='adm_elim_delegados.php';</script>";
    }

}

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";        
}   
?>

</body>
</html>