<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    echo "<script> window.location.href='index.php';</script>";
}

$id_votacion_encriptado = str_replace(" ", "+", $_GET['x_i_2']);
$votacion_descripcion_encriptado = str_replace(" ", "+", $_GET['x_i_1']);

//$id_votacion_encriptado = $_GET['x_i_2'];
$id_votacion = (int)doDecrypt($id_votacion_encriptado);

//$votacion_descripcion_encriptado = $_GET['x_i_1'];
$votacion_descripcion = (string)doDecrypt($votacion_descripcion_encriptado);

$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();

//Tabla de resultados y gráfico

$lista_votacion = listarvotacionresultado($id_votacion);

//Variables de tabla
$temp_vot_tabla= "";
$num_tabla = 0;
$num_opcion = 1;
$id_opc_temp=0;
$tablas_html = "";

//Variables de gráfico
$labels_grafico = array();
$valores_grafico = array();
$cantidades_tabla = array();
$nombres_tabla = "";
$canvas_grafico = "";
$temp_num_opc = 0; // Cantidad de opciones acumuladas
$temp_num_opc_unic = 0; //Cantidad de opciones por tabla
$max_votos = 0;

while ($row=mysqli_fetch_assoc($lista_votacion)) {
    $id_votacion_tabla =$row['id_votacion_tabla'];
    $descripcion_votacion_tabla =$row['descripcion_votacion_tabla'];
    $id_votacion_opcion =$row['id_votacion_opcion'];
    $descripcion_votacion_opcion =$row['descripcion_votacion_opcion'];
    $cuenta_votos =$row['cuenta_votos'];
    
    if($cuenta_votos > $max_votos){
        $max_votos = $cuenta_votos;
    }
    
    $temp_num_opc++;//Construccion tabla
    
    if($id_votacion_opcion == $id_opc_temp){
        $cuenta_votos = 0;
    }else{
        $id_opc_temp = $id_opc_temp;
    }
    
    if($temp_vot_tabla != $descripcion_votacion_tabla){ 
        
        ////////////CONSTRUCCION DE TABLA////////////////
        
        $num_tabla++;
        
        if($num_tabla>1){
            //Tabla
            $tablas_html.="</tbody></table>";
            
            //Gráfico
            $cantidades_tabla[] = $temp_num_opc;
            $num_tabla--;
            $height_canvas = $temp_num_opc_unic*50;
            $canvas_grafico .= "<div class='tit-tabla-res'>$nombres_tabla</div><div style='height:".$height_canvas."px;min-width:700px;position:relative'><canvas id='charthorizontal-".$num_tabla."'></canvas></div>";
            $temp_num_opc_unic=0;
            $num_tabla++;
        }
        
        //echo $descripcion_votacion_tabla."<br>";
        //echo $id_votacion_opcion =$row['id_votacion_opcion']."<br>";
        //echo $descripcion_votacion_opcion =$row['descripcion_votacion_opcion']."<br>";
        
        $tablas_html.="<table class='table table-striped tabla-vot-tit' id='$num_tabla-vot' data-id='$id_votacion_tabla'><thead><tr><th>$num_tabla.</th><th>$descripcion_votacion_tabla</th><th>Votos</th></tr></thead><tbody>";
        
        $tablas_html.="<tr><td>&#9702;</td><td>$descripcion_votacion_opcion</td><td>$cuenta_votos</td></tr>";
        
        $temp_vot_tabla = $descripcion_votacion_tabla;
        
        ////////////CONSTRUCCION DE GRAFICOS////////////////
        
        $labels_grafico[] = $descripcion_votacion_opcion;
        
        //$valores_grafico[] = 0;
        $valores_grafico[] = $cuenta_votos;
        $nombres_tabla = $descripcion_votacion_tabla;
        
        
    }else{
        ////////////CONSTRUCCION DE TABLA////////////////
        
        $num_opcion++;
        
        $tablas_html.="<tr><td>&#9702;</td><td>$descripcion_votacion_opcion</td><td>$cuenta_votos</td></tr>";
        
        ////////////CONSTRUCCION DE GRAFICOS////////////////
        $labels_grafico[] = $descripcion_votacion_opcion;
        $valores_grafico[] = $cuenta_votos;
        
    }
    $temp_num_opc_unic++;//Construccion tabla
}

//Contruccion tabla
$temp_num_opc++;
$cantidades_tabla[] = $temp_num_opc;
$height_canvas = $temp_num_opc_unic*50;
$canvas_grafico .= "<div class='tit-tabla-res'>$nombres_tabla</div><div style='height:".$height_canvas."px;min-width:700px;position:relative'><canvas id='charthorizontal-$num_tabla'></canvas></div>";

$tablas_html.="</tbody></table>";
mysqli_free_result($lista_votacion);
desconectar();
?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Eventos</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body style="min-width:450px;">
<script type="text/javascript" src="js/jquery.min.js"></script>
       
<div id="header"></div>

<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Resultados de votación: <?php echo $votacion_descripcion; ?></span>
    </div>
</div>

<div class="container cont-eventos">
    <div class="row">
        <div class="tit-res">Resultados en tabla</div>
        <?php echo $tablas_html; ?>
    </div>
</div>

<div class="container cont-eventos">
    <div class="tit-res">Resultados Gráficos</div>
    <?php echo $canvas_grafico; ?> 
</div>
<div class="container">
    <div class="row">
        <div>
            <a class="btn btn-default" href="adm_votacion_lista.php" style="float:right">Regresar</a>
        </div>  
    </div>
</div>
<div id="footer" style="margin-top:50px;"></div>
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script src="js/Chart.min.js"></script>
<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 4, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<script>
$(function() {
    dibujarbarras();
});

function dibujarbarras(){
    var colordef="#132e73";
    var colorhoverdef="#ccc";
    
    <?php 
        $temp_cantidad_acumulada = 0;
        $temp_canvas = 0;
        foreach ($cantidades_tabla as $cantidad_tabla) {
            $cantidad_tabla--;
            $temp_canvas++;
    ?>
    
    var canvashorizontal = document.getElementById("charthorizontal-<?php echo $temp_canvas ?>");
    var MeSeContext = canvashorizontal.getContext("2d");
    MeSeContext.canvas.height = '100%';
    var MeSeData = {
        labels: [
            <?php 
                for ($cant_temp_1 = $temp_cantidad_acumulada; $cant_temp_1 < $cantidad_tabla; $cant_temp_1++) {
                    echo '"'.$labels_grafico[$cant_temp_1].'",';
                }
            ?>
        ],
        datasets: [{
            data: [
                <?php 
                for ($cant_temp_2 = $temp_cantidad_acumulada; $cant_temp_2 < $cantidad_tabla; $cant_temp_2++) {
                        echo $valores_grafico[$cant_temp_2].",";
                    }
                ?>
                ],

            
            backgroundColor: colordef,
            hoverBackgroundColor: colorhoverdef,
            borderWidth: 1,
            borderSkipped:"top",
        }]
    };

    var MeSeChart = new Chart(MeSeContext, {
        type: 'horizontalBar',
        data: MeSeData,
            options: {
            legend: {
                display: false,
            },
            scales: {
                xAxes: [{
                    ticks: {
                        min: 0,
                        max: <?php echo $max_votos; ?>,
                    },
                }],
                yAxes: [{
                    barThickness:20,
                    gridLines:{
                        display:false,
                    },
                      afterFit: function(scaleInstance) {
                        scaleInstance.width = 400; // sets the width to 100px
                      }
                }],
                pointLabels: {
                  fontSize: 20
                }
                
            }
        }
    });
    <?php 
            $temp_cantidad_acumulada = $cantidad_tabla;
        }
    ?>
}    

</script>
    
<?php

if (isset($_POST['univ-adm-sub'])){

    if(!empty($_POST['departamento']) && !empty($_POST['univag-dep'])){
        
        $departamento=$_POST['departamento'];
        $nombre_universidad=$_POST['univag-dep'];
        $estado_universidad = 1;
        $id_universidad= anadiruniversidad($nombre_universidad, $departamento, $estado_universidad);
        
        if($id_universidad =="Error"){
            echo "<script>alert('Hubo un error al añadir la universidad. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script> window.location.href='generarcodigos.php';</script>";
        }
    }else{

        echo "<script>alert('Ingrese ambos datos.')</script>";
    }

}

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}   
?>
    
</body>
</html>