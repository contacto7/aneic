<?php 
//xph es el tipo de pagina
//xhs es para ver si es administrador

//0 : Inicio
//1 : Ver Delegados
//2 : Acreditación
//3 : Elecciones
//4 : Eventos
//5 : Nosotros
//6 : Comité
//7 : Administrar

//validando las variables de sesion
include 'funciones.php';

//Inicializamos variables
$sesioninic = 0;
$tipo_pagina = 0;
$img_logo_1 = "";
$img_logo_2 = "";

$img_logo_1  = $_POST['xdf1'] ;
$img_logo_2  = $_POST['xdf2'] ;
$sesioninic  = $_POST['xhs'] ;

//CLASES DE LOS LINKS
$a_class_1= "";
$a_class_2= "";
$a_class_3= "";
$a_class_4= "";
$a_class_5= "";
$a_class_6= "";
$a_class_7= "";

if (isset($_POST['xph'])){
    
    $tipo_pagina = $_POST['xph'] ;
    switch ($tipo_pagina) {
        case 1:
            $a_class_1= "active-nav-a";
            break;
        case 2:
            $a_class_2= "active-nav-a";
            break;
        case 3:
            $a_class_3= "active-nav-a";
            break;
        case 4:
            $a_class_4= "active-nav-a";
            break;
        case 5:
            $a_class_5= "active-nav-a";
            break;
        case 6:
            $a_class_6= "active-nav-a";
            break;
        case 7:
            $a_class_7= "active-nav-a";
            break;
    }

}
?>

<nav class="navbar navbar-nav navbar-fixed-top" role="navigation">
<?php if($sesioninic==1){ ?>
		<div id="navbar" class="" style="position: fixed; ">
            <div class="container">
                <ul class="nav navbar-nav">
				    <li style="display:inline-block">
                        <button class="btn btn-sm btn-info" onclick="location.href='adm-regdel.php'" type="button"> ADMINISTRAR </button>
                    </li>
                    <li style="display:inline-block"><form class="form-inline" role="form"    action="" method="get"><button type="submit" class="btn btn-sm btn-warning" name="cerrar-sesion">Cerrar Sesión</button>
                        </form></li>

                </ul>
            </div>

		</div>
 <?php } ?>
	<div class="container cont-nav">
		<div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-dwn" aria-expanded="false" aria-controls="navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>
            <a class="navbar-brand" href="index.php"><img src="<?php echo $img_logo_2; ?>" class="nav-img-logo" alt="logo"></a>
		</div>
		<div id="navbar-dwn" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-middle">
                <li class="dropdown <?php echo $a_class_1." ".$a_class_2; ?>">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Universidades y delegados <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li class="<?php echo $a_class_1; ?>"><a href="univydelegados.php">Delegados por universidad</a></li>
                    <li role="separator" class="divider"></li>
                    <li class="<?php echo $a_class_2; ?>"><a href="registrodelegados.php">Acreditación</a></li>
                    <li class="<?php echo $a_class_3; ?>"><a  href='#del-votar-mod' data-target='#del-votar-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin'>Elecciones</a></li>
                    <li class="<?php echo $a_class_3; ?>"><a  href='#del-asistencia-mod' data-target='#del-asistencia-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin'>Asistencia</a></li>
                  </ul>
                </li>
 				<li class="<?php echo $a_class_4; ?>">
                    <a href="eventos.php">Eventos</a>
                </li>
 				<li class="<?php echo $a_class_5; ?>">
                    <a href="nosotros.php">Nosotros</a>
                </li>
 				<li class="<?php echo $a_class_6; ?>">
                    <a href="comite.php">Comité</a>
                </li>
            </ul>
		</div>
	</div>
</nav>
   
<div id="del-votar-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Votar</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <form action="" method="post">
                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label>Ingrese su DNI y su código de votaciones de Aneic para ingresar al sistema de votaciones.</label>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <input class="form-control" type="number" placeholder="Ingrese su DNI" name="dni-votar" id="dni-votar">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <input class="form-control" type="password" placeholder="Código de votaciones" name="cod-votar" id="cod-votar">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <a href="#" onclick="recuperarcod()">¿Olvidaste tu código de votación? Recuperar código de votación</a>
                      </div>
                    </div>
                    <div class="mensaje-opc-agre"></div>
                    <div class="vota-modbod-btn">
                        <button type="submit" class="btn btn-default btn-sm ver-votdel-btn" name="ver-votdel-btn" data-table="0" data-idagre="">Ir</button>                        
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>   
<div id="del-asistencia-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Asistencia</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <form action="" method="post">
                    <div class="form-group row">
                      <div class="col-sm-12">
                          <label>Ingrese su DNI y el código de asistencia para registrar su asistencia.</label>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <input class="form-control" type="number" placeholder="Ingrese su DNI" name="dni-asist" id="dni-asist">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <input class="form-control" type="text" placeholder="Código de asistencia" name="cod-asist" id="cod-asist">
                      </div>
                    </div>
                    <div class="mensaje-opc-agre"></div>
                    <div class="vota-modbod-btn">
                        <button type="submit" class="btn btn-default btn-sm ver-asisdel-btn" name="ver-asisdel-btn">Registrar</button>                        
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
    
<div id="olvide-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Recuperar código</h4>
            </div>
            <div class="modal-body">
                 <form class="form-horizontal" role="form" action="" method="post">
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <label for="correo-recu" class="lbl-login-mod">Ingrese su DNI y se le enviará el código al correo</label>
                        <input type="number" class="form-control input-sm" name="dni-recu" id="dni-recu" placeholder="Ingrese su DNI" required>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                         <div class="logbut-mod">
                            <button type="submit" class="btn btn-entr-mod btn-sm" name="codigo-recu" >Recuperar</button>
                         </div>
                      </div>
                    </div>

                    
                </form>
            </div>

        </div>
    </div>
</div>