<?php
include 'funciones.php';
$id_universidad=$_GET['xid_universidad'];

$resultado=listardelegados($id_universidad);

$delegado1 = new stdClass;
$delegado2 = new stdClass;
$delegado3 = new stdClass;
$delegado4 = new stdClass;

$delegadoex1=0;
$delegadoex2=0;
$delegadoex3=0;
$delegadoex4=0;

while ($row=mysqli_fetch_assoc($resultado)) {
    $id_delegado=$row['id_delegado'];
    $id_universidad=$row['id_universidad'];
    $puesto_delegado=$row['puesto_delegado'];
    $nombres_delegado=$row['nombres_delegado'];
    $apellidos_delegado=$row['apellidos_delegado'];
    $correo_delegado=$row['correo_delegado'];
    $celular_delegado=$row['celular_delegado'];
    $foto_delegado=$row['foto_delegado'];
    
    $tmp = new stdClass;
    $tmp->nombres_delegado = $nombres_delegado;
    $tmp->apellidos_delegado = $apellidos_delegado;
    $tmp->correo_delegado = $correo_delegado;
    $tmp->celular_delegado = $celular_delegado;
    $tmp->foto_delegado = $foto_delegado;

    switch($puesto_delegado){
        case 1:
            $delegado1=$tmp;
            $delegadoex1=1;
            break;
        case 2:
            $delegado2=$tmp;
            $delegadoex2=1;
            break;
        case 3:
            $delegado3=$tmp;
            $delegadoex3=1;
            break;
        case 4:
            $delegado4=$tmp;
            $delegadoex4=1;
            break;
        
    }
    
    
}
?>
    
<?php if($delegadoex1 == 1){ ?>
    <div class="univdel-card-inn">
        <div class="univdel-card-arri">
            <div class="univdel-card-nom"><?php echo $delegado1->apellidos_delegado.", ".$delegado1->nombres_delegado ?></div>
            <div class="univdel-card-pos">Delegado</div>
        </div>

        <div class="univdel-card-aba">
            <div class="univdel-card-descr">
                <div class="univdel-card-detit">Correo:</div>
                <div class="univdel-card-deval">
                    <?php
                        if($delegado1->correo_delegado){ 
                            echo $delegado1->correo_delegado;
                        }else{
                            echo "-";
                        }
                    ?>
                </div>
            </div>
            <div class="univdel-card-descr">
                <div class="univdel-card-detit">Celular:</div>
                <div class="univdel-card-deval">
                    <?php
                        if($delegado1->celular_delegado){ 
                            echo $delegado1->celular_delegado;
                        }else{
                            echo "-";
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="univdel-card-imgwrapp">
            <img alt="imagen de delegado" class="img-circle" src="<?php echo $delegado1->foto_delegado ?>">
        </div>

    </div>
<?php } ?>

<?php if($delegadoex2 == 1){ ?>
    <div class="univdel-card-inn">
        <div class="univdel-card-arri">
            <div class="univdel-card-nom"><?php echo $delegado2->apellidos_delegado.", ".$delegado2->nombres_delegado ?></div>
            <div class="univdel-card-pos">Sub-delegado</div>
        </div>

        <div class="univdel-card-aba">
            <div class="univdel-card-descr">
                <div class="univdel-card-detit">Correo:</div>
                <div class="univdel-card-deval">
                    <?php
                        if($delegado2->correo_delegado){ 
                            echo $delegado2->correo_delegado;
                        }else{
                            echo "-";
                        }
                    ?>
                </div>
            </div>
            <div class="univdel-card-descr">
                <div class="univdel-card-detit">Celular:</div>
                <div class="univdel-card-deval">
                    <?php
                        if($delegado2->celular_delegado){ 
                            echo $delegado2->celular_delegado;
                        }else{
                            echo "-";
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="univdel-card-imgwrapp">
            <img alt="imagen de delegado" class="img-circle" src="<?php echo $delegado2->foto_delegado ?>">
        </div>

    </div>
<?php } ?>

<?php if($delegadoex3 == 1){ ?>
    <div class="univdel-card-inn">
        <div class="univdel-card-arri">
            <div class="univdel-card-nom"><?php echo $delegado3->apellidos_delegado.", ".$delegado3->nombres_delegado ?></div>
            <div class="univdel-card-pos">Primer accesitario</div>
        </div>

        <div class="univdel-card-aba">
            <div class="univdel-card-descr">
                <div class="univdel-card-detit">Correo:</div>
                <div class="univdel-card-deval">
                    <?php
                        if($delegado3->correo_delegado){ 
                            echo $delegado3->correo_delegado;
                        }else{
                            echo "-";
                        }
                    ?>
                </div>
            </div>
            <div class="univdel-card-descr">
                <div class="univdel-card-detit">Celular:</div>
                <div class="univdel-card-deval">
                    <?php
                        if($delegado3->celular_delegado){ 
                            echo $delegado3->celular_delegado;
                        }else{
                            echo "-";
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="univdel-card-imgwrapp">
            <img alt="imagen de delegado" class="img-circle" src="<?php echo $delegado3->foto_delegado ?>">
        </div>

    </div>
<?php } ?>

<?php if($delegadoex4 == 1){ ?>
    <div class="univdel-card-inn">
        <div class="univdel-card-arri">
            <div class="univdel-card-nom"><?php echo $delegado4->apellidos_delegado.", ".$delegado4->nombres_delegado ?></div>
            <div class="univdel-card-pos">Segundo accesitario</div>
        </div>

        <div class="univdel-card-aba">
            <div class="univdel-card-descr">
                <div class="univdel-card-detit">Correo:</div>
                <div class="univdel-card-deval">
                    <?php
                        if($delegado4->correo_delegado){ 
                            echo $delegado4->correo_delegado;
                        }else{
                            echo "-";
                        }
                    ?>
                </div>
            </div>
            <div class="univdel-card-descr">
                <div class="univdel-card-detit">Celular:</div>
                <div class="univdel-card-deval">
                    <?php
                        if($delegado4->celular_delegado){ 
                            echo $delegado4->celular_delegado;
                        }else{
                            echo "-";
                        }
                    ?>
                </div>
            </div>
        </div>
        <div class="univdel-card-imgwrapp">
            <img alt="imagen de delegado" class="img-circle" src="<?php echo $delegado4->foto_delegado ?>">
        </div>

    </div>

<?php } ?>
<?php if( ($delegadoex1 + $delegadoex2+$delegadoex3+$delegadoex4) == 0 && $id_universidad != 0){ ?>
    <div class="nodelegado-registrado">
        Aún no se registran delegados para la universidad seleccionada.
    </div>
<?php } ?>