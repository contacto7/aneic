<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    echo "<script> window.location.href='index.php';</script>";
}

if(!isset($_GET['xidevento'])){
    echo "<script> window.location.href='generarcertificados.php';</script>";
}


$certificado_subido = 0;

if(isset($_GET['xcertificado_sub'])){
    $certificado_subido = $_GET['xcertificado_sub'];
}

$id_evento=$_GET['xidevento'];

$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();
?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Eventos</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body style="min-width:450px;height:100%">
<script type="text/javascript" src="js/jquery.min.js"></script>
    
<div id="header"></div>
    
<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Generar certificados</span>
    </div>
</div>
<div class="container">
    <h1 class="h2">
        <span>Paso 2</span>
    </h1>
    <h1 class="h4">
        <span>
            Subir el certificado firmado.
        </span>
    </h1>
</div>

<div class="container cont-eventos" style="min-height: 100%;margin: -472px auto 0;">
    <div class="row row-eventos" style="padding-top: 472px;">
        <div class="col-xs-12">Certificado <?php if($certificado_subido){ echo "<span class='glyphicon glyphicon-ok' style='color:#008000'></span>"; } ?></div>
        <div class="col-xs-12">
            <form action="" method="post" enctype="multipart/form-data" id="importFrm">
                <label for="file"><a class="btn btn-md form-control input-sm selec-arch">Seleccionar archivo</a></label>
                <div id="nombrefoto" style="display:inline-block"></div>
                  <input type="file" class="archivo-subir" id="file" name="file" style="display:none" data-id="1">
                <span class="span-foto-estado" id="estado-cambio-1"></span>
                <input type="submit" class="btn btn-primary" name="importSubmit" value="Subir" style="display:inline-block">
            </form>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-8 col-lg-9" style="padding-top: 15px;">
            <a class="btn btn-md btn-info" id="link-sgte" href="<?php if($certificado_subido){ echo 'generarcertificados3.php?xidevento='.$id_evento.'&xcertificado_sub='.$certificado_subido; }else{ echo "#"; } ?>">Siguiente</a>
        </div>
    </div>
</div>

<div id="footer"></div>
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
    
<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 2, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<?php

if(isset($_POST['importSubmit'])){
    //validate whether uploaded file is a csv file
    $csvMimes = array('image/gif','image/png' ,'image/jpg','image/jpeg','image/bmp','image/ico','image/apng','image/xbm','image/webp','image/svg','image/GIF','image/PNG' ,'image/JPG', 'image/JPEG','image/BMP','image/ICO','image/APNG','image/XBM','image/WEBP','image/SVG');
    
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
        
        
        $filename = $_FILES['file']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        
        $nuevo_nombre=randomalfanume(25);
        $nuevo_nombre= $nuevo_nombre.".".$ext;
        $url_foto= "fotos_certificado/".$nuevo_nombre;
        
        $image_temp_name=$_FILES['file']['tmp_name'];
        
        if(move_uploaded_file($image_temp_name,$url_foto)){

            $id_evento_cert = $id_evento;
            $id_cert_foto = anadircertificadoevento($id_evento_cert, $url_foto);
        
            if($id_cert_foto =="Error"){
                echo "<script>alert('Hubo un error al subir el certificado. Por favor intenta más tarde.')</script>";
                exit();
            }else{
                //echo "<script>console.log('".$_FILES['file']['type']."')</script>";
                echo "<script> window.location.href='generarcertificados2.php?xidevento=$id_evento&xcertificado_sub=$url_foto';</script>";
            }
            
            
        }else{
            echo "<script>alert('Hubo un error al cargar la imagen. Por favor intenta más tarde.')</script>";
            exit();
        }
        
        
    }else{
        echo "<script>alert('Por favor suba una imagen. ')</script>";
    }
}

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

 
if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}    
    
?>
</body>
</html>