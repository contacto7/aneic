<?php 
session_start();
//validando las variables de sesion
    include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    echo "<script> window.location.href='index.php';</script>";
}

$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();

?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Administrar</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
</head>
<body style="height:100%">
<script type="text/javascript" src="js/jquery.min.js"></script>

<div id="header"></div>
    
<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Administrar</span>
    </div>
</div>

<?php
    
$resultado=listarprimdelegadoreg();


while ($row=mysqli_fetch_assoc($resultado)) { //Por cada universidad se crea una tabla con los delegados por aprobar

    //DATOS UNIVERSIDAD
    $id_universidad=$row['id_universidad'];
    $nombre_universidad=$row['nombre_universidad'];
    $departamento=$row['departamento'];
    $estado_universidad=$row['estado_universidad'];
    
    
    //DATOS DELEGADO
  //  $id_delegado=$row['id_delegado'];
   // $id_universidad=$row['id_universidad'];

    $resultado1=listardocumentocertdel($id_universidad);

    while ($row=mysqli_fetch_assoc($resultado1)) { //Solo existe un delegado por universidad, y los documentos de acreditacion y el excel solo están vinculados a este único delegado por universidad
        $id_documento_delegado1=$row['id_documento_delegado'];
        $url_documento_delegado1=$row['url_documento_delegado'];
        $tipo_documento_delegado1=$row['tipo_documento_delegado'];
	}    
    
    $resultado2=listardocumentoexcdel($id_universidad);

    while ($row=mysqli_fetch_assoc($resultado2)) {
        $id_documento_delegado2=$row['id_documento_delegado'];
        $url_documento_delegado2=$row['url_documento_delegado'];
        $tipo_documento_delegado2=$row['tipo_documento_delegado'];
    }
    

?>
	<div class="container cont-adm" style="min-height: 100% auto 0;"><!-- Comienza listado por univerisdad -->
    <div class="row row-info-univ" "> 
        <div class="info-univ-wrapp">
            <div class="info-univ-ln">
                <span class="info-univ-sp" style="float:left">Universidad:</span> 
                <div class="info-univ-nomb"><?php echo $nombre_universidad; ?> <?php if(!$estado_universidad){ ?>
                    
                    (Se agregó universidad)
                    <div class="univ-href-acep">
                        <a class="btn btn-md form-control input-sm selec-arch acept-del" data-id="<?php echo $id_universidad; ?>" data-tipo="3" style="display:inline-block;width:80px;">Agregar</a>
                        <a class="btn btn-md form-control input-sm deldel-arch acept-del" data-id="<?php echo $id_universidad; ?>" data-tipo="4" style="display:inline-block; margin-top:0;width:100px;">Rechazar</a>
                    </div>
                    <?php }; ?>
                
                </div>
            </div>
            <div class="info-univ-ln"><span class="info-univ-sp">Departamento:</span> <?php echo $departamento; ?></div>
            <div class="info-univ-ln"><span class="info-univ-sp">Documento de acreditación:</span> <a href="<?php echo $url_documento_delegado1; ?>" class="descarg-exc" download="acreditacion"><span class="glyphicon glyphicon-download"></span> Documento</a></div>
            <div class="info-univ-ln"><span class="info-univ-sp">Excel con registro:</span> <a href="<?php echo $url_documento_delegado2; ?>" class="descarg-exc" download="registrodedelegados"><span class="glyphicon glyphicon-download"></span> Excel</a></div>
        </div>
    </div>
	

    <div class="row row-info-univ">
        <div class="info-univ-wrapp">
          <table class="table table-striped table-del-adm">
            <thead>
              <tr>
                <th>Foto</th>
                <th>Nombre</th>
                <th>Cargo</th>
                <th>Correo</th>
                <th>Teléfono</th>
				<th>Universidad</th>
                <th style="width:110px">Estado</th>
              </tr>
            </thead>
            <tbody>
<?php
    
if($id_universidad <> NULL){
    

$resultado_delegados=listardelegadosreg($id_universidad);


while ($row=mysqli_fetch_assoc($resultado_delegados)) {
    
    //DATOS DELEGADO
    $id_delegado=$row['id_delegado'];
    $id_universidad_delegado=$row['id_universidad'];
	$nombre_universidad=$row['nombre_universidad'];
    $puesto_delegado=$row['puesto_delegado'];
    $nombres_delegado=$row['nombres_delegado'];
    $apellidos_delegado=$row['apellidos_delegado'];
    $correo_delegado=$row['correo_delegado'];
    $celular_delegado=$row['celular_delegado'];
    $foto_delegado=$row['foto_delegado'];
    $dni_delegado=$row['dni_delegado'];
    if($id_universidad == $id_universidad_delegado){
?>
              <tr>
                <td style="width:100px">
                    <div class="adm-tab-imgwrapp">
                        <img alt="imagen de delegado" class="adm-img-tab" src="<?php echo $foto_delegado; ?>">
                    </div>
                </td>
                <td><?php echo $nombres_delegado; ?></td>
                <td><?php 
                        switch($puesto_delegado){
                            case 1:
                                echo "Delegado";
                                break;
                            case 2:
                                echo "Sub-Delegado";
                                break;
                            case 3:
                                echo "1° Accesitario";
                                break;
                            case 4:
                                echo "2° Accesitario";
                                break;
                            case 5:
                                echo "Miembro Honorario";
                                break;
                            case 6:
                                echo "Secretario Corede";
                                break;
                            case 7:
                                echo "Consejo Directivo";
                                break;
							case 8:
								echo "Sub Secretario de Corede";
								break;	
							case 9:
								echo "Embajador Internacional";
								break;
                        } 
                    
                    ?></td>
                <td><?php echo $correo_delegado; ?></td>
                <td><?php echo $celular_delegado; ?></td>
				<td><?php echo $nombre_universidad; ?></td>
                <td id="td-<?php echo $id_delegado; ?>">
                    <a class="btn btn-md form-control input-sm selec-arch acept-del" data-id="<?php echo $id_delegado; ?>" data-tipo="1" data-dni="<?php echo $dni_delegado; ?>" data-puesto="<?php echo $puesto_delegado; ?>">Aceptar</a>
                    <a class="btn btn-md form-control input-sm deldel-arch acept-del" data-id="<?php echo $id_delegado; ?>" data-tipo="2" data-puesto="<?php echo $puesto_delegado; ?>">Rechazar</a>
                </td>
              </tr>
<?php  
}?>
 <?php } //Finaliza tabla de delegados por universidad 
 ?> 
  </tbody>
          </table> 
 <?php
}else{
?>
    
              <tr>
                <td style="width:100px"></td>
                <td>No se han encontrado delegados</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td></td>
              </tr>
    
<?php  
}//Finaliza el while de universidades 
?>
           

        </div>
    </div>
</div>	<!--Finaliza el cuadro de delegados por universidad-->
<hr style="border-top: 3px dashed blue;">
<?php } ?>

<div id="footer"></div>
    
<div id="login-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title">Login</h4>
            </div>
            <div class="modal-body">
                 <form class="form-inline" role="form" action="" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="correo">Correo</label>
                        <input type="email" class="form-control input-sm" name="correo" id="user_mail" placeholder="Correo" required>
                    </div>     
                    <div class="form-group">
                        <label class="sr-only" for="contrasena">Contraseña</label>
                        <input type="password" class="form-control input-sm" name="contrasena" id="password" placeholder="Contraseña" required>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox"><small>Recordarme</small></label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm" name="ingresar" >Entrar</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script src="js/jquery.carouFredSel.js"></script>

<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 1, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<script>

var id_universidad = <?php if(isset($id_universidad)){ echo $id_universidad;}else{ echo "0"; }; ?>;
$("a.toscroll").on('click',function() {
    var url =$(this).attr("href");
    //console.log(url);
    var hash = url.substring(url.indexOf("#")+1);
    $('html, body').animate({
        scrollTop: $('#'+hash).offset().top -200
    }, 500);
    return false;
});
    
</script>
<?php

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}    
    
?>
</body>
</html>