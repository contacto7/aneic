<?php 
session_start();
$nombres="";
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';

?>
<!DOCTYPE html>
<html ng-app="">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jquery.raty.css">
    <link href="css/princ.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery.min.older.js"></script>
</head>
<body style="background-color:#eee;">
<script type="text/javascript" src="js/jquery.min.js"></script>
<nav class="navbar navbar-nav navbar-fixed-top" role="navigation">
    <div class="nav-top-wrap">
        <div class="nav-top-inn">
            <img src="foto_index/1.jpg" class="nav-top-img" alt="foto de cabecera">
            <div class="nav-top-cuad"></div>
        </div>
    </div>
	<div class="container cont-nav">
		<div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar,#navbar-dwn" aria-expanded="false" aria-controls="navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>
				<a class="navbar-brand" href="index.php"><img src="img/logoaneicv1.png" class="nav-img-logo" alt="logo"></a>
		</div>
		<div id="navbar-dwn" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-middle">
 				<li class="active">
                    <a href="adm-regdel.php">Administrar delegados</a>
                </li>
 				<li class="active-nav-a">
                    <a href="generarcodigos.php">Generar códigos qr</a>
                </li>
            </ul>
		</div>
	</div>
</nav>
    
<div class="container" style="padding-top:250px;">

    <div class="row">
        <div class="col-xs-12">
            <form class="form-horizontal form-qr-settings" style="">
              <div class="form-group">
                <label for="altpapel">Altura de papel</label>
                <input type="number" class="form-control" id="altpapel" >
              </div>
              <div class="form-group">
                <label for="anchpapel">Ancho de papel</label>
                <input type="number" class="form-control" id="anchpapel" >
              </div>
              <div class="form-group">
                <label for="tamqr">Tamaño de Qr</label>
                <input type="number" class="form-control" id="tamqr" >
              </div>
              <div class="form-group">
                <label for="grosorln">Grosor de línea de borde</label>
                <input type="number" class="form-control" id="grosorln">
              </div>
              <div class="form-group">
                <label for="espaciadoqr">Espaciado entre Qr y borde</label>
                <input type="number" class="form-control" id="espaciadoqr">
              </div>
              <div class="form-group">
                <label for="distarr">Distancia a arriba</label>
                <input type="number" class="form-control" id="distarr">
              </div>
              <div class="form-group">
                <label for="distizq">Distancia a izquierda</label>
                <input type="number" class="form-control" id="distizq">
              </div>
              <div class="form-group">
                <a class="btn btn-info" id="genqr" onclick="dataqr()">Generar QR </a> 
                  <a class="btn btn-info" id="download" data-nombre="cert-h-e1.png" data-padreid="#demo" onclick="descargarqrs()">Descargar </a>
              </div>

                <a class="btn btn-info" id="download-verd" style="display:none">Descargar </a>
            </form>
        </div>

        <div class="canvas-ant" style="display:none"></div>
        <div id="demo"></div>
<?php 

$result=listarcertificados(4);
$temp=0;

while ($row=mysqli_fetch_assoc($result)) {
    
    $id_participante_certificado=$row['id_participante_certificado'];
    $id_evento=$row['id_evento'];
    $id_certificado=$row['id_certificado'];
    $nombre_asistente=$row['nombre_asistente'];
    $calidad_participante_certificado=$row['calidad_participante_certificado'];

?>
        <div id="demo-<?php echo $temp; ?>" class="qr-canvas" data-idevento="<?php echo $id_evento; ?>" data-idcert="<?php echo $id_certificado; ?>" data-id="<?php echo $id_participante_certificado; ?>"  data-nombre="<?php echo $nombre_asistente; ?>" data-calidad="<?php echo $calidad_participante_certificado; ?>"></div>
        
<?php 
$temp++;
}

?>
    </div>
</div>
<footer class="foot-wrap">
    <div class="footer-inner">
        <div class="footer-izq">
            <div class="aneic-footer">&copy; Aneic Perú 2017</div>
            <div class="div-footer-2"> - </div>
            <div class="copr-aqm"><span>Desarrollado por </span>
                <a href="http://www.inoloop.com" target="_blank" class="logo-foot-wrap">    <img class="logo-foot" src="img/logo-ino.png"> 
                </a>
            </div>
        </div>
        <div class="footer-der">
            <div class="fb-footer"><span>Síguenos en: </span><a href="https://www.facebook.com/aneicp" class="fb-link" target="_blank"><img src="img/fb-logo.png" class="logo-fb-img" alt="logo de facebook"></a></div>
        </div>
        <div class="footr-dwn">
            <a class="busq-cert-wrap" href="buscarcert.php" target="_blank">
                <div class="busq-cer-icon"><span class="glyphicon glyphicon-search"></span></div>
                <div class="busq-cer-lin">Buscar</div>
                <div class="busq-cer-lin">Certificado</div>
            </a>
        </div>
    </div>
</footer>

<div id="login-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title">Login</h4>
            </div>
            <div class="modal-body">
                 <form class="form-inline" role="form" action="" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="correo">Correo</label>
                        <input type="email" class="form-control input-sm" name="correo" id="user_mail" placeholder="Correo" required>
                    </div>     
                    <div class="form-group">
                        <label class="sr-only" for="contrasena">Contraseña</label>
                        <input type="password" class="form-control input-sm" name="contrasena" id="password" placeholder="Contraseña" required>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox"><small>Recordarme</small></label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm" name="ingresar" >Entrar</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script type="text/javascript" src="js/jquery.qrcode.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    
});
</script>
</body>
</html>