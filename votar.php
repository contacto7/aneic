<?php 
session_start();
$sesioninic=0;
//validando las variables de sesion
    include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    //echo "<script> window.location.href='index.php';</script>";
}
$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();


?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Administrar</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
</head>
<body>
<script type="text/javascript" src="js/jquery.min.js"></script>
<nav class="navbar navbar-nav navbar-fixed-top" role="navigation">
<?php if($sesioninic==1){ ?>
		<div id="navbar" class="" style="position: fixed; z-index:100; left : 50px; top: 50px">
            <div class="container">
                <ul class="nav navbar-nav">
				    <li style="display:inline-block">
                        <button class="btn btn-sm btn-info" onclick="location.href='adm-regdel.php'" type="button"> ADMINISTRAR </button>
                    </li>
                    <li style="display:inline-block"><form class="form-inline" role="form"    action="" method="get"><button type="submit" class="btn btn-sm btn-warning" name="cerrar-sesion">Cerrar Sesión</button>
                        </form></li>

                </ul>
            </div>

		</div>
 <?php } ?>
    <div class="nav-top-wrap">
        <div class="nav-top-inn">
            <img src="<?php echo $url_fotos_arr[0]; ?>" class="nav-top-img" alt="foto de cabecera">
            <div class="nav-top-cuad"></div>
        </div>
    </div>
	<div class="container cont-nav">
		<div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar,#navbar-dwn" aria-expanded="false" aria-controls="navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>
				<a class="navbar-brand" href="index.php"><img src="<?php echo $url_fotos_arr[1]; ?>" class="nav-img-logo" alt="logo"></a>
		</div>
		<div id="navbar-dwn" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-middle">
 				<li class="active">
                    <a href="univydelegados.php">Universidades y delegados</a>
                </li>
 				<li class="active">
                    <a href="eventos.php">Eventos</a>
                </li>
 				<li class="active">
                    <a href="nosotros.php">Nosotros</a>
                </li>
 				<li class="active">
                    <a href="comite.php">Comité</a>
                </li>
 				<li class="active">
                    <a href="registrodelegados.php">Acreditación</a>
                </li>
            </ul>
		</div>
	</div>
</nav>
<div class="bot-nav-wrap" style="top:180px;">
    <div class=" container bot-nav-inn">
        <div class="bot-nav-ap">
            <span class="glyphicon glyphicon-time"></span><?php echo $horainic_adm_info; ?> <?php echo $horafin_adm_info; ?> &nbsp; 
        </div>
        <div class="bot-nav-tel">
             <span class="glyphicon glyphicon-earphone"></span><?php echo $contacto_adm_info; ?> &nbsp;
        </div>
    </div>
</div>
<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Votar</span>
    </div>
</div>
<div class="container cont-adm">
    
    <div class="row" style="margin-top:25px;">
        
        <div class="col-xs-12">

            <form class="form-horizontal" method="post">
              <div class="form-group">
                <label class="control-label col-sm-2" for="correo-adm">Correo:</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" id="correo-adm" name="correo-adm" placeholder="Ingrese correo de administrador">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="cont-adm">Contraseña:</label>
                <div class="col-sm-10"> 
                  <input type="password" class="form-control" id="cont-adm" name="cont-adm" placeholder="Enter password">
                </div>
              </div>
              <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-default" name="ingresa-adm">Ingresar</button>
                </div>
              </div>
            </form>
        
        </div>
        
    </div>
    
</div>

<footer class="foot-wrap">
    <div class="footer-inner">
        <div class="footer-izq">
            <div class="aneic-footer">&copy; Aneic Perú 2017</div>
            <div class="div-footer-2"> - </div>
            <div class="copr-aqm"><span>Desarrollado por </span>
                <a href="http://www.inoloop.com" target="_blank" class="logo-foot-wrap">    <img class="logo-foot" src="img/logo-ino.png"> 
                </a>
            </div>
        </div>
        <div class="footer-der">
            <div class="fb-footer"><span>Síguenos en: </span><a href="https://www.facebook.com/aneicp" class="fb-link" target="_blank"><img src="img/fb-logo.png" class="logo-fb-img" alt="logo de facebook"></a></div>
        </div>
        <div class="footr-dwn">
            <a class="busq-cert-wrap" href="buscarcert.php" target="_blank">
                <div class="busq-cer-icon"><span class="glyphicon glyphicon-search"></span></div>
                <div class="busq-cer-lin">Buscar</div>
                <div class="busq-cer-lin">Certificado</div>
            </a>
        </div>
    </div>
</footer>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script src="js/jquery.carouFredSel.js"></script>
<script>

var id_universidad = <?php if(isset($id_universidad)){ echo $id_universidad;}else{ echo "0"; }; ?>;
$("a.toscroll").on('click',function() {
    var url =$(this).attr("href");
    //console.log(url);
    var hash = url.substring(url.indexOf("#")+1);
    $('html, body').animate({
        scrollTop: $('#'+hash).offset().top -200
    }, 500);
    return false;
});
</script>
    
    
<?php
 
if (isset($_POST['ingresa-adm'])){

    if(!empty($_POST['correo-adm']) || !empty($_POST['cont-adm']) ){

        $correo_adm=$_POST['correo-adm'];
        $contrasena_adm=$_POST['cont-adm'];
        
        $pagdireccion = "adm-regdel.php";

        ingresa($correo_adm,$contrasena_adm,$pagdireccion);

    }

}
    
?>
</body>
</html>