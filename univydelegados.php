<?php 
session_start();
$nombres="";
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}

$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();

$universidadeslista = listaruniversidades();

$amazonas   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ancash     = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$apurimac   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$arequipa   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ayacucho   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$cajamarca  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$callao     = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$cusco      = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$huancavelica = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$huanuco = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ica = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$junin = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$lalibertad = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$lambayeque = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$lima  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$loreto  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$madrededios = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$moquegua   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$pasco   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$piura  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$puno   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$sanmartin   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$tacna   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$tumbes   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ucayali   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";

$universidadesarr = array();

while ($row=mysqli_fetch_assoc($universidadeslista)) {
    $id_universidad=$row['id_universidad'];
    $nombre_universidad=$row['nombre_universidad'];
    $departamento=$row['departamento'];

    switch ($departamento) {
        case "Amazonas":
            $amazonas   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Áncash":
            $ancash     .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Apurímac":
            $apurimac   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Arequipa":
            $arequipa   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Ayacucho":
            $ayacucho   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Cajamarca":
            $cajamarca  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Callao":
            $callao     .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Cusco":
            $cusco      .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Huancavelica":
            $huancavelica .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Huánuco":
            $huanuco .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Ica":
            $ica .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Junín":
            $junin .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "La Libertad":
            $lalibertad .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Lambayeque":
            $lambayeque .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Lima":
            $lima  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Loreto":
            $loreto  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Madre de Dios":
            $madrededios .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Moquegua":
            $moquegua   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Pasco":
            $pasco   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Piura":
            $piura  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Puno":
            $puno   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "San Martín":
            $sanmartin   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Tacna":
            $tacna   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Tumbes":
            $tumbes   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Ucayali":
            $ucayali   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
    }


}
mysqli_free_result($universidadeslista);

?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Universidades y delegados</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
</head>
<body>
<script type="text/javascript" src="js/jquery.min.js"></script>


<div id="header"></div>
    
<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Universidades y delegados</span>
    </div>
</div>
<div class="container cont-unidele">
    <div row="row">
        <div class="col-xs-12">
            <div class="leyenda-mapa-wrapp">
                <div class="leyenda-mapa-inn">
                    <div class="leyenda-mapa-tit">
                        COREDES:
                    </div>
                    <div class="leyenda-mapa-leys">
                        <div class="leyenda-mapa-ley">
                            <div class="leyenda-mapa-corecolor">
                                <svg width="14" height="14">
                                 <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff"></circle>
                                 Disculpa, pero tu navegador no soporta SVG en línea.
                              </svg>
                            </div>
                            <div class="leyenda-mapa-coretit">Centro</div>
                        </div>
                        <div class="leyenda-mapa-ley">
                            <div class="leyenda-mapa-corecolor">
                                <svg width="14" height="14">
                                 <circle cx="7" cy="7" r="5" stroke="#0e8813" stroke-width="2" fill="#fff"></circle>
                                 Disculpa, pero tu navegador no soporta SVG en línea.
                              </svg>
                            </div>
                            <div class="leyenda-mapa-coretit">Norte</div>
                        </div>
                        <div class="leyenda-mapa-ley">
                            <div class="leyenda-mapa-corecolor">
                                <svg width="14" height="14">
                                 <circle cx="7" cy="7" r="5" stroke="#e27f53" stroke-width="2" fill="#fff"></circle>
                                 Disculpa, pero tu navegador no soporta SVG en línea.
                              </svg>
                            </div>
                            <div class="leyenda-mapa-coretit">Sur</div>
                        </div>
                        <div class="leyenda-mapa-ley">
                            <div class="leyenda-mapa-corecolor">
                                <svg width="14" height="14">
                                 <circle cx="7" cy="7" r="5" stroke="#4cb6bf" stroke-width="2" fill="#fff"></circle>
                                 Disculpa, pero tu navegador no soporta SVG en línea.
                              </svg>
                            </div>
                            <div class="leyenda-mapa-coretit">Oriente</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-5 col-univdel-princ">
            <div class="info-tip tip-info">
                Selecciona tu departamento 
            </div>
            <div class="img-mapa-wrapp">
                <div class="img-mapa-puntos">
                    <div class="puntos-mapa-wrapp" id="amazonas">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Amazonas" data-dep="Amazonas" data-corede="4">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="ancash">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Ancash" data-dep="Áncash" data-corede="2">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg>
                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="apurimac">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Apurímac" data-dep="Apurímac" data-corede="3">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="arequipa">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Arequipa" data-dep="Arequipa" data-corede="3">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="ayacucho">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Ayacucho" data-dep="Ayacucho" data-corede="1">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="cajamarca">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Cajamarca" data-dep="Cajamarca" data-corede="2">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>     
                    <div class="puntos-mapa-wrapp" id="callao">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Callao" data-dep="Callao" data-corede="1">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="cusco">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Cusco" data-dep="Cusco" data-corede="3">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="huancavelica">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Huancavelica" data-dep="Huancavelica" data-corede="1">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="huanuco">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Huánuco" data-dep="Huánuco" data-corede="1">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="ica">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Ica" data-dep="Ica" data-corede="1">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>           
                    <div class="puntos-mapa-wrapp" id="junin">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Junín" data-dep="Junín" data-corede="1">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>         
                    <div class="puntos-mapa-wrapp" id="lalibertad">
                        <a class="punto-dep-link" data-toggle="tooltip" title="La Libertad" data-dep="La Libertad" data-corede="2">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>    
                    <div class="puntos-mapa-wrapp" id="lambayeque">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Lambayeque" data-dep="Lambayeque" data-corede="2">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>    
                    <div class="puntos-mapa-wrapp" id="lima">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Lima" data-dep="Lima" data-corede="1">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>    
                    <div class="puntos-mapa-wrapp" id="loreto">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Loreto" data-dep="Loreto" data-corede="4">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="madrededios">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Madre de Dios" data-dep="Madre de Dios" data-corede="4">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="moquegua">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Moquegua" data-dep="Moquegua" data-corede="3">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="pasco">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Pasco" data-dep="Pasco" data-corede="1">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="piura">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Piura" data-dep="Piura" data-corede="2">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="puno">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Puno" data-dep="Puno" data-corede="3">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="sanmartin">
                        <a class="punto-dep-link" data-toggle="tooltip" title="San Martín" data-dep="San Martín" data-corede="4">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="tacna">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Tacna" data-dep="Tacna" data-corede="3">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="tumbes">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Tumbes" data-dep="Tumbes" data-corede="2">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>
                    <div class="puntos-mapa-wrapp" id="ucayali">
                        <a class="punto-dep-link" data-toggle="tooltip" title="Ucayali" data-dep="Ucayali" data-corede="4">
                          <svg width="14" height="14">
                             <circle cx="7" cy="7" r="5" stroke="#c4435b" stroke-width="2" fill="#fff" />
                             Disculpa, pero tu navegador no soporta SVG en línea.
                          </svg> 

                        </a>
                    </div>       
                </div>
                <div class="img-mapa-inner">
                    <img class="img-mapa-del" alt="Mapa de universidades" src="img/mapa.jpg">
                </div>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-5 col-univdel-princ">
            <div class="elecc-ubiuni-wrapp">
                <div class="row">
                    <div class="col-xs-6" style="display:none">
                        <div class="tit-sel-univdel">Departamento:</div>
                        <select class="form-control sel-unibydel" name="departamento" id="departamento">
                          <option class="sel-dep" value="Departamento">Departamento</option>
                          <option class="sel-dep" value="Amazonas">Amazonas</option>
                            <option class="sel-dep" value="Áncash">Ancash</option>
                            <option class="sel-dep" value="Apurímac">Apurímac</option>
                            <option class="sel-dep" value="Arequipa">Arequipa</option>
                            <option class="sel-dep" value="Ayacucho">Ayacucho</option>
                            <option class="sel-dep" value="Cajamarca">Cajamarca</option>
                            <option class="sel-dep" value="Callao">Callao</option>
                            <option class="sel-dep" value="Cusco">Cusco</option>
                            <option class="sel-dep" value="Huancavelica">Huancavelica</option>
                            <option class="sel-dep" value="Huánuco">Huánuco</option>
                            <option class="sel-dep" value="Ica">Ica</option>
                            <option class="sel-dep" value="Junín">Junín</option>
                            <option class="sel-dep" value="La Libertad">La Libertad</option>
                            <option class="sel-dep" value="Lambayeque">Lambayeque</option>
                            <option class="sel-dep" value="Lima">Lima</option>
                            <option class="sel-dep" value="Loreto">Loreto</option>
                            <option class="sel-dep" value="Madre de Dios">Madre de Dios</option>
                            <option class="sel-dep" value="Moquegua">Moquegua</option>
                            <option class="sel-dep" value="Pasco">Pasco</option>
                            <option class="sel-dep" value="Piura">Piura</option>
                            <option class="sel-dep" value="Puno">Puno</option>
                            <option class="sel-dep" value="San Martín">San Martín</option>
                            <option class="sel-dep" value="Tacna">Tacna</option>
                            <option class="sel-dep" value="Tumbes">Tumbes</option>
                            <option class="sel-dep" value="Ucayali">Ucayali</option>
                        </select>
                    </div>
                    <div class="col-xs-12">
                        <div class="tit-sel-univdel">Universidad:</div>
                        <select class="form-control sel-unibydel" name="univ-dep" id="univ-dep">
                          <option class="sel-prov" value="universidad">Seleccione el departamento</option>
                        </select>
                    </div>
                </div>
                


                <div class="row univdel-row">
                    <div class="univdel-card-wrapp">
                        <div class="nodelegado-registrado">

                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>
    
    <div style="margin-right:0!important; margin-left:0px!important;">
            Soy un delegado: <a href="registrodelegados.php" target="_blank">Registrarme</a>
    </div>
    <div class="row univdel-row">
    </div>
</div>

<div id="footer"></div>

<div id="login-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title">Login</h4>
            </div>
            <div class="modal-body">
                 <form class="form-inline" role="form" action="" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="correo">Correo</label>
                        <input type="email" class="form-control input-sm" name="correo" id="user_mail" placeholder="Correo" required>
                    </div>     
                    <div class="form-group">
                        <label class="sr-only" for="contrasena">Contraseña</label>
                        <input type="password" class="form-control input-sm" name="contrasena" id="password" placeholder="Contraseña" required>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox"><small>Recordarme</small></label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm" name="ingresar" >Entrar</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script src="js/jquery.carouFredSel.js"></script>
    
<script>
$(function(){
    
   $("#header").load("header.php", {
       xph: 1, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
    
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
    
<script>

var departamentos ={

    "Departamento":'<option class="sel-prov" value="universidad">Seleccione el departamento</option>',
    "Amazonas": "<?php echo $amazonas; ?>",
    "Áncash": "<?php echo $ancash; ?>",
    "Apurímac": "<?php echo $apurimac; ?>",
    "Arequipa": "<?php echo $arequipa; ?>",
    "Ayacucho": "<?php echo $ayacucho; ?>",
    "Cajamarca": "<?php echo $cajamarca; ?>",
    "Callao": "<?php echo $callao; ?>",
    "Cusco": "<?php echo $cusco; ?>",
    "Huancavelica": "<?php echo $huancavelica; ?>",
    "Huánuco": "<?php echo $huanuco; ?>",
    "Ica": "<?php echo $ica; ?>",
    "Junín": "<?php echo $junin; ?>",
    "La Libertad": "<?php echo $lalibertad; ?>",
    "Lambayeque": "<?php echo $lambayeque; ?>",
    "Lima": "<?php echo $lima; ?>",
    "Loreto": "<?php echo $loreto; ?>",
    "Madre de Dios": "<?php echo $madrededios; ?>",
    "Moquegua": "<?php echo $moquegua; ?>",
    "Pasco": "<?php echo $pasco; ?>",
    "Piura": "<?php echo $piura; ?>",
    "Puno": "<?php echo $puno; ?>",
    "San Martín": "<?php echo $sanmartin; ?>",
    "Tacna": "<?php echo $tacna; ?>",
    "Tumbes": "<?php echo $tumbes; ?>",
    "Ucayali": "<?php echo $ucayali; ?>",
    
};
/*
$("#departamento").change(function() {
    var temp=0;
    var departamento= this.value;
    var prov=departamentos[departamento];
    var provarr = prov.split(",");
    var startlista="<ul>";

    var midlista="";
    for(provinc in provarr){
        midlista+="<option class='sel-prov' value='"+provarr[temp]+"'>"+provarr[temp]+"</option>";
        temp++;
    }
    var listaprov=startlista+midlista;
    $("#univ-dep").html(listaprov);
});
*/
$("#departamento").change(function() {
    var temp=0;
    var departamento= this.value;
    var prov=departamentos[departamento];
    $("#univ-dep").html(prov);
    //console.log(prov);
});
    
$("#univ-dep").change(function() {
    console.log("asd");
    var id_universidad=this.value;

    $(".cont-unidele").css({
        "opacity": "0.5",
        "pointer-events":"none"
    });
    $(".univdel-card-wrapp").html("Cargando...");

    $.ajax({url:"apost_append_delegados.php?xid_universidad="+id_universidad,cache:false,success:function(result){
        $(".univdel-card-wrapp").html(result);
                $(".cont-unidele").css({
                    "opacity": "1",
                    "pointer-events":"inherit"
                });
    }});
    
});
</script>
<?php

if(isset($_POST['ver-votdel-btn'])){

    if(!empty($_POST['dni-votar']) && !empty($_POST['cod-votar'])){
        
        $dni_delegado_votar = $_POST['dni-votar'];
        $cod_delegado_votar = $_POST['cod-votar'];

        $info_delegado = listarinfodelegadodni($dni_delegado_votar);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_delegado)) {
            $id_delegado =$row['id_delegado'];
            $puesto_delegado =$row['puesto_delegado'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){
            
            $codigo_delegado_tmp=$dni_delegado_votar*7*13*37;//para codificar el código

            $codigo_delegado_tmp = substr($codigo_delegado_tmp, -4);

            if($cod_delegado_votar == $codigo_delegado_tmp){
                
                unset($_SESSION['id_delegado']);
                unset($_SESSION['puesto_delegado']);
                
                $_SESSION["id_delegado"]=$id_delegado;
                $_SESSION["puesto_delegado"]=$puesto_delegado;
                
                $id_delegado_encriptado = doEncrypt($id_delegado);
                $puesto_delegado_encriptado = doEncrypt($puesto_delegado);
                                
                //echo "<script>window.location.href='lista_delegado_votacion.php?xi_1=$id_delegado_encriptado&xi_2=$puesto_delegado_encriptado';</script>";
                echo "<script>window.location.href='lista_delegado_votacion.php';</script>";

            }else{
                echo "<script> alert('Código de votación incorrecto.');</script>";
            }
            
        }else{
            echo "<script> alert('No se encuentra un delegado con el DNI brindado.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}

if(isset($_POST['ver-asisdel-btn'])){

    if(!empty($_POST['dni-asist']) && !empty($_POST['cod-asist'])){
        
        $dni_delegado_asist = $_POST['dni-asist'];
        $cod_delegado_asist = $_POST['cod-asist'];

        $info_asistencia = listarinfoasis($dni_delegado_asist, $cod_delegado_asist);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_asistencia)) {
            $id_delegado_asis =$row['id_delegado'];
            $id_asistencia_asis =$row['id_asistencia'];
            $estado_asistencia_asis =$row['estado_asistencia'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){

            if($estado_asistencia_asis > 0){

                $resultado_asi_agr = registrarasistenciadel($id_delegado_asis, $id_asistencia_asis);
                $cuenta_tmp = 0;

                if($resultado_asi_agr){
                    echo "<script> alert('Se registró la asistencia.');</script>";
                }else{
                    echo "<script> alert('Ha ocurrido un problema al registrar la asistencia. Inténtelo de nuevo, o comuníquese con algún representante de Aneic para solucionar el problema.');</script>";
                }

            }else{
                echo "<script> alert('La asistencia ya se ha cerrado.');</script>";
            }
            
        }else{
            echo "<script> alert('La asistencia no se ha abierto para usted, o el código brindado es incorrecto.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}


if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}


if (isset($_POST['codigo-recu'])){
	if(!empty($_POST['dni-recu'])){
		$dni_delegado_rec=$_POST['dni-recu'];

		$result=recuperarcontradel($dni_delegado_rec);

	}else{
        echo "<script> alert('Ingrese un DNI.');</script>";
    }
}

 
if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}    
    
?>
</body>
</html>