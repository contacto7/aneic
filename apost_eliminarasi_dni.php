<?php
//error_reporting(E_ALL);
//ini_set('display_errors','on');
if(!($_SERVER['HTTP_ORIGIN'] == "http://aneicperu.com" || $_SERVER['HTTP_ORIGIN'] == "https://aneicperu.com" || $_SERVER['HTTP_ORIGIN'] == "http://www.aneicperu.com" || $_SERVER['HTTP_ORIGIN'] == "https://www.aneicperu.com")) {
?>
        
<div class="vota-modbod-msg">Por favor, accede del servidor de ANEIC.</div>
<div class="mensaje-vota-cerrar"></div>
<div class="vota-modbod-btn">
    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">Cerrar</button>
</div>  

<?php   
}else{
    
    include 'funciones.php';
    $cuenta_temp = 0;
    $opciones = "";

    $link = conectar();

    if (!isset($_POST['x_i1'])) exit;

    $dni_delegado = $_POST['x_i1'];//Id de universidad
    
    $cmdsql="SELECT A.*, B.*, C.* FROM asistente A JOIN participante_certificado B ON A.id_asistente = B.id_asistente JOIN evento C ON B.id_evento = C.id_evento WHERE A.dni_asistente = $dni_delegado";

    $resultado = mysqli_query($link, $cmdsql);

    $error_votacion_code = mysqli_errno($link);
    
    if ($error_votacion_code > 0) {
?>
        
<div class="vota-modbod-msg">Ha ocurrido un error al listar el asistente. Si este problema persiste, comuníquese con servicio técnico.</div>
<div class="mensaje-vota-cerrar"></div>
<div class="vota-modbod-btn">
    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">Cerrar</button>
</div>  

<?php   
    }else{
        while ($row=mysqli_fetch_assoc($resultado)) {
            
            $cuenta_temp++;
            
            //ASISTENTE
            $id_asistente = $row['id_asistente'];
            $nombre_asistente = $row['nombre_asistente'];
            $dni_asistente = $row['dni_asistente'];
            //CERTIFICADO
            $id_participante_certificado = $row['id_participante_certificado'];
            $calidad_participante_certificado = $row['calidad_participante_certificado'];
            //EVENTO
            $nombre_evento = $row['nombre_evento'];
            
            $opciones.= "<option class='sel-prov' value='$id_participante_certificado'>$nombre_evento en calidad de $calidad_participante_certificado</option>";            
            
        }
        
        if($cuenta_temp == 0){
?>
        
<div class="vota-modbod-msg">No se ha encontrado algún certificado para el asistente con el DNI brindado.</div>
<div class="mensaje-vota-cerrar"></div>
<div class="vota-modbod-btn">
    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">Cerrar</button>
</div>  

<?php   
        }else{
                            
    
?>


<div class="vota-modbod-msg">Elija el certificado que desea eliminar del asistente: <span class="elimdel-nomb"><?php echo $nombre_asistente ?></span> con DNI <span class="elimdel-dni"><?php echo $dni_asistente;  ?></span></div>
<div class="mensaje-vota-cerrar"></div>
<div class="vota-modbod-btn">
    <form class="form-inline" role="form"  action="" method="post">
        <select class="form-control sel-unibydel" name="idcert-elim-inp" id="idcert-elim-inp" style="max-width:100%">
            <?php echo $opciones ?>
        </select>
        <br><br>
        <button type="submit" class="btn btn-sm btn-warning elimdel-dni-btn" name="elimasi-dni-btn">Si</button>
        <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
    </form>
</div>


<?php
            
        }
    }

    //return $resultado;
    
}

?>
