<?php

/*
function conectar(){
//error_reporting(E_ALL);
//ini_set('display_errors','on');
	$server="colegioeuclidesdb.cgysp6rvk4jr.us-west-2.rds.amazonaws.com";
	$user="awsuser";
	$pass="colegioeuclides2017";
	$database="colegio_euclides";
	$connect=null;
	$connect=mysqli_connect($server,$user,$pass,$database);
    mysqli_set_charset($connect,"utf8");
	return $connect;
}
*/
function conectar(){
//error_reporting(E_ALL);
//ini_set('display_errors','on');
	$server="localhost";
	$user="root";
	$pass="";
	$database="colegio_euclides";
	$connect=null;
	$connect=mysqli_connect($server,$user,$pass,$database);
    mysqli_set_charset($connect,"utf8");
	return $connect;
}
function desconectar(){
	mysqli_close(conectar());
}

function throw_ex($er){  
  throw new Exception($er);  
}

function randomalfanume($length = 8) {
    $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    $string = '';

    for ($i = 0; $i < $length; $i++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $string;
}

function ingresa($dni,$contrasena_alumno,$pagdireccion){
	$cmdsql="SELECT * FROM alumno WHERE dni='$dni' AND contrasena_alumno='$contrasena_alumno'";
	$result=mysqli_query(conectar(), $cmdsql);
	if($result){
		$cuenta=0;
        $alumno_matriculado=100;
		while ($row=mysqli_fetch_assoc($result)){
        $dni=$row['dni'];
        $id_alumno=$row['id_alumno'];
        $nombres_alumno=$row['nombres_alumno'];
        $apellidos_alumno=$row['apellidos_alumno'];
		$id_apoderado=$row['id_apoderado'];
		$id_seccion=$row['id_seccion'];
        $alumno_matriculado=$row['alumno_matriculado'];
        $cuenta++;
        
		}
		if($cuenta>0){
            if(isset($_SESSION)){
                session_destroy();
            }
			session_start();
            $_SESSION['dni']=$dni;
            $_SESSION['id_alumno']=$id_alumno;
			$_SESSION['nombres_alumno']=$nombres_alumno;
			$_SESSION['apellidos_alumno']=$apellidos_alumno;
			$_SESSION['id_apoderado']=$id_apoderado;
			$_SESSION['id_seccion']=$id_seccion;
            $_SESSION['alumno_matriculado']=$alumno_matriculado;
			//header('location: '.$pagactual);
            echo "<script> window.location.href='".$pagdireccion."';</script>";
		}else{
            echo "<script>alert('DNI o contraseña invalidos');</script>";
		}
	}else{
		echo "<h3>Error de procedimiento</h3>";
	}
	return $result; 	
}
function ingresaotro($usuario_otro,$contrasena_otro,$pagdireccion){
	$cmdsql="SELECT * FROM otro_usuario WHERE correo='$usuario_otro' AND contrasena_usuario='$contrasena_otro'";
	$result=mysqli_query(conectar(), $cmdsql);
	if($result){
		$cuenta=0;
		while ($row=mysqli_fetch_assoc($result)){
        $correo=$row['correo'];
        $id_otro_usuario=$row['id_otro_usuario'];
        $nombres_usuario=$row['nombres_usuario'];
        $apellidos_usuario=$row['apellidos_usuario'];
        date_default_timezone_set('America/Lima');
        $fecha_ultimo_ingreso=date("d/m/Y");
			$cuenta++;
		}
		if($cuenta>0){
            if(isset($_SESSION)){
                session_destroy();
            }
			session_start();
            
            $cmdsql="UPDATE `otro_usuario` SET `fecha_ultimo_ingreso`='".$fecha_ultimo_ingreso."' WHERE `id_otro_usuario`=".$id_otro_usuario;
           $result=mysqli_query(conectar(), $cmdsql);
            $_SESSION['correo']=$correo;
            $_SESSION['id_otro_usuario']=$id_otro_usuario;
			$_SESSION['nombres_usuario']=$nombres_usuario;
			$_SESSION['apellidos_usuario']=$apellidos_usuario;

            echo "<script> window.location.href='".$pagdireccion."';</script>";
		}else{
            echo "<script>alert('Correo o contraseña invalidos');</script>";
		}
	}else{
		echo "<h3>Error de procedimiento</h3>";
	}
	return $result; 	
}

function ingresaprofesor($usuario_prof,$contrasena_prof,$pagdireccion){
	$cmdsql="SELECT * FROM profesor WHERE correo='$usuario_prof' AND contrasena_profesor='$contrasena_prof'";
	$result=mysqli_query(conectar(), $cmdsql);
	if($result){
		$cuenta=0;
		while ($row=mysqli_fetch_assoc($result)){
            $correo=$row['correo'];
            $id_profesor=$row['id_profesor'];
            $nombres_profesor=$row['nombres_profesor'];
            $apellidos_profesor=$row['apellidos_profesor'];
			$cuenta++;
		}
		if($cuenta>0){
            if(isset($_SESSION)){
                session_destroy();
            }
			session_start();
            $_SESSION['correo']=$correo;
            $_SESSION['id_profesor']=$id_profesor;
			$_SESSION['nombres_profesor']=$nombres_profesor;
			$_SESSION['apellidos_profesor']=$apellidos_profesor;

            echo "<script> window.location.href='".$pagdireccion."';</script>";
		}else{
            echo "<script>alert('Correo o contraseña invalidos');</script>";
		}
	}else{
		echo "<h3>Error de procedimiento</h3>";
	}
	return $result; 	
}

function ingresaAdmin($correo,$clave,$pagdireccion){
	$cmdsql="SELECT * FROM administrador WHERE correo_administrador='$correo' AND contrasena='$clave'";
	$result=mysqli_query(conectar(), $cmdsql);
	if($result){
		$cuenta=0;
		while ($row=mysqli_fetch_assoc($result)){
            $id_administrador = $row['id_administrador'];
            $correo_administrador = $row['correo_administrador'];
            $poder_administrador = $row['poder_administrador'];
			$cuenta++;
		}
		if($cuenta>0){
            if(isset($_SESSION)){
                session_destroy();
            }
			session_start();
            $_SESSION['id_administrador']=$id_administrador;
            $_SESSION['correo_administrador']=$correo_administrador;
			$_SESSION['poder_administrador']=$poder_administrador;
			//header('location: '.$pagactual);
            echo "<script> window.location.href='".$pagdireccion."';</script>";
		}else{
            echo "<script>alert('correo o contraseña invalidos');</script>";
		}
	}else{
		echo "<h3>Error de procedimiento</h3>";
	}
	return $result; 	
}

function recuperarContraAl($dni){
	$cmdsql="SELECT alumno.*, apoderado.* FROM alumno JOIN apoderado ON alumno.id_apoderado=apoderado.id_apoderado WHERE alumno.dni=$dni";
	$result=mysqli_query(conectar(), $cmdsql);
	if($result){
		$cuenta=0;
		while ($row=mysqli_fetch_assoc($result)){
            $contrasena=$row['contrasena_alumno'];
            $correo=$row['correo_apoderado'];
            $nombres_alumno=$row['nombres_alumno'];
            $apellidos_alumno=$row['apellidos_alumno'];
            $nombres=$row['nombres_apoderado'];
            $apellidos=$row['apellidos_apoderado'];
			$cuenta++;
		}
		if($cuenta>0){
            $asunto="Recuperar contraseña";
            $mensaje='<div style="color:#888">
                        <div >Estimado(a) '.$nombres." ".$apellidos.',</div>
                        <br><div style="display:inline">La contraseña del alumno '.$nombres_alumno.' '.$apellidos_alumno.' es "'.$contrasena.'".</div><br><br>
                        <div>Atentamente,</div>
                        <div>Colegio Euclides</div>
                        <div>Ubicación: Calle Miraflores n° 128 - Huánuco</div>
                        <div>Teléfono: <a href="tel:962074825">962 074 825</a></div>
                        <div>Correo: <a href="mailto:cepeuclides@gmail.com" target="_top">cepeuclides@gmail.com</a> </div>
                    </div>';
            enviarcorreo($correo,$nombres,$apellidos,$asunto,$mensaje);
           echo "<script>alert('Se le envió un correo al apoderado con la contraseña.');</script>";
		}else{
            echo "<script>alert('DNI no registrado en la base de datos.');</script>";
		}
	}else{
		echo "<h3>Error de procedimiento</h3>";
	}
	return $result; 	
}

function recuperarContraCapa($correo){
	$cmdsql="SELECT * FROM otro_usuario WHERE correo='$correo'";
	$result=mysqli_query(conectar(), $cmdsql);
	if($result){
		$cuenta=0;
		while ($row=mysqli_fetch_assoc($result)){
            $contrasena=$row['contrasena_usuario'];
            $nombres=$row['nombres_usuario'];
            $apellidos=$row['apellidos_usuario'];
			$cuenta++;
		}
		if($cuenta>0){
            $asunto="Recuperar contraseña";
            $mensaje='<div style="color:#888">
                        <div >Estimado(a) '.$nombres." ".$apellidos.',</div>
                        <br><div style="display:inline">Su contraseña es "'.$contrasena.'".</div><br><br>
                        <div>Atentamente,</div>
                        <div>Colegio Euclides</div>
                        <div>Ubicación: Calle Miraflores n° 128 - Huánuco</div>
                        <div>Teléfono: <a href="tel:962074825">962 074 825</a></div>
                        <div>Correo: <a href="mailto:cepeuclides@gmail.com" target="_top">cepeuclides@gmail.com</a> </div>
                    </div>';
            enviarcorreo($correo,$nombres,$apellidos,$asunto,$mensaje);
           echo "<script>alert('Se le envió un correo con la contraseña.');</script>";
		}else{
            echo "<script>alert('Correo no registrado en la base de datos.');</script>";
		}
	}else{
		echo "<h3>Error de procedimiento</h3>";
	}
	return $result; 	
}

function recuperarContraProf($correo){
	$cmdsql="SELECT * FROM profesor WHERE correo='$correo'";
	$result=mysqli_query(conectar(), $cmdsql);
	if($result){
		$cuenta=0;
		while ($row=mysqli_fetch_assoc($result)){
            $contrasena=$row['contrasena_profesor'];
            $nombres=$row['nombres_profesor'];
            $apellidos=$row['apellidos_profesor'];
			$cuenta++;
		}
		if($cuenta>0){
            $asunto="Recuperar contraseña";
            $mensaje='<div style="color:#888">
                        <div >Estimado(a) '.$nombres." ".$apellidos.',</div>
                        <br><div style="display:inline">Su contraseña es "'.$contrasena.'".</div><br><br>
                        <div>Atentamente,</div>
                        <div>Colegio Euclides</div>
                        <div>Ubicación: Calle Miraflores n° 128 - Huánuco</div>
                        <div>Teléfono: <a href="tel:962074825">962 074 825</a></div>
                        <div>Correo: <a href="mailto:cepeuclides@gmail.com" target="_top">cepeuclides@gmail.com</a> </div>
                    </div>';
            enviarcorreo($correo,$nombres,$apellidos,$asunto,$mensaje);
           echo "<script>alert('Se le envió un correo con la contraseña.');</script>";
		}else{
            echo "<script>alert('Correo no registrado en la base de datos.');</script>";
		}
	}else{
		echo "<h3>Error de procedimiento</h3>";
	}
	return $result; 	
}

function enviarCorreoAlumno($seccion_correo,$pago_correo,$asunto_correo,$mensaje_correo,$pagdireccion){
    $cuentacmdsql=0;
    if($seccion_correo ==0){
        $cmdsql="SELECT alumno.id_alumno,apoderado.*,pago.estado_pago FROM alumno JOIN apoderado ON alumno.id_apoderado=apoderado.id_apoderado JOIN pago ON alumno.id_alumno=pago.id_alumno";
    }else{
        if($seccion_correo==15){
            $cmdsql="SELECT alumno.id_alumno,apoderado.*,pago.estado_pago FROM alumno JOIN apoderado ON alumno.id_apoderado=apoderado.id_apoderado JOIN pago ON alumno.id_alumno=pago.id_alumno WHERE alumno.id_seccion BETWEEN 1 AND 3";
        }elseif($seccion_correo==16){
            $cmdsql="SELECT alumno.id_alumno,apoderado.*,pago.estado_pago FROM alumno JOIN apoderado ON alumno.id_apoderado=apoderado.id_apoderado JOIN pago ON alumno.id_alumno=pago.id_alumno WHERE alumno.id_seccion BETWEEN 4 AND 9";
        }elseif($seccion_correo==17){
            $cmdsql="SELECT alumno.id_alumno,apoderado.*,pago.estado_pago FROM alumno JOIN apoderado ON alumno.id_apoderado=apoderado.id_apoderado JOIN pago ON alumno.id_alumno=pago.id_alumno WHERE alumno.id_seccion BETWEEN 10 AND 14";
        }else{
            $cmdsql="SELECT alumno.id_alumno,apoderado.*,pago.estado_pago FROM alumno JOIN apoderado ON alumno.id_apoderado=apoderado.id_apoderado JOIN pago ON alumno.id_alumno=pago.id_alumno WHERE alumno.id_seccion=$seccion_correo";
        }
        $cuentacmdsql++;
    }
    
    $pago_correo--;//porque el que tiene deuda en el select es 1 y el que no es 2
    
    if($pago_correo==0){
        
        
        if($cuentacmdsql ==0){
            $cmdsql.=" WHERE pago.estado_pago=$pago_correo";
        }else{
            $cmdsql.=" AND pago.estado_pago=$pago_correo";
        }
        
    }
	
    
	$result=mysqli_query(conectar(), $cmdsql);
    
    //echo mysqli_error($connect);
    $correos_info = array();
    $correos_condeuda=array();
	if($result){
		while ($row=mysqli_fetch_assoc($result)){
            $id_apoderado=$row['id_apoderado'];
            $correo_apoderado=$row['correo_apoderado'];
            $nombres_apoderado=$row['nombres_apoderado'];
            $apellidos_apoderado=$row['apellidos_apoderado'];
            $estado_pago=$row['estado_pago'];
            
            if($estado_pago==0){
                array_push($correos_condeuda,$id_apoderado);
            }
            
            if(!array_key_exists ( $id_apoderado , $correos_info)){
                $tmp = new stdClass;
                $tmp->correo = $correo_apoderado;
                $tmp->nombres = $nombres_apoderado;
                $tmp->apellidos = $apellidos_apoderado;
                $tmp->estado = $estado_pago;
                
                $correos_info[$id_apoderado]=$tmp;
                
            }
            
            if($pago_correo == 1 && in_array($id_apoderado,$correos_condeuda)){
                unset($correos_info[$id_apoderado]);
            }elseif($pago_correo == 0 && !in_array($id_apoderado,$correos_condeuda)){
                unset($correos_info[$id_apoderado]);
            }
		}

        $asunto=$asunto_correo;
        $mensaje=$mensaje_correo;
        
        enviarCorreosMasivos($correos_info,$asunto,$mensaje);
       echo "<script>alert('Se enviaron los correos.');</script>";

	}else{
		echo "<h3>Error de procedimiento</h3>";
	}
    
    
    
	return $result; 	
}

function enviarCorreoOtro($programa_correo,$pago_correo,$asunto_correo,$mensaje_correo,$pagdireccion){
    //echo "<script>console.log('$asunto_correo.');</script>";
    $cuentacmdsql=0;
    if($programa_correo ==0){
        $cmdsql="SELECT otro_usuario.*,programa_usuario.id_programa,programa_usuario.estado_pago_prog FROM otro_usuario JOIN programa_usuario ON otro_usuario.correo=programa_usuario.correo";
    }else{
        $cmdsql="SELECT otro_usuario.*,programa_usuario.id_programa,programa_usuario.estado_pago_prog FROM otro_usuario JOIN programa_usuario ON otro_usuario.correo=programa_usuario.correo WHERE programa_usuario.id_programa=$programa_correo";
        $cuentacmdsql++;
    }
    
    $pago_correo--;//porque el que tiene deuda en el select es 1 y el que no es 2
    
    if($pago_correo!= -1){
        if($cuentacmdsql ==0){
            $cmdsql.=" WHERE programa_usuario.estado_pago_prog=$pago_correo";
        }else{
            $cmdsql.=" AND programa_usuario.estado_pago_prog=$pago_correo";
        }
    }


    
	$result=mysqli_query(conectar(), $cmdsql);
    
    //echo mysqli_error($connect);
    $correos_info = array();

	if($result){
		while ($row=mysqli_fetch_assoc($result)){
            $id_otro_usuario=$row['id_otro_usuario'];
            $correo=$row['correo'];
            $nombres_usuario=$row['nombres_usuario'];
            $apellidos_usuario=$row['apellidos_usuario'];
            $estado_pago_prog=$row['estado_pago_prog'];
            if(!array_key_exists ( $id_otro_usuario , $correos_info)){
                $tmp = new stdClass;
                $tmp->correo = $correo;
                $tmp->nombres = $nombres_usuario;
                $tmp->apellidos = $apellidos_usuario;
                $tmp->estado = $estado_pago_prog;
                
                $correos_info[$id_otro_usuario]=$tmp;
                
                //echo "<script>console.log('$nombres_usuario.');</script>";
            }
		}

        $asunto=$asunto_correo;
        $mensaje=$mensaje_correo;
        

        enviarCorreosMasivos($correos_info,$asunto,$mensaje);

        
       echo "<script>alert('Se enviaron los correos.');</script>";

	}else{
		echo "<h3>Error de procedimiento</h3>";
	}
    
    
    
	return $result; 	
}

function listarseccion($id_seccion,$ano){
    if($id_seccion==0){
        $cmdsql="select * from seccion ORDER BY id_seccion";
    }else{
        $cmdsql="select * from seccion WHERE id_seccion=".$id_seccion;
    }
	
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}
function listarapoderado($id_apoderado){
	$cmdsql="select * from apoderado WHERE id_apoderado=".$id_apoderado;
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}
function listarcursos($id_seccion){
    if($id_seccion==0){
        $cmdsql="SELECT curso.*, area.*,profesor.* FROM curso JOIN area ON curso.id_area=area.id_area JOIN profesor ON curso.id_profesor=profesor.id_profesor WHERE id_curso > 14 ORDER BY curso.id_seccion";
    }else{
        $cmdsql="SELECT curso.*, area.*,profesor.* FROM curso JOIN area ON curso.id_area=area.id_area JOIN profesor ON curso.id_profesor=profesor.id_profesor WHERE id_seccion=".$id_seccion." AND id_curso > 14 ORDER BY area.nombre_area";
    }
	
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}
function listarcursosministerio(){
    $cmdsql="SELECT cursosministerio.*, areaministerio.* FROM cursosministerio JOIN areaministerio ON cursosministerio.id_area_ministerio=areaministerio.id_area_ministerio ORDER BY cursosministerio.id_nivel ASC, cursosministerio.id_curso_ministerio ASC";

	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}
function listarcursosadmin($id_seccion){
    if($id_seccion==0){
        $cmdsql="SELECT curso.*, area.*,profesor.* FROM curso JOIN area ON curso.id_area=area.id_area JOIN profesor ON curso.id_profesor=profesor.id_profesor ORDER BY curso.id_seccion";
    }else{
        $cmdsql="SELECT curso.*, area.*,profesor.* FROM curso JOIN area ON curso.id_area=area.id_area JOIN profesor ON curso.id_profesor=profesor.id_profesor WHERE id_seccion=".$id_seccion." ORDER BY area.nombre_area";
    }
	
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}
function listarcursosprofesor($id_profesor){
	$cmdsql="SELECT curso.*, seccion.* FROM curso JOIN seccion ON curso.id_seccion=seccion.id_seccion WHERE curso.id_profesor=".$id_profesor;
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}
function listarhorarios($id_curso){
	$cmdsql="select * from horario WHERE id_curso=".$id_curso;
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}
function horario_seccion($id_seccion){
	$cmdsql="select curso.*,horario.*,seccion.nivel,seccion.grado FROM curso JOIN horario ON curso.id_curso = horario.id_curso JOIN seccion ON curso.id_seccion=seccion.id_seccion  WHERE curso.id_seccion=".$id_seccion;
	$resultado=mysqli_query(conectar(), $cmdsql);
    
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}
function listarpagos($id_alumno){
	$cmdsql="select * from pago WHERE id_alumno=".$id_alumno." ORDER BY id_pago DESC";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarPagoId($id_pago_alumno){
	$cmdsql="select * from pago WHERE id_pago=$id_pago_alumno";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}
/*
function listarCodigosveri($codigo){
	$cmdsql="select codigo_programa from programa WHERE codigo_programa='$codigo'";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}*/

function listarTextoAnuales($tipo_anual){
	$cmdsql="select * from texto_anuales WHERE tipo_anual=$tipo_anual";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarTextoNosotros(){
	$cmdsql="select * from texto_nosotros";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarImagenesGaleria($id_album){
    if($id_album ==0){
        $cmdsql="select albumes.*, imagenes_galeria.* FROM albumes JOIN imagenes_galeria ON albumes.id_album=imagenes_galeria.id_album ORDER BY albumes.id_album DESC";
    }else{
        $cmdsql="select albumes.*, imagenes_galeria.* FROM albumes JOIN imagenes_galeria ON albumes.id_album=imagenes_galeria.id_album WHERE albumes.id_album=$id_album ORDER BY imagenes_galeria.id_imagenes_galeria DESC";
    }
	
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarPublicaciones(){
    $cmdsql="select * from publicaciones ORDER BY id_publicacion DESC";

	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarprogramas($correo){
    if($correo == "todos"){
       $cmdsql="select curso_programa.*,programa.* FROM programa LEFT JOIN curso_programa ON curso_programa.id_programa = programa.id_programa";    
    }else{
       $cmdsql="select programa.*,programa_usuario.*,curso_programa.*,archivo_curso_programa.* FROM programa LEFT JOIN programa_usuario ON programa.id_programa = programa_usuario.id_programa LEFT JOIN curso_programa ON programa_usuario.id_programa = curso_programa.id_programa LEFT JOIN archivo_curso_programa ON curso_programa.id_curso_programa = archivo_curso_programa.id_curso_programa WHERE programa_usuario.correo='$correo' AND programa_usuario.estado_pago_prog=1 ORDER BY programa.id_programa";     
    }

    
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarsoloprogramas(){
    
   $cmdsql="select id_programa,nombre_programa FROM programa ORDER BY id_programa DESC";    

	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarprogramasporid($id_programa){
    $cmdsql="select programa.*,curso_programa.*,archivo_curso_programa.* FROM programa LEFT JOIN curso_programa ON programa.id_programa = curso_programa.id_programa LEFT JOIN archivo_curso_programa ON curso_programa.id_curso_programa = archivo_curso_programa.id_curso_programa WHERE programa.id_programa=$id_programa ORDER BY curso_programa.id_curso_programa ASC";

	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}
function listarcursosdeprograma($nombre_curso_programa){
    $cmdsql="select id_curso_programa from curso_programa where nombre_curso_programa = '$nombre_curso_programa'";

	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarCostoAnuales($nivel){
	$cmdsql="select * from seccion WHERE nivel='$nivel'";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarImagenOtros($id_fotos_otros){

    $cmdsql="SELECT * FROM fotos_otros WHERE id_fotos_otros=$id_fotos_otros";

	
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}
function listarestadotramites($num_pag,$offset){
    $lim_menor=($num_pag-1)*$offset;
    $lim_mayor=$offset;
    $cmdsql="SELECT estado_tramite.*,alumno.id_alumno,alumno.nombres_alumno, alumno.apellidos_alumno FROM estado_tramite JOIN alumno ON estado_tramite.id_alumno= alumno.id_alumno ORDER BY estado_tramite.id_estado_tramite DESC limit $lim_menor,$lim_mayor";

	
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarprofesores(){
   // $cmdsql="SELECT profesor.*,curso.* FROM profesor JOIN curso ON profesor.id_profesor= curso.id_profesor";
    $cmdsql="SELECT curso.*,profesor.* FROM profesor LEFT JOIN curso ON profesor.id_profesor= curso.id_profesor";
	
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarprofesoressimple(){
   // $cmdsql="SELECT profesor.*,curso.* FROM profesor JOIN curso ON profesor.id_profesor= curso.id_profesor";
    $cmdsql="SELECT * from profesor";
	
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}
function listarareas(){
	$cmdsql="select * from area WHERE id_area>1";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarCursosPorArea($id_area){
	$cmdsql="select * from curso where id_area = $id_area";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}
    
function grabaralumno($nombre_alnuevo,$apellido_alnuevo,$dni_alnuevo,$contrasena_alnuevo,$nivel_alumno,$grado,$id_apoderado,$pagdireccion){
    date_default_timezone_set('America/Lima');
    $ano=date("Y");
    $fecha=date("d/m/Y");
    //El grado es el id de la seccion, por eso las secciones no se deben actualizar
    $cmdsql="insert into `alumno`(`nombres_alumno`, `apellidos_alumno`, `id_apoderado`, `id_seccion`, `dni`, `contrasena_alumno`, `alumno_matriculado`, `porcentaje_pago`) values ('$nombre_alnuevo','$apellido_alnuevo',$id_apoderado,$grado,$dni_alnuevo,'$contrasena_alnuevo',0,100)";
    $resultado=mysqli_query(conectar(), $cmdsql);
    if(!$resultado){
        $mensaje="Repetido";
    }else{
        desconectar();
        
        $cmdsql="SELECT alumno.id_alumno, seccion.costo_seccion, seccion.costo_matricula FROM alumno JOIN seccion ON alumno.id_seccion=seccion.id_seccion WHERE alumno.dni=$dni_alnuevo AND seccion.id_seccion=$grado";
        $result2=mysqli_query(conectar(), $cmdsql);
            while ($row=mysqli_fetch_assoc($result2)){
            $id_alumno=$row['id_alumno'];
            $costo_seccion=$row['costo_seccion'];
            $costo_matricula=$row['costo_matricula'];
        }
        
        desconectar();
        $cmdsql="insert into `pago`(`id_alumno`, `ano_pago`,`mes_pago`, `tipo_ciclo`,`monto`, `estado_pago`,`fecha`, `metodo_pago`) values ($id_alumno, '$ano', 'Marzo', 'Anual', $costo_seccion, 0, '-', 1) , ($id_alumno, '$ano', 'Matrícula', 'Anual', $costo_matricula, 0, '-', 1)";
        $result=mysqli_query(conectar(), $cmdsql);
        if(isset($_SESSION)){
            session_destroy();
        }

        session_start();
        $_SESSION['dni']=$dni_alnuevo;
        $_SESSION['id_alumno']=$id_alumno;
        $_SESSION['nombres_alumno']=$nombre_alnuevo;
        $_SESSION['apellidos_alumno']=$apellido_alnuevo;
        $_SESSION['id_apoderado']=$id_apoderado;
        $_SESSION['id_seccion']=$grado;
        echo "<script> window.location.href='".$pagdireccion."';</script>";

        $mensaje=$pagdireccion;
    }

    return $mensaje;

}
function grabarapoderado($nombre_apoderado,$apellido_apoderado,$correo_apoderado,$direccion_apoderado,$telefono_apoderado,$pagdireccion){

    //El grado es el id de la seccion, por eso las secciones no se deben actualizar
    $cmdsql="insert into `apoderado`(`nombres_apoderado`, `apellidos_apoderado`, `correo_apoderado`, `celular_apoderado`, `direccion_apoderado`) values ('$nombre_apoderado','$apellido_apoderado','$correo_apoderado',$telefono_apoderado,'$direccion_apoderado')";
    $resultado=mysqli_query(conectar(), $cmdsql);
    if(!$resultado){
        $mensaje="Repetido";
    }else{
        desconectar();
        
        $cmdsql="SELECT id_apoderado from apoderado where correo_apoderado='$correo_apoderado'";
        $result2=mysqli_query(conectar(), $cmdsql);
            while ($row=mysqli_fetch_assoc($result2)){
            $id_apoderado=$row['id_apoderado'];
        }
        if(isset($_SESSION)){
            session_destroy();
        }
        session_start();
        $_SESSION['id_apoderado']=$id_apoderado;

        echo "<script> window.location.href='".$pagdireccion."';</script>";

        $mensaje=$pagdireccion;
    }

    return $mensaje;

}

function grabaralumnoApoNueAdmin($nombre_apoderado,$apellido_apoderado,$correo_apoderado,$direccion_apoderado,$telefono_apoderado, $nombre_alnuevo, $apellido_alnuevo, $dni_alnuevo, $contrasena_alnuevo, $id_seccion_alnuevo, $porcentaje_pago_alnuevo, $estado_matricula_alnuevo, $pagdireccion){
    $mensaje="";
    //El grado es el id de la seccion, por eso las secciones no se deben actualizar
    $cmdsql="insert into `apoderado`(`nombres_apoderado`, `apellidos_apoderado`, `correo_apoderado`, `celular_apoderado`, `direccion_apoderado`) values ('$nombre_apoderado','$apellido_apoderado','$correo_apoderado',$telefono_apoderado,'$direccion_apoderado')";
    $resultado=mysqli_query(conectar(), $cmdsql);
    if(!$resultado){
        $mensaje="Repetido1";
    }else{
        desconectar();
        
        $cmdsql="SELECT id_apoderado from apoderado where correo_apoderado='$correo_apoderado'";
        $result2=mysqli_query(conectar(), $cmdsql);
            while ($row=mysqli_fetch_assoc($result2)){
            $id_apoderado=$row['id_apoderado'];
        }
        
        date_default_timezone_set('America/Lima');
        $ano=date("Y");
        $fecha=date("d/m/Y");
        //El grado es el id de la seccion, por eso las secciones no se deben actualizar
        $cmdsql="insert into `alumno`(`nombres_alumno`, `apellidos_alumno`, `id_apoderado`, `id_seccion`, `dni`, `contrasena_alumno`, `alumno_matriculado`, `porcentaje_pago`) values ('$nombre_alnuevo','$apellido_alnuevo',$id_apoderado,$id_seccion_alnuevo,$dni_alnuevo,'$contrasena_alnuevo',$estado_matricula_alnuevo,$porcentaje_pago_alnuevo)";
        $resultado=mysqli_query(conectar(), $cmdsql);
        if(!$resultado){
            $mensaje="Repetido2";
        }else{
            desconectar();

            $cmdsql="SELECT alumno.id_alumno, alumno.porcentaje_pago, seccion.costo_seccion, seccion.costo_matricula FROM alumno JOIN seccion ON alumno.id_seccion=seccion.id_seccion WHERE alumno.dni=$dni_alnuevo AND seccion.id_seccion=$id_seccion_alnuevo";
            $result2=mysqli_query(conectar(), $cmdsql);
                while ($row=mysqli_fetch_assoc($result2)){
                $id_alumno=$row['id_alumno'];
                $costo_seccion=($row['costo_seccion'])*($porcentaje_pago_alnuevo/100);
                $costo_matricula=($row['costo_matricula'])*($porcentaje_pago_alnuevo/100);
            }

            desconectar();
            $cmdsql="insert into `pago`(`id_alumno`, `ano_pago`,`mes_pago`, `tipo_ciclo`,`monto`, `estado_pago`,`fecha`, `metodo_pago`) values ($id_alumno, '$ano', 'Marzo', 'Anual', $costo_seccion, 0, '-', 1) , ($id_alumno, '$ano', 'Matrícula', 'Anual', $costo_matricula, 0, '-', 1)";
            $result=mysqli_query(conectar(), $cmdsql);
            
            echo "<script> window.location.href='".$pagdireccion."';</script>";

            $mensaje=$pagdireccion;
        }

    }

    return $mensaje;

}


function grabaralumnoApoAntAdmin($dni_al_ant, $nombre_alnuevo1, $apellido_alnuevo1, $dni_alnuevo1, $contrasena_alnuevo1, $id_seccion_alnuevo1, $porcentaje_pago_alnuevo1, $estado_matricula_alnuevo1, $pagdireccion){
    $mensaje="";

    $cmdsql="SELECT id_apoderado from alumno where dni=$dni_al_ant";
    $result2=mysqli_query(conectar(), $cmdsql);
        while ($row=mysqli_fetch_assoc($result2)){
        $id_apoderado=$row['id_apoderado'];
    }

    date_default_timezone_set('America/Lima');
    $ano=date("Y");
    $fecha=date("d/m/Y");
    //El grado es el id de la seccion, por eso las secciones no se deben actualizar
    $cmdsql="insert into `alumno`(`nombres_alumno`, `apellidos_alumno`, `id_apoderado`, `id_seccion`, `dni`, `contrasena_alumno`, `alumno_matriculado`, `porcentaje_pago`) values ('$nombre_alnuevo1','$apellido_alnuevo1',$id_apoderado,$id_seccion_alnuevo1,$dni_alnuevo1,'$contrasena_alnuevo1',$estado_matricula_alnuevo1,$porcentaje_pago_alnuevo1)";
    $resultado=mysqli_query(conectar(), $cmdsql);
    if(!$resultado){
        $mensaje="Repetido2";
    }else{
        desconectar();

        $cmdsql="SELECT alumno.id_alumno, alumno.porcentaje_pago, seccion.costo_seccion, seccion.costo_matricula FROM alumno JOIN seccion ON alumno.id_seccion=seccion.id_seccion WHERE alumno.dni=$dni_alnuevo1 AND seccion.id_seccion=$id_seccion_alnuevo1";
        $result2=mysqli_query(conectar(), $cmdsql);
            while ($row=mysqli_fetch_assoc($result2)){
            $id_alumno=$row['id_alumno'];
            $costo_seccion=($row['costo_seccion'])*($porcentaje_pago_alnuevo1 /100);
            $costo_matricula=($row['costo_matricula'])*($porcentaje_pago_alnuevo1 /100);
        }

        desconectar();
        $cmdsql="insert into `pago`(`id_alumno`, `ano_pago`,`mes_pago`, `tipo_ciclo`,`monto`, `estado_pago`,`fecha`, `metodo_pago`) values ($id_alumno, '$ano', 'Marzo', 'Anual', $costo_seccion, 0, '-', 1) , ($id_alumno, '$ano', 'Matrícula', 'Anual', $costo_matricula, 0, '-', 1)";
        $result=mysqli_query(conectar(), $cmdsql);

        echo "<script> window.location.href='".$pagdireccion."';</script>";

        $mensaje=$pagdireccion;
    }


    return $mensaje;

}

function grabarUserNueAdmin($nombres_usuario,$apellidos_usuario,$correo,$celular,$contrasena,$id_programa,$modo_registro_otro,$estado_registro_otro,$pagdireccion){
    $mensaje="";

    date_default_timezone_set('America/Lima');
    $fecha_inscripcion=date("d/m/Y");
    $fecha_ultimo_ingreso=date("d/m/Y");
    $cmdsql="insert into `otro_usuario`(`nombres_usuario`, `apellidos_usuario`, `correo`, `contrasena_usuario`, `fecha_inscripcion`, `fecha_ultimo_ingreso`, `celular_otro_usuario`) values ('$nombres_usuario','$apellidos_usuario','$correo','$contrasena','$fecha_inscripcion','$fecha_ultimo_ingreso',$celular)";
    $resultado=mysqli_query(conectar(), $cmdsql);
    if(!$resultado){
        $mensaje="Repetido";
    }else{
        desconectar();
        $cmdsql="insert into `programa_usuario`(`id_programa`, `correo`,`modo_pago_prog`, `estado_pago_prog`) values ($id_programa,'$correo',$modo_registro_otro,$estado_registro_otro)";
        $result=mysqli_query(conectar(), $cmdsql);
        desconectar();
        echo "<script> alert('Se agregó usuario');</script>";
        echo "<script> window.location.href='".$pagdireccion."';</script>";

        $mensaje=$pagdireccion;
    }

    return $mensaje;

}
/*function grabarusuario($nombres_usuario,$apellidos_usuario,$correo,$codigo,$contrasena,$pagdireccion){
    $mensaje="";
    $temp=0;
    $cmdsql="select * from programa WHERE codigo_programa='$codigo'";
	$resultado=mysqli_query(conectar(), $cmdsql);
    while ($row=mysqli_fetch_assoc($resultado)){
           $temp++;
            $id_programa=$row['id_programa'];
    }
    mysqli_free_result($resultado);
    desconectar();
    if($temp != 0){
        date_default_timezone_set('America/Lima');
        $fecha_inscripcion=date("d/m/Y");
        $fecha_ultimo_ingreso=date("d/m/Y");
        $cmdsql="insert into `otro_usuario`(`nombres_usuario`, `apellidos_usuario`, `correo`, `contrasena_usuario`, `fecha_inscripcion`, `fecha_ultimo_ingreso`) values ('$nombres_usuario','$apellidos_usuario','$correo','$contrasena','$fecha_inscripcion','$fecha_ultimo_ingreso')";
        $resultado=mysqli_query(conectar(), $cmdsql);
        if(!$resultado){
            $mensaje=$resultado;
        }else{
            desconectar();
            $cmdsql="insert into `programa_usuario`(`id_programa`, `correo`) values ($id_programa,'$correo')";
            $result=mysqli_query(conectar(), $cmdsql);
            if(isset($_SESSION)){
                session_destroy();
            }
            
            session_start();
            $_SESSION['nombres_usuario']=$nombres_usuario;
            $_SESSION['apellidos_usuario']=$apellidos_usuario;
            $_SESSION['correo']=$correo;
            //echo "<script> window.location.href='".$pagdireccion."';</script>";
            
            $mensaje=$pagdireccion;
        }
        
    }else{
        $mensaje= "InvalidCode";
    }
    return $mensaje;

}*/

function grabarusuario($nombres_usuario,$apellidos_usuario,$correo,$celular,$contrasena,$pagdireccion,$id_programa){
    $mensaje="";

    date_default_timezone_set('America/Lima');
    $fecha_inscripcion=date("d/m/Y");
    $fecha_ultimo_ingreso=date("d/m/Y");
    $cmdsql="insert into `otro_usuario`(`nombres_usuario`, `apellidos_usuario`, `correo`, `contrasena_usuario`, `fecha_inscripcion`, `fecha_ultimo_ingreso`, `celular_otro_usuario`) values ('$nombres_usuario','$apellidos_usuario','$correo','$contrasena','$fecha_inscripcion','$fecha_ultimo_ingreso',$celular)";
    $resultado=mysqli_query(conectar(), $cmdsql);
    if(!$resultado){
        $mensaje="Repetido";
    }else{
        desconectar();
        $cmdsql="insert into `programa_usuario`(`id_programa`, `correo`,`modo_pago_prog`, `estado_pago_prog`) values ($id_programa,'$correo',1,0)";
        $result=mysqli_query(conectar(), $cmdsql);
        if(isset($_SESSION)){
            session_destroy();
        }
        desconectar();
        
        $cmdsql="SELECT id_otro_usuario, correo FROM otro_usuario WHERE correo='$correo'";
        $result2=mysqli_query(conectar(), $cmdsql);
            while ($row=mysqli_fetch_assoc($result2)){
            $id_otro_usuario=$row['id_otro_usuario'];
            }
        session_start();
        $_SESSION['nombres_usuario']=$nombres_usuario;
        $_SESSION['apellidos_usuario']=$apellidos_usuario;
        $_SESSION['correo']=$correo;
        $_SESSION['id_programa']=$id_programa;
         $_SESSION['id_otro_usuario']=$id_otro_usuario;
        echo "<script> window.location.href='".$pagdireccion."';</script>";

        $mensaje=$pagdireccion;
    }

    return $mensaje;

}

function grabarusuarioant($correo,$contrasena,$pagdireccion,$id_programa){
	$cmdsql="SELECT * FROM otro_usuario WHERE correo='$correo' AND contrasena_usuario='$contrasena'";
	$result=mysqli_query(conectar(), $cmdsql);
	if($result){
		$cuenta=0;
		while ($row=mysqli_fetch_assoc($result)){
        $correo=$row['correo'];
        $id_otro_usuario=$row['id_otro_usuario'];
        $nombres_usuario=$row['nombres_usuario'];
        $apellidos_usuario=$row['apellidos_usuario'];
			$cuenta++;
		}
		if($cuenta>0){
            desconectar();
            if(isset($_SESSION)){
                session_destroy();
            }
			session_start();
            $cmdsql="insert into `programa_usuario`(`id_programa`, `correo`,`modo_pago_prog`, `estado_pago_prog`) values ($id_programa,'$correo',1,0)";
            $result=mysqli_query(conectar(), $cmdsql);
            $_SESSION['correo']=$correo;
            $_SESSION['id_otro_usuario']=$id_otro_usuario;
			$_SESSION['nombres_usuario']=$nombres_usuario;
			$_SESSION['apellidos_usuario']=$apellidos_usuario;
            $_SESSION['id_programa']=$id_programa;
            echo "<script> window.location.href='".$pagdireccion."';</script>";
		}else{
            echo "<script>alert('Correo o contraseña invalidos');</script>";
		}
	}else{
		echo "<h3>Error de procedimiento</h3>";
	}
	return $result; 	
}

function anadirAlbum($nombre_album){
    date_default_timezone_set('America/Lima');
    $fecha_creacion_album=date("d/m/Y");
    $cmdsql="insert into `albumes`(`nombre_album`, `fecha_creacion_album`) values ('$nombre_album','$fecha_creacion_album')";
    $resultado=mysqli_query(conectar(), $cmdsql);
    if(!$resultado){
        echo "<br>Error de procedimiento";
    }else{
        $cmdsql="SELECT * FROM albumes WHERE nombre_album='$nombre_album' AND fecha_creacion_album='$fecha_creacion_album' LIMIT 1";
        $result=mysqli_query(conectar(), $cmdsql);
        	if($result){
                $cuenta=0;
                while ($row=mysqli_fetch_assoc($result)){
                    $id_album=$row['id_album'];
                    $nombre_album=$row['nombre_album'];
                    $cuenta++;
                }
                if($cuenta>0){
                    $pagdireccion="modificargaleria.php?xid_album=$id_album&xnombre_album=$nombre_album";
                    echo "<script> window.location.href='".$pagdireccion."';</script>";
                }else{
                    echo "<script>alert('Correo o contraseña invalidos');</script>";
                }
            }else{
                echo "<h3>Error de procedimiento</h3>";
            }
    }

    return $resultado;

}

function anadirEstadoNotasAsist($id_curso){
    date_default_timezone_set('America/Lima');
    $ano=date("Y");
    $cmdsql="insert into `estado_notas_cursos`(`id_curso`, `ev1_1`, `ev1_2`, `ev1_3`, `ev1_4`, `ev2_1`, `ev2_2`, `ev2_3`, `ev2_4`, `ev3_1`, `ev3_2`, `ev3_3`, `ev3_4`, `ev4_1`, `ev4_2`, `ev4_3`, `ev4_4`,`ano_estado_notas`) values ($id_curso,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,$ano)";
    $resultado=mysqli_query(conectar(), $cmdsql);
    if(!$resultado){
        echo "<br>Error de procedimiento";
    }else{
        $cmdsql1="insert into `estado_asist_cursos`(`id_curso`, `asist_1`, `asist_2`, `asist_3`, `asist_4`, `asist_5`, `asist_6`, `asist_7`, `asist_8`, `asist_9`, `asist_10`,`ano_estado_asist`) values ($id_curso,0,0,0,0,0,0,0,0,0,0,$ano)";
        $resultado1=mysqli_query(conectar(), $cmdsql1);
        if(!$resultado1){
            echo "<br>Error de procedimiento";
        }
    }

    return $resultado;

}

function anadirUserPrograma($correo_usuario_an,$id_programa_an,$modo_pago_progan,$estado_pago_progan,$pagdireccion){
    $cmdsql="insert into `programa_usuario`(`id_programa`, `correo`,`modo_pago_prog`, `estado_pago_prog`) values ($id_programa_an,'$correo_usuario_an',$modo_pago_progan,$estado_pago_progan)";
    $result=mysqli_query(conectar(), $cmdsql);
    if(!$result){
        echo "<br>Error de procedimiento";
    }else{
        echo "<script> alert('Usuario añadido a programa');</script>";
        echo "<script> window.location.href='".$pagdireccion."';</script>";
    }

    return $result;

}
function anadirCursoExtra($id_programa,$nombre_curso_extra, $pagdireccion){
    date_default_timezone_set('America/Lima');
    $fecha_creacion_cursoextra=date("d/m/Y");
    $cmdsql="insert into `curso_programa`(`id_programa`, `nombre_curso_programa`,`fecha_curso_programa`) values ($id_programa,'$nombre_curso_extra','$fecha_creacion_cursoextra')";
    $result=mysqli_query(conectar(), $cmdsql);
    if(!$result){
        echo "<br>Error de procedimiento";
    }else{
        echo "<script> alert('Curso añadido a programa');</script>";
        echo "<script> window.location.href='".$pagdireccion."';</script>";
    }

    return $result;

}

function anadirPagoAlumno($mes_pago_agregar,$pagdireccion){

    date_default_timezone_set('America/Lima');
    $ano_pago=date("Y");
    //SELECCIONO LOS ALUMNOS QUE YA TIENEN ALGO EN PAGOS
    $cmdsql="SELECT alumno.*, pago.* FROM alumno LEFT JOIN pago ON alumno.id_alumno=pago.id_alumno WHERE (pago.mes_pago='$mes_pago_agregar' AND ano_pago=$ano_pago AND pago.id_alumno IS NOT NULL)";
    //$cmdsql="insert into `curso_programa`(`id_programa`, `nombre_curso_programa`,`fecha_curso_programa`) values ($id_programa,'$nombre_curso_extra','$fecha_creacion_cursoextra')";
    $result=mysqli_query(conectar(), $cmdsql);
    
    if(!$result){
        echo "<script> alert('Error al listar los alumnos que ya tienen pago pendiente');</script>";
    }else{
        
        $alumnos_con_pago=array();

        $temporal_alumnos=0;
        while ($row=mysqli_fetch_assoc($result)) {
            $id_alumno=$row['id_alumno'];
            array_push($alumnos_con_pago,$id_alumno);
            $temporal_alumnos++;
        }
        
        //SELECCIONO LOS ALUMNOS QUE NO TIENEN EL PAGO
        if($temporal_alumnos == 0){
            $cmdsql1="SELECT alumno.id_alumno,alumno.porcentaje_pago, seccion.costo_seccion FROM alumno JOIN seccion ON alumno.id_seccion=seccion.id_seccion";
        }else{
            if($mes_pago_agregar == "Matrícula"){
                $cmdsql1="SELECT alumno.id_alumno,alumno.porcentaje_pago, seccion.costo_matricula FROM alumno JOIN seccion ON alumno.id_seccion=seccion.id_seccion WHERE alumno.id_alumno not in (";
            }else{
                $cmdsql1="SELECT alumno.id_alumno,alumno.porcentaje_pago, seccion.costo_seccion FROM alumno JOIN seccion ON alumno.id_seccion=seccion.id_seccion WHERE alumno.id_alumno not in (";
            }
            
            $cuenta=0;
            foreach($alumnos_con_pago as $alun){
                //echo "++++".$alun;
                if($cuenta==0){
                    $cmdsql1.= $alun;
                }else{
                    $cmdsql1.= ", ".$alun;
                }
                $cuenta++;
            }
            $cmdsql1.=")";
        }



        //$cmdsql="insert into `curso_programa`(`id_programa`, `nombre_curso_programa`,`fecha_curso_programa`) values ($id_programa,'$nombre_curso_extra','$fecha_creacion_cursoextra')";
        $result1=mysqli_query(conectar(), $cmdsql1);

        if(!$result1){
            echo "<script> alert('Error al listar los alumnos a los que se les generará pago');</script>";
        }else{
            $cmdsql2="INSERT INTO pago (id_alumno,ano_pago,mes_pago,tipo_ciclo,monto,estado_pago,fecha,metodo_pago) VALUES ";
            $cuenta2=0;
            while ($row=mysqli_fetch_assoc($result1)) {
                $id_alumno=$row['id_alumno'];
                $porcentaje_pago=$row['porcentaje_pago'];

                if($mes_pago_agregar == "Matrícula"){
                    $monto_pago=$row['costo_matricula'];
                }else{
                    $monto_pago=$row['costo_seccion'];
                }

                $monto_pagar=$monto_pago*($porcentaje_pago/100);

                if($cuenta2==0){
                    $cmdsql2.= "($id_alumno,$ano_pago,'$mes_pago_agregar','Anual',$monto_pagar,0,'-',1)";
                }else{
                    $cmdsql2.= ", ($id_alumno,$ano_pago,'$mes_pago_agregar','Anual',$monto_pagar,0,'-',1)";
                }
                $cuenta2++;
            }
            
            if($cuenta2==0){
                echo "<script> alert('Pago añadido');</script>";
                echo "<script> window.location.href='".$pagdireccion."';</script>";
            }else{
                $result2=mysqli_query(conectar(), $cmdsql2);
            }
            
            if(!$result2){
                echo "<script> alert('Error al generar los pagos');</script>";
            }else{
                echo "<script> alert('Pago añadido');</script>";
                echo "<script> window.location.href='".$pagdireccion."';</script>";
            }


        }
    }
    
    return $result;

}

function actuaPagoMetUsuario($metodo_pago, $destinatario, $nombres, $apellidos, $nombre_programa,$id_programa,$costo_programa){
//error_reporting(E_ALL);
//ini_set('display_errors','on');
        $estado_pago=0;
    
        //metodo de pago 2 es con tarjeta
        if($metodo_pago==2){
            $estado_pago=1;
        }
    
        $cmdsql="update programa_usuario set modo_pago_prog = $metodo_pago, estado_pago_prog=$estado_pago where correo ='$destinatario' and id_programa=$id_programa";
        $result=mysqli_query(conectar(), $cmdsql);
    
        $asunto="Autoconfirmación-Inscripción";
        $mensaje='<div style="color:#888">
                    <div >Estimado(a) '.$nombres." ".$apellidos.',</div>
                    <br>';
                    if($metodo_pago==1){
                        $mensaje.='<div style="display:inline">Se ha registrado su intención de matrícula al programa "'.$nombre_programa.'".<br> Para completar la matrícula </div><div style="display:inline">diríjase a la secretaría del Colegio Matemático Euclides, ubicado en la calle Miraflores n° 128, y realice el pago de S/.'.$costo_programa.' correspondientes al programa.</div>';
                    }elseif($metodo_pago==2){
                        $mensaje.='<div style="display:inline">Se ha registrado correctamente su matrícula al programa"'.$nombre_programa.'".</div>';
                    }else{
                        $mensaje.='<div style="display:inline">Se ha registrado su intención de matrícula al programa "'.$nombre_programa.'".<br> Para completar la matrícula </div><div style="display:inline">diríjase a algún banco o agente BCP y realice el depósito de S/.'.$costo_programa.', correspondientes al programa, a la cuenta N°123 131231231312313123, luego responda a este correo con una foto del voucher.</div>';
                    }
        $mensaje.='<br><br>
                    <div>Atentamente,</div>
                    <div>Colegio Euclides</div>
                    <div>Ubicación: Calle Miraflores n° 128 - Huánuco</div>
                    <div>Teléfono: <a href="tel:962074825">962 074 825</a></div>
                    <div>Correo: <a href="mailto:cepeuclides@gmail.com" target="_top">cepeuclides@gmail.com</a> </div>
                </div>';
        enviarcorreo($destinatario,$nombres,$apellidos,$asunto,$mensaje);
        $_SESSION['modo_pago_prog']=$metodo_pago;
        if (!$result) {
            echo "<br>Error de procedimiento";
        }
	       return $result;
        //echo "<script> window.location.href='".$pagdireccion."';</script>";

        //$mensaje=$pagdireccion;
}

function actuaPagoMetAlumno($metodo_pago,$correo_apoderado,$nombres_apoderado,$apellidos_apoderado,$nombres_alumno,$apellidos_alumno,$nivel,$grado,$nombre_programa,$id_alumno,$costo_programa){
//error_reporting(E_ALL);
//ini_set('display_errors','on');
        //metodo de pago 2 es con tarjeta
        if($metodo_pago==2){
            date_default_timezone_set('America/Lima');
            $fecha=date("d/m/Y");
            $cmdsql="update pago JOIN alumno ON (pago.id_alumno=alumno.id_alumno) set pago.metodo_pago = $metodo_pago, pago.estado_pago=1, pago.fecha='$fecha',alumno.alumno_matriculado=1 where pago.id_alumno ='$id_alumno' and pago.estado_pago=0";
            $result=mysqli_query(conectar(), $cmdsql);
        }else{
            $cmdsql="update pago set metodo_pago = $metodo_pago where id_alumno ='$id_alumno' and estado_pago=0";
            $result=mysqli_query(conectar(), $cmdsql);
        }
    

    
        $asunto="Autoconfirmación-Inscripción";
        $mensaje='<div style="color:#888">
                    <div >Estimado(a) '.$nombres_apoderado." ".$apellidos_apoderado.',</div>
                    <br>';
                    if($metodo_pago==1){
                        $mensaje.='<div style="display:inline">Se ha registrado la intención de matrícula del alumno '.$nombres_alumno." ".$apellidos_alumno.' al ciclo anual del Colegio Matemático Euclides.<br> Para completar la matrícula </div><div style="display:inline">diríjase a la secretaría del Colegio Matemático Euclides, ubicado en la calle Miraflores n° 128, y realice el pago de S/.'.$costo_programa.' correspondientes.</div>';
                    }elseif($metodo_pago==2){
                        $mensaje.='<div style="display:inline">Se ha registrado correctamente la matrícula del alumno '.$nombres_alumno." ".$apellidos_alumno.' al ciclo anual del Colegio Matemático Euclides.</div>';
                    }else{
                        $mensaje.='<div style="display:inline">Se ha registrado la intención de matrícula del alumno '.$nombres_alumno." ".$apellidos_alumno.' al ciclo anual del Colegio Matemático Euclides.<br> Para completar la matrícula </div><div style="display:inline">diríjase a algún banco o agente BCP y realice el depósito de S/.'.$costo_programa.' a la cuenta N°123 131231231312313123, luego responda a este correo con una foto del voucher.</div>';
                    }
        $mensaje.='<br><br>
                    <div>Atentamente,</div>
                    <div>Colegio Euclides</div>
                    <div>Ubicación: Calle Miraflores n° 128 - Huánuco</div>
                    <div>Teléfono: <a href="tel:962074825">962 074 825</a></div>
                    <div>Correo: <a href="mailto:cepeuclides@gmail.com" target="_top">cepeuclides@gmail.com</a> </div>
                </div>';
        enviarcorreo($correo_apoderado,$nombres_apoderado,$apellidos_apoderado,$asunto,$mensaje);
        $_SESSION['modo_pago_prog']=$metodo_pago;
        if (!$result) {
            echo "<br>Error de procedimiento";
        }
	       return $result;
        //echo "<script> window.location.href='".$pagdireccion."';</script>";

        //$mensaje=$pagdireccion;
}

function actuaPagoPensAlumno($mes_pago,$metodo_pago,$correo_apoderado,$nombres_apoderado,$apellidos_apoderado,$nombres_alumno,$apellidos_alumno,$id_pago,$id_alumno,$costo_programa){
//error_reporting(E_ALL);
//ini_set('display_errors','on');
        //metodo de pago 2 es con tarjeta
        if($metodo_pago==2){
            date_default_timezone_set('America/Lima');
            $fecha=date("d/m/Y");
            $cmdsql="update pago set metodo_pago = $metodo_pago, estado_pago=1, fecha='$fecha' where id_pago=$id_pago";
            $result=mysqli_query(conectar(), $cmdsql);
        }else{
            $cmdsql="update pago set metodo_pago = $metodo_pago where id_pago=$id_pago";
            $result=mysqli_query(conectar(), $cmdsql);
        }
    

    
        $asunto="Autoconfirmación-Pago de pensión";
        $mensaje='<div style="color:#888">
                    <div >Estimado(a) '.$nombres_apoderado." ".$apellidos_apoderado.',</div>
                    <br>';
                    if($metodo_pago==1){
                        $mensaje.='<div style="display:inline">Se ha registrado la intención de pago de pensión del alumno '.$nombres_alumno." ".$apellidos_alumno.' de '.$mes_pago.'.<br> Para completar la matrícula </div><div style="display:inline">diríjase a la secretaría del Colegio Matemático Euclides, ubicado en la calle Miraflores n° 128, y realice el pago de S/.'.$costo_programa.' correspondientes.</div>';
                    }elseif($metodo_pago==2){
                        $mensaje.='<div style="display:inline">Se ha registrado correctamente el pago de '.$mes_pago.' del alumno '.$nombres_alumno." ".$apellidos_alumno.'.</div>';
                    }else{
                        $mensaje.='<div style="display:inline">Se ha registrado la intención de pago de pensión del alumno '.$nombres_alumno." ".$apellidos_alumno.' de '.$mes_pago.'.<br> Para completar la matrícula </div><div style="display:inline">diríjase a algún banco o agente BCP y realice el depósito de S/.'.$costo_programa.' a la cuenta N°123 131231231312313123, luego responda a este correo con una foto del voucher.</div>';
                    }
        $mensaje.='<br><br>
                    <div>Atentamente,</div>
                    <div>Colegio Euclides</div>
                    <div>Ubicación: Calle Miraflores n° 128 - Huánuco</div>
                    <div>Teléfono: <a href="tel:962074825">962 074 825</a></div>
                    <div>Correo: <a href="mailto:cepeuclides@gmail.com" target="_top">cepeuclides@gmail.com</a> </div>
                </div>';
        enviarcorreo($correo_apoderado,$nombres_apoderado,$apellidos_apoderado,$asunto,$mensaje);
        $_SESSION['modo_pago_prog']=$metodo_pago;
        if (!$result) {
            echo "<br>Error de procedimiento";
        }
	       return $result;
        //echo "<script> window.location.href='".$pagdireccion."';</script>";

        //$mensaje=$pagdireccion;
}

function actuaInfoProfesor($id_profesor_ac,$correo_ac,$nombres_profesor_ac,$apellidos_profesor_ac,$contrasena_profesor_ac,$pagdireccion){
    //error_reporting(E_ALL);
    //ini_set('display_errors','on');
    $cmdsql="update profesor set nombres_profesor = '$nombres_profesor_ac',apellidos_profesor = '$apellidos_profesor_ac',correo = '$correo_ac',contrasena_profesor = '$contrasena_profesor_ac' where id_profesor=$id_profesor_ac";
    $result=mysqli_query(conectar(), $cmdsql);

    if (!$result) {
        echo "<br>Error de procedimiento";
    }else{
        echo "<script> alert('Se actualizó profesor.');</script>";
        echo "<script> window.location.href='".$pagdireccion."';</script>";
    }
    return $result;
    //echo "<script> window.location.href='".$pagdireccion."';</script>";

    //$mensaje=$pagdireccion;
}

function actuaInfoAlumno($id_alumno_ac,$id_apoderado_ac, $nombres_alumno_ac, $apellidos_alumno_ac, $dni_ac, $id_seccion_ac, $estado_matricula_ac, $porcentaje_pago_ac, $nombres_apoderado_ac, $apellidos_apoderado_ac, $correo_apoderado_ac, $celular_apoderado_ac, $direccion_apoderado_ac, $pagdireccion){
    //error_reporting(E_ALL);
    //ini_set('display_errors','on');
    $cmdsql="update alumno set nombres_alumno = '$nombres_alumno_ac',apellidos_alumno = '$apellidos_alumno_ac',id_apoderado = $id_apoderado_ac,id_seccion = $id_seccion_ac,dni = $dni_ac,alumno_matriculado = $estado_matricula_ac,porcentaje_pago = $porcentaje_pago_ac where id_alumno=$id_alumno_ac";
    $result=mysqli_query(conectar(), $cmdsql);

    if (!$result) {
        echo "<br>Error de procedimiento";
    }else{
        $cmdsql1="update apoderado set nombres_apoderado = '$nombres_apoderado_ac',apellidos_apoderado = '$apellidos_apoderado_ac',correo_apoderado = '$correo_apoderado_ac',celular_apoderado = $celular_apoderado_ac,direccion_apoderado = '$direccion_apoderado_ac' where id_apoderado=$id_apoderado_ac";
        $result1=mysqli_query(conectar(), $cmdsql1);
        if (!$result) {
            echo "<br>Error de procedimiento";
        }else{
            echo "<script> alert('Se actualizó alumno.');</script>";
            echo "<script> window.location.href='".$pagdireccion."';</script>";
        }
        
    }
    return $result;
    //echo "<script> window.location.href='".$pagdireccion."';</script>";

    //$mensaje=$pagdireccion;
}

function actuaInfoUser($id_otro_usuarioac,$correoant_usuario_ac,$nombres_usuario_ac,$apellidos_usuario_ac,$correo_usuario_ac,$celular_usuario_ac,$pagdireccion){
    //error_reporting(E_ALL);
    //ini_set('display_errors','on');
    $cmdsql="update otro_usuario set nombres_usuario = '$nombres_usuario_ac',apellidos_usuario = '$apellidos_usuario_ac',correo = '$correo_usuario_ac',celular_otro_usuario = $celular_usuario_ac where id_otro_usuario=$id_otro_usuarioac";
    $result=mysqli_query(conectar(), $cmdsql);

    if (!$result) {
        echo "<br>Error de procedimiento";
    }else{
        $cmdsql1="update programa_usuario set correo = '$correo_usuario_ac' where correo='$correoant_usuario_ac'";
        $result1=mysqli_query(conectar(), $cmdsql1);
        if (!$result) {
            echo "<br>Error de procedimiento";
        }else{
            echo "<script> alert('Se actualizó usuario.');</script>";
            echo "<script> window.location.href='".$pagdireccion."';</script>";
        }
        
    }
    return $result;
    //echo "<script> window.location.href='".$pagdireccion."';</script>";

    //$mensaje=$pagdireccion;
}

function actuaInfoCurso($id_curso_ac,$nombre_ac,$id_area_ac,$id_seccion_ac,$id_profesor_ac,$pagdireccion){
    //error_reporting(E_ALL);
    //ini_set('display_errors','on');
    $cmdsql="update curso set nombre = '$nombre_ac',id_area = $id_area_ac,id_seccion = $id_seccion_ac,id_profesor = $id_profesor_ac where id_curso=$id_curso_ac";
    $result=mysqli_query(conectar(), $cmdsql);

    if (!$result) {
        echo "<br>Error de procedimiento";
    }else{
        echo "<script> window.location.href='".$pagdireccion."';</script>";
    }
    return $result;
    //echo "<script> window.location.href='".$pagdireccion."';</script>";

    //$mensaje=$pagdireccion;
}

function actuaInfoArea($id_area_ac,$nombre_area_ac,$pagdireccion){
    //error_reporting(E_ALL);
    //ini_set('display_errors','on');
    $cmdsql="update area set nombre_area = '$nombre_area_ac' where id_area=$id_area_ac";
    $result=mysqli_query(conectar(), $cmdsql);

    if (!$result) {
        echo "<br>Error de procedimiento";
    }else{
        echo "<script> window.location.href='".$pagdireccion."';</script>";
    }
    return $result;
    //echo "<script> window.location.href='".$pagdireccion."';</script>";

    //$mensaje=$pagdireccion;
}

function actuaInfoPagoAlumno($id_pago_aluac,$id_alumno_aluac,$mes_pago_aluac,$modo_pago_aluac,$estado_pago_aluac,$pagdireccion){
    //error_reporting(E_ALL);
    //ini_set('display_errors','on');
    if($estado_pago_aluac == 1){
        date_default_timezone_set('America/Lima');
        $fecha=date("d/m/Y");
        $cmdsql="update pago set metodo_pago = '$modo_pago_aluac', estado_pago=$estado_pago_aluac, fecha='$fecha' where id_pago=$id_pago_aluac";
    }else{
        $cmdsql="update pago set metodo_pago = '$modo_pago_aluac', estado_pago=$estado_pago_aluac where id_pago=$id_pago_aluac";
    }
    
    $result=mysqli_query(conectar(), $cmdsql);

    if (!$result) {
        echo "<br>Error de procedimiento";
    }else{
        if($mes_pago_aluac=="Matrícula"){
            $cmdsql1="update alumno set alumno_matriculado = $estado_pago_aluac where id_alumno=$id_alumno_aluac";
            $result2=mysqli_query(conectar(), $cmdsql1);
            if (!$result2) {
                echo "<br>Error de procedimiento";
            }else{
                if($estado_pago_aluac==1){
                    echo "<script> alert('Se actualizó el pago, el alumno ya se encuentra matriculado.');</script>";
                }else{
                    echo "<script> alert('Se actualizó el estado de mtrícula del alumno a no matriculado.');</script>";
                }
                
                echo "<script> window.location.href='".$pagdireccion."';</script>";            
            }
        }else{
            echo "<script> alert('Se actualizó el pago.');</script>";
            echo "<script> window.location.href='".$pagdireccion."';</script>";
        }
    }
    return $result;
    //echo "<script> window.location.href='".$pagdireccion."';</script>";

    //$mensaje=$pagdireccion;
}

function actuaInfoPagoOtro($id_pago_otroac,$modo_pago_otroac,$estado_pago_otroac,$pagdireccion){
    //error_reporting(E_ALL);
    //ini_set('display_errors','on');
    
    $cmdsql="update programa_usuario set modo_pago_prog = '$modo_pago_otroac', estado_pago_prog=$estado_pago_otroac where id_programa_usuario=$id_pago_otroac";

    $result=mysqli_query(conectar(), $cmdsql);

    if (!$result) {
        echo "<br>Error de procedimiento";
    }else{
        echo "<script> alert('Se actualizó el pago.');</script>";
        echo "<script> window.location.href='".$pagdireccion."';</script>";
    }
    return $result;
    //echo "<script> window.location.href='".$pagdireccion."';</script>";

    //$mensaje=$pagdireccion;
}

function agregarProfesor($correo_nue,$nombres_profesor_nue,$apellidos_profesor_nue,$contrasena_profesor_nue,$pagdireccion){
    //error_reporting(E_ALL);
    //ini_set('display_errors','on');
    $cmdsql="insert into `profesor`(`nombres_profesor`, `apellidos_profesor`, `correo`, `contrasena_profesor`) values ('$nombres_profesor_nue','$apellidos_profesor_nue','$correo_nue','$contrasena_profesor_nue')";
    $result=mysqli_query(conectar(), $cmdsql);

    if (!$result) {
        echo "<br>Error de procedimiento";
    }else{
        echo "<script> alert('Se agregó el profesor');</script>";
        echo "<script> window.location.href='".$pagdireccion."';</script>";
    }
    return $result;
    //echo "<script> window.location.href='".$pagdireccion."';</script>";

    //$mensaje=$pagdireccion;
}

function agregarPrograma($nombres_programa_nue,$fecha_inicio_programa,$costo_programa_nue,$estado_programa_nue){
    $cmdsql="insert into `programa`(`nombre_programa`, `fecha_programa`, `costo_programa`, `estado_inscripcion`) values ('$nombres_programa_nue', '$fecha_inicio_programa', '$costo_programa_nue', $estado_programa_nue)";
    $result=mysqli_query(conectar(), $cmdsql);

    if (!$result) {
        echo "<br>Error de procedimiento";
    }else{
        $cmdsql2="select id_programa from programa WHERE nombre_programa='$nombres_programa_nue' AND fecha_programa='$fecha_inicio_programa'";
        $result2=mysqli_query(conectar(), $cmdsql2);
            while ($row=mysqli_fetch_assoc($result2)) {
                $id_programa=$row['id_programa'];
            }
        echo "<script> alert('Se agregó el programa');</script>";
        echo "<script> window.location.href='adcursosextra.php?xid_programa=$id_programa';</script>";
    }
    return $result;
    //echo "<script> window.location.href='".$pagdireccion."';</script>";

    //$mensaje=$pagdireccion;
}

function agregarCurso($nombre_nue,$id_area_nue,$id_seccion_nue,$id_profesor_nue,$pagdireccion){
    //error_reporting(E_ALL);
    //ini_set('display_errors','on');
    $cmdsql="insert into `curso`(`id_area`, `id_seccion`, `id_profesor`, `nombre`) values ($id_area_nue,$id_seccion_nue,$id_profesor_nue,'$nombre_nue')";
    
    $result=mysqli_query(conectar(), $cmdsql);

    if (!$result) {
        echo "<br>Error de procedimiento";
    }else{
        $cmdsql="select id_curso from curso WHERE id_seccion=$id_seccion_nue AND nombre='$nombre_nue'";
        $result2=mysqli_query(conectar(), $cmdsql);
            while ($row=mysqli_fetch_assoc($result2)) {
                $id_curso=$row['id_curso'];
            }
        
        $resultado=anadirEstadoNotasAsist($id_curso);
        if(!$resultado){
            echo "<br>Error de procedimiento";
        }else{
            echo "<script> alert('Se agregó el curso');</script>";
            echo "<script> window.location.href='".$pagdireccion."';</script>";
        }
        
    }
    return $result;
    //echo "<script> window.location.href='".$pagdireccion."';</script>";

    //$mensaje=$pagdireccion;
}

function agregarArea($nombre_area_nue,$pagdireccion){
    //error_reporting(E_ALL);
    //ini_set('display_errors','on');
    $cmdsql="insert into `area`(`nombre_area`) values ('$nombre_area_nue')";
    $result=mysqli_query(conectar(), $cmdsql);

    if (!$result) {
        echo "<br>Error de procedimiento";
    }else{
        echo "<script> alert('Se agregó el area');</script>";
        echo "<script> window.location.href='".$pagdireccion."';</script>";
    }
    
    return $result;
}

function listarDiasCalend($mes,$ano){
	$cmdsql="select * from dia_calendario WHERE mes='$mes' AND ano='$ano'";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarAsistencia($ano,$id_alumno){
	$cmdsql="select asistencia.*,curso.id_curso, curso.nombre from asistencia JOIN curso ON asistencia.id_curso =curso.id_curso  WHERE asistencia.ano='$ano' AND asistencia.id_alumno=$id_alumno AND curso.id_curso > 14";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarNotas($ano,$id_alumno){
	$cmdsql="select nota.*, curso.id_curso, curso.nombre from nota JOIN curso ON nota.id_curso = curso.id_curso  WHERE nota.ano='$ano' AND nota.id_alumno=$id_alumno";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarNotasHistorico($id_alumno){
	$cmdsql="select notas_historial.*, cursosministerio.*, areaministerio.* from notas_historial JOIN cursosministerio ON notas_historial.id_curso_ministerio = cursosministerio.id_curso_ministerio JOIN areaministerio ON cursosministerio.id_area_ministerio = areaministerio.id_area_ministerio WHERE notas_historial.id_alumno=$id_alumno ORDER BY cursosministerio.id_curso_ministerio ASC";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarEstadoNotas($id_curso){
    date_default_timezone_set('America/Lima');
    $ano=date("Y");
	$cmdsql="select * from estado_notas_cursos where id_curso=$id_curso and ano_estado_notas=$ano";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}
function listarEstadoAsistencias($id_curso){
    date_default_timezone_set('America/Lima');
    $ano=date("Y");
	$cmdsql="select * from estado_asist_cursos where id_curso=$id_curso and ano_estado_asist=$ano";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarTodosLosProgramas($id_programa){
    $cmdsql="";
    if( $id_programa === "todos" ){
        $cmdsql="select * from programa";
    }else if($id_programa === 0){
        $cmdsql="select * from programa where estado_inscripcion=1";
    }else{
        $cmdsql="select * from programa where estado_inscripcion=1 and id_programa=$id_programa";
    }
	
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function crearNotasAlumos($id_curso,$bimestre,$evaluacion){
    date_default_timezone_set('America/Lima');
    $ano=date("Y");
	$cmdsql="select alumno.id_alumno from curso JOIN seccion ON curso.id_seccion = seccion.id_seccion JOIN alumno ON seccion.id_seccion=alumno.id_seccion where curso.id_curso=$id_curso";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        $cuenta=0;
        $cmdsql= "insert into `nota`(`id_alumno`, `id_curso`,`nota`, `bimestre`, `evaluacion`,`ano` ) values ";
		while ($row=mysqli_fetch_assoc($resultado)){
            $id_alumno=$row['id_alumno'];
            
            if($cuenta==0){
                $cmdsql.= "($id_alumno,$id_curso,0,$bimestre,$evaluacion,$ano)";
            }else{
                $cmdsql.= ", ($id_alumno,$id_curso,0,$bimestre,$evaluacion,$ano)";
            }
            $cuenta++;
		}
        if($cuenta==0){
            echo "<script> alert('No hay ningún alumno en la sección');</script>";
        }else{
            $result=mysqli_query(conectar(), $cmdsql);
            echo "<script> window.location.reload();</script>";
        }

        //$cmdsql="insert into `nota`(`id_alumno`, `id_curso`,`nota`, `bimestre`, `evaluacion`) values ($id_alumno,$id_curso,50,$bimestre,$evaluacion)";
        
    }
	return $resultado;
}

function crearNotasAlumosFaltantes($ids_alumnos,$id_curso,$bimestre,$evaluacion){
    date_default_timezone_set('America/Lima');
    $ano=date("Y");

    $cuenta=0;
    $cmdsql= "insert into `nota`(`id_alumno`, `id_curso`,`nota`, `bimestre`, `evaluacion`,`ano` ) values ";
    
    
    foreach($ids_alumnos as $id_alumno){
        if($cuenta==0){
            $cmdsql.= "($id_alumno,$id_curso,0,$bimestre,$evaluacion,$ano)";
        }else{
            $cmdsql.= ", ($id_alumno,$id_curso,0,$bimestre,$evaluacion,$ano)";
        }
        $cuenta++;
    }
    $result=mysqli_query(conectar(), $cmdsql);
    if($result){
        echo "<script> window.location.reload();</script>";    
    }else{
        echo "<script> console.log('Ocurrió un error, inténtelo de nuevo más tarde.');</script>";
    }

    //$cmdsql="insert into `nota`(`id_alumno`, `id_curso`,`nota`, `bimestre`, `evaluacion`) values ($id_alumno,$id_curso,50,$bimestre,$evaluacion)";
        

	return $result;
}

function crearAsistAlumos($messtr,$id_curso, $ano){
	$cmdsql="select alumno.id_alumno from curso JOIN seccion ON curso.id_seccion = seccion.id_seccion JOIN alumno ON seccion.id_seccion=alumno.id_seccion where curso.id_curso=$id_curso";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        $cuenta=0;
        $cmdsql= "insert into `asistencia`(`id_curso`, `mes`,`ano`, `porcentaje_asist`, `id_alumno`) values ";
		while ($row=mysqli_fetch_assoc($resultado)){
            $id_alumno=$row['id_alumno'];
            
            if($cuenta==0){
                $cmdsql.= "($id_curso,'$messtr','$ano',0,$id_alumno)";
            }else{
                $cmdsql.= ", ($id_curso,'$messtr','$ano',0,$id_alumno)";
            }
            $cuenta++;
		}
        
        if($cuenta==0){
            echo "<script> alert('No hay ningún alumno en la sección');</script>";
        }else{
            $result=mysqli_query(conectar(), $cmdsql);
            echo "<script> window.location.reload();</script>";
        }
        
    }
	return $resultado;
}

function crearAsistAlumosFaltantes($ids_alumnos,$messtr,$id_curso, $ano){
    $cuenta=0;
    $cmdsql= "insert into `asistencia`(`id_curso`, `mes`,`ano`, `porcentaje_asist`, `id_alumno`) values ";

    foreach($ids_alumnos as $id_alumno){
        if($cuenta==0){
            $cmdsql.= "($id_curso,'$messtr','$ano',0,$id_alumno)";
        }else{
            $cmdsql.= ", ($id_curso,'$messtr','$ano',0,$id_alumno)";
        }
        $cuenta++;
    }
    $result=mysqli_query(conectar(), $cmdsql);

    if($result){
        echo "<script> window.location.reload();</script>";    
    }else{
        echo "<script> console.log('Ocurrió un error, inténtelo de nuevo más tarde.');</script>";
    }

	return $resultado;
}

function listarNotasCursoProf($id_curso,$bimestre,$evaluacion){
	$cmdsql="select alumno.id_alumno, alumno.apellidos_alumno, alumno.nombres_alumno, nota.* from nota JOIN alumno ON nota.id_alumno=alumno.id_alumno where nota.id_curso=$id_curso and nota.bimestre=$bimestre and nota.evaluacion=$evaluacion";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarAsistCursoProf($messtr,$id_curso, $ano){
//error_reporting(E_ALL);
//ini_set('display_errors','on');
	$cmdsql="select alumno.id_alumno, alumno.apellidos_alumno, alumno.nombres_alumno,asistencia.* from asistencia JOIN alumno ON asistencia.id_alumno=alumno.id_alumno  WHERE asistencia.mes='$messtr' AND asistencia.id_curso=$id_curso AND asistencia.ano=$ano";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}
function listarAlumnos($id_alumno){
//error_reporting(E_ALL);
//ini_set('display_errors','on');
    if($id_alumno==0){
        $cmdsql="select alumno.*, apoderado.* from alumno JOIN apoderado ON alumno.id_apoderado=apoderado.id_apoderado ORDER BY alumno.id_alumno DESC";
    }else{
        $cmdsql="select alumno.*, apoderado.* from alumno JOIN apoderado ON alumno.id_apoderado=apoderado.id_apoderado WHERE id_alumno=$id_alumno";
    }
	
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarAlumnosSeccion($id_curso){
//error_reporting(E_ALL);
//ini_set('display_errors','on');
    $cmdsql="select id_alumno from alumno WHERE id_seccion=(SELECT id_seccion from curso where id_curso=$id_curso)";

	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function listarOtroUsuario($id_otro_usuario){
//error_reporting(E_ALL);
//ini_set('display_errors','on');
    $cmdsql="select otro_usuario.*, programa_usuario.*,programa.* from otro_usuario JOIN programa_usuario ON otro_usuario.correo=programa_usuario.correo JOIN programa ON programa_usuario.id_programa= programa.id_programa WHERE otro_usuario.id_otro_usuario=$id_otro_usuario";
	
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}
	return $resultado;
}

function actualizarTextoAnuales($texto_anual_actual,$tipo_anual,$num_texto,$pagdireccion){
	$cmdsql="UPDATE texto_anuales SET texto_anual='$texto_anual_actual' WHERE tipo_anual=$tipo_anual and num_texto=$num_texto";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        echo "<script> window.location.href='$pagdireccion';</script>";
    }
	return $resultado;
}

function actualizarTextoNosotros($texto_nosotros,$id_texto_nosotros,$pagdireccion){
	$cmdsql="UPDATE texto_nosotros SET texto_nosotros='$texto_nosotros' WHERE id_texto_nosotros=$id_texto_nosotros";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        echo "<script> window.location.href='$pagdireccion';</script>";
    }
	return $resultado;
}

function actualizarNombAlb($id_album,$nombre_album_actualizado,$pagdireccion){
	$cmdsql="UPDATE albumes SET nombre_album='$nombre_album_actualizado' WHERE id_album=$id_album";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        echo "<script> window.location.href='$pagdireccion';</script>";
    }
	return $resultado;
}
    
    
function actualizarPublicacion($id_publicacion,$titulo_publicacion,$texto_publicacion,$foto){

    if($foto == "no"){
        $cmdsql="UPDATE publicaciones SET titulo_publicacion='$titulo_publicacion', texto_publicacion='$texto_publicacion' WHERE id_publicacion=$id_publicacion";
    }else{
        $cmdsql="UPDATE publicaciones SET titulo_publicacion='$titulo_publicacion', texto_publicacion='$texto_publicacion', url_img_publicacion='$foto' WHERE id_publicacion=$id_publicacion";
    }
	
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        echo "<script> window.location.href='publicaciones.php';</script>";
    }
	return $resultado;
}

function actualizarOtros($id_fotos_otros,$foto,$pagdireccion){
    $cmdsql="UPDATE fotos_otros SET url_fotos_otros='$foto' WHERE id_fotos_otros=$id_fotos_otros";

	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        echo "<script> window.location.href='$pagdireccion';</script>";
    }
	return $resultado;
}

function crearPublicacion($titulo_publicacion_act,$texto_publicacion_act,$foto){
    $cmdsql="INSERT INTO `publicaciones` (`titulo_publicacion`,`texto_publicacion`, `url_img_publicacion`) VALUES ('$titulo_publicacion_act','$texto_publicacion_act','$foto')";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        echo "<script> window.location.href='publicaciones.php';</script>";
    }
	return $resultado;
}
    
function agregarFotoGaleria($foto,$id_album,$pagdireccion){
	$cmdsql="insert into `imagenes_galeria` (`url_imagenes_galeria`,`id_album`) values ('$foto',$id_album)";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        echo "<script> window.location.href='$pagdireccion';</script>";
    }
	return $resultado;
}
function agregarArchivoCurso($id_curso_agregar,$nombre_archivo_agregar,$archivo,$tamanoenbytes,$pagdireccion){
    date_default_timezone_set('America/Lima');
    $fecha_subida=date("d/m/Y");
	$cmdsql="insert into `archivo_curso_programa` (`id_curso_programa`,`nombre_archivo_programa`,`link_archivo_programa`,`tamano_archivo_programa`,`fecha_subida`) values ($id_curso_agregar,'$nombre_archivo_agregar','$archivo','$tamanoenbytes','$fecha_subida')";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        echo "<script> window.location.href='$pagdireccion';</script>";
    }
	return $resultado;
}

function agregarTextoNosotros($texto_nosotros,$pagdireccion){
	$cmdsql="insert into `texto_nosotros` (`texto_nosotros`) values ('$texto_nosotros')";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        echo "<script> window.location.href='$pagdireccion';</script>";
    }
	return $resultado;
}

function eliminarTextoNosotros($id_texto_nosotros,$pagdireccion){
	$cmdsql="DELETE FROM texto_nosotros WHERE id_texto_nosotros=$id_texto_nosotros";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        echo "<script> window.location.href='$pagdireccion';</script>";
    }
	return $resultado;
}
function eliminarImagen($id_imagenes_galeria,$url_imagenes_galeria,$pagdireccion){
    $url_imagenes_galeria=str_replace("'", "", $url_imagenes_galeria);
    unlink($url_imagenes_galeria);
	$cmdsql="DELETE FROM imagenes_galeria WHERE id_imagenes_galeria=$id_imagenes_galeria";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        echo "<script> window.location.href='$pagdireccion';</script>";
    }
	return $resultado;
}
    
function eliminarDiaCalend($dia,$mes,$ano,$pagdireccion){
    
	$cmdsql="DELETE FROM dia_calendario WHERE dia=$dia AND mes = '$mes' AND ano =$ano";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        echo "<script> window.location.href='$pagdireccion';</script>";
    }
	return $resultado;
}

function eliminarAlbum($id_album){
    $resultado=listarImagenesGaleria($id_album);
    $cmdsqlant="DELETE FROM imagenes_galeria WHERE id_imagenes_galeria IN (";
    $cmdsqdesp=")";
    $temp=0;
    while ($row=mysqli_fetch_assoc($resultado)) {
        $id_imagenes_galeria=$row['id_imagenes_galeria'];
        $url_imagenes_galeria=$row['url_imagenes_galeria'];
        unlink($url_imagenes_galeria);
        if($temp==0){
            $cmdsqlant.=$id_imagenes_galeria;
        }else{
            $cmdsqlant.= ", ".$id_imagenes_galeria;
        }
        $temp++;
    }
    $cmdsql=$cmdsqlant.$cmdsqdesp;
    $resultado1=mysqli_query(conectar(), $cmdsql);
    if (!$resultado1) {
		echo "<br>Error de procedimiento";
	}else{
        $pagdireccion="galeria.php";
        $cmdsql="DELETE FROM albumes WHERE id_album=$id_album";
        $resultado2=mysqli_query(conectar(), $cmdsql);
        if (!$resultado2) {
            echo "<br>Error de procedimiento";
        }else{
            echo "<script> window.location.href='$pagdireccion';</script>";
        }
    }
    return $resultado;
}

function eliminarPublicacion($id_publicacion,$url_img_publicacion){
    $pagdireccion="publicaciones.php";
    $cmdsql="DELETE FROM publicaciones WHERE id_publicacion=$id_publicacion";
    $resultado=mysqli_query(conectar(), $cmdsql);
    if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        unlink($url_img_publicacion);
    }
    
    echo "<script> window.location.href='$pagdireccion';</script>";
    return $resultado;
}

function eliminarProfesor($id_profesor_elim,$pagdireccion){
    $cmdsql="DELETE FROM profesor WHERE id_profesor=$id_profesor_elim";
    $resultado=mysqli_query(conectar(), $cmdsql);
    if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        $id_profesor_reemplazo=1;
   
        $cmdsql2="SELECT id_curso FROM curso WHERE id_profesor=$id_profesor_elim";
        $resultado3=mysqli_query(conectar(), $cmdsql2);

        $cmdsqlant= "UPDATE curso SET id_profesor = (case ";
        $cmdsqldesp=" WHERE id_curso IN (";
        $cuenta=0;
        while ($row=mysqli_fetch_assoc($resultado3)) {
            $id_curso=$row['id_curso'];
            if($cuenta==0){
                $cmdsqlant.="when id_curso = $id_curso then $id_profesor_reemplazo ";
                $cmdsqldesp.= "$id_curso";
            }else{
                $cmdsqlant.="when id_curso = $id_curso then $id_profesor_reemplazo ";
                $cmdsqldesp.= ", $id_curso";
            }
            $cuenta++;
        }

        $cmdsqlant.="end)";
        $cmdsqldesp.=");";

        $cmdsql3=$cmdsqlant.$cmdsqldesp;
        $resultado4=mysqli_query(conectar(), $cmdsql3);
        if (!$resultado4) {
            echo "<script> alert('Ha ocurrido algún otro problema. Inténtelo de nuevo más tarde');</script>";
		  echo "<br>Error de procedimiento";
       }else{
            echo "<script> alert('Se eliminó el profesor');</script>";
            echo "<script> window.location.href='$pagdireccion';</script>";
        }
       
    }
    
    return $resultado;
}

function eliminarCurso($id_curso_elim,$id_seccion_elim,$pagdireccion){
    $cmdsql="DELETE FROM curso WHERE id_curso=$id_curso_elim";
    $resultado=mysqli_query(conectar(), $cmdsql);
    if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        $result2=eliminarEstadoNotasAsist($id_curso_elim);
        if (!$result2) {
            echo "<br>Error de procedimiento";
        }else{
            $id_curso_reemplazo=$id_seccion_elim;

            $cmdsql2="SELECT id_horario FROM horario WHERE id_curso=$id_curso_elim";
            $resultado4=mysqli_query(conectar(), $cmdsql2);
            
            $cmdsqlant= "UPDATE horario SET id_curso = (case ";
            $cmdsqldesp=" WHERE id_horario IN (";
            $cuenta=0;
            while ($row=mysqli_fetch_assoc($resultado4)) {
                $id_horario=$row['id_horario'];
                if($cuenta==0){
                    $cmdsqlant.="when id_horario = $id_horario then $id_curso_reemplazo ";
                    $cmdsqldesp.= "$id_horario";
                }else{
                    $cmdsqlant.="when id_horario = $id_horario then $id_curso_reemplazo ";
                    $cmdsqldesp.= ", $id_horario";
                }
                $cuenta++;
            }
            
            $cmdsqlant.="end)";
            $cmdsqldesp.=");";

            $cmdsql3=$cmdsqlant.$cmdsqldesp;
            $resultado4=mysqli_query(conectar(), $cmdsql3);
           echo "<script> window.location.href='$pagdireccion';</script>";
        }

    }
    
    return $resultado;
}

function eliminarArea($id_area_elim,$pagdireccion){
    $cmdsql="DELETE FROM area WHERE id_area=$id_area_elim";
    $resultado=mysqli_query(conectar(), $cmdsql);
    if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        $id_area_reemplazo=1;

        $cmdsql2="SELECT id_curso FROM curso WHERE id_area=$id_area_elim";
        $resultado3=mysqli_query(conectar(), $cmdsql2);

        $cmdsqlant= "UPDATE curso SET id_area = (case ";
        $cmdsqldesp=" WHERE id_curso IN (";
        $cuenta=0;
        while ($row=mysqli_fetch_assoc($resultado3)) {
            $id_curso=$row['id_curso'];
            if($cuenta==0){
                $cmdsqlant.="when id_curso = $id_curso then $id_area_reemplazo ";
                $cmdsqldesp.= "$id_curso";
            }else{
                $cmdsqlant.="when id_curso = $id_curso then $id_area_reemplazo ";
                $cmdsqldesp.= ", $id_curso";
            }
            $cuenta++;
        }

        $cmdsqlant.="end)";
        $cmdsqldesp.=");";

        $cmdsql3=$cmdsqlant.$cmdsqldesp;
        $resultado4=mysqli_query(conectar(), $cmdsql3);
       echo "<script> window.location.href='$pagdireccion';</script>";
    }
    
    return $resultado;
}

function eliminarEstadoNotasAsist($id_curso_elim){
    date_default_timezone_set('America/Lima');
    $ano=date("Y");
    $cmdsql="DELETE FROM estado_notas_cursos WHERE id_curso=$id_curso_elim and ano_estado_notas = $ano";
    $resultado=mysqli_query(conectar(), $cmdsql);
    if (!$resultado) {
		echo "<br>Error de procedimiento";
	}else{
        $cmdsql2="DELETE FROM estado_asist_cursos WHERE id_curso=$id_curso_elim and ano_estado_asist = $ano";
        $resultado2=mysqli_query(conectar(), $cmdsql2);

    }
    
    return $resultado;
}

function enviarcorreo($destinatario,$nombres,$apellidos,$asunto,$mensaje){
//error_reporting(E_ALL);
//ini_set('display_errors','on');
    require_once('vendor/phpmailer/phpmailer/PHPMailerAutoload.php');
    $mail = new PHPMailer();
    //indico a la clase que use SMTP
    $mail->IsSMTP();
    //permite modo debug para ver mensajes de las cosas que van ocurriendo
    //$mail->SMTPDebug = 2;
    //Debo de hacer autenticación SMTP
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "ssl";
    //indico el servidor de Gmail para SMTP
    $mail->Host = "smtp.gmail.com";
    //indico el puerto que usa Gmail
    $mail->Port = 465;
    //indico un usuario / clave de un usuario de gmail
$mail->CharSet = 'UTF-8';
    $mail->Username = "cepeuclides@gmail.com";
    $mail->Password = "colegioeuclides2017";
    $mail->SetFrom('cepeuclides@gmail.com', 'Colegio Euclides');
    $mail->AddReplyTo('cepeuclides@gmail.com', 'Colegio Euclides');
    $mail->Subject = $asunto;
    $mail->MsgHTML($mensaje);
    //indico destinatario
    
    $mail->AddAddress($destinatario,$nombres." ".$apellidos);
    if(!$mail->Send()) {
    echo "Error al enviar: " . $mail->ErrorInfo;
    } else {
    //echo "Mensaje enviado!";
    }
}

function enviarCorreosMasivos($destinatarios,$asunto,$mensaje){
//error_reporting(E_ALL);
//ini_set('display_errors','on');
    require_once('vendor/phpmailer/phpmailer/PHPMailerAutoload.php');
    $mail = new PHPMailer();
    //indico a la clase que use SMTP
    $mail->IsSMTP();
    //permite modo debug para ver mensajes de las cosas que van ocurriendo
    //$mail->SMTPDebug = 2;
    //Debo de hacer autenticación SMTP
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = "ssl";
    //indico el servidor de Gmail para SMTP
    $mail->Host = "smtp.gmail.com";
    //indico el puerto que usa Gmail
    $mail->Port = 465;
    //indico un usuario / clave de un usuario de gmail
$mail->CharSet = 'UTF-8';
    $mail->Username = "cepeuclides@gmail.com";
    $mail->Password = "colegioeuclides2017";
    $mail->SetFrom('cepeuclides@gmail.com', 'Colegio Euclides');
    $mail->AddReplyTo('cepeuclides@gmail.com', 'Colegio Euclides');
    $mail->Subject = $asunto;
    $mail->MsgHTML($mensaje);
    //indico destinatario
    
    foreach($destinatarios as $destinatario){
        $mail->AddBCC($destinatario->correo,$destinatario->nombres." ".$destinatario->apellidos);
    }
    
    if(!$mail->Send()) {
    echo "Error al enviar: " . $mail->ErrorInfo;
    } else {
    //echo "Mensaje enviado!";
    }
}

function inscrirAluAnualAnt($dni){
    date_default_timezone_set('America/Lima');
    $ano=date("Y");
	$cmdsql="SELECT alumno.*,seccion.*,pago.*,apoderado.* FROM alumno JOIN seccion ON alumno.id_seccion=seccion.id_seccion JOIN pago ON alumno.id_alumno=pago.id_alumno JOIN apoderado ON alumno.id_apoderado=apoderado.id_apoderado  WHERE alumno.dni='$dni' AND pago.estado_pago=0";
	$result=mysqli_query(conectar(), $cmdsql);
	if(!$result){
        echo "<h3>Error de procedimiento</h3>";
	}
	return $result; 	
}

function buscarAlumnoAdmin($buscar_alumno_dni,$buscar_alumno_nombres,$buscar_alumno_apellidos,$buscar_alumno_seccion){
	$cmdsql="select alumno.*, apoderado.* from alumno JOIN apoderado ON alumno.id_apoderado=apoderado.id_apoderado WHERE";
	$cuenta=0;
	if ($buscar_alumno_dni != "") {
		$cmdsql=$cmdsql." dni=$buscar_alumno_dni";
		$cuenta++;
	}
	if ($buscar_alumno_nombres != "") {
		if ($cuenta>0) {
			$cmdsql=$cmdsql." AND";
		}
		$cmdsql=$cmdsql." nombres_alumno LIKE '%$buscar_alumno_nombres%'";
		$cuenta++;
	}
	if ($buscar_alumno_apellidos != "") {
		if ($cuenta>0) {
			$cmdsql=$cmdsql." AND";
		}
		$cmdsql=$cmdsql." apellidos_alumno LIKE '%$buscar_alumno_apellidos%'";
		$cuenta++;
	}
	if ($buscar_alumno_seccion != "") {
		if ($cuenta>0) {
			$cmdsql=$cmdsql." AND";
		}
        
        if($buscar_alumno_seccion==15){
            $cmdsql=$cmdsql." id_seccion BETWEEN 1 AND 3 ORDER BY id_seccion ASC";
        }elseif($buscar_alumno_seccion==16){
            $cmdsql=$cmdsql." id_seccion BETWEEN 4 AND 9 ORDER BY id_seccion ASC";
        }elseif($buscar_alumno_seccion==17){
            $cmdsql=$cmdsql." id_seccion BETWEEN 10 AND 14 ORDER BY id_seccion ASC";
        }elseif($buscar_alumno_seccion==18){
            $cmdsql.=" id_seccion > 0 ORDER BY id_seccion ASC";
        }else{
            $cmdsql=$cmdsql." id_seccion='$buscar_alumno_seccion'";
        }
 	  
		$cuenta++;
	}
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "Error de procedimiento";
	}
	return $resultado;
}

function buscarPagoAlumnoAdmin($buscar_alumno_dni,$buscar_alumno_nombres,$buscar_alumno_apellidos,$buscar_alumno_seccion){
    date_default_timezone_set('America/Lima');
    $ano=date("Y");
	$cmdsql="select alumno.*, pago.* from alumno JOIN pago ON alumno.id_alumno=pago.id_alumno WHERE ano_pago = $ano AND";
	$cuenta=0;
	if ($buscar_alumno_dni != "") {
		$cmdsql=$cmdsql." dni=$buscar_alumno_dni";
		$cuenta++;
	}
	if ($buscar_alumno_nombres != "") {
		if ($cuenta>0) {
			$cmdsql=$cmdsql." AND";
		}
		$cmdsql=$cmdsql." nombres_alumno LIKE '%$buscar_alumno_nombres%'";
		$cuenta++;
	}
	if ($buscar_alumno_apellidos != "") {
		if ($cuenta>0) {
			$cmdsql=$cmdsql." AND";
		}
		$cmdsql=$cmdsql." apellidos_alumno LIKE '%$buscar_alumno_apellidos%'";
		$cuenta++;
	}
	if ($buscar_alumno_seccion != "") {
		if ($cuenta>0) {
			$cmdsql=$cmdsql." AND";
		}
        
        if($buscar_alumno_seccion==15){
            $cmdsql=$cmdsql." id_seccion BETWEEN 1 AND 3 ORDER BY id_seccion ASC";
        }elseif($buscar_alumno_seccion==16){
            $cmdsql=$cmdsql." id_seccion BETWEEN 4 AND 9 ORDER BY id_seccion ASC";
        }elseif($buscar_alumno_seccion==17){
            $cmdsql=$cmdsql." id_seccion BETWEEN 10 AND 14 ORDER BY id_seccion ASC";
        }elseif($buscar_alumno_seccion==18){
            $cmdsql.=" id_seccion > 0 ORDER BY id_seccion ASC";
        }else{
            $cmdsql=$cmdsql." id_seccion='$buscar_alumno_seccion'";
        }
 	  
	}
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "Error de procedimiento";
	}
	return $resultado;
}

function buscarOtroUserAdmin($buscar_otro_correo,$buscar_otro_nombres,$buscar_otro_apellidos,$buscar_otro_programa){
	$cmdsql="select otro_usuario.*, programa_usuario.*,programa.* from otro_usuario LEFT JOIN programa_usuario ON otro_usuario.correo=programa_usuario.correo LEFT JOIN programa ON programa_usuario.id_programa= programa.id_programa WHERE";
	$cuenta=0;
	if ($buscar_otro_correo != "") {
		$cmdsql=$cmdsql." otro_usuario.correo='$buscar_otro_correo'";
		$cuenta++;
	}
	if ($buscar_otro_nombres != "") {
		if ($cuenta>0) {
			$cmdsql=$cmdsql." AND";
		}
		$cmdsql=$cmdsql." otro_usuario.nombres_usuario LIKE '%$buscar_otro_nombres%'";
		$cuenta++;
	}
	if ($buscar_otro_apellidos != "") {
		if ($cuenta>0) {
			$cmdsql=$cmdsql." AND";
		}
		$cmdsql=$cmdsql." otro_usuario.apellidos_usuario LIKE '%$buscar_otro_apellidos%'";
		$cuenta++;
	}
	if ($buscar_otro_programa != "") {
		if ($cuenta>0) {
			$cmdsql=$cmdsql." AND";
		}
        
         if($buscar_otro_programa==="todos"){
            $cmdsql=$cmdsql." programa_usuario.id_programa > 0";
        }else{
            $cmdsql=$cmdsql." programa_usuario.id_programa='$buscar_otro_programa'";
        }
        
		
		$cuenta++;
	}
    $cmdsql.=" ORDER BY otro_usuario.id_otro_usuario";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "Error de procedimiento";
	}
	return $resultado;
}

function listarPagoOtroId($id_programa_usuario){
	$cmdsql="select otro_usuario.*, programa_usuario.*,programa.* from otro_usuario LEFT JOIN programa_usuario ON otro_usuario.correo=programa_usuario.correo LEFT JOIN programa ON programa_usuario.id_programa= programa.id_programa WHERE programa_usuario.id_programa_usuario=$id_programa_usuario";
	$resultado=mysqli_query(conectar(), $cmdsql);
	if (!$resultado) {
		echo "Error de procedimiento";
	}
	return $resultado;
}

function transformarmesanum($mes){
    $mesnum="";
    switch ($mes) {
        case "Enero":
            $mesnum =01;
            break;
        case "Febrero":
            $mesnum =02;
            break;
        case "Marzo":
            $mesnum =03;
            break;
        case "Abril":
            $mesnum =04;
            break;
        case "Mayo":
            $mesnum =05;
            break;
        case "Junio":
            $mesnum =06;
            break;
        case "Julio":
            $mesnum =07;
            break;
        case "Agosto":
            $mesnum =08;
            break;
        case "Septiembre":
            $mesnum =09;
            break;
        case "Octubre":
            $mesnum =10;
            break;
        case "Noviembre":
            $mesnum =11;
            break;
        case "Diciembre":
            $mesnum =12;
            break;
    }
    
    return $mesnum;
}

function transformarmesastr($mes){//Se usa en asistencia profesor
    $messtr="";
    switch ($mes) {
        case "01":
            $messtr ="Enero";
            break;
        case "02":
            $messtr ="Febrero";
            break;
        case "03":
            $messtr ="Marzo";
            break;
        case "04":
            $messtr ="Abril";
            break;
        case "05":
            $messtr ="Mayo";
            break;
        case "06":
            $messtr ="Junio";
            break;
        case "07":
            $messtr ="Julio";
            break;
        case "08":
            $messtr ="Agosto";
            break;
        case "09":
            $messtr ="Septiembre";
            break;
        case "10":
            $messtr ="Octubre";
            break;
        case "11":
            $messtr ="Noviembre";
            break;
        case "12":
            $messtr ="Diciembre";
            break;
    }
    
    return $messtr;
}

function encriptarnum($num){
    $num1=rand(1, 5);
    $cad1="";
    $temp=0;
    for ($i=1;$i<=$num1;$i++ ){
        $temp=rand(0, 9);
        $cad1.=$temp;
    }

    $num2=rand(1, 5);
    $cad2="";
    $temp2=0;
    for ($j=1;$j<=$num2;$j++ ){
        $temp2=rand(0, 9);
        $cad2.=$temp2;
    }
    $numtmp=($num+727)*929;
    
    return $num1.$num2.$cad1.$numtmp.$cad2;
    
}
function decriptarnum($num){
    $num1=substr($num ,0,1);
    $num2=substr($num ,1,1);
    $cadenaext=substr($num, 2+$num1);
    $cadena=substr($cadenaext, 0,-$num2);
    $numero = ($cadena/929)-727;
    
    return $numero;
}

function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' kB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
}
?>