<?php
//error_reporting(E_ALL);
//ini_set('display_errors','on');
if(!($_SERVER['HTTP_ORIGIN'] == "http://aneicperu.com" || $_SERVER['HTTP_ORIGIN'] == "https://aneicperu.com" || $_SERVER['HTTP_ORIGIN'] == "http://www.aneicperu.com" || $_SERVER['HTTP_ORIGIN'] == "https://www.aneicperu.com")) {
?>
        
<div class="vota-modbod-msg">Por favor, accede del servidor de ANEIC.</div>
<div class="mensaje-vota-cerrar"></div>
<div class="vota-modbod-btn">
    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">Cerrar</button>
</div>  

<?php   
}else{
    
    include 'funciones.php';
    $cuenta_temp = 0;

    $link = conectar();

    if (!isset($_POST['x_i1'])) exit;

    $dni_delegado = $_POST['x_i1'];//Id de universidad
    
    $cmdsql="SELECT * FROM `delegado` WHERE dni_delegado = $dni_delegado AND estado_delegado=1 LIMIT 1";

    $resultado = mysqli_query($link, $cmdsql);

    $error_votacion_code = mysqli_errno($link);
    
    if ($error_votacion_code > 0) {
?>
        
<div class="vota-modbod-msg">Ha ocurrido un error al listar el delegado. Si este problema persiste, comuníquese con servicio técnico.</div>
<div class="mensaje-vota-cerrar"></div>
<div class="vota-modbod-btn">
    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">Cerrar</button>
</div>  

<?php   
    }else{
        while ($row=mysqli_fetch_assoc($resultado)) {
            
            $cuenta_temp++;
            
            $id_delegado = $row['id_delegado'];
            $nombres_delegado = $row['nombres_delegado'];
            $apellidos_delegado = $row['apellidos_delegado'];
            $dni_delegado = $row['dni_delegado'];
        }
        
        if($cuenta_temp == 0){
?>
        
<div class="vota-modbod-msg">No se ha encontrado ningún delegado con el DNI brindado.</div>
<div class="mensaje-vota-cerrar"></div>
<div class="vota-modbod-btn">
    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">Cerrar</button>
</div>  

<?php   
        }else{
                            
    
?>


<div class="vota-modbod-msg">¿Está seguro que desea eliminar el delegado: <span class="elimdel-nomb"><?php echo $nombres_delegado." ".$apellidos_delegado ?></span> con DNI <span class="elimdel-dni"><?php echo $dni_delegado ?></span>?</div>
<div class="mensaje-vota-cerrar"></div>
<div class="vota-modbod-btn">
    <form class="form-inline" role="form"  action="" method="post">
        <input type="hidden" value="<?php echo $id_delegado ?>" name="iddel-elim-inp">
        <button type="submit" class="btn btn-sm btn-warning elimdel-dni-btn" name="elimdel-dni-btn">Si</button>
        <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
    </form>
</div>


<?php
            
        }
    }

    //return $resultado;
    
}

?>
