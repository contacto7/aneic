<!DOCTYPE html>
<html ng-app="">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Impulsocial</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jquery.raty.css">
    <link href="css/princ.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery.min.older.js"></script>
</head>
<body>
<script type="text/javascript" src="js/jquery.min.js"></script>
<nav class="navbar navbar-nav navbar-fixed-top" role="navigation">
    <div class="nav-top-wrap">
        <div class="nav-top-inn">
            <img src="img/COL15-03.JPG" class="nav-top-img" alt="foto de cabecera">
            <div class="nav-top-cuad"></div>
        </div>
    </div>
	<div class="container cont-nav">
		<div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar,#navbar-dwn" aria-expanded="false" aria-controls="navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>
				<a class="navbar-brand" href="index.html"><img src="img/logoaneicv1.png" class="nav-img-logo" alt="logo"></a>
		</div>
		<div id="navbar-dwn" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-middle">
 				<li class="active">
                    <a href="index.html">Uniersidades y delegados</a>
                </li>
 				<li class="active">
                    <a href="index.html">Eventos</a>
                </li>
 				<li class="active">
                    <a href="index.html">Nosotros</a>
                </li>
 				<li class="active">
                    <a href="denuncias.html">Comité</a>
                </li>
            </ul>
		</div>
	</div>
</nav>


    
<div class="container" style="padding-top:200px;">

    <div class="row">

    <form class="form-inline form-qr-settings" style="">
      <div class="form-group">
        <label class="sr-only" for="tamqr">Tamaño de Qr</label>
        <input type="number" class="form-control" id="tamqr" placeholder="Tamaño de Qr">
      </div>
      <div class="form-group">
        <label class="sr-only" for="grosorln">Grosor de línea de borde</label>
        <input type="number" class="form-control" id="grosorln" placeholder="Grosor de línea de borde">
      </div>
      <div class="form-group">
        <label class="sr-only" for="espaciadoqr">Espaciado entre Qr y borde</label>
        <input type="number" class="form-control" id="espaciadoqr" placeholder="Espaciado entre Qr y borde">
      </div>
      <div class="form-group">
        <label class="sr-only" for="distarr">Distancia a arriba</label>
        <input type="number" class="form-control" id="distarr" placeholder="Distancia a arriba">
      </div>
      <div class="form-group">
        <label class="sr-only" for="distizq">Distancia a izquierda</label>
        <input type="number" class="form-control" id="distizq" placeholder="Distancia a izquierda">
      </div>
      <div class="form-group">
        <a class="btn btn-info" id="genqr" onclick="dataqr()">Generar QR </a> 
      </div>
        <a class="btn btn-info" id="download" data-nombre="cert-h-e1.png" data-padreid="#demo">Descargar </a>
    </form>
        <div class="canvas-ant"></div>
        <div id="demo-0" class="qr-canvas" data-idevento="1" data-idcert="1" data-id="1"></div>
        <div id="demo-1" class="qr-canvas" data-idevento="1" data-idcert="2" data-id="2"></div>
        

    </div>
</div>
<footer class="foot-wrap">
    <div class="footer-copy">
        <div class="copr">Contáctanos</div>
        <div class="divvert">| </div>
        <div class="copr">Dónde puedes encontrarnos</div>
        <div class="divvert">| </div>
        <div class="copr">¿Quiénes somos?</div>
    </div>
    <div class="footer-copy">
        <div class="copr">Copyright &copy; Aneic Perú</div>
        <div class="divvert">| </div>
        <div class="copr-aqm">Powered by <a href="http://www.inoloop.com" target="_blank"><img class="logo-foot" src="img/aqm.png"> Inoloop </a></div>
    </div>
</footer>

<div id="login-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title">Login</h4>
            </div>
            <div class="modal-body">
                 <form class="form-inline" role="form" action="" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="correo">Correo</label>
                        <input type="email" class="form-control input-sm" name="correo" id="user_mail" placeholder="Correo" required>
                    </div>     
                    <div class="form-group">
                        <label class="sr-only" for="contrasena">Contraseña</label>
                        <input type="password" class="form-control input-sm" name="contrasena" id="password" placeholder="Contraseña" required>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox"><small>Recordarme</small></label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm" name="ingresar" >Entrar</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script type="text/javascript" src="js/jquery.qrcode.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    
});
</script>
</body>
</html>