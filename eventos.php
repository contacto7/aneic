<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    //echo "<script> window.location.href='index.php';</script>";
}

$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();




$universidadeslista = listaruniversidades();

$amazonas   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ancash     = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$apurimac   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$arequipa   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ayacucho   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$cajamarca  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$callao     = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$cusco      = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$huancavelica = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$huanuco = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ica = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$junin = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$lalibertad = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$lambayeque = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$lima  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$loreto  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$madrededios = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$moquegua   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$pasco   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$piura  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$puno   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$sanmartin   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$tumbes   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ucayali   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";

$universidadesarr = array();

while ($row=mysqli_fetch_assoc($universidadeslista)) {
    $id_universidad=$row['id_universidad'];
    $nombre_universidad=$row['nombre_universidad'];
    $departamento=$row['departamento'];

    switch ($departamento) {
        case "Amazonas":
            $amazonas   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Áncash":
            $ancash     .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Apurímac":
            $apurimac   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Arequipa":
            $arequipa   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Ayacucho":
            $ayacucho   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Cajamarca":
            $cajamarca  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Callao":
            $callao     .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Cusco":
            $cusco      .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Huancavelica":
            $huancavelica .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Huánuco":
            $huanuco .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Ica":
            $ica .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Junín":
            $junin .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "La Libertad":
            $lalibertad .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Lambayeque":
            $lambayeque .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Lima":
            $lima  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Loreto":
            $loreto  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Madre de Dios":
            $madrededios .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Moquegua":
            $moquegua   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Pasco":
            $pasco   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Piura":
            $piura  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Puno":
            $puno   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "San Martín":
            $sanmartin   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Tumbes":
            $tumbes   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Ucayali":
            $ucayali   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
    }


}
mysqli_free_result($universidadeslista);



?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Eventos</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body style="min-width:450px;">
<script type="text/javascript" src="js/jquery.min.js"></script>

<div id="header"></div>
    
<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Eventos</span>
    </div>
</div>
<?php if($sesioninic){ ?>
            <div class="btn-foto-navmed" href="#andir-evento-mod" data-target="#andir-evento-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin">
                <button>Añadir Evento</button>
            </div>
<?php } ?>
<div class="container cont-eventos">
    <div class="row row-eventos">
        <div class="eventos-wrapp-izq">
            
<?php 
    $eventos_res=listareventosadm();
            
    $nombre_adm_eventos="";
    $inicio_adm_eventos="";
    $dscr_adm_eventos="";
    $link_adm_eventos="";
    $foto_adm_eventos="";
    
                                    
    while ($row=mysqli_fetch_assoc($eventos_res)){

        $id_adm_eventos=$row['id_evento'];
        $nombre_adm_eventos=$row['nombre_evento'];
        $inicio_adm_eventos=$row['fechainic_evento'];
        $dscr_adm_eventos=$row['descripcion_evento'];
        $link_adm_eventos=$row['link_evento'];
        $foto_adm_eventos=$row['foto_adm_eventos'];

        setlocale(LC_TIME, "");
        setlocale(LC_ALL,"es_ES.UTF8");
        
        //$fechain= date('m/Y', strtotime($fechain));
        $inicio_adm_eventos = DateTime::createFromFormat("Y-m-d", $inicio_adm_eventos);
        $inicio_adm_eventos = strftime("%d de %B",$inicio_adm_eventos->getTimestamp());
        
?>
            <div class="cuadro-evento-wrapp">
                <div class="cuadro-evento-supe">
                    <div class="cuadro-evenimg-wrapp">
                        <div class="cuadro-evento-img">
                            <img alt="imagen de evento" src="<?php echo $foto_adm_eventos ?>">
                        </div>
                    </div>
                    <div class="cuadro-evento-fecha">
                        <?php echo $inicio_adm_eventos ?>
                    </div>
<?php if($sesioninic){ ?>
            <div class="btn-foto-navmed modalLink" href="#camb-evento-mod" data-target="#camb-evento-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" style="margin-top:-5px; position: absolute" data-id="<?php echo $id_adm_eventos ?>">
                <button>Editar</button>
            </div>
<?php } ?>
                </div>
                <div class="cuadro-evento-infe">
                    <div class="cuadro-evento-nombre">
                        <?php echo $nombre_adm_eventos ?>
                    </div>
                    <div class="cuadro-evento-descr">
                        <?php echo $dscr_adm_eventos ?>
                    </div>
                    <div class="cuadro-evento-link">
                        <a href="<?php echo $link_adm_eventos ?>" target="_blank"><?php echo $link_adm_eventos ?></a>
                    </div>
                </div>      
            </div>
            
<?php 

    };
?>
        </div>
        <div class="eventos-wrapp-der">
            <div class="eventos-imgder-wrapp">
                <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                    <div class="item active">
                      <img alt="gif de eventos" src="fotos_evento/1.png">
                    </div>

                    <div class="item">
                      <img alt="gif de eventos" src="fotos_evento/2.JPG">
                    </div>

                    <div class="item">
                      <img alt="gif de eventos" src="fotos_evento/3.JPG">
                    </div>
                  </div>
                </div>
                
            </div>
        </div>
    
    </div>
</div>

<div id="footer"></div>
    
<?php if($sesioninic){ ?>
    
<div id="andir-evento-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Añadir Evento</h4>
            </div>
            <div class="modal-body modbod-exp">
                 <form class="form-horizontal form-datoper" role="form" action="" method="post" enctype="multipart/form-data">
                     <div class="row form-datper-left">
                        <div class="col-xs-12">
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="nombre-evento">Nombre de evento:</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="nombre-evento" name="nombre-evento" placeholder="Ingrese el nombre del evento">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="nombre-evento">Departamento:</label>
                            <div class="col-sm-8">
                                <select class="form-control sel-unibydel" name="departamento" id="departamento">
                                  <option class="sel-dep" value="Departamento">Departamento</option>
                                  <option class="sel-dep" value="Amazonas">Amazonas</option>
                                    <option class="sel-dep" value="Áncash">Ancash</option>
                                    <option class="sel-dep" value="Apurímac">Apurímac</option>
                                    <option class="sel-dep" value="Arequipa">Arequipa</option>
                                    <option class="sel-dep" value="Ayacucho">Ayacucho</option>
                                    <option class="sel-dep" value="Cajamarca">Cajamarca</option>
                                    <option class="sel-dep" value="Callao">Callao</option>
                                    <option class="sel-dep" value="Cusco">Cusco</option>
                                    <option class="sel-dep" value="Huancavelica">Huancavelica</option>
                                    <option class="sel-dep" value="Huánuco">Huánuco</option>
                                    <option class="sel-dep" value="Ica">Ica</option>
                                    <option class="sel-dep" value="Junín">Junín</option>
                                    <option class="sel-dep" value="La Libertad">La Libertad</option>
                                    <option class="sel-dep" value="Lambayeque">Lambayeque</option>
                                    <option class="sel-dep" value="Lima">Lima</option>
                                    <option class="sel-dep" value="Loreto">Loreto</option>
                                    <option class="sel-dep" value="Madre de Dios">Madre de Dios</option>
                                    <option class="sel-dep" value="Moquegua">Moquegua</option>
                                    <option class="sel-dep" value="Pasco">Pasco</option>
                                    <option class="sel-dep" value="Piura">Piura</option>
                                    <option class="sel-dep" value="Puno">Puno</option>
                                    <option class="sel-dep" value="San Martín">San Martín</option>
                                    <option class="sel-dep" value="Tacna">Tacna</option>
                                    <option class="sel-dep" value="Tumbes">Tumbes</option>
                                    <option class="sel-dep" value="Ucayali">Ucayali</option>
                                </select>
                            </div>
                          </div>
                          <div class="form-group" id="row-univ">
                            <label class="control-label col-sm-4" for="nombre-evento">Universidad:</label>
                            <div class="col-sm-8">
                                <select class="form-control sel-unibydel" name="univ-dep" id="univ-dep">
                                  <option class="sel-prov" value="universidad">Seleccione el departamento</option>
                                </select>
                            </div>
                          </div>
                        <div class="form-group row" id="row-univ-nue" style="display:none">
                          <label for="univag-dep" class="col-sm-4 col-form-label" style="text-align: right;">Agregar universidad</label>
                          <div class="col-sm-8">
                            <input class="form-control" type="text" placeholder="Ingrese el nombre de la universidad a agregar." name="univag-dep" id="univag-dep">
                            <span>No se olvide de también seleccionar el departamento donde se encuentra la universidad.<br> Al ingresar el nombre especifique si es sede. Por por ejemplo: "Universidad del Perú - Sede Lima"</span>
                          </div>
                        </div>
                        <div class="form-group row row-delegado" style="text-align: right;">
                            <div class="col-sm-4">
                              <input type="checkbox" class="archivo-subir" id="nuevau-delegado" name="nuevau-delegado" value="noenc">
                            </div>
                            <label class="control-label col-sm-8" for="nuevau-delegado" style="text-align: left;">No encuentro la universidad</label>
                        </div>
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="inicio-evento">Inicio de evento<br>(XX de Mes):</label>
                            <div class="col-sm-8">
                                <div class='input-group date' style="display:block" id='datetimepicker7'>
                                    <input type="text" class="form-control inp-sevolunt" id="inicio-evento" name="inicio-evento" placeholder="Inicio de evento" required style="border-radius:4px;">
                                    <span class="input-group-addon" id="btn-dp-1" style="visibility: hidden;position: relative;top: -60px;">
                                        <span class="fa fa-calendar" id="btn-dp-2" >
                                        </span>
                                    </span>
                                </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="dscr-evento">Descripción de evento:</label>
                            <div class="col-sm-8">
                                <textarea rows="2" style="width:100%" class="form-control" name="dscr-evento" id="dscr-evento"></textarea>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="link-evento">Link de evento (Facebook):</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="link-evento" name="link-evento" placeholder="Ingrese el link del evento">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="link-evento">Imagen de evento:</label>
                            <div class="col-sm-8">
                                  <label for="foto-evento"><a class="btn btn-md form-control input-sm selec-arch-mod">Selecciona archivo</a></label>
                                <div id="nombrefoto"></div>
                                  <input type="file" class="archivo-subir" id="foto-evento" name="foto-evento" style="display:none" data-id="1">
                                <span class="span-foto-estado" id="estado-cambio-1"></span>
                            </div>
                          </div>

                        </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-12 datpersbut-wrap">
                              <div class="datpersbut-mod">
                                <button type="submit" class="btn btn-datper-mod btn-sm" name="evento-adm-sub" >Guardar</button>
                                <button type="button" class="btn btn-datper-canc btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                             </div>
                         </div>
                     </div>
                </form>
            </div>

        </div>
    </div>
</div>
    
<div class="modal fade" id="camb-evento-mod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <img src="img/ajax-loader.gif" id="loading-indicator" style="display:none">
        <div class="modal-content modal-content-cv" id="mod-cont">

        </div>
    </div>
</div>
    
<?php } ?>
    
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script src="js/jquery.carouFredSel.js"></script>
<script src="js/moment-with-locales.js"></script>
<script src="js/bootstrap-datetimepicker.js"></script>
<script>
$(function(){
    
   $("#header").load("header.php", {
       xph: 4, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
<script>    
$(document).ready(function(){

    $('#camb-evento-mod').on('hidden.bs.modal', function () {
        var myNode = document.getElementById("mod-cont");
        while (myNode.firstChild) {
            myNode.removeChild(myNode.firstChild);
        }
    });
    $(document).ajaxSend(function(event, request, settings) {
      $('#loading-indicator').show();
    });

    $(document).ajaxComplete(function(event, request, settings) {
      $('#loading-indicator').hide();
    });

});
    
$(document).on("click", '.modalLink',function(){
    console.log("asd");
    var evento=$(this).attr('data-id');

    $(".modal-content-cv").css({
       "min-height":"400px" 
    });
    $.ajax({url:"apost_modal_evento.php?xevento="+evento,cache:false,success:function(result){
        $(".modal-content-cv").html(result);
    }});



});

$(function () {
    $('#datetimepicker7').datetimepicker({
        viewMode: 'years',
        format:'YYYY-MM-DD',
        locale: 'es'
    });
});
    
</script>
    
<script>

var departamentos ={

    "Departamento":'<option class="sel-prov" value="universidad">Seleccione el departamento</option>',
    "Amazonas": "<?php echo $amazonas; ?>",
    "Áncash": "<?php echo $ancash; ?>",
    "Apurímac": "<?php echo $apurimac; ?>",
    "Arequipa": "<?php echo $arequipa; ?>",
    "Ayacucho": "<?php echo $ayacucho; ?>",
    "Cajamarca": "<?php echo $cajamarca; ?>",
    "Callao": "<?php echo $callao; ?>",
    "Cusco": "<?php echo $cusco; ?>",
    "Huancavelica": "<?php echo $huancavelica; ?>",
    "Huánuco": "<?php echo $huanuco; ?>",
    "Ica": "<?php echo $ica; ?>",
    "Junín": "<?php echo $junin; ?>",
    "La Libertad": "<?php echo $lalibertad; ?>",
    "Lambayeque": "<?php echo $lambayeque; ?>",
    "Lima": "<?php echo $lima; ?>",
    "Loreto": "<?php echo $loreto; ?>",
    "Madre de Dios": "<?php echo $madrededios; ?>",
    "Moquegua": "<?php echo $moquegua; ?>",
    "Pasco": "<?php echo $pasco; ?>",
    "Piura": "<?php echo $piura; ?>",
    "Puno": "<?php echo $puno; ?>",
    "San Martín": "<?php echo $sanmartin; ?>",
    "Tumbes": "<?php echo $tumbes; ?>",
    "Ucayali": "<?php echo $ucayali; ?>",
    
};
/*
$("#departamento").change(function() {
    var temp=0;
    var departamento= this.value;
    var prov=departamentos[departamento];
    var provarr = prov.split(",");
    var startlista="<ul>";

    var midlista="";
    for(provinc in provarr){
        midlista+="<option class='sel-prov' value='"+provarr[temp]+"'>"+provarr[temp]+"</option>";
        temp++;
    }
    var listaprov=startlista+midlista;
    $("#univ-dep").html(listaprov);
});
*/
$("#departamento").change(function() {
    var temp=0;
    var departamento= this.value;
    var prov=departamentos[departamento];
    $("#univ-dep").html(prov);
    //console.log(prov);
});
    
$("#univ-dep").change(function() {
    console.log("asd");
    var id_universidad=this.value;

    $(".cont-unidele").css({
        "opacity": "0.5",
        "pointer-events":"none"
    });
    $(".univdel-card-wrapp").html("Cargando...");

    $.ajax({url:"apost_append_delegados.php?xid_universidad="+id_universidad,cache:false,success:function(result){
        $(".univdel-card-wrapp").html(result);
                $(".cont-unidele").css({
                    "opacity": "1",
                    "pointer-events":"inherit"
                });
    }});
    
});
</script>
    
<?php

if (isset($_POST['evento-adm-sub'])){

    if($image_size=$_FILES['foto-evento']['size']>0 && !empty($_POST['nombre-evento']) && !empty($_POST['inicio-evento']) && !empty($_POST['dscr-evento']) && !empty($_POST['link-evento']) && (!empty($_POST['univ-dep']) || !empty($_POST['univag-dep'])) ){

        $allowed =  array('gif','png' ,'jpg','GIF','PNG' ,'JPG');
        $filename = $_FILES['foto-evento']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!in_array($ext,$allowed) ) {
            echo "<script>alert('Por favor selecciona una imagen')</script>";
            exit();
        }else{
            
            
            $departamento=$_POST['departamento'];
            
            if(isset($_POST['nuevau-delegado'])){
                $nombre_universidad=$_POST['univag-dep'];
                $estado_universidad = 1;
                $id_universidad= anadiruniversidad($nombre_universidad, $departamento, $estado_universidad);
                if($id_universidad =="Error"){
                    echo "<script>alert('Hubo un error al añadir la universidad. Por favor intenta más tarde.')</script>";
                    exit();
                }
            }else{
                $id_universidad=$_POST['univ-dep'];

            }
            
            
            $nuevo_nombre=randomalfanume(25);
            $nuevo_nombre= $nuevo_nombre.".".$ext;
            $url_foto= "foto_evento_cuadro/".$nuevo_nombre;

            $image_temp_name=$_FILES['foto-evento']['tmp_name'];
            if(move_uploaded_file($image_temp_name,$url_foto)){

                $pagdireccion = "eventos.php";

                $nombre_evento_mod=$_POST['nombre-evento'];
                $inicio_evento_mod=$_POST['inicio-evento'];
                $dscr_evento_mod=$_POST['dscr-evento'];
                $link_evento_mod=$_POST['link-evento'];

                $resultado_anad_evento = anadireventosadm($id_universidad, $nombre_evento_mod, $inicio_evento_mod, $dscr_evento_mod, $link_evento_mod, $url_foto, $pagdireccion);

            }else{
                echo "<script>alert('Hubo un error al cargar la imagen. Por favor intenta más tarde.')</script>";
                exit();
            }
            
            
            
        }

    }

}

if (isset($_POST['evento-mod-sub'])){
    if( $image_size=$_FILES['foto-evento-mod']['size']>0 ){

        if(!empty($_POST['nombre-evento-mod']) && !empty($_POST['inicio-evento-mod']) && !empty($_POST['dscr-evento-mod']) && !empty($_POST['link-evento-mod']) ){

            $allowed =  array('gif','png' ,'jpg','GIF','PNG' ,'JPG');
            $filename = $_FILES['foto-evento-mod']['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if(!in_array($ext,$allowed) ) {
                echo "<script>alert('Por favor selecciona una imagen')</script>";
                exit();
            }else{

                if($_POST['univ-dep-mod'] != "universidad"){
                    $id_universidad_mod =$_POST['univ-dep-mod'];
                }else{
                    $id_universidad_mod =$_POST['id-univant-mod'];
                }

                $nuevo_nombre=randomalfanume(25);
                $nuevo_nombre= $nuevo_nombre.".".$ext;
                $url_foto_mod= "foto_evento_cuadro/".$nuevo_nombre;

                $image_temp_name=$_FILES['foto-evento-mod']['tmp_name'];
                if(move_uploaded_file($image_temp_name,$url_foto_mod)){

                    $pagdireccion = "eventos.php";

                    $id_evento_mod=$_POST['id-evento-mod'];
                    $nombre_evento_mod=$_POST['nombre-evento-mod'];
                    $inicio_evento_mod=$_POST['inicio-evento-mod'];
                    $dscr_evento_mod=$_POST['dscr-evento-mod'];
                    $link_evento_mod=$_POST['link-evento-mod'];

                    modificareventosadm($id_evento_mod, $id_universidad_mod, $nombre_evento_mod, $inicio_evento_mod, $dscr_evento_mod, $link_evento_mod, $url_foto_mod, $pagdireccion);


                }else{
                    echo "<script>alert('Hubo un error al cargar la imagen. Por favor intenta más tarde.')</script>";
                    exit();
                }



            }

        }
    }else{
        if($_POST['univ-dep-mod'] != "universidad"){
            $id_universidad_mod=$_POST['univ-dep-mod'];
        }else{
            $id_universidad_mod=$_POST['id-univant-mod'];
        }

        $pagdireccion = "eventos.php";

        $url_foto_mod="nofoto";

        $id_evento_mod=$_POST['id-evento-mod'];
        $nombre_evento_mod=$_POST['nombre-evento-mod'];
        $inicio_evento_mod=$_POST['inicio-evento-mod'];
        $dscr_evento_mod=$_POST['dscr-evento-mod'];
        $link_evento_mod=$_POST['link-evento-mod'];

        modificareventosadm($id_evento_mod, $id_universidad_mod, $nombre_evento_mod, $inicio_evento_mod, $dscr_evento_mod, $link_evento_mod, $url_foto_mod, $pagdireccion);
        
    }

}

if(isset($_POST['ver-votdel-btn'])){

    if(!empty($_POST['dni-votar']) && !empty($_POST['cod-votar'])){
        
        $dni_delegado_votar = $_POST['dni-votar'];
        $cod_delegado_votar = $_POST['cod-votar'];

        $info_delegado = listarinfodelegadodni($dni_delegado_votar);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_delegado)) {
            $id_delegado =$row['id_delegado'];
            $puesto_delegado =$row['puesto_delegado'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){
            
            $codigo_delegado_tmp=$dni_delegado_votar*7*13*37;//para codificar el código

            $codigo_delegado_tmp = substr($codigo_delegado_tmp, -4);

            if($cod_delegado_votar == $codigo_delegado_tmp){
                
                unset($_SESSION['id_delegado']);
                unset($_SESSION['puesto_delegado']);
                
                $_SESSION["id_delegado"]=$id_delegado;
                $_SESSION["puesto_delegado"]=$puesto_delegado;
                
                $id_delegado_encriptado = doEncrypt($id_delegado);
                $puesto_delegado_encriptado = doEncrypt($puesto_delegado);
                                
                //echo "<script>window.location.href='lista_delegado_votacion.php?xi_1=$id_delegado_encriptado&xi_2=$puesto_delegado_encriptado';</script>";
                echo "<script>window.location.href='lista_delegado_votacion.php';</script>";

            }else{
                echo "<script> alert('Código de votación incorrecto.');</script>";
            }
            
        }else{
            echo "<script> alert('No se encuentra un delegado con el DNI brindado.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}

if(isset($_POST['ver-asisdel-btn'])){

    if(!empty($_POST['dni-asist']) && !empty($_POST['cod-asist'])){
        
        $dni_delegado_asist = $_POST['dni-asist'];
        $cod_delegado_asist = $_POST['cod-asist'];

        $info_asistencia = listarinfoasis($dni_delegado_asist, $cod_delegado_asist);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_asistencia)) {
            $id_delegado_asis =$row['id_delegado'];
            $id_asistencia_asis =$row['id_asistencia'];
            $estado_asistencia_asis =$row['estado_asistencia'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){

            if($estado_asistencia_asis > 0){

                $resultado_asi_agr = registrarasistenciadel($id_delegado_asis, $id_asistencia_asis);
                $cuenta_tmp = 0;

                if($resultado_asi_agr){
                    echo "<script> alert('Se registró la asistencia.');</script>";
                }else{
                    echo "<script> alert('Ha ocurrido un problema al registrar la asistencia. Inténtelo de nuevo, o comuníquese con algún representante de Aneic para solucionar el problema.');</script>";
                }

            }else{
                echo "<script> alert('La asistencia ya se ha cerrado.');</script>";
            }
            
        }else{
            echo "<script> alert('La asistencia no se ha abierto para usted, o el código brindado es incorrecto.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}



if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}


if (isset($_POST['codigo-recu'])){
	if(!empty($_POST['dni-recu'])){
		$dni_delegado_rec=$_POST['dni-recu'];

		$result=recuperarcontradel($dni_delegado_rec);

	}else{
        echo "<script> alert('Ingrese un DNI.');</script>";
    }
}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}    
    
?>
</body>
</html>