<?php 
session_start();
$sesioninic=0;
//validando las variables de sesion
    include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    echo "<script> window.location.href='index.php';</script>";
}
$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();


?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Administrar</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
</head>
<body style="height:100%">
<script type="text/javascript" src="js/jquery.min.js"></script>

<div id="header"></div>
    
<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Cambio de clave</span>
    </div>
</div>
<div class="container cont-adm" style="min-height: 100%;margin: -472px auto 0;">
    
    <div class="row" style="margin-top:25px;padding-top: 472px;">
        
        <div class="col-xs-12">

            <form class="form-horizontal" method="post">
              <div class="form-group">
                <label class="control-label col-xs-12 col-sm-4" for="cont-adm-ant">Contraseña anterior:</label>
                <div class="col-sm-8"> 
                  <input type="password" class="form-control" id="cont-adm-ant" name="cont-adm-ant" placeholder="Ingrese contraseña">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-xs-12 col-sm-4" for="cont-adm-nue">Contraseña nueva:</label>
                <div class="col-sm-8"> 
                  <input type="password" class="form-control" id="cont-adm-nue" name="cont-adm-nue" placeholder="Ingrese contraseña">
                </div>
              </div>
              <div class="form-group" id="contra-repetir">
                <label class="control-label col-xs-12 col-sm-4" for="cont-adm-nue2">Repita la contraseña nueva:</label>
                <div class="col-sm-8"> 
                  <input type="password" class="form-control" id="cont-adm-nue2" name="cont-adm-nue2" placeholder="Ingrese contraseña">
                    <span class="help-block" style="display:none">Las contraseñas no coinciden.</span>
                </div>
              </div>
              <div class="form-group"> 
                <div class="col-sm-offset-4 col-sm-8">
                  <button type="submit" class="btn btn-default" name="cabm-clave-btn" id="cabm-clave-btn" disabled>Cambiar</button>
                </div>
              </div>
            </form>
        
        </div>
        
    </div>
    
</div>

<div id="footer"></div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
    
<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 5, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<script>

var id_universidad = <?php if(isset($id_universidad)){ echo $id_universidad;}else{ echo "0"; }; ?>;
$("a.toscroll").on('click',function() {
    var url =$(this).attr("href");
    //console.log(url);
    var hash = url.substring(url.indexOf("#")+1);
    $('html, body').animate({
        scrollTop: $('#'+hash).offset().top -200
    }, 500);
    return false;
});
</script>
    
    
<?php
 
if (isset($_POST['ingresa-adm'])){

    if(!empty($_POST['correo-adm']) || !empty($_POST['cont-adm']) ){

        $correo_adm=$_POST['correo-adm'];
        $contrasena_adm=$_POST['cont-adm'];
        
        $pagdireccion = "adm-regdel.php";

        ingresa($correo_adm,$contrasena_adm,$pagdireccion);

    }

}

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if(isset($_POST['cabm-clave-btn'])){
    
    if(!empty($_POST['cont-adm-ant']) && !empty($_POST['cont-adm-nue'])){
        
        $contrasena_ant_camb = $_POST['cont-adm-ant'];
        $contrasena_nue_camb = $_POST['cont-adm-nue'];
        
        $estado_cambio_cont = cambiarcontrasenaadm($contrasena_ant_camb, $contrasena_nue_camb);

        if($estado_cambio_cont =="1"){
            echo "<script>alert('Hubo un error al cambiar las contraseñas. Por favor intenta más tarde.')</script>";
        }elseif($estado_cambio_cont == "2"){
            echo "<script>alert('La contraseña anterior no es correcta.')</script>";
        }else{
            echo "<script>alert('La contraseña se ha actualizado.')</script>";
        }
        
        
    }else{
        echo "<script>alert('Por favor ingrese los datos requeridos. ')</script>";
    }
}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}   
    
?>
</body>
</html>