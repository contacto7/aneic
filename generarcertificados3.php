<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    echo "<script> window.location.href='index.php';</script>";
}

if(!isset($_GET['xidevento']) && !isset($_GET['xcertificado_sub'])){
    echo "<script> window.location.href='generarcertificados.php';</script>";
}


$id_evento=$_GET['xidevento'];
$certificado_subido = $_GET['xcertificado_sub'];

$width_canvas = 870*2;
$height_canvas = 620*2;


$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();
?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Eventos</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body style="min-width:950px;">
<script type="text/javascript" src="js/jquery.min.js"></script>
    
<div id="header"></div>

<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Generar certificados</span>
    </div>
</div>
<div class="container">
    <h1 class="h2">
        <span>Paso 3</span>
    </h1>
    <h1 class="h4">
        <span>
            Subir el certificado firmado.
        </span>
    </h1>
</div>

<div class="container">
    <div class="row row-eventos">
        <div class="col-xs-12">
            <div class="canvas-cert-wrapp">
                <section class="formato-wrapp row">
                    <article class="formato-inn col-xs-12 col-sm-6">
                        <h2>Formato de nombre de participante</h2>
                        <section class="formato-unit-grp">
                            
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Tamaño:</label>
                                <div class="col-sm-8">
                                     <select class="form-control sel-unibydel" id="tam-form-nombre">
                                        <option class="sel-prov" value="10">10px</option>
                                        <option class="sel-prov" value="11">11px</option>
                                        <option class="sel-prov" value="12">12px</option>
                                        <option class="sel-prov" value="13">13px</option>
                                        <option class="sel-prov" value="14" selected>14px</option>
                                        <option class="sel-prov" value="15">15px</option>
                                        <option class="sel-prov" value="16">16px</option>
                                        <option class="sel-prov" value="17">17px</option>
                                        <option class="sel-prov" value="18">18px</option>
                                        <option class="sel-prov" value="19">19px</option>
                                        <option class="sel-prov" value="20">20px</option>
                                        <option class="sel-prov" value="22">22px</option>
                                        <option class="sel-prov" value="24">24px</option>
                                        <option class="sel-prov" value="26">26px</option>
                                        <option class="sel-prov" value="28">28px</option>
                                        <option class="sel-prov" value="30">30px</option>
                                        <option class="sel-prov" value="32">32px</option>
                                        <option class="sel-prov" value="34">34px</option>
                                        <option class="sel-prov" value="35">35px</option>
                                        <option class="sel-prov" value="36">36px</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Estilo:</label>
                                <div class="col-sm-8">
                                     <select class="form-control sel-unibydel" id="estilo-form-nombre">
                                        <option class="sel-prov" value="1">Normal</option>
                                        <option class="sel-prov" value="2">Italic</option>
                                        <option class="sel-prov" value="3">Negrita</option>
                                        <option class="sel-prov" value="4">Negrita y cursiva</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Fuente:</label>
                                <div class="col-sm-8">
                                    <select class="form-control sel-unibydel" id="fuente-form-nombre">
                                        <option class="sel-prov" value="monospace" selected>Monospace</option>
                                        <option class="sel-prov" value="sans-serif">San Serif</option>
                                        <option class="sel-prov" value="serif">Serif</option>
                                    </select>
                            
                
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Color:</label>
                                <div class="col-sm-8">
                                    <select class="form-control sel-unibydel" id="color-form-nombre">
                                        <option class="sel-prov" value="#000000" selected>Negro</option>
                                        <option class="sel-prov" value="#ffffff">Blanco</option>
                                        <option class="sel-prov" value="#132e73">Azul</option>
                                        <option class="sel-prov" value="#118a16">Verde</option>
                                        <option class="sel-prov" value="#c30909">Rojo</option>
                                    </select>
                            
                
                                </div>
                            </div>
                            
                        </section>
                    </article>
                    <article class="formato-inn col-xs-12 col-sm-6">
                        <h2>Formato de calidad de participante</h2>
                        <section class="formato-unit-grp">
                            
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Tamaño:</label>
                                <div class="col-sm-8">
                                     <select class="form-control sel-unibydel" id="tam-form-calidad">
                                        <option class="sel-prov" value="10">10px</option>
                                        <option class="sel-prov" value="11">11px</option>
                                        <option class="sel-prov" value="12">12px</option>
                                        <option class="sel-prov" value="13">13px</option>
                                        <option class="sel-prov" value="14" selected>14px</option>
                                        <option class="sel-prov" value="15">15px</option>
                                        <option class="sel-prov" value="16">16px</option>
                                        <option class="sel-prov" value="17">17px</option>
                                        <option class="sel-prov" value="18">18px</option>
                                        <option class="sel-prov" value="19">19px</option>
                                        <option class="sel-prov" value="20">20px</option>
                                        <option class="sel-prov" value="22">22px</option>
                                        <option class="sel-prov" value="24">24px</option>
                                        <option class="sel-prov" value="26">26px</option>
                                        <option class="sel-prov" value="28">28px</option>
                                        <option class="sel-prov" value="30">30px</option>
                                        <option class="sel-prov" value="32">32px</option>
                                        <option class="sel-prov" value="34">34px</option>
                                        <option class="sel-prov" value="35">35px</option>
                                        <option class="sel-prov" value="36">36px</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Estilo:</label>
                                <div class="col-sm-8">
                                     <select class="form-control sel-unibydel" id="estilo-form-calidad">
                                        <option class="sel-prov" value="1">Normal</option>
                                        <option class="sel-prov" value="2">Italic</option>
                                        <option class="sel-prov" value="3">Negrita</option>
                                        <option class="sel-prov" value="4">Negrita y cursiva</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Fuente:</label>
                                <div class="col-sm-8">
                                    <select class="form-control sel-unibydel" id="fuente-form-calidad">
                                        <option class="sel-prov" value="monospace" selected>Monospace</option>
                                        <option class="sel-prov" value="sans-serif">San Serif</option>
                                        <option class="sel-prov" value="serif">Serif</option>
                                    </select>
                                </div>
                            </div>
                            
                            
                            <div class="form-group row">
                                <label class="control-label col-sm-4">Color:</label>
                                <div class="col-sm-8">
                                    <select class="form-control sel-unibydel" id="color-form-calidad">
                                        <option class="sel-prov" value="#000000" selected>Negro</option>
                                        <option class="sel-prov" value="#ffffff">Blanco</option>
                                        <option class="sel-prov" value="#132e73">Azul</option>
                                        <option class="sel-prov" value="#118a16">Verde</option>
                                        <option class="sel-prov" value="#c30909">Rojo</option>
                                    </select>
                            
                
                                </div>
                            </div>
                            
                        </section>
                    </article>
                
                    <article class="formato-inn col-xs-12">
                        <h2>Formato de código QR</h2>
                        <section class="formato-unit-grp">
                            
                            <div class="form-group row">
                                <label class="control-label col-sm-2">Tamaño:</label>
                                <div class="col-sm-3">
                                    <input class="form-control" type="number" placeholder="Ingrese el tamaño del código" id="tam-form-qr">
                                </div>
                            </div> 
                        </section>
                    </article>
                
                
                </section>
            
                <div class="canvas-cert-inn">
                    <div class="elemento-drag nombr-participante" style="text-align:center;">
                        <span style="left:-50%;position:relative">NOMBRE DE PARTICIPANTE</span>
                    </div>                    
                    <div class="elemento-drag calidad-participante" style="text-align:center;">
                        <span style="left:-50%;position:relative">CALIDAD DE PARTICIPANTE</span>
                    </div>                  
                    <div class="elemento-drag cert-participante">
                        <img class="img-qr-cert" src="img/qr_ejemplo.png">
                    </div>
                    <canvas id="canvas_certificado" width="<?php echo $width_canvas; ?>" height="<?php echo $height_canvas; ?>"></canvas>
                </div>
            </div>
        </div>
        <div class="col-xs-12" style="padding-top: 15px;">
            <a class="btn btn-md btn-info subir-cert" id="link-sgte" <?php if($certificado_subido){ echo "href='#subir-cert-mod' data-target='#subir-cert-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin'"; }else{ echo "href='#'"; } ?>>Subir</a>
            <a class="btn btn-md btn-info dwn-cert" id="link-gen" <?php if($certificado_subido){ echo "href='#dwn-cert-mod' data-target='#dwn-cert-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin'"; }else{ echo "href='#'"; } ?> style="display:none">Generar lista de certificados</a>
            <div style="" id="dwn-botones-wrapp"></div>
            <a class="btn btn-warning cargar-nuev-cert" href="#hardcore" onclick="location.reload()">Cargar de nuevo</a>
        </div>
    </div>
</div>
<div class="">
    <div id="append-certs">
    
    
    </div>    
    
</div>
<div id="footer" style="margin-top:50px;"></div>
    
<div id="subir-cert-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Subir certificados</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="subcert-modbod-msg">Cargando...</div>
                <div class="vota-modbod-btn">
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>     
<div id="dwn-cert-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Subir certificados</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="dwncert-modbod-msg">Cargando...</div>
                <div class="vota-modbod-btn">
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div> 
    
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery.event.drag-2.0.js"></script>
<script type="text/javascript" src="js/draggable.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/html2canvas.min.js"></script>
<script type="text/javascript" src="js/jquery.qrcode.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 2, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
<script>
    
var myCanvas = document.getElementById('canvas_certificado');
var ctx = myCanvas.getContext('2d');
var img = new Image;
img.onload = function(){
ctx.drawImage(img, 0, 0, img.width,    img.height,     // source rectangle
                   0, 0, myCanvas.width, myCanvas.height); // destination rectangle
};
img.src = "<?php echo $certificado_subido; ?>";

//VARIABLE DE DESCARGABLES
    
var cant_cert_descargables = 100;
var temp_inicio = 0;
var listar_botones = 1;    
var certificado_index = 1;

//VARIABLES DE NOMBRE
var left_nombre = 0;
var top_nombre = 0;
var tamano_nombre = "14";
var estilo_nombre = "1";
var fuente_nombre = "monospace";   
var color_nombre = "#000";    
    
//VARIABLES DE CALIDAD
var left_calidad = 0;
var top_calidad = 0;
var tamano_calidad = "14";
var estilo_calidad = "1";
var fuente_calidad = "monospace";  
var color_calidad = "#000";    
    
//VARIABLES DE QR
var left_qr = 0;
var top_qr = 0;
var tamano_qr = "70"; 

//AJAX//
    

    
$(document).on("click", '.subir-cert',function(){
    
    //VARIABLES DE NOMBRE
    var i1_1 = parseInt(left_nombre);
    var i1_2 = parseInt(top_nombre);
    var i1_3 = tamano_nombre;
    var i1_4 = estilo_nombre;
    var i1_5 = fuente_nombre;
    var i1_6 = color_nombre;
    
    //VARIABLES DE CALIDAD
    var i2_1 = parseInt(left_calidad);
    var i2_2 = parseInt(top_calidad);
    var i2_3 = tamano_calidad;
    var i2_4 = estilo_calidad;
    var i2_5 = fuente_calidad;
    var i2_6 = color_calidad;
    
    //VARIABLES DE QR
    var i3_1 = parseInt(left_qr);
    var i3_2 = parseInt(top_qr);
    var i3_3 = tamano_qr;
    
    $(".subcert-modbod-msg").html("Cargando...");
    
    $.ajax({
        url:'apost_subir_certificado.php',
        type:'post',
        data:{
            'x_i1': i1_1,
            'x_i2': i1_2,
            'x_i3': i1_3,
            'x_i4': i1_4,
            'x_i5': i1_5,
            'x_i6': i1_6,
            'x_i7': i2_1,
            'x_i8': i2_2,
            'x_i9': i2_3,
            'x_i10': i2_4,
            'x_i11': i2_5,
            'x_i12': i2_6,
            'x_i13': i3_1,
            'x_i14': i3_2,
            'x_i15': i3_3,
            'x_i16': <?php echo $id_evento; ?>
        },
        success: function (result) {
            
            if(result.includes("error")){
                $(".subcert-modbod-msg").html("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
            }else{
                
                $(".subcert-modbod-msg").html(result);
                $("#link-sgte").hide();
                $("#link-gen").show();
            }
            
            
        }
    });
});    
    
$(document).on("click", '.dwn-cert',function(){
    
    //VARIABLES DE NOMBRE
    var i1_1 = parseInt(left_nombre);
    
    $(".dwncert-modbod-msg").html("Cargando...");
    
    $.ajax({
        url:'apost_descargar_certificados.php',
        type:'post',
        data:{
            'x_i1': <?php echo $id_evento; ?>,
            'x_i2': cant_cert_descargables,
            'x_i3': temp_inicio,
        },
        success: function (result) {
            
            if(result.includes("error")){
                $(".dwncert-modbod-msg").html("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
            }else{
                $(".dwncert-modbod-msg").html("Se generó lista de certificados");

                $("#append-certs").html(result);
                
                $("#link-gen").hide();
                
                var num_cert = $('.canvas-adm-cert').length;
                
                if(listar_botones){
                    var btn_cert_dwn = '<a class="btn btn-md btn-info dwn2-cert" id="link-dwn" style="cursor: pointer;">Descargar certificados desde ' + (temp_inicio + 1 ) + ' hasta '+ (temp_inicio + num_cert ) +'</a>';
                    $("#dwn-botones-wrapp").html(btn_cert_dwn);
                    temp_inicio += cant_cert_descargables;
                   
                }else{
                    $("#dwn-botones-wrapp").html("<div class='ult-listcert-msg'>No hay más certificados.</div>");
               }


                if(num_cert < cant_cert_descargables){
                    listar_botones = 0;
                }

            }

            

        }
    });
});
    

    
$(document).on("click", '.dwn2-cert',function(){
    
    let index = 0;
    renderHtmlIntoCanvas(index);
    
});
    
function renderHtmlIntoCanvas(index){
    

    var nombre = $("#canvas-"+index+" .nombr-participante span").html();
    html2canvas($("#canvas-dwn-"+index).get(0)).then(canvas => {

         var url = canvas.toDataURL();
          $("<a>", {
            href: url,
            download: certificado_index+"-"+nombre+".png"
          })
          .on("click", function() {$(this).remove()})
          .appendTo("body")[0].click();
                
        console.log("Index: "+index);
        ++index;
        ++certificado_index;   
        if(index < cant_cert_descargables){
             renderHtmlIntoCanvas(index);
            
         }else{
            console.log("Acabó en index: "+index);
            ajaxCreateNewCerts();
         }
        

    });
    console.log(cant_cert_descargables);
    console.log(temp_inicio);
    
}
    
    
    
function ajaxCreateNewCerts(){
    
    $.ajax({
        url:'apost_descargar_certificados.php',
        type:'post',
        data:{
            'x_i1': <?php echo $id_evento; ?>,
            'x_i2': cant_cert_descargables,
            'x_i3': temp_inicio,
        },
        success: function (result) {

            if(result.includes("error")){
                $("#dwn-botones-wrapp").html("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
            }else{


                $("#append-certs").html(result);
                    
                $("#link-gen").hide();
                
                var num_cert = $('.canvas-adm-cert').length;
                
                if(listar_botones){
                    var btn_cert_dwn = '<a class="btn btn-md btn-info dwn2-cert" id="link-dwn" style="cursor: pointer;">Descargar certificados desde ' + (temp_inicio + 1 ) + ' hasta '+ (temp_inicio + num_cert ) +'</a>';
                    $("#dwn-botones-wrapp").html(btn_cert_dwn);
                    temp_inicio += cant_cert_descargables;
                   
                }else{
                    $("#dwn-botones-wrapp").html("<div class='ult-listcert-msg'>No hay más certificados.</div>");
               }


                if(num_cert < cant_cert_descargables){
                    listar_botones = 0;
                }

            }



        }
    });
}
    
    
//END AJAX//
    
$( function() {
    $( ".nombr-participante" ).draggable({
        stop: function(event, ui) {

            // Show dropped position.
            var Stoppos = $(this).position();
            left_nombre = Stoppos.left;
            top_nombre = Stoppos.top;
            console.log("STOP NOMBRE: \nLeft: "+ left_nombre + "\nTop: " + top_nombre);
        }
    });
    $( ".calidad-participante" ).draggable({
        stop: function(event, ui) {

            // Show dropped position.
            var Stoppos = $(this).position();
            left_calidad = Stoppos.left;
            top_calidad = Stoppos.top;
            console.log("STOP CALIDAD: \nLeft: "+ left_calidad + "\nTop: " + top_calidad);
        }
    });
    $( ".cert-participante" ).draggable({
        stop: function(event, ui) {

            // Show dropped position.
            var Stoppos = $(this).position();
            left_qr = Stoppos.left;
            top_qr = Stoppos.top;
            console.log("STOP CALIDAD: \nLeft: "+ left_qr + "\nTop: " + top_qr);
        }
    });
} );

//NOMBRE
$("#tam-form-nombre").change(function() {
    tamano_nombre = $(this).val();
    var fontccseable = tamano_nombre+"px";
    $(".nombr-participante").css({'font-size': fontccseable});
    console.log("Tamaño nombre: " + tamano_nombre);
});

$("#estilo-form-nombre").change(function() {
    estilo_nombre = $(this).val();
    var fontccseable ="";
    if(estilo_nombre == 1){
        fontccseable = "normal";
        $(".nombr-participante").css({'font-weight': 100});
        $(".nombr-participante").css({'font-style': fontccseable});
    }else if(estilo_nombre == 2){
        fontccseable = "italic";
        $(".nombr-participante").css({'font-weight': 100});
        $(".nombr-participante").css({'font-style': fontccseable});
    }else if(estilo_nombre == 3){
        fontccseable = "bold";
        $(".nombr-participante").css({'font-style': 'normal'});
        $(".nombr-participante").css({'font-weight': fontccseable});
    }else if(estilo_nombre == 4){
        fontccseable = "negrita y cursiva";
        $(".nombr-participante").css({'font-style': 'italic'});
        $(".nombr-participante").css({'font-weight': 'bold'});
    }
    console.log("Estilo nombre: " + fontccseable);
});
    
$("#fuente-form-nombre").change(function() {
    fuente_nombre = $(this).val();
    var fontccseable = fuente_nombre;
    $(".nombr-participante").css({'font-family': fontccseable});
    console.log("Fuente nombre: " + fontccseable);
});
    
$("#color-form-nombre").change(function() {
    color_nombre = $(this).val();
    var fontccseable = color_nombre;
    $(".nombr-participante").css({'color': fontccseable});
    console.log("Color nombre: " + fontccseable);
});
    
    
//CALIDAD
$("#tam-form-calidad").change(function() {
    tamano_calidad = $(this).val();
    var fontccseable = tamano_calidad+"px";
    $(".calidad-participante").css({'font-size': fontccseable});
    console.log("Tamaño calidad: " + fontccseable);
});

$("#estilo-form-calidad").change(function() {
    estilo_calidad = $(this).val();
    var fontccseable ="";
    if(estilo_calidad == 1){
        fontccseable = "normal";
        $(".calidad-participante").css({'font-weight': 100});
        $(".calidad-participante").css({'font-style': fontccseable});
    }else if(estilo_calidad == 2){
        fontccseable = "italic";
        $(".calidad-participante").css({'font-weight': 100});
        $(".calidad-participante").css({'font-style': fontccseable});
    }else if(estilo_calidad == 3){
        fontccseable = "bold";
        $(".calidad-participante").css({'font-style': 'normal'});
        $(".calidad-participante").css({'font-weight': fontccseable});
    }else if(estilo_calidad == 4){
        fontccseable = "negrita y cursiva"
        $(".calidad-participante").css({'font-style': 'italic'});
        $(".calidad-participante").css({'font-weight': 'bold'});
    }
    console.log("Estilo calidad: " + fontccseable);
});
    
$("#fuente-form-calidad").change(function() {
    fuente_calidad = $(this).val();
    var fontccseable = fuente_calidad;
    $(".calidad-participante").css({'font-family': fontccseable});
    console.log("Fuente calidad: " + fontccseable);
});
    
$("#color-form-calidad").change(function() {
    color_calidad = $(this).val();
    var fontccseable = color_calidad;
    $(".calidad-participante").css({'color': fontccseable});
    console.log("Color calidad: " + fontccseable);
});
    
$("#tam-form-qr").on( "change input", function() {
    tamano_qr = $(this).val();
    var fontccseable = tamano_qr+"px";
    $(".img-qr-cert").css({'width': fontccseable});
    console.log("Tamaño qr: " + fontccseable);
});
    
    
</script>
    
<?php

if(isset($_POST['importSubmit'])){
    //validate whether uploaded file is a csv file
    $csvMimes = array('image/gif','image/png' ,'image/jpg','image/jpeg','image/bmp','image/ico','image/apng','image/xbm','image/webp','image/svg','image/GIF','image/PNG' ,'image/JPG', 'image/JPEG','image/BMP','image/ICO','image/APNG','image/XBM','image/WEBP','image/SVG');
    
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
        
        
        $filename = $_FILES['file']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        
        $nuevo_nombre=randomalfanume(25);
        $nuevo_nombre= $nuevo_nombre.".".$ext;
        $url_foto= "fotos_certificado/".$nuevo_nombre;
        
        $image_temp_name=$_FILES['file']['tmp_name'];
        
        if(move_uploaded_file($image_temp_name,$url_foto)){

            $id_evento_cert = $id_evento;
            $id_cert_foto = anadircertificadoevento($id_evento_cert, $url_foto);
        
            if($id_cert_foto =="Error"){
                echo "<script>alert('Hubo un error al subir el certificado. Por favor intenta más tarde.')</script>";
                exit();
            }else{
                //echo "<script>console.log('".$_FILES['file']['type']."')</script>";
                echo "<script> window.location.href='generarcertificados2.php?xidevento=$id_evento&xcertificado_sub=$url_foto';</script>";
            }
            
            
        }else{
            echo "<script>alert('Hubo un error al cargar la imagen. Por favor intenta más tarde.')</script>";
            exit();
        }
        
        
    }else{
        echo "<script>alert('Por favor suba una imagen. ')</script>";
    }
}
 

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}    
    
?>
</body>
</html>