<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    echo "<script> window.location.href='index.php';</script>";
}

$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();

//Lista de asistencias

$lista_asistencias = listarasistencias();

$asistencias_html="";
$asistencias_html.="<select class='form-control sel-unibydel' name='asistencia-vota' id='asistencia-vota'>";
$asistencias_html.="<option class='sel-prov' value='0'>No asociada</option>";

while ($row=mysqli_fetch_assoc($lista_asistencias)) {
    $id_asistencia_list =$row['id_asistencia'];
    $descripcion_asistencia_list =$row['descripcion_asistencia'];
    $asistencias_html.="<option class='sel-prov' value='$id_asistencia_list'>$descripcion_asistencia_list</option>";
    
}
$asistencias_html.="</select>";
mysqli_free_result($lista_asistencias);


//Tabla de votaciones
$lista_votaciones = listarvotaciones();

$tablas_html="";
$tablas_html.="</tbody></table>";
$tablas_html.="<table class='table table-striped tabla-vot-list'><thead><tr><th>Elección</th><th>Fecha de Publicación</th><th>Estado</th><th>Asistencia</th><th>Acción</th></tr></thead><tbody>";
$estado_texto = "Cerrada";
$btn_accion = "";
while ($row=mysqli_fetch_assoc($lista_votaciones)) {
    $id_votacion =$row['id_votacion'];
    $fecha_votacion =$row['fecha_votacion'];
    $estado_votacion =$row['estado_votacion'];
    $id_asistencia =$row['id_asistencia'];
    $vota_delegados = $row['vota_delegados'];
    $vota_subdelegados =$row['vota_subdelegados'];
    $vota_accesitario1 =$row['vota_accesitario1'];
    $vota_accesitario2 =$row['vota_accesitario2'];
    $vota_miembros_hon =$row['vota_miembros_hon'];
    $vota_secretario_corede =$row['vota_secretario_corede'];
    $vota_consejo_direct =$row['vota_consejo_direct'];
    $votacion_descripcion =$row['votacion_descripcion'];
    
    //Encriptando votacion
    $id_votacion_encriptado= doEncrypt($id_votacion);
    $votacion_descripcion_encriptado= doEncrypt($votacion_descripcion);
    
    //Asistencia
    $descripcion_asistencia =$row['descripcion_asistencia'];
    if(!$descripcion_asistencia){
        $descripcion_asistencia = "No asociada";
    }
    
    if($estado_votacion == 0){
        $estado_texto = "Cerrada";
        $btn_accion = "<a class='btn btn-sm btn-info' href='adm_votacion_resultados.php?x_i_1=$votacion_descripcion_encriptado&x_i_2=$id_votacion_encriptado'>Ver resultado</a>";
    }elseif($estado_votacion == 1){
        $btn_accion = "<button class='btn btn-sm btn-danger cerrar-vota'   href='#cerrar-vota-mod' data-target='#cerrar-vota-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id='$id_votacion_encriptado' data-dscr='$votacion_descripcion'>Cerrar</button>";
        
        $estado_texto = "Abierta";
    }else{
        $btn_accion = "<button class='btn btn-sm btn-success publicar-vota'   href='#publi-vota-mod' data-target='#publi-vota-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id='$id_votacion_encriptado' data-dscr='$votacion_descripcion'>Publicar</button>";
        
        $votacion_descripcion =  "<a class='link-edit-vota' href='adm_votacion_editar.php?x_i_1=$votacion_descripcion_encriptado&x_i_2=$id_votacion_encriptado'><span class='glyphicon glyphicon-pencil'></span> $votacion_descripcion</a>";
        $estado_texto = "Sin publicar";
    }
    
    
    $fecha_lista = date_create($fecha_votacion);
    $fecha_lista = date_format($fecha_lista,"d/m/Y");
        
        
    $tablas_html.="<tr><td>$votacion_descripcion</td><td>$fecha_lista</td><td>$estado_texto</td><td>$descripcion_asistencia</td><td>$btn_accion</td></tr>";

}

$tablas_html.="</tbody></table>";
mysqli_free_result($lista_votaciones);
desconectar();
?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Eventos</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body style="min-width:450px;height:100%">
<script type="text/javascript" src="js/jquery.min.js"></script>
    
<div id="header"></div>

<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Sistema de votación</span>
    </div>
</div>

<div class="container cont-eventos" style="min-height: 100%;margin: -472px auto 0;">
    <div class="row" style="padding-top: 472px;">
        <?php echo $tablas_html; ?>
    </div>
    <div class="agregar-vot-wrap">
        <a class="agregar-votacion-adm" data-toggle="modal" data-target="#agregar-votacion"><span class="glyphicon glyphicon-plus"></span> Agregar votación</a>
    </div>
</div>
    
<div id="footer" style="margin-top:50px;"></div>

<div id="agregar-votacion" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar Votación</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-registdel" role="form" action="" method="post">
            <div class="form-group row">
              <label for="descr-votacion" class="col-sm-2 col-form-label">Descripcion</label>
              <div class="col-sm-10">
                <input class="form-control" type="text" placeholder="Ingrese el nombre de la votación" name="descr-votacion" id="descr-votacion">
              </div>
            </div>
            <div class="form-group row">
              <label for="cargo-delegado" class="col-sm-2 col-form-label">Asistencia</label>
              <div class="col-sm-10">
                <?php echo $asistencias_html; ?>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Votan:</label>
            </div>
            
            
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="del-vot" id="del-vot">
              </div>
              <label for="del-vot" class="col-xs-11 col-form-label">Delegados</label>
            </div>
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="subdel-vot" id="subdel-vot">
              </div>
              <label for="subdel-vot" class="col-xs-11 col-form-label">Sub-delegados</label>
            </div>
            
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="acce1-vot" id="acce1-vot">
              </div>
              <label for="acce1-vot" class="col-xs-11 col-form-label">Accesitario 1</label>
            </div>
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="acce2-vot" id="acce2-vot">
              </div>
              <label for="acce2-vot" class="col-xs-11 col-form-label">Accesitario 2</label>
            </div>
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="miembhon-vot" id="miembhon-vot">
              </div>
              <label for="miembhon-vot" class="col-xs-11 col-form-label">Miembros Honorarios</label>
            </div>
            
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="secreta-vot" id="secreta-vot">
              </div>
              <label for="secreta-vot" class="col-xs-11 col-form-label">Secretario Coredes</label>
            </div>
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="consejo-vot" id="consejo-vot">
              </div>
              <label for="consejo-vot" class="col-xs-11 col-form-label">Consejo Directivo</label>
            </div>
			
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="subsecreta-vot" id="subsecreta-vot">
              </div>
              <label for="subsecreta-vot" class="col-xs-11 col-form-label">Sub Secretario de Corede</label>
            </div>
			
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="embajador-vot" id="embajador-vot">
              </div>
              <label for="embajador-vot" class="col-xs-11 col-form-label">Emabajador Internacional</label>
            </div>			
            
            

             <div class="form-group row">
                  <div class="col-xs-12 col-mod-btn">
                    <button type="submit" class="btn btn-ag-vot btn-sm" name="agre-vot-sub">Agregar</button>
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal">Cerrar</button>
                  </div>
             </div>
          </form>
          
          
      </div>
    </div>

  </div>
</div>
    
 
    
<div id="publi-vota-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Publicar votación</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea publicar la votación: " <span class="vota-dscr"></span>"?</div>
                <div class="mensaje-vota-pub"></div>
                <div class="vota-modbod-btn">
                    <button type="button" class="btn btn-sm btn-warning publi-vota-btn" data-id="0">Si</button>
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                </div>
            </div>

        </div>
    </div>
</div> 
    
<div id="cerrar-vota-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Cerrar votación</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea cerrar la votación: " <span class="vota-dscr"></span>"?</div>
                <div class="mensaje-vota-cerrar"></div>
                <div class="vota-modbod-btn">
                    <button type="button" class="btn btn-sm btn-warning cerrar-vota-btn" data-id="0">Si</button>
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                </div>
            </div>

        </div>
    </div>
</div> 
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
    
<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 4, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<script>

    
$(document).on("click", '.publi-vota-btn',function(){
    
    var i1_1 = $(this).attr('data-id');
    
    $(".mensaje-vota-pub").html("Publicando...");
    
    $.ajax({
        url:'apost_publicar_votacion.php',
        type:'post',
        data:{
            'x_i1': i1_1
        },
        success: function (result) {
            $('#publi-vota-mod').modal('hide');
            if(result == 1){
                //console.log(result);
                alert("Ocurrió algún problema al publicar la votación. Si este inconveniente persiste, comuníquese con servicio técnico.");
            }else if(result == 2){
                alert("Se publicó la votación con éxito.");
                location.reload();
            }else if(result == 3){
                alert("El sistema de votación no ha identificado el dominio como Aneic. Por favor, contáctese con servicio técnico.");
            }else{

            }
            $(".mensaje-vota-pub").html("");
            
        }
    });


});
    
$(document).on("click", '.cerrar-vota-btn',function(){
    
    var i1_1 = $(this).attr('data-id');
    
    $(".mensaje-vota-cerrar").html("Cerrando...");
    
    $.ajax({
        url:'apost_cerrar_votacion.php',
        type:'post',
        data:{
            'x_i1': i1_1
        },
        success: function (result) {
            $('#cerrar-vota-mod').modal('hide');
            if(result == 1){
                //console.log(result);
                alert("Ocurrió algún problema al cerrar la votación. Si este inconveniente persiste, comuníquese con servicio técnico.");
            }else if(result == 2){
                alert("Se cerró la votación con éxito.");
                location.reload();
            }else if(result == 3){
                alert("El sistema de votación no ha identificado el dominio como Aneic. Por favor, contáctese con servicio técnico.");
            }else{

            }
            $(".mensaje-vota-cerrar").html("");
            
        }
    });


});
    
$(document).on("click", '.publicar-vota',function(){
    var dscr_vota = $(this).attr('data-dscr');
    var id_vota = $(this).attr('data-id');
    
    $(".vota-dscr").html(dscr_vota);
    $(".publi-vota-btn").attr('data-id',id_vota);

});   
$(document).on("click", '.cerrar-vota',function(){
    var dscr_vota = $(this).attr('data-dscr');
    var id_vota = $(this).attr('data-id');
    
    $(".vota-dscr").html(dscr_vota);
    $(".cerrar-vota-btn").attr('data-id',id_vota);

});    
    
    
</script>
<?php

if (isset($_POST['agre-vot-sub'])){

    if(!empty($_POST['descr-votacion'])){
        
        $votacion_descripcion_nue = $_POST['descr-votacion'];
        $id_asistencia_nue = $_POST['asistencia-vota'];
        
        $vota_delegados = 0;
        $vota_subdelegados = 0;
        $vota_accesitario1 = 0;
        $vota_accesitario2 = 0;
        $vota_miembros_hon = 0;
        $vota_secretario_corede = 0;
        $vota_consejo_direct = 0;
        $vota_subscreta = 0;
		$vota_embajador = 0;
        
        if(isset($_POST['del-vot'])){
            $vota_delegados = 1;
        }
        if(isset($_POST['subdel-vot'])){
            $vota_subdelegados = 1;
        }
        if(isset($_POST['acce1-vot'])){
            $vota_accesitario1 = 1;
        }
        if(isset($_POST['acce2-vot'])){
            $vota_accesitario2 = 1;
        }
        if(isset($_POST['miembhon-vot'])){
            $vota_miembros_hon = 1;
        }
        if(isset($_POST['secreta-vot'])){
            $vota_secretario_corede = 1;
        }
        if(isset($_POST['consejo-vot'])){
            $vota_consejo_direct = 1;
        }
        if(isset($_POST['subsecreta-vot'])){
            $vota_subscreta = 1;
        }
        if(isset($_POST['embajador-vot'])){
            $vota_embajador = 1;
        }
        
        
        $id_votacion = anadirvotacion($votacion_descripcion_nue, $id_asistencia_nue, $vota_delegados, $vota_subdelegados, $vota_accesitario1, $vota_accesitario2, $vota_miembros_hon, $vota_secretario_corede, $vota_consejo_direct,$vota_subscreta,$vota_embajador);
        
        if($id_votacion =="Error"){
            echo "<script>alert('Hubo un error al añadir la votación. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script> window.location.href='adm_votacion_lista.php';</script>";
        }
    }else{

        echo "<script>alert('Ingrese la descripción de la votación.')</script>";
    }

}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}   
?>
</body>
</html>