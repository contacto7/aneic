<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    echo "<script> window.location.href='index.php';</script>";
}

if(!isset($_GET['xiduniv']) && !isset($_GET['xidevento'])){
    echo "<script> window.location.href='generarcodigos.php';</script>";
}

if(isset($_GET['xprimer_id_cert'])){
    $primer_idcert_insertado = $_GET['xprimer_id_cert'];
    $primer_id_insertado = $_GET['xprimer_id'];
}elseif(isset($_GET['xprimer_id'])){
    $primer_id_insertado = $_GET['xprimer_id'];
    $primer_idcert_insertado = 0;
}else{
    $primer_id_insertado = 0;
    $primer_idcert_insertado = 0;
}
$id_universidad=$_GET['xiduniv'];
$id_evento=$_GET['xidevento'];

$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();
?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Eventos</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body style="min-width:450px;height:100%">
<script type="text/javascript" src="js/jquery.min.js"></script>
    
<div id="header"></div>

<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Generar Códigos</span>
    </div>
</div>
<div class="container">
    <h1 class="h2">
        <span>Paso 3</span>
    </h1>
    <h1 class="h4">
        <span>
            Consulte con el encargado para realizar el registro.
        </span>
        <span style="display:none">
            Descargue la siguiente guía de registro de asistentes y certificados y siga los pasos:
            <a href="documentoej/guia%20de%20registro.pdf" dowload="" target="_blank">
                <span class="glyphicon glyphicon-info-sign"></span> Guía de registro
            </a>
        </span>
        <div class="link-docu">
            <a href="documentoej/asistentes.csv" dowload="">
                <span class="glyphicon glyphicon-circle-arrow-down"></span> ASISTENTES
            </a>
        </div>
        <div class="link-docu">
            <a href="documentoej/certificados.csv" dowload="">
                <span class="glyphicon glyphicon-circle-arrow-down"></span> CERTIFICADOS
            </a>
        </div>
    </h1>
</div>

<div class="container cont-eventos" style="min-height: 100%;margin: -472px auto 0;">
    <div class="row row-eventos" style="padding-top: 472px;">
        <div class="col-xs-12">REGISTRO DE ASISTENTES <?php if($primer_id_insertado > 0){ echo "<span class='glyphicon glyphicon-ok' style='color:#008000'></span>"; } ?></div>
        <div class="col-xs-12">
            <form action="" method="post" enctype="multipart/form-data" id="importFrm">
                <label for="file"><a class="btn btn-md form-control input-sm selec-arch">Seleccionar archivo</a></label>
                <div id="nombrefoto" style="display:inline-block"></div>
                  <input type="file" class="archivo-subir" id="file" name="file" style="display:none" data-id="1">
                <span class="span-foto-estado" id="estado-cambio-1"></span>
                <input type="submit" class="btn btn-primary" name="importSubmit" value="Subir" style="display:inline-block">
            </form>
        </div>
        <div class="col-xs-12" style="padding-top: 15px;">REGISTRO DE CERTIFICADOS <?php if($primer_idcert_insertado > 0){ echo "<span class='glyphicon glyphicon-ok' style='color:#008000'></span>"; } ?></div>
        <div class="col-xs-12">
            <form action="" method="post" enctype="multipart/form-data" id="importFrm2">
                <label for="file2"><a class="btn btn-md form-control input-sm selec-arch">Seleccionar archivo</a></label>
                <div id="nombrefoto" style="display:inline-block"></div>
                  <input type="file" class="archivo-subir" id="file2" name="file2" style="display:none" data-id="2">
                <span class="span-foto-estado" id="estado-cambio-2"></span>
                <input type="submit" class="btn btn-primary" name="importSubmit2" value="Subir" style="display:inline-block">
            </form>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-8 col-lg-9" style="padding-top: 15px;">
            <a class="btn btn-md btn-info" id="link-sgte" href="<?php if($primer_id_insertado > 0 && $primer_idcert_insertado > 0){ echo 'generarcodigos4.php?xiduniv='.$id_universidad.'&xidevento='.$id_evento.'&xprimer_id='.$primer_id_insertado.'&xprimer_id_cert='.$primer_idcert_insertado; }else{ echo "#"; } ?>">Siguiente</a>
        </div>
    </div>
</div>

<div id="footer"></div>
    
<?php if($sesioninic){ ?>
    
<div id="andir-evento-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Cambiar Fotografía</h4>
            </div>
            <div class="modal-body modbod-exp">
                 <form class="form-horizontal form-datoper" role="form" action="" method="post" enctype="multipart/form-data">
                     <div class="row form-datper-left">
                        <div class="col-xs-12">
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="nombre-evento">Nombre de evento:</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="nombre-evento" name="nombre-evento" placeholder="Ingrese el nombre del evento">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="inicio-evento">Inicio de evento<br>(XX de Mes):</label>
                            <div class="col-sm-8">
                                <div class='input-group date' style="display:block" id='datetimepicker7'>
                                    <input type="text" class="form-control inp-sevolunt" id="inicio-evento" name="inicio-evento" placeholder="Inicio de evento" required style="border-radius:4px;">
                                    <span class="input-group-addon" id="btn-dp-1" style="visibility: hidden;position: relative;top: -60px;">
                                        <span class="fa fa-calendar" id="btn-dp-2" >
                                        </span>
                                    </span>
                                </div>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="dscr-evento">Descripción de evento:</label>
                            <div class="col-sm-8">
                                <textarea rows="2" style="width:100%" class="form-control" name="dscr-evento" id="dscr-evento"></textarea>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="link-evento">Link de evento (Facebook):</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" id="link-evento" name="link-evento" placeholder="Ingrese el link del evento">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="link-evento">Imagen de evento:</label>
                            <div class="col-sm-8">
                                  <label for="foto-evento"><a class="btn btn-md form-control input-sm selec-arch-mod">Selecciona archivo</a></label>
                                <div id="nombrefoto"></div>
                                  <input type="file" class="archivo-subir" id="foto-evento" name="foto-evento" style="display:none" data-id="1">
                                <span class="span-foto-estado" id="estado-cambio-1"></span>
                            </div>
                          </div>

                        </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-12 datpersbut-wrap">
                              <div class="datpersbut-mod">
                                <button type="submit" class="btn btn-datper-mod btn-sm" name="evento-adm-sub" >Guardar</button>
                                <button type="button" class="btn btn-datper-canc btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                             </div>
                         </div>
                     </div>
                </form>
            </div>

        </div>
    </div>
</div>
    
<?php } ?>
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script src="js/jquery.carouFredSel.js"></script>
<script src="js/moment-with-locales.js"></script>
<script src="js/bootstrap-datetimepicker.js"></script>
<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 2, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
<script>

$(function () {
    $('#datetimepicker7').datetimepicker({
        viewMode: 'years',
        format:'YYYY-MM-DD',
        locale: 'es'
    });
});
$("#id-evento").change(function() {
    if($(this).val() != 0){
        var linksgte= "generarcodigos2.php?xid="+$(this).val();
    }else{
        var linksgte= "#";
    }

    $("#link-sgte").attr("href",linksgte);
});
    
</script>
    
<?php

if (isset($_POST['evento-adm-sub'])){

    if($image_size=$_FILES['foto-evento']['size']>0 || !empty($_POST['nombre-evento']) || !empty($_POST['inicio-evento']) || !empty($_POST['dscr-evento']) || !empty($_POST['link-evento'])){

        $allowed =  array('gif','png' ,'jpg','GIF','PNG' ,'JPG');
        $filename = $_FILES['foto-evento']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!in_array($ext,$allowed) ) {
            echo "<script>alert('Por favor selecciona una imagen')</script>";
            exit();
        }else{
            $nuevo_nombre=randomalfanume(25);
            $nuevo_nombre= $nuevo_nombre.".".$ext;
            $url_foto= "foto_evento_cuadro/".$nuevo_nombre;
            
            $image_temp_name=$_FILES['foto-evento']['tmp_name'];
            if(move_uploaded_file($image_temp_name,$url_foto)){
        
                $pagdireccion = "generarcodigos2.php?xiduniv=".$id_universidad;

                $nombre_evento_mod=$_POST['nombre-evento'];
                $inicio_evento_mod=$_POST['inicio-evento'];
                $dscr_evento_mod=$_POST['dscr-evento'];
                $link_evento_mod=$_POST['link-evento'];

                anadireventosadm($nombre_evento_mod, $inicio_evento_mod, $dscr_evento_mod, $link_evento_mod, $url_foto, $pagdireccion);

                
            }else{
                echo "<script>alert('Hubo un error al cargar la imagen. Por favor intenta más tarde.')</script>";
                exit();
            }
            
            
        }

    }

}
    
if(isset($_POST['importSubmit'])){
    //validate whether uploaded file is a csv file
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            //open uploaded csv file with read only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            //skip first line
            fgetcsv($csvFile);
            
            //CREAMOS LA SENTENCIA SQL
            $cmdsql="INSERT INTO `asistente`(`id_asistente`, `nombre_asistente`, `grado_acad_asistente`, `dni_asistente`) VALUES ";
            
            $temp = 0;
            
            $nombres_arr = [];
            
            //parse data from csv file line by line
            while(($line = fgetcsv($csvFile)) !== FALSE){
                $line[1] = utf8_encode($line[1]);
                $line[2] = utf8_encode($line[2]);
                $line[3] = utf8_encode($line[3]);
                if($temp == 0){
                    $cmdsql.= "(NULL, :nombre_$temp, '".$line[2]."', '".$line[3]."')";
                }else{
                    //insert member data into database
                    $cmdsql.= ", (NULL, :nombre_$temp, '".$line[2]."', '".$line[3]."')";
                }
                
                $nombres_arr[] = $line[1];
                
                $temp++;
            }
            /*
            echo($cmdsql);
            var_dump($nombres_arr);
            */
            //PREPARAMOS LA SENTENCIA SQL
            $conectarPDO = conectarPDO();
            $stmnt = $conectarPDO->prepare($cmdsql);
            
            //CREAMOS LA SENTENCIA SQL
            $temp=0;
            foreach ($nombres_arr as $nombre) {
                //echo("<div>nombre_".$temp."</div>");
                //echo("<div>".$nombre."</div>");
                $stmnt->bindValue(":nombre_$temp", $nombre, PDO::PARAM_STR);
                $temp++;
            }
            
            $resultado = $stmnt->execute();
            
            $mensaje= "";
            if(!$resultado){
                //echo "<br>Error de procedimiento";
                var_dump($stmnt->errorInfo());
                echo "<script>alert('Ocurrió algún error al insertar los asistentes, por favor intente de nuevo.')</script>";
            }else{
                global $primer_id_insertado;
                $primer_id_insertado=$conectarPDO->lastInsertId();
                echo "<script> window.location.href='generarcodigos3.php?xiduniv=$id_universidad&xidevento=$id_evento&xprimer_id=$primer_id_insertado';</script>";
                echo "<script>console.log('$primer_id_insertado')</script>";
            }

        }else{
            echo "<script>alert('Ocurrió algún error al subir el archivo, por favor intente de nuevo.')</script>";
        }
    }else{
        echo "<script>alert('Por favor suba un archivo CSV.')</script>";
    }
}

if(isset($_POST['importSubmit2'])){
    //validate whether uploaded file is a csv file
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    if(!empty($_FILES['file2']['name']) && in_array($_FILES['file2']['type'],$csvMimes)){
        if(is_uploaded_file($_FILES['file2']['tmp_name'])){
            
            date_default_timezone_set('America/Lima');
            $fecha_registro=date("Y-m-d");
            
            //open uploaded csv file with read only mode
            $csvFile = fopen($_FILES['file2']['tmp_name'], 'r');
            
            //skip first line
            fgetcsv($csvFile);
            
            //parse data from csv file line by line
            $temp= 0;
            
            $cmdsql="INSERT INTO `participante_certificado`(`id_participante_certificado`, `id_evento`, `id_certificado`, `id_asistente`, `calidad_participante_certificado`, `fecemi_participante_certificado`, `id_universidad`) VALUES ";
                        
            $calidad_arr = [];
            
            while(($line = fgetcsv($csvFile)) !== FALSE){
                global $primer_id_insertado;
                $line[1]= $line[1] + $primer_id_insertado - 1;//ESTO PORQ LOS ASISTENTES NO EMPIEZAN DESDE 1
                $line[2] = utf8_encode($line[2]);
                
                if($temp == 0){
                    $cmdsql.= "(NULL, $id_evento, $line[0], $line[1],  :calidad_$temp, '".$fecha_registro."', $id_universidad)";
                }else{
                    //insert member data into database
                    $cmdsql.= ", (NULL, $id_evento, $line[0], $line[1], :calidad_$temp, '".$fecha_registro."', $id_universidad)";
                }
                
                $calidad_arr[] = $line[2];
                
                $temp++;
            }
            /*
            echo($cmdsql);
            var_dump($nombres_arr);
            */
            //PREPARAMOS LA SENTENCIA SQL
            $conectarPDO = conectarPDO();
            $stmnt = $conectarPDO->prepare($cmdsql);
            
            //CREAMOS LA SENTENCIA SQL
            $temp=0;
            foreach ($calidad_arr as $calidad) {
                $stmnt->bindValue(":calidad_$temp", $calidad, PDO::PARAM_STR);
                $temp++;
            }
            
            $resultado = $stmnt->execute();
            
            $mensaje= "";
            
            if(!$resultado){
                //echo "<br>Error de procedimiento
                var_dump($stmnt->errorInfo());
                echo "<script>alert('Ocurrió algún error al insertar los asistentes, por favor intente de nuevo.')</script>";
            }else{
                $primer_idcert_insertado=$conectarPDO->lastInsertId();
                echo "<script> window.location.href='generarcodigos3.php?xiduniv=$id_universidad&xidevento=$id_evento&xprimer_id=$primer_id_insertado&xprimer_id_cert=$primer_idcert_insertado';</script>";
                echo "<script>console.log('$primer_idcert_insertado')</script>";
            }

            echo "<script>alert('De acá redirigiría.')</script>";
        }else{
            echo "<script>alert('Ocurrió algún error al subir el archivo, por favor intente de nuevo.')</script>";
        }
    }else{
        echo "<script>alert('Por favor suba un archivo CSV.')</script>";
    }
}

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}    
    
?>
</body>
</html>