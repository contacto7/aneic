<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    //echo "<script> window.location.href='index.php';</script>";
}

$ano=1000;// ano 1000 es para el último comité

if (isset($_GET['xano'])) {
    $ano=$_GET['xano'];
}


//AHORA METEMOS TODOS LOS AÑOS EN EL SELET

$anos_comite_res= listaranosadm();

$anoscomite_arr = "";
global $anos_dif_arr;
$anos_dif_arr = array();

$cuenta_ano=0;
while ($row=mysqli_fetch_assoc($anos_comite_res)) {
    
    $ano_comite_list = $row['ano_comite'];
    $ano_selec=" ";
    
    $anos_dif_arr[]=$ano_comite_list;
    
    if($cuenta_ano == 0 && $ano ==1000){
        $ano = $ano_comite_list;
    }
    
    if($ano_comite_list == $ano){
        $ano_selec=" selected ";
    }
    $anoscomite_arr.='<option class="sel-dep" value="'.$ano_comite_list.'" '.$ano_selec.'>'.$ano_comite_list.'</option>';
    
    $cuenta_ano++;
}

//AHORA LISTAMOS EL COMITE
$comite_arr = array();

$comite_res= listarcomiteadm($ano);
while ($row=mysqli_fetch_assoc($comite_res)) {
    $id_adm_comite =$row['id_adm_comite'];
    $puesto_adm_comite =$row['puesto_adm_comite'];
    $nombres_adm_comite =$row['nombres_adm_comite'];
    $apellidos_adm_comite =$row['apellidos_adm_comite'];
    $foto_adm_comite =$row['foto_adm_comite'];
    $descripcion_adm_comite =$row['descripcion_adm_comite'];
    $fb_adm_comite =$row['fb_adm_comite'];
    $li_adm_comite = $row['li_adm_comite'];
    $ano_comite = $row['ano_comite'];
    
    if($foto_adm_comite == "" || $foto_adm_comite == " "){
        $foto_adm_comite = "fotos_comite/def.png";
    }

    $tmp = new stdClass;
    $tmp->puesto_adm_comite = $puesto_adm_comite;
    $tmp->nombres_adm_comite = $nombres_adm_comite;
    $tmp->apellidos_adm_comite = $apellidos_adm_comite;
    $tmp->foto_adm_comite = $foto_adm_comite;
    $tmp->descripcion_adm_comite = $descripcion_adm_comite;
    $tmp->fb_adm_comite = $fb_adm_comite;
    $tmp->li_adm_comite = $li_adm_comite;

    $objetodia[]=$tmp;
}
mysqli_free_result($comite_res);
desconectar();

mysqli_free_result($anos_comite_res);
desconectar();

//LISTAMOS LA FOTO
$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();

?>


<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Comité</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
</head>
<body>
<script type="text/javascript" src="js/jquery.min.js"></script>

<div id="header"></div>

<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Comite</span>
    </div>
</div>

<div class="container cont-comit-hist">
    <div class="row">
        <div class="col-xs-6 col-sm-3">
            <div class="tit-sel-comithist">Registro histórico:</div>
        </div>        
        <div class="col-xs-6 col-sm-3">
            <select class="form-control sel-comithist">
                <?php echo $anoscomite_arr; ?>
            </select>
        </div>
<?php if($sesioninic){ ?>
            <div class="btn btn-md btn-warning" href="#agreg-comite-mod" data-target="#agreg-comite-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" style="margin-top:-5px">
                Agregar Comité
            </div>
<?php } ?>

    </div>
</div>
    
    
<div class="container cont-comite">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-6 row-diagrama">
            <div class="diagrama-organizacional">
                <div class="diagrama-org-ext">
    <?php if($sesioninic){ ?>
                <div class="btn-foto-navmed modalLink" href="#camb-comite-mod" data-target="#camb-comite-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" style="margin-top:-5px" data-id="1">
                    <button>Editar</button>
                </div>
    <?php } ?>
                    <div class="diagrama-org-wrapp diagrama-org-1">
                        <div class="diagrama-org-nombre">
                            <span>
                                <?php 
                                    if($objetodia[0]->apellidos_adm_comite == "" || $objetodia[0]->apellidos_adm_comite == " "){
                                        echo "Por definir";
                                    }else{
                                        echo $objetodia[0]->apellidos_adm_comite.", ".$objetodia[0]->nombres_adm_comite; 
                                    }
                                ?>
                            </span>
                        </div>
                        <div class="diagrama-org-puesto">
                            Presidente
                        </div>
                        <a class="diagrama-org-img toscroll" href="#descr-comite-1">
                            <img alt="Imagen de integrante de comite" class="img-circle" src="<?php echo $objetodia[0]->foto_adm_comite ?>">
                        </a>
                        <div class="diagr-lin-cent"></div>
                    </div>
    <?php if($sesioninic){ ?>
                <div class="btn-foto-navmed modalLink" href="#camb-comite-mod" data-target="#camb-comite-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" style="margin-top:-5px" data-id="2">
                    <button>Editar</button>
                </div>
    <?php } ?>
                    <div class="diagrama-org-wrapp diagrama-org-2">
                        <div class="diagrama-org-nombre">
                            <span>
                                <?php 
                                    if($objetodia[1]->apellidos_adm_comite == "" || $objetodia[1]->apellidos_adm_comite == " "){
                                        echo "Por definir";
                                    }else{
                                        echo $objetodia[1]->apellidos_adm_comite.", ".$objetodia[1]->nombres_adm_comite; 
                                    }
                                ?>
                            </span>
                        </div>
                        <div class="diagrama-org-puesto">
                            Secretario General
                        </div>
                        <a class="diagrama-org-img toscroll" href="#descr-comite-2">
                            <img alt="Imagen de integrante de comite" class="img-circle" src="<?php echo $objetodia[1]->foto_adm_comite ?>">
                        </a>
                        <div class="diagr-lin-cent"></div>
                    </div>
    <?php if($sesioninic){ ?>
                <div class="btn-foto-navmed modalLink" href="#camb-comite-mod" data-target="#camb-comite-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" style="margin-top:-5px" data-id="3">
                    <button>Editar</button>
                </div>
    <?php } ?>
                    <div class="diagrama-org-wrapp diagrama-org-3">
                        <div class="diagrama-org-nombre">
                            <span>
                                <?php 
                                    if($objetodia[2]->apellidos_adm_comite == "" || $objetodia[2]->apellidos_adm_comite == " "){
                                        echo "Por definir";
                                    }else{
                                        echo $objetodia[2]->apellidos_adm_comite.", ".$objetodia[2]->nombres_adm_comite; 
                                    }
                                ?>
                            </span>
                        </div>
                        <div class="diagrama-org-puesto">
                            Tesorero
                        </div>
                        <a class="diagrama-org-img toscroll" href="#descr-comite-3" >
                            <img alt="Imagen de integrante de comite" class="img-circle" src="<?php echo $objetodia[2]->foto_adm_comite ?>">
                        </a>
                        <div class="diagr-lin-cent"></div>
                    </div>
    <?php if($sesioninic){ ?>
                <div class="btn-foto-navmed modalLink" href="#camb-comite-mod" data-target="#camb-comite-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" style="margin-top:-5px" data-id="4">
                    <button>Editar</button>
                </div>
    <?php } ?>
                    <div class="diagrama-org-wrapp diagrama-org-4">
                        <div class="diagrama-org-nombre">
                            <span>
                                <?php 
                                    if($objetodia[3]->apellidos_adm_comite == "" || $objetodia[3]->apellidos_adm_comite == " "){
                                        echo "Por definir";
                                    }else{
                                        echo $objetodia[3]->apellidos_adm_comite.", ".$objetodia[3]->nombres_adm_comite; 
                                    }
                                ?>
                            </span>
                        </div>
                        <div class="diagrama-org-puesto">
                            Secretario de Planeación
                        </div>
                        <a class="diagrama-org-img toscroll" href="#descr-comite-4">
                            <img alt="Imagen de integrante de comite" class="img-circle" src="<?php echo $objetodia[3]->foto_adm_comite ?>">
                        </a>
                        <div class="diagr-lin-cent"></div>
                    </div>
    <?php if($sesioninic){ ?>
                <div class="btn-foto-navmed modalLink" href="#camb-comite-mod" data-target="#camb-comite-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" style="margin-top:-5px" data-id="5">
                    <button>Editar</button>
                </div>
    <?php } ?>
                    <div class="diagrama-org-wrapp diagrama-org-5">
                        <div class="diagrama-org-nombre">
                            <span>
                                <?php 
                                    if($objetodia[4]->apellidos_adm_comite == "" || $objetodia[4]->apellidos_adm_comite == " "){
                                        echo "Por definir";
                                    }else{
                                        echo $objetodia[4]->apellidos_adm_comite.", ".$objetodia[4]->nombres_adm_comite; 
                                    }
                                ?>
                            </span>
                        </div>
                        <div class="diagrama-org-puesto">
                            Secretario de Relaciones Públicas
                        </div>
                        <a class="diagrama-org-img toscroll" href="#descr-comite-5">
                            <img alt="Imagen de integrante de comite" class="img-circle" src="<?php echo $objetodia[4]->foto_adm_comite ?>">
                        </a>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-xs-12 col-sm-5 col-md-6 row-comite-descr">
            <div class="descr-comite-wrapp">
                <div class="descr-comite-inn" id="descr-comite-1">
                    <div class="descr-comite-nombr">
                        <?php 
                            if($objetodia[0]->apellidos_adm_comite == "" || $objetodia[0]->apellidos_adm_comite == " "){
                                echo "Por definir";
                            }else{
                                echo $objetodia[0]->apellidos_adm_comite.", ".$objetodia[0]->nombres_adm_comite; 
                            }
                        ?>
                    </div>
                    <div class="descr-comite-redes">
                        <a class="img-comite-red" href="<?php echo $objetodia[0]->fb_adm_comite ?>" target="_blank">
                            <img alt="imagen logo de red social" class="img-circle" src="img/fb-comite.png">
                        </a>
                        <a class="img-comite-red" href="<?php echo $objetodia[0]->li_adm_comite ?>" target="_blank">
                            <img alt="imagen logo de red social" class="img-circle" src="img/linkedin-comite.png">
                        </a>
                    </div>
                    <div class="descr-comite-descr">
                        <?php echo $objetodia[0]->descripcion_adm_comite ?>
                    </div>
                </div>

                <div class="descr-comite-inn" id="descr-comite-2">
                    <div class="descr-comite-nombr">
                        <?php 
                            if($objetodia[1]->apellidos_adm_comite == "" || $objetodia[1]->apellidos_adm_comite == " "){
                                echo "Por definir";
                            }else{
                                echo $objetodia[1]->apellidos_adm_comite.", ".$objetodia[1]->nombres_adm_comite; 
                            }
                        ?>
                    </div>
                    <div class="descr-comite-redes">
                        <a class="img-comite-red" href="<?php echo $objetodia[1]->fb_adm_comite ?>" target="_blank">
                            <img alt="imagen logo de red social" class="img-circle" src="img/fb-comite.png">
                        </a>
                        <a class="img-comite-red" href="<?php echo $objetodia[1]->li_adm_comite ?>" target="_blank">
                            <img alt="imagen logo de red social" class="img-circle" src="img/linkedin-comite.png">
                        </a>
                    </div>
                    <div class="descr-comite-descr">
                        <?php echo $objetodia[1]->descripcion_adm_comite ?>
                    </div>
                </div>

                <div class="descr-comite-inn" id="descr-comite-3">
                    <div class="descr-comite-nombr">
                        <?php 
                            if($objetodia[2]->apellidos_adm_comite == "" || $objetodia[2]->apellidos_adm_comite == " "){
                                echo "Por definir";
                            }else{
                                echo $objetodia[2]->apellidos_adm_comite.", ".$objetodia[2]->nombres_adm_comite; 
                            }
                        ?>
                    </div>
                    <div class="descr-comite-redes">
                        <a class="img-comite-red" href="<?php echo $objetodia[2]->fb_adm_comite ?>" target="_blank">
                            <img alt="imagen logo de red social" class="img-circle" src="img/fb-comite.png">
                        </a>
                        <a class="img-comite-red" href="<?php echo $objetodia[2]->li_adm_comite ?>" target="_blank">
                            <img alt="imagen logo de red social" class="img-circle" src="img/linkedin-comite.png">
                        </a>
                    </div>
                    <div class="descr-comite-descr">
                        <?php echo $objetodia[2]->descripcion_adm_comite ?>
                    </div>
                </div>

                <div class="descr-comite-inn" id="descr-comite-4">
                    <div class="descr-comite-nombr">
                        <?php 
                            if($objetodia[3]->apellidos_adm_comite == "" || $objetodia[3]->apellidos_adm_comite == " "){
                                echo "Por definir";
                            }else{
                                echo $objetodia[3]->apellidos_adm_comite.", ".$objetodia[3]->nombres_adm_comite; 
                            }
                        ?>
                    </div>
                    <div class="descr-comite-redes">
                        <a class="img-comite-red" href="<?php echo $objetodia[3]->fb_adm_comite ?>" target="_blank">
                            <img alt="imagen logo de red social" class="img-circle" src="img/fb-comite.png">
                        </a>
                        <a class="img-comite-red" href="<?php echo $objetodia[3]->li_adm_comite ?>" target="_blank">
                            <img alt="imagen logo de red social" class="img-circle" src="img/linkedin-comite.png">
                        </a>
                    </div>
                    <div class="descr-comite-descr">
                        <?php echo $objetodia[3]->descripcion_adm_comite ?>
                    </div>
                </div>
                <div class="descr-comite-inn" id="descr-comite-5">
                    <div class="descr-comite-nombr">
                        <?php 
                            if($objetodia[4]->apellidos_adm_comite == "" || $objetodia[4]->apellidos_adm_comite == " "){
                                echo "Por definir";
                            }else{
                                echo $objetodia[4]->apellidos_adm_comite.", ".$objetodia[4]->nombres_adm_comite; 
                            }
                        ?>
                    </div>
                    <div class="descr-comite-redes">
                        <a class="img-comite-red" href="<?php echo $objetodia[4]->fb_adm_comite ?>" target="_blank">
                            <img alt="imagen logo de red social" class="img-circle" src="img/fb-comite.png">
                        </a>
                        <a class="img-comite-red" href="<?php echo $objetodia[4]->li_adm_comite ?>" target="_blank">
                            <img alt="imagen logo de red social" class="img-circle" src="img/linkedin-comite.png">
                        </a>
                    </div>
                    <div class="descr-comite-descr">
                        <?php echo $objetodia[4]->descripcion_adm_comite ?>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>

<div id="footer"></div>
    
<?php if($sesioninic){ ?>
    
<div class="modal fade" id="camb-comite-mod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <img src="img/ajax-loader.gif" id="loading-indicator" style="display:none">
        <div class="modal-content modal-content-cv" id="mod-cont">

        </div>
    </div>
</div>
    
<div class="modal fade" id="agreg-comite-mod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Agregar Comité</h4>
            </div>
            <div class="modal-body modbod-exp">
                 <form class="form-horizontal form-datoper" role="form" action="" method="post" enctype="multipart/form-data">
                     <div class="row form-datper-left">
                        <div class="col-xs-12">
                          <div class="form-group" style="">
                            <label class="control-label col-sm-4" for="">Año de comité:</label>
                            <div class="col-sm-8">
                              <input type="number" class="form-control" id="anonuevo-comite" name="anonuevo-comite" placeholder="Escriba el año. Ej. 2021" min="1900" max="9999" step="1">
                            </div>
                          </div>

                        </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-12 datpersbut-wrap">
                              <div class="datpersbut-mod">
                                <button type="submit" class="btn btn-datper-mod btn-sm" name="nuevocom-adm-sub" >Agregar</button>
                                <button type="button" class="btn btn-datper-canc btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                             </div>
                         </div>
                     </div>
                </form>
            </div>
            
            
        </div>
    </div>
</div>
    
<?php } ?>
    
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script src="js/jquery.carouFredSel.js"></script>
    

<script>
$(function(){
    
   $("#header").load("header.php", {
       xph: 6, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<script>    
var ano_comite = <?php echo $ano; ?>;
$(document).ready(function(){

    $('#camb-comite-mod').on('hidden.bs.modal', function () {
        var myNode = document.getElementById("mod-cont");
        while (myNode.firstChild) {
            myNode.removeChild(myNode.firstChild);
        }
    });
    $(document).ajaxSend(function(event, request, settings) {
      $('#loading-indicator').show();
    });

    $(document).ajaxComplete(function(event, request, settings) {
      $('#loading-indicator').hide();
    });

});
    
$("a.toscroll").on('click',function() {
    var url =$(this).attr("href");
    //console.log(url);
    var hash = url.substring(url.indexOf("#")+1);
    $('html, body').animate({
        scrollTop: $('#'+hash).offset().top -200
    }, 500);
    return false;
});
    
$(document).on("click", '.modalLink',function(){
    console.log("asd");
    var puesto=$(this).attr('data-id');

    $(".modal-content-cv").css({
       "min-height":"400px" 
    });
    $.ajax({url:"apost_modal_comite.php?xpuesto="+puesto+"&xano="+ano_comite,cache:false,success:function(result){
        $(".modal-content-cv").html(result);
    }});



});
</script>
    
<?php

if (isset($_POST['miembro-adm-sub'])){

    if( $image_size=$_FILES['foto-miembro']['size']>0 ){

        $nuevo_nombre=randomalfanume(25);
        $nuevo_nombre= $nuevo_nombre.".".$ext;
        $url_foto= "fotos_comite/".$nuevo_nombre;

        $image_temp_name=$_FILES['foto-miembro']['tmp_name'];
        if(move_uploaded_file($image_temp_name,$url_foto)){

            $puesto_miembro_mod=$_POST['puesto-comite'];
            $nombres_miembro_mod=$_POST['nombre-miembro'];
            $apellidos_miembro_mod=$_POST['apellidos-miembro'];
            $descripcion_miembro_mod=$_POST['dscr-miembro'];
            $fb_miembro_mod=$_POST['link-fb'];
            $li_miembro_mod=$_POST['link-li'];
            $ano_comite_mod=$_POST['ano-comite'];

            $pagdireccion = "comite.php?xano=$ano_comite_mod";

            modificarcomiteadm($puesto_miembro_mod, $nombres_miembro_mod, $apellidos_miembro_mod, $descripcion_miembro_mod, $fb_miembro_mod, $li_miembro_mod, $ano_comite_mod, $url_foto, $pagdireccion);


        }else{
            echo "<script>alert('Hubo un error al cargar la imagen. Por favor intenta más tarde.')</script>";
            exit();
        }

    }else{

        $url_foto= "yafoto";
        
        $puesto_miembro_mod=$_POST['puesto-comite'];
        $nombres_miembro_mod=$_POST['nombre-miembro'];
        $apellidos_miembro_mod=$_POST['apellidos-miembro'];
        $descripcion_miembro_mod=$_POST['dscr-miembro'];
        $fb_miembro_mod=$_POST['link-fb'];
        $li_miembro_mod=$_POST['link-li'];
        $ano_comite_mod=$_POST['ano-comite'];

        $pagdireccion = "comite.php?xano=$ano_comite_mod";

        modificarcomiteadm($puesto_miembro_mod, $nombres_miembro_mod, $apellidos_miembro_mod, $descripcion_miembro_mod, $fb_miembro_mod, $li_miembro_mod, $ano_comite_mod, $url_foto, $pagdireccion);
    }

}

if (isset($_POST['nuevocom-adm-sub'])){

    if($_POST['anonuevo-comite'] > 1900 && $_POST['anonuevo-comite'] < 9999){

        if( !in_array($_POST['anonuevo-comite'], $anos_dif_arr)){
            $nuevo_comite_ano=$_POST['anonuevo-comite'];

            agregarcomiteadm($nuevo_comite_ano);
        }else{
            echo "<script>alert('El año ingresado ya existe.');</script>";
            exit();
        }



    }else{
        echo "<script>alert('Agregue un año válido.');</script>";
        exit();
    }

}

if(isset($_POST['ver-votdel-btn'])){

    if(!empty($_POST['dni-votar']) && !empty($_POST['cod-votar'])){
        
        $dni_delegado_votar = $_POST['dni-votar'];
        $cod_delegado_votar = $_POST['cod-votar'];

        $info_delegado = listarinfodelegadodni($dni_delegado_votar);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_delegado)) {
            $id_delegado =$row['id_delegado'];
            $puesto_delegado =$row['puesto_delegado'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){
            
            $codigo_delegado_tmp=$dni_delegado_votar*7*13*37;//para codificar el código

            $codigo_delegado_tmp = substr($codigo_delegado_tmp, -4);

            if($cod_delegado_votar == $codigo_delegado_tmp){
                
                unset($_SESSION['id_delegado']);
                unset($_SESSION['puesto_delegado']);
                
                $_SESSION["id_delegado"]=$id_delegado;
                $_SESSION["puesto_delegado"]=$puesto_delegado;
                
                $id_delegado_encriptado = doEncrypt($id_delegado);
                $puesto_delegado_encriptado = doEncrypt($puesto_delegado);
                                
                //echo "<script>window.location.href='lista_delegado_votacion.php?xi_1=$id_delegado_encriptado&xi_2=$puesto_delegado_encriptado';</script>";
                echo "<script>window.location.href='lista_delegado_votacion.php';</script>";

            }else{
                echo "<script> alert('Código de votación incorrecto.');</script>";
            }
            
        }else{
            echo "<script> alert('No se encuentra un delegado con el DNI brindado.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}


if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if(isset($_POST['ver-asisdel-btn'])){

    if(!empty($_POST['dni-asist']) && !empty($_POST['cod-asist'])){
        
        $dni_delegado_asist = $_POST['dni-asist'];
        $cod_delegado_asist = $_POST['cod-asist'];

        $info_asistencia = listarinfoasis($dni_delegado_asist, $cod_delegado_asist);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_asistencia)) {
            $id_delegado_asis =$row['id_delegado'];
            $id_asistencia_asis =$row['id_asistencia'];
            $estado_asistencia_asis =$row['estado_asistencia'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){

            if($estado_asistencia_asis > 0){

                $resultado_asi_agr = registrarasistenciadel($id_delegado_asis, $id_asistencia_asis);
                $cuenta_tmp = 0;

                if($resultado_asi_agr){
                    echo "<script> alert('Se registró la asistencia.');</script>";
                }else{
                    echo "<script> alert('Ha ocurrido un problema al registrar la asistencia. Inténtelo de nuevo, o comuníquese con algún representante de Aneic para solucionar el problema.');</script>";
                }

            }else{
                echo "<script> alert('La asistencia ya se ha cerrado.');</script>";
            }
            
        }else{
            echo "<script> alert('La asistencia no se ha abierto para usted, o el código brindado es incorrecto.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}



if (isset($_POST['codigo-recu'])){
	if(!empty($_POST['dni-recu'])){
		$dni_delegado_rec=$_POST['dni-recu'];

		$result=recuperarcontradel($dni_delegado_rec);

	}else{
        echo "<script> alert('Ingrese un DNI.');</script>";
    }
}
if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}    
    
?>
    
</body>
</html>