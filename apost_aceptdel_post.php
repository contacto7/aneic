<?php

// Esta funcion acepta|rechaza al delegado o la universidad

include 'funciones.php';
$id_universidad=$_GET['xid_universidad'];
$id_delegado=$_GET['xid_accion'];
$puesto_delegado=$_GET['xpuesto'];
$dni_delegado=$_GET['xdni'];


if($puesto_delegado > 4){

    $cmdsql="UPDATE delegado SET estado_delegado=1 WHERE id_delegado=$id_delegado";
    $link=conectar();
    $resultado=mysqli_query($link, $cmdsql);
    if (!$resultado) {
        echo mysqli_error($link);
        echo "Error al cambiar el estado del delegado";
    }else{
        enviarcodigodelegado($dni_delegado);
        echo "Aceptado";
    }
    
}else{
    
    $cmdsql="DELETE FROM `delegado` WHERE id_universidad=$id_universidad AND estado_delegado=1 AND puesto_delegado=$puesto_delegado";

    $link=conectar();
    $resultado=mysqli_query($link, $cmdsql);
    if (!$resultado) {
        echo mysqli_error($link);
        echo "Error al eliminar delegado anterior.";
    }else{

        $cmdsql="UPDATE delegado SET estado_delegado=1 WHERE id_delegado=$id_delegado";
        $link=conectar();
        $resultado=mysqli_query($link, $cmdsql);
        if (!$resultado) {
            echo mysqli_error($link);
            echo "Error al cambiar el estado del delegado";
        }else{
            enviarcodigodelegado($dni_delegado);
            echo "Aceptado";
        }

    }
    
}



?>