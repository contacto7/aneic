<?php 
session_start();
$nombres="";
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}

setlocale(LC_TIME, "");
setlocale(LC_ALL,"es_ES.UTF8");

$busquedaqr=0;
$busquedanomb=0;
$busquedahecha=1;
$credencial=0;

if (isset($_GET['xid'])) {
	$xid = $_GET['xid'];
    $busquedaqr=1;
} elseif(isset($_GET['xid_pers'])){
	// This month
    $xid = $_GET['xid_pers'];
    $busquedanomb=1;
}else{
    $busquedahecha=0;
}


$width_canvas = 870*2;
$height_canvas = 620*2;

$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();


?>
<!DOCTYPE html>
<html ng-app="">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jquery.raty.css">
    <link href="css/princ.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery.min.older.js"></script>
</head>
<body style="height:100%">
<script type="text/javascript" src="js/jquery.min.js"></script>

<div id="header"></div>
    
<?php 
    if($busquedahecha==1 && !isset($_POST['buscar-subm'])){
?>
<div class="cert-inst-wrapp">
    <div class="cert-inst-inn">
        <div class="cert-inst-texto" id="certificado-titulo">
            CERTIFICADO EMITIDO POR ANEIC PERU
        </div>
    </div>
</div>
<?php 
    }
?>

    
<div class="container cont-list-cert" style="min-height: 100%;margin: -472px auto 0;">
    <div class="row" style="padding-top: 472px;">
        
<?php 

if(($busquedaqr==1 || $busquedanomb==1) && !isset($_POST['buscar-subm'])){
    
    if($busquedaqr==1){
        $result=listarcertificadoaistente($xid);
    }else{
        $result=listarcertificadosaistente($xid);
    }
    
    $temp=0;

    while ($row=mysqli_fetch_assoc($result)) {

        $id_participante_certificado=$row['id_participante_certificado'];
        $nombre_asistente=$row['nombre_asistente'];
        $calidad_participante_certificado=$row['calidad_participante_certificado'];
        $nombre_evento=$row['nombre_evento'];
        $nombre_universidad=$row['nombre_universidad'];
        $fecemi_participante_certificado=$row['fecemi_participante_certificado'];
        
        //VARIABLES DE NOMBRE
        $left_nombre = $row['left_nombre'];
        $top_nombre = $row['top_nombre'];
        $tamano_nombre = $row['tamano_nombre'];
        $estilo_nombre = $row['estilo_nombre'];
        $fuente_nombre = $row['fuente_nombre'];
        $color_nombre = $row['color_nombre'];
        
        //VARIABLES DE CALIDAD
        $left_calidad = $row['left_calidad'];
        $top_calidad = $row['top_calidad'];
        $tamano_calidad = $row['tamano_calidad'];
        $estilo_calidad = $row['estilo_calidad'];
        $fuente_calidad = $row['fuente_calidad'];
        $color_calidad = $row['color_calidad'];

        //VARIABLES DE QR
        $left_qr = $row['left_qr'];
        $top_qr = $row['top_qr'];
        $tamano_qr = $row['tamano_qr'];
        //EVENTO
        $foto_certificado = $row['foto_certificado'];
        
        //INLINE CSS NOMBRE
        $font_nombre="";
        
        $left_nombre_css = $left_nombre."px";
        $top_nombre_css = $top_nombre."px";
        $tamano_nombre_css = $tamano_nombre."px";
        
        $font_nombre.="$tamano_nombre_css ";
        
        if($estilo_nombre == 1){
            $estilo_nombrestyle_css = "normal";
            $estilo_nombreweight_css = "100";
            
            $font_nombre.="";
        }elseif($estilo_nombre == 2){
            $estilo_nombrestyle_css = "italic";
            $estilo_nombreweight_css = "100";
            
            $font_nombre.="italic";
        }elseif($estilo_nombre == 3){
            $estilo_nombrestyle_css = "normal";
            $estilo_nombreweight_css = "bold";
            
            $font_nombre.="bold";
        }elseif($estilo_nombre == 4){
            $estilo_nombrestyle_css = "italic";
            $estilo_nombreweight_css = "bold";
            
            $font_nombre.="bold";
        }
        $fuente_nombre_css = $fuente_nombre;
        $color_nombre_css = $color_nombre;
        
        //INLINE CSS CALIDAD
        $left_calidad_css = $left_calidad."px";
        $top_calidad_css = $top_calidad."px";
        $tamano_calidad_css = $tamano_calidad."px";
        
        if($estilo_calidad == 1){
            $estilo_calidadstyle_css = "normal";
            $estilo_calidadweight_css = "100";
        }elseif($estilo_calidad == 2){
            $estilo_calidadstyle_css = "italic";
            $estilo_calidadweight_css = "100";
        }elseif($estilo_calidad == 3){
            $estilo_calidadstyle_css = "normal";
            $estilo_calidadweight_css = "bold";
        }elseif($estilo_calidad == 4){
            $estilo_calidadstyle_css = "italic";
            $estilo_calidadweight_css = "bold";
        }
        $fuente_calidad_css = $fuente_calidad;
        $color_calidad_css = $color_calidad;
        
        if($calidad_participante_certificado == "DELEGADO" || $calidad_participante_certificado == "SUBDELEGADO" || $calidad_participante_certificado == "PRESIDENTE DE ANEIC PERU" ){
            $credencial=1;
        }

        //INLINE CSS QR
        $left_qr_css = $left_qr."px";
        $top_qr_css = $top_qr."px";
        $tamano_qr_css = $tamano_qr."px";

?>
        <div class="cert-wrapp">
            <div class="cert-inn">
                <div class="cert-inn-fila">
                    <div class="cert-inn-tit">Otorgado a:</div>
                    <div class="cert-inn-descr"><?php echo $nombre_asistente; ?></div>
                </div>
                <div class="cert-inn-fila">
                    <div class="cert-inn-tit">Evento:</div>
                    <div class="cert-inn-descr"><?php echo $nombre_evento; ?></div>
                </div>
                <div class="cert-inn-fila">
                    <div class="cert-inn-tit">En calidad de:</div>
                    <div class="cert-inn-descr"><?php echo $calidad_participante_certificado; ?></div>
                </div>
                <div class="cert-inn-fila">
                    <div class="cert-inn-tit">Universidad sede:</div>
                    <div class="cert-inn-descr"><?php echo $nombre_universidad; ?></div>                   
                </div>
                <div class="cert-inn-fila">
                    <div class="cert-inn-tit">Fecha de emisión:</div>
                    <div class="cert-inn-descr"><?php 
                            $fecemi_participante_certificado = DateTime::createFromFormat("Y-m-d", $fecemi_participante_certificado);
                            $fecemi_participante_certificado = strftime("%d de %B del %Y",$fecemi_participante_certificado->getTimestamp());
                            echo $fecemi_participante_certificado; 
                        
                        ?></div>
                </div>
                <div class="cert-inn-fila" style="<?php if(!$foto_certificado){ echo "display: none"; }?>">
                    <div class="cert-inn-tit">Certificado:</div>
                    <div class="cert-inn-descr">
                        <a class="mostrar-cert" onclick="mostrarcert(<?php echo $temp; ?>)"><span class="glyphicon glyphicon-search"></span> Ver</a> 
                        <a class="mostrar-cert descargar-cert" data-id="<?php echo $temp; ?>"><span class="glyphicon glyphicon-download-alt"></span> Descargar</a>
                    </div>
                </div>
            </div>
<?php 
            if($foto_certificado){
?>
            <div class="fila-certificado-canvas" id="canvas-<?php echo $temp; ?>">
            
                <div class="tit-cert-canvas">Certificado:</div>
                <div class="canvas-cert-inn" id="canvas-dwn-<?php echo $temp; ?>">
                    <div class="elemento-drag nombr-participante" style="<?php echo "left:$left_nombre_css; top:$top_nombre_css; font-size:$tamano_nombre_css; font-style: $estilo_nombrestyle_css; font-weight: $estilo_nombreweight_css; font-family: $fuente_nombre_css; color: $color_nombre_css;"; ?>text-align:center;">
                        <span style="left:-50%;position:relative"><?php echo $nombre_asistente; ?></span>
                    </div>                    
                    <div class="elemento-drag calidad-participante" style="<?php echo "left:$left_calidad_css; top:$top_calidad_css; font-size:$tamano_calidad_css; font-style: $estilo_calidadstyle_css; font-weight: $estilo_calidadweight_css; font-family: $fuente_calidad_css; color: $color_calidad_css;"; ?>text-align:center;">
                        <span style="left:-50%;position:relative"><?php echo $calidad_participante_certificado; ?></span>
                    </div>
                    <div id="demo-<?php echo $temp ?>" class="qr-canvas-cert" data-id="<?php echo $id_participante_certificado; ?>" style="position:absolute; left: <?php echo $left_qr_css ?>; top: <?php echo $top_qr_css ?>;" data-tam="<?php echo $tamano_qr ?>"></div>
                    <canvas id="canvas_certificado-<?php echo $temp ?>" width="<?php echo $width_canvas; ?>" height="<?php echo $height_canvas; ?>"></canvas>
                </div>
                <div id="img-out"></div>
            </div>
            <script>

                var myCanvas<?php echo $temp ?> = document.getElementById('canvas_certificado-<?php echo $temp ?>');
                var ctx<?php echo $temp ?> = myCanvas<?php echo $temp ?>.getContext('2d');
                                
                var img<?php echo $temp ?> = new Image;
                
                 img<?php echo $temp ?>.onload = function(){
                    ctx<?php echo $temp ?>.drawImage(img<?php echo $temp ?>, 0, 0, img<?php echo $temp ?>.width,    img<?php echo $temp ?>.height,     // source rectangle
                                       0, 0, myCanvas<?php echo $temp ?>.width, myCanvas<?php echo $temp ?>.height); 
                 };
                
                img<?php echo $temp ?>.src = "<?php echo $foto_certificado; ?>";
            </script>
            
<?php 
    
               }
?>
        </div>


<?php 
    $temp++;
    }
?>
</div>
    
<?php 
}elseif(isset($_POST['buscar-subm'])){
	if(!empty($_POST['buscar-persona'])){
		$nombre_asistente=$_POST['buscar-persona'];

        $pagdireccion="certificado.php";
		$result=buscarpersnombre($nombre_asistente,$pagdireccion);
        $cuenta_asist=1;
?>
    <div class="col-xs-12 row-pers-busq"> 
        <div class="table-pers-busq">
          <table class="table table-striped">
            <tbody>
<?php    
        $ids_asistentes=array();
        while ($row=mysqli_fetch_assoc($result)) {
            $id_asistente=$row['id_asistente'];
            $nombre_asistente=$row['nombre_asistente'];
            
            if (!in_array($id_asistente, $ids_asistentes)) {
                array_push($ids_asistentes, $id_asistente);


?>
              <tr>
                <td><?php echo $cuenta_asist; ?></td>
                <td><a class="link-pers-busq" href="certificado.php?xid_pers=<?php echo $id_asistente; ?>"><?php echo $nombre_asistente; ?></a></td>
              </tr>
<?php 
            $cuenta_asist++;
            }
        }
        
?>
            </tbody>
          </table>
        </div>
    </div>
<?php    
	}
}
    
?>

    
</div>
    
<div id="footer"></div>

<div id="login-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title">Login</h4>
            </div>
            <div class="modal-body">
                 <form class="form-inline" role="form" action="" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="correo">Correo</label>
                        <input type="email" class="form-control input-sm" name="correo" id="user_mail" placeholder="Correo" required>
                    </div>     
                    <div class="form-group">
                        <label class="sr-only" for="contrasena">Contraseña</label>
                        <input type="password" class="form-control input-sm" name="contrasena" id="password" placeholder="Contraseña" required>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox"><small>Recordarme</small></label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm" name="ingresar" >Entrar</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script type="text/javascript" src="js/jquery.qrcode.min.js"></script>
<script type="text/javascript" src="js/html2canvas.min.js"></script>
    
<script>
$(function(){
    
   $("#header").load("header.php", {
       xph: 0, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<script type="text/javascript">
$(document).ready(function() {
<?php 

if($credencial==1){
    echo "$('#certificado-titulo').html('CREDENCIAL EMITIDO POR ANEIC PERU');";
}

?>
    
});
    
    

$(document).ready(function() {
  $(".descargar-cert").click(function(){
      /*
    $("#canvas-"+e).show();
    $('body').css({"min-width":"950px"});
      */
    var id_elem = $(this).attr("data-id");
    html2canvas($("#canvas-dwn-"+id_elem).get(0)).then(canvas => {
        //document.body.appendChild(canvas)

         var url = canvas.toDataURL();
          $("<a>", {
            href: url,
            download: "certificado"
          })
          .on("click", function() {$(this).remove()})
          .appendTo("body")[0].click()
        
    });
  });
    
    $('.qr-canvas-cert').each(function(i, obj) {
        var id_certificado_basedatos=$(obj).attr("data-id");
        var tamano_certificado=$(obj).attr("data-tam");

        var linkarch="http://www.aneicperu.com/certificado.php?xid="+id_certificado_basedatos;
        //var nombre_arch_ver=id_evento+"_"+id_certificado;
        
        jQuery("#demo-"+i).qrcode({
            //render:"table"
            width: tamano_certificado,
            height: tamano_certificado,
            text: linkarch
        });

    });
    
});
</script>
    
    
<?php 
if (isset($_POST['buscar-subm'])){
	if(!empty($_POST['buscar-persona'])){
		$nombre_asistente=$_POST['buscar-persona'];

        $pagdireccion="certificado.php";
		$result=buscarpersnombre($nombre_asistente,$pagdireccion);

	}
    
}


if(isset($_POST['ver-votdel-btn'])){

    if(!empty($_POST['dni-votar']) && !empty($_POST['cod-votar'])){
        
        $dni_delegado_votar = $_POST['dni-votar'];
        $cod_delegado_votar = $_POST['cod-votar'];

        $info_delegado = listarinfodelegadodni($dni_delegado_votar);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_delegado)) {
            $id_delegado =$row['id_delegado'];
            $puesto_delegado =$row['puesto_delegado'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){
            
            $codigo_delegado_tmp=$dni_delegado_votar*7*13*37;//para codificar el código

            $codigo_delegado_tmp = substr($codigo_delegado_tmp, -4);

            if($cod_delegado_votar == $codigo_delegado_tmp){
                
                unset($_SESSION['id_delegado']);
                unset($_SESSION['puesto_delegado']);
                
                $_SESSION["id_delegado"]=$id_delegado;
                $_SESSION["puesto_delegado"]=$puesto_delegado;
                
                $id_delegado_encriptado = doEncrypt($id_delegado);
                $puesto_delegado_encriptado = doEncrypt($puesto_delegado);
                                
                //echo "<script>window.location.href='lista_delegado_votacion.php?xi_1=$id_delegado_encriptado&xi_2=$puesto_delegado_encriptado';</script>";
                echo "<script>window.location.href='lista_delegado_votacion.php';</script>";

            }else{
                echo "<script> alert('Código de votación incorrecto.');</script>";
            }
            
        }else{
            echo "<script> alert('No se encuentra un delegado con el DNI brindado.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if(isset($_POST['ver-asisdel-btn'])){

    if(!empty($_POST['dni-asist']) && !empty($_POST['cod-asist'])){
        
        $dni_delegado_asist = $_POST['dni-asist'];
        $cod_delegado_asist = $_POST['cod-asist'];

        $info_asistencia = listarinfoasis($dni_delegado_asist, $cod_delegado_asist);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_asistencia)) {
            $id_delegado_asis =$row['id_delegado'];
            $id_asistencia_asis =$row['id_asistencia'];
            $estado_asistencia_asis =$row['estado_asistencia'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){

            if($estado_asistencia_asis > 0){

                $resultado_asi_agr = registrarasistenciadel($id_delegado_asis, $id_asistencia_asis);
                $cuenta_tmp = 0;

                if($resultado_asi_agr){
                    echo "<script> alert('Se registró la asistencia.');</script>";
                }else{
                    echo "<script> alert('Ha ocurrido un problema al registrar la asistencia. Inténtelo de nuevo, o comuníquese con algún representante de Aneic para solucionar el problema.');</script>";
                }

            }else{
                echo "<script> alert('La asistencia ya se ha cerrado.');</script>";
            }
            
        }else{
            echo "<script> alert('La asistencia no se ha abierto para usted, o el código brindado es incorrecto.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}



if (isset($_POST['codigo-recu'])){
	if(!empty($_POST['dni-recu'])){
		$dni_delegado_rec=$_POST['dni-recu'];

		$result=recuperarcontradel($dni_delegado_rec);

	}else{
        echo "<script> alert('Ingrese un DNI.');</script>";
    }
}
if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}    
    
?>
</body>
</html>