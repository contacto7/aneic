<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    //echo "<script> window.location.href='index.php';</script>";
}

$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
$position_y_style="";

while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
    
    $position_y =$row['position_y'];
    
    if($position_y){
        $position_y_style = "background-position-y:".$position_y."px";
    }else{
        $position_y = "-";
    }
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();

while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];

}
mysqli_free_result($info_res);
desconectar();

?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Página principal</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
</head>
<body style="min-height:inherit;">
<script type="text/javascript" src="js/jquery.min.js"></script>

<div id="header"></div>
    
<div class="cont-index" style=";background-image: url(<?php echo $url_fotos_arr[2]; ?>);<?php echo $position_y_style; ?>">
<?php if($sesioninic){ ?>
    <div class="btn-foto-body" href="#camb-fotsup-mod" data-target="#camb-fotsup-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" onclick="cambtipo(3)">
        <button>Editar</button>
    </div>
    <div class="cambiar-distancia-wrapp">
        <form class="form-horizontal form-datoper" role="form" action="" method="post" enctype="multipart/form-data">
             <div class="row form-datper-left">
                <div class="col-xs-12">
                    <label class="lbl-login-mod">Cambiar distancia</label>
                    <input type="hidden" class="archivo-subir" id="dist-foto-adm" name="dist-foto-adm" value="no-cambio" data-attr="<?php echo $position_y; ?>">
                </div>
                <div class="btn-cambdist-wrapp">
                    <a class="btn btn-warning camb-pos-fotin" data-tipo="1"><span class="glyphicon glyphicon-chevron-up"></span></a>
                    <a class="btn btn-warning camb-pos-fotin" data-tipo="2"><span class="glyphicon glyphicon-chevron-down"></span></a>
                </div>
             </div>
             <div class="row">
                 <div class="col-xs-12 datpersbut-wrap">
                      <div class="datpersbut-mod">
                        <button type="submit" class="btn btn-datper-mod btn-sm" name="fotodist-adm-sub">Guardar</button>
                     </div>
                 </div>
             </div>
        </form>
    </div>
<?php } ?>
    <div class="cuadro-index-wrapper">

    </div>
    <div class="container container-ind">

        <div class="row">
            <div class="index-contenido-princ">
                <div class="col-xs-12">
                    <div class="index-bienv">
                        Bienvenido
                    </div>
                </div>
                <div class="col-xs-12 index-aneic-wrapp">
                    <div class="index-aneic">
                        ASOCIACIÓN NACIONAL DE ESTUDIANTES DE INGENIERÍA CIVIL
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="index-btn-wrap">
                        <a class="index-btn" href="nosotros.php">
                            <span>Nosotros</span>
                        </a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="index-btn-wrap">
                        <a class="index-btn" href="eventos.php">
                            Eventos
                        </a>
                    </div>
                </div>            
            </div>

        </div>
        

    </div>
    
    
    <div class="wrapp-univ-aso">
        <div class="univ-aso-inn">
            <div class="univ-asoinn-tit"><span class="glyphicon glyphicon-book"></span> Universidades asociadas</div>
        </div>
        <div id="carrec-wrapper">
            <div class="list_carousel">
                <ul id="foo0">
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-1">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-2">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-3">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-4">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-5">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-6">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-7">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-8">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-9">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-10">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-11">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-12">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-13">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-14">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-15">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-16">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-17">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-18">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-19">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-20">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-21">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-22">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-23">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-24">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-25">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-26">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-27">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-28">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-29">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-30">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-31">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-32">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-33">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-34">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-35">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-36">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-37">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-38">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-39">
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="recurso-wrapp">
                            <div class="recurso-inn">
                                <img class="recurso-img" alt="imagen de universidad" src="img/img-trans.png" id="img-recurso-40">
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
    
    </div>
    
</div>
<?php if($sesioninic){ ?>
        <div class="bot-nav-ap">
            <div class="btn-info-nav" href="#camb-infosup-mod" data-target="#camb-infosup-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" onclick="cambtipo(2)">
                <button>Editar</button>
            </div> 
        </div>
<?php } ?>
<div id="footer"></div>

<?php if($sesioninic){ ?>
    
<div id="camb-fotsup-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Cambiar Fotografía</h4>
            </div>
            <div class="modal-body modbod-exp">
                 <form class="form-horizontal form-datoper" role="form" action="" method="post" enctype="multipart/form-data">
                     <div class="row form-datper-left">
                        <div class="col-xs-12">
                          <input type="number" class="archivo-subir" id="tipo-foto" name="tipo-foto" style="display:none" value="0">
                        </div>
                        <div class="col-xs-12">
                            <label class="lbl-login-mod">Cambiar foto</label>
                            <label for="foto-adm"><a class="btn btn-md form-control input-sm selec-arch-mod">Selecciona archivo</a></label>
                            <div id="nombrefoto"></div>
                              <input type="file" class="archivo-subir" id="foto-adm" name="foto-adm" style="display:none" data-id="1">
                            <span class="span-foto-estado" id="estado-cambio-1"></span>
                        </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-12 datpersbut-wrap">
                              <div class="datpersbut-mod">
                                <button type="submit" class="btn btn-datper-mod btn-sm" name="foto-adm-sub" >Guardar</button>
                                <button type="button" class="btn btn-datper-canc btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                             </div>
                         </div>
                     </div>
                </form>
            </div>

        </div>
    </div>
</div>
    
    
<div id="camb-infosup-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title">Cambiar información</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post">
                  <div class="form-group" style="display:none">
                    <label class="control-label col-sm-2" for="email">Hora apertura:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="hora-ingreso" name="hora-ingreso" placeholder="HH:MM" value="<?php echo $horainic_adm_info; ?>">
                    </div>
                  </div>
                  <div class="form-group" style="display:none">
                    <label class="control-label col-sm-2" for="email">Hora cierre:</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="hora-salida" name="hora-salida" placeholder="HH:MM" value="<?php echo $horafin_adm_info; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-2" for="email">Celular:</label>
                    <div class="col-sm-10">
                      <input type="number" class="form-control" id="numero-contacto" name="numero-contacto" placeholder="Ingrese número de contacto" value="<?php echo $contacto_adm_info; ?>">
                    </div>
                  </div>

                     <div class="row">
                         <div class="col-xs-12 datpersbut-wrap">
                              <div class="datpersbut-mod">
                                <button type="submit" class="btn btn-datper-mod btn-sm" name="info-adm-sub" >Guardar</button>
                                <button type="button" class="btn btn-datper-canc btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                             </div>
                         </div>
                     </div>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>

<?php } ?>
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script src="js/jquery.carouFredSel.js"></script>

<script>
$(function(){
    
   $("#header").load("header.php", {
       xph: 0, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<script>

//	Basic carousel, no options
//	Basic carousel, no options
$.noConflict();
jQuery(document).ready(function($){
    $('#foo0').carouFredSel({

             width: '200px',
        responsive: true,
        scroll: {
            items: 1,
            duration: 3000,
            timeoutDuration: 0,
            easing: 'linear',
        },
        items: {
             width: '200',
        //	height: '30%',	//	optionally resize item-height
            visible: {
                min: 1,
                max:10
            }
        }
    });
});


</script>
    
    
<?php

if (isset($_POST['foto-adm-sub'])){

    if($image_size=$_FILES['foto-adm']['size']>0 || !empty($_POST['tipo-foto']) ){

        $allowed =  array('gif','png' ,'jpg', 'jpeg','GIF','PNG' ,'JPG', 'JPEG');
        $csvMimes = array('image/gif','image/png' ,'image/jpg','image/jpeg','image/bmp','image/ico','image/apng','image/xbm','image/webp','image/svg','image/GIF','image/PNG' ,'image/JPG', 'image/JPEG','image/BMP','image/ICO','image/APNG','image/XBM','image/WEBP','image/SVG');
        $filename = $_FILES['foto-adm']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        
        $nuevo_nombre=randomalfanume(25);
        $nuevo_nombre= $nuevo_nombre.".".$ext;
        $url_foto= "adm_img/".$nuevo_nombre;

        $image_temp_name=$_FILES['foto-adm']['tmp_name'];
        
        $moved = move_uploaded_file($image_temp_name,$url_foto);
        
        if($moved){

            $pagdireccion = "index.php";

            $tipo_foto_mod=$_POST['tipo-foto'];

            modificarfotoadm($tipo_foto_mod, $url_foto, $pagdireccion);


        }else{
            echo "<script>alert('Hubo un error al cargar la imagen. Por favor intenta más tarde. Error: ".$_FILES["foto-adm"]["error"]."')</script>";
            exit();
        }

    }

}
    
if (isset($_POST['info-adm-sub'])){

    if(!empty($_POST['hora-ingreso']) || !empty($_POST['hora-salida']) || !empty($_POST['numero-contacto']) ){

        $hora_ingreso_mod=$_POST['hora-ingreso'];
        $hora_salida_mod=$_POST['hora-salida'];
        $num_contacto_mod=$_POST['numero-contacto'];
        
        $pagdireccion = "index.php";

        modificarinfoadm($hora_ingreso_mod, $hora_salida_mod, $num_contacto_mod, $pagdireccion);

    }

}
    
if (isset($_POST['fotodist-adm-sub'])){

        $distancia_nueva=$_POST['dist-foto-adm'];

        modificarfotodistanciaadm($distancia_nueva);

}

if(isset($_POST['ver-votdel-btn'])){

    if(!empty($_POST['dni-votar']) && !empty($_POST['cod-votar'])){
        
        $dni_delegado_votar = $_POST['dni-votar'];
        $cod_delegado_votar = $_POST['cod-votar'];

        $info_delegado = listarinfodelegadodni($dni_delegado_votar);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_delegado)) {
            $id_delegado =$row['id_delegado'];
            $puesto_delegado =$row['puesto_delegado'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){
            
            $codigo_delegado_tmp=$dni_delegado_votar*7*13*37;//para codificar el código

            $codigo_delegado_tmp = substr($codigo_delegado_tmp, -4);

            if($cod_delegado_votar == $codigo_delegado_tmp){
                
                unset($_SESSION['id_delegado']);
                unset($_SESSION['puesto_delegado']);
                
                $_SESSION["id_delegado"]=$id_delegado;
                $_SESSION["puesto_delegado"]=$puesto_delegado;
                
                $id_delegado_encriptado = doEncrypt($id_delegado);
                $puesto_delegado_encriptado = doEncrypt($puesto_delegado);
                                
                //echo "<script>window.location.href='lista_delegado_votacion.php?xi_1=$id_delegado_encriptado&xi_2=$puesto_delegado_encriptado';</script>";
                echo "<script>window.location.href='lista_delegado_votacion.php';</script>";

            }else{
                echo "<script> alert('Código de votación incorrecto.');</script>";
            }
            
        }else{
            echo "<script> alert('No se encuentra un delegado con el DNI brindado.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}

if(isset($_POST['ver-asisdel-btn'])){

    if(!empty($_POST['dni-asist']) && !empty($_POST['cod-asist'])){
        
        $dni_delegado_asist = $_POST['dni-asist'];
        $cod_delegado_asist = $_POST['cod-asist'];

        $info_asistencia = listarinfoasis($dni_delegado_asist, $cod_delegado_asist);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_asistencia)) {
            $id_delegado_asis =$row['id_delegado'];
            $id_asistencia_asis =$row['id_asistencia'];
            $estado_asistencia_asis =$row['estado_asistencia'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){

            if($estado_asistencia_asis > 0){

                $resultado_asi_agr = registrarasistenciadel($id_delegado_asis, $id_asistencia_asis);
                $cuenta_tmp = 0;

                if($resultado_asi_agr){
                    echo "<script> alert('Se registró la asistencia.');</script>";
                }else{
                    echo "<script> alert('Ha ocurrido un problema al registrar la asistencia. Inténtelo de nuevo, o comuníquese con algún representante de Aneic para solucionar el problema.');</script>";
                }

            }else{
                echo "<script> alert('La asistencia ya se ha cerrado.');</script>";
            }
            
        }else{
            echo "<script> alert('La asistencia no se ha abierto para usted, o el código brindado es incorrecto.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if (isset($_POST['codigo-recu'])){
	if(!empty($_POST['dni-recu'])){
		$dni_delegado_rec=$_POST['dni-recu'];

		$result=recuperarcontradel($dni_delegado_rec);

	}else{
        echo "<script> alert('Ingrese un DNI.');</script>";
    }
}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}
?>
</body>
</html>