<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    echo "<script> window.location.href='index.php';</script>";
}

$id_asistencia_encriptado = str_replace(" ", "+", $_GET['x_i_2']);
$descripcion_asistencia_encriptado = str_replace(" ", "+", $_GET['x_i_1']);

//$id_votacion_encriptado = $_GET['x_i_2'];
$id_asistencia = (int)doDecrypt($id_asistencia_encriptado);

//$votacion_descripcion_encriptado = $_GET['x_i_1'];
$descripcion_asistencia = (string)doDecrypt($descripcion_asistencia_encriptado);

$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();

//Tabla de votaciones
$lista_votaciones = listarasistenciasdelegados($id_asistencia);

$tablas_html="";
$tablas_html.="</tbody></table>";

$tablas_html.="<table class='table table-striped tabla-vot-list' id='tabla-asistencia'><thead><tr><th>Persona</th><th>Cargo</th><th>Asiste</th></tr></thead><tbody>";
$estado_texto = "Cerrada";
$btn_accion = "";


while ($row=mysqli_fetch_assoc($lista_votaciones)) {
    $id_delegado = $row['id_delegado'];
    $nombres_delegado = $row['nombres_delegado'];
    $apellidos_delegado = $row['apellidos_delegado'];
    $puesto_delegado = $row['puesto_delegado'];        
    
    $puesto_nombre = "";

    switch($puesto_delegado){
        case 1:
            $puesto_nombre = "Delegado";
            break;
        case 2:
            $puesto_nombre = "Sub-Delegado";
            break;
        case 3:
            $puesto_nombre = "1° Accesitario";
            break;
        case 4:
            $puesto_nombre = "2° Accesitario";
            break;
        case 5:
            $puesto_nombre = "Miembro Honorario";
            b6eak;
        case 6:
            $puesto_nombre = "Secretario Corede";
            break;
        case 7:
            $puesto_nombre = "Consejo Directivo";
            break;
		case 8:
            $puesto_nombre = "Sub Secretario de Corede";
            break;
		case 9:
            $puesto_nombre = "Embajador Internacional";
            break;
    } 
    
    $tablas_html.="<tr><td>$nombres_delegado $apellidos_delegado</td><td>$puesto_nombre</td><td><input type='checkbox' class='form-control input-sm asistencias-input' value='1' data-id='$id_delegado'></td></tr>";

}

$tablas_html.="</tbody></table>";
mysqli_free_result($lista_votaciones);
desconectar();
?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Eventos</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body style="min-width:450px;height:100%">
<script type="text/javascript" src="js/jquery.min.js"></script>
    
<div id="header"></div>
    
<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Asistencia: <?php echo $descripcion_asistencia; ?></span>
    </div>
</div>

<div class="container cont-eventos" style="min-height: 100%;margin: -472px auto 0;">
    <div class="row" style="padding-top: 472px;">
        <?php echo $tablas_html; ?>
    </div>
    <div class="row">

        <div>
            <button class="btn btn-guardar-asist" style="float:right"><span class="glyphicon glyphicon-pencil"></span> Tomar asistencia</button>
        </div>

    </div>
</div>

<div id="footer"></div>

<div id="publi-vota-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Publicar votación</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea publicar la votación: " <span class="vota-dscr"></span>"?</div>
                <div class="mensaje-vota-pub"></div>
                <div class="vota-modbod-btn">
                    <button type="button" class="btn btn-sm btn-warning publi-vota-btn" data-id="0">Si</button>
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                </div>
            </div>

        </div>
    </div>
</div> 
    
<div id="cerrar-asis-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Cerrar asistencia</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea cerrar la votación: " <span class="vota-dscr"></span>"?</div>
                <div class="mensaje-vota-cerrar"></div>
                <div class="vota-modbod-btn">
                    <button type="button" class="btn btn-sm btn-warning cerrar-vota-btn" data-id="0">Si</button>
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                </div>
            </div>

        </div>
    </div>
</div> 
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
    
<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 3, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<script>

    
$(document).on("click", '.btn-guardar-asist',function(){
    
    $("#tabla-asistencia").css({
        'opacity':'0.5',
        'pointer-events':'none'
    });

    var idpersonas=new Array();
    var temp=0;

    $( ".asistencias-input" ).each(function() {
        if($(this).is(':checked')){
            temp=$(this).attr("data-id");
            idpersonas.push(temp);
        }
    });
    //console.log(notas);
    //console.log(idnotas);
    //$id_alumno,$id_curso,50,$bimestre,$evaluacion

        $.ajax({
        url:'modificar_asistencias.php',
        type:'get',
        data:{
            'xidpersonas':idpersonas,
            'xidasistencia':"<?php echo $id_asistencia_encriptado; ?>",
        },
        success: function (result) {
            $("#tabla-asistencia").css({
                'opacity':'1',
                'pointer-events':'inherit'
            });
            if(result.includes("Error")){
                //console.log(result);
                alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
            }else{
                console.log(result);
                alert("Se realizaron los cambios.");
                window.location.href = "adm_asistencia_lista.php";
            }
        }
    });

});
    
    
</script>
<?php

if (isset($_POST['agre-vot-sub'])){

    if(!empty($_POST['descr-votacion'])){
        
        $votacion_descripcion_nue = $_POST['descr-votacion'];
        $id_asistencia_nue = $_POST['asistencia-vota'];
        
        $vota_delegados = 0;
        $vota_subdelegados = 0;
        $vota_accesitario1 = 0;
        $vota_accesitario2 = 0;
        $vota_miembros_hon = 0;
        $vota_secretario_corede = 0;
        $vota_consejo_direct = 0;
        $vota_subsecreta=0;
		$vota_embajador = 0;
        
        if(isset($_POST['del-vot'])){
            $vota_delegados = 1;
        }
        if(isset($_POST['subdel-vot'])){
            $vota_subdelegados = 1;
        }
        if(isset($_POST['acce1-vot'])){
            $vota_accesitario1 = 1;
        }
        if(isset($_POST['acce2-vot'])){
            $vota_accesitario2 = 1;
        }
        if(isset($_POST['miembhon-vot'])){
            $vota_miembros_hon = 1;
        }
        if(isset($_POST['secreta-vot'])){
            $vota_secretario_corede = 1;
        }
        if(isset($_POST['consejo-vot'])){
            $vota_consejo_direct = 1;
        } 
		if(isset($_POST['subsecreta_vot'])){
            $vota_subsecreta = 1;
        }
        if(isset($_POST['embajador-vot'])){
            $vota_embajador = 1;
        }
        
        
        $id_votacion = anadirvotacion($votacion_descripcion_nue, $id_asistencia_nue, $vota_delegados, $vota_subdelegados, $vota_accesitario1, $vota_accesitario2, $vota_miembros_hon, $vota_secretario_corede, $vota_consejo_direct,$vota_subsecreta,$vota_embajador);
        
        if($id_votacion =="Error"){
            echo "<script>alert('Hubo un error al añadir la votación. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script> window.location.href='adm_votacion_lista.php';</script>";
        }
    }else{

        echo "<script>alert('Ingrese la descripción de la votación.')</script>";
    }

}

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}


if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}   
?>
</body>
</html>