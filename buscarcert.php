<?php 
session_start();
$nombres="";
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';

if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}
setlocale(LC_TIME, "");
setlocale(LC_ALL,"es_ES.UTF8");

$busquedaqr=0;
$busquedanomb=0;
$busquedahecha=1;

if (isset($_GET['xid'])) {
	$xid = $_GET['xid'];
    $busquedaqr=1;
} elseif(isset($_GET['xid_pers'])){
	// This month
    $xid = $_GET['xid_pers'];
    $busquedanomb=1;
}else{
    $busquedahecha=0;
}
$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();

?>
<!DOCTYPE html>
<html ng-app="">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jquery.raty.css">
    <link href="css/princ.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery.min.older.js"></script>
</head>
<body style="height:100%">
<script type="text/javascript" src="js/jquery.min.js"></script>

<div id="header"></div>
    
<div class="container cont-buscarcert">

    <div class="row">
        <div class="col-xs-12">
             <form class="form-horizontal form-qr-settings" style="" method="post">
              <div class="form-group">
                <label for="buscar-persona">Buscar personas por nombre</label>
                <input type="text" class="form-control" id="buscar-persona" name="buscar-persona">
              </div>
              <div class="form-group">
                <button class="btn btn-info" type="submit" id="buscar-subm" name="buscar-subm">Buscar</button> 
              </div>
            </form>
        </div>

    </div>

</div>
<?php 
    if($busquedahecha==1 && !isset($_POST['buscar-subm'])){
?>
<div class="cert-inst-wrapp">
    <div class="cert-inst-inn">
        <div class="cert-inst-texto">
            CERTIFICADO EMITIDO POR ANEIC PERU
        </div>
    </div>
</div>
<?php 
    }
?>

    
<div class="container cont-list-cert" style="min-height: 100%;margin: -472px auto 0;">
    <div class="row" style="padding-top: 472px;">
        
<?php 

if(($busquedaqr==1 || $busquedanomb==1) && !isset($_POST['buscar-subm'])){
    
    if($busquedaqr==1){
        $result=listarcertificadoaistente($xid);
    }else{
        $result=listarcertificadosaistente($xid);
    }
    
    $temp=0;

    while ($row=mysqli_fetch_assoc($result)) {

        $nombre_asistente=$row['nombre_asistente'];
        $calidad_participante_certificado=$row['calidad_participante_certificado'];
        $nombre_evento=$row['nombre_evento'];
        $nombre_universidad=$row['nombre_universidad'];
        $fecemi_participante_certificado=$row['fecemi_participante_certificado'];

?>
        <div class="cert-wrapp">
            <div class="cert-inn">
                <div class="cert-inn-fila">
                    <div class="cert-inn-tit">Otorgado a:</div>
                    <div class="cert-inn-descr"><?php echo $nombre_asistente; ?></div>
                </div>
                <div class="cert-inn-fila">
                    <div class="cert-inn-tit">Evento:</div>
                    <div class="cert-inn-descr"><?php echo $nombre_evento; ?></div>
                </div>
                <div class="cert-inn-fila">
                    <div class="cert-inn-tit">En calidad de:</div>
                    <div class="cert-inn-descr"><?php echo $calidad_participante_certificado; ?></div>
                </div>
                <div class="cert-inn-fila">
                    <div class="cert-inn-tit">Universidad sede:</div>
                    <div class="cert-inn-descr"><?php echo $nombre_universidad; ?></div>                   
                </div>
                <div class="cert-inn-fila">
                    <div class="cert-inn-tit">Fecha de emisión:</div>
                    <div class="cert-inn-descr"><?php 
                            $fecemi_participante_certificado = DateTime::createFromFormat("Y-m-d", $fecemi_participante_certificado);
                            $fecemi_participante_certificado = strftime("%d de %B del %Y",$fecemi_participante_certificado->getTimestamp());
                            echo $fecemi_participante_certificado; 
                        
                        ?></div>
                </div>
            </div>
        </div>


<?php 
    $temp++;
    }
?>

    
<?php 
}elseif(isset($_POST['buscar-subm'])){
	if(!empty($_POST['buscar-persona'])){
		$nombre_asistente=$_POST['buscar-persona'];

        $pagdireccion="certificado.php";
		$result=buscarpersnombre($nombre_asistente,$pagdireccion);
        $cuenta_asist=1;
?>
    <div class="col-xs-12 row-pers-busq"> 
        <div class="table-pers-busq">
          <table class="table table-striped">
            <tbody>
<?php    
        $ids_asistentes=array();
        while ($row=mysqli_fetch_assoc($result)) {
            $id_asistente=$row['id_asistente'];
            $nombre_asistente=$row['nombre_asistente'];
            
            if (!in_array($id_asistente, $ids_asistentes)) {
                array_push($ids_asistentes, $id_asistente);


?>
              <tr>
                <td><?php echo $cuenta_asist; ?></td>
                <td><a class="link-pers-busq" href="certificado.php?xid_pers=<?php echo $id_asistente; ?>"><?php echo $nombre_asistente; ?></a></td>
              </tr>
<?php 
            $cuenta_asist++;
            }
        }
        
?>
            </tbody>
          </table>
        </div>
    </div>
<?php    
	}
}
    
?>

    
</div>
</div>
    
<div id="footer"></div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script type="text/javascript" src="js/jquery.qrcode.min.js"></script>
    
<script>
$(function(){
    
   $("#header").load("header.php", {
       xph: 0, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<script type="text/javascript">
$(document).ready(function() {

    
});
</script>
    
    
<?php 
if (isset($_POST['buscar-subm'])){
	if(!empty($_POST['buscar-persona'])){
		$nombre_asistente=$_POST['buscar-persona'];

        $pagdireccion="certificado.php";
		$result=buscarpersnombre($nombre_asistente,$pagdireccion);

	}
}


if(isset($_POST['ver-votdel-btn'])){

    if(!empty($_POST['dni-votar']) && !empty($_POST['cod-votar'])){
        
        $dni_delegado_votar = $_POST['dni-votar'];
        $cod_delegado_votar = $_POST['cod-votar'];

        $info_delegado = listarinfodelegadodni($dni_delegado_votar);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_delegado)) {
            $id_delegado =$row['id_delegado'];
            $puesto_delegado =$row['puesto_delegado'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){
            
            $codigo_delegado_tmp=$dni_delegado_votar*7*13*37;//para codificar el código

            $codigo_delegado_tmp = substr($codigo_delegado_tmp, -4);

            if($cod_delegado_votar == $codigo_delegado_tmp){
                
                unset($_SESSION['id_delegado']);
                unset($_SESSION['puesto_delegado']);
                
                $_SESSION["id_delegado"]=$id_delegado;
                $_SESSION["puesto_delegado"]=$puesto_delegado;
                
                $id_delegado_encriptado = doEncrypt($id_delegado);
                $puesto_delegado_encriptado = doEncrypt($puesto_delegado);
                                
                //echo "<script>window.location.href='lista_delegado_votacion.php?xi_1=$id_delegado_encriptado&xi_2=$puesto_delegado_encriptado';</script>";
                echo "<script>window.location.href='lista_delegado_votacion.php';</script>";

            }else{
                echo "<script> alert('Código de votación incorrecto.');</script>";
            }
            
        }else{
            echo "<script> alert('No se encuentra un delegado con el DNI brindado.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}

if(isset($_POST['ver-asisdel-btn'])){

    if(!empty($_POST['dni-asist']) && !empty($_POST['cod-asist'])){
        
        $dni_delegado_asist = $_POST['dni-asist'];
        $cod_delegado_asist = $_POST['cod-asist'];

        $info_asistencia = listarinfoasis($dni_delegado_asist, $cod_delegado_asist);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_asistencia)) {
            $id_delegado_asis =$row['id_delegado'];
            $id_asistencia_asis =$row['id_asistencia'];
            $estado_asistencia_asis =$row['estado_asistencia'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){

            if($estado_asistencia_asis > 0){

                $resultado_asi_agr = registrarasistenciadel($id_delegado_asis, $id_asistencia_asis);
                $cuenta_tmp = 0;

                if($resultado_asi_agr){
                    echo "<script> alert('Se registró la asistencia.');</script>";
                }else{
                    echo "<script> alert('Ha ocurrido un problema al registrar la asistencia. Inténtelo de nuevo, o comuníquese con algún representante de Aneic para solucionar el problema.');</script>";
                }

            }else{
                echo "<script> alert('La asistencia ya se ha cerrado.');</script>";
            }
            
        }else{
            echo "<script> alert('La asistencia no se ha abierto para usted, o el código brindado es incorrecto.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}


if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if (isset($_POST['codigo-recu'])){
	if(!empty($_POST['dni-recu'])){
		$dni_delegado_rec=$_POST['dni-recu'];

		$result=recuperarcontradel($dni_delegado_rec);

	}else{
        echo "<script> alert('Ingrese un DNI.');</script>";
    }
}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}    
    
?>
</body>
</html>