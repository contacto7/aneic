<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    //echo "<script> window.location.href='index.php';</script>";
}

$id_votacion_encriptado = str_replace(" ", "+", $_GET['x_i_2']);
$votacion_descripcion_encriptado = str_replace(" ", "+", $_GET['x_i_1']);

//$id_votacion_encriptado = $_GET['x_i_2'];
$id_votacion = (int)doDecrypt($id_votacion_encriptado);

//$votacion_descripcion_encriptado = $_GET['x_i_1'];
$votacion_descripcion = (string)doDecrypt($votacion_descripcion_encriptado);

//Inicio variables de header
$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();
//Fin variables de header

$lista_votacion = listarvotacionadm($id_votacion);
$temp_vot_tabla= "";
$num_tabla = 0;
$num_opcion = 0;
$id_votacion_tabla_temp = 0;
$tablas_html = "";

while ($row=mysqli_fetch_assoc($lista_votacion)) {
    $id_votacion_tabla =$row['id_votacion_tabla'];
    $descripcion_votacion_tabla =$row['descripcion_votacion_tabla'];
    $id_votacion_opcion =$row['id_votacion_opcion'];
    $descripcion_votacion_opcion =$row['descripcion_votacion_opcion'];
    
    if($id_votacion_opcion > 0){
        $id_votacion_opcion_encr = doEncrypt($id_votacion_opcion);
    }else{
        $id_votacion_opcion_encr = 0;
    }
    
    
    if($temp_vot_tabla != $descripcion_votacion_tabla){
        $num_tabla++;
        $num_opcion++;
        
        if($num_tabla>1){
            $tablas_html.="<tr id='agtr-$num_opcion'><td><a class='agregar-votacion-adm' data-toggle='modal' data-target='#agregar-voto-mod' data-table='$id_votacion_tabla_temp' data-id='agtr-$num_opcion'><span class='glyphicon glyphicon-plus'></span></a></span></a></td><td>Agregar opción</td></tr>";
            $tablas_html.="</tbody></table>";
        }
        
        
        $tablas_html.="<table class='table table-striped tabla-vot-tit' id='$num_tabla-vot' data-id='$id_votacion_tabla'><thead><tr><th>$num_tabla.</th><th>$descripcion_votacion_tabla <a class='btn btn-xs btn-danger remover-voto-tabla' href='#elim-vototab-mod' data-target='#elim-vototab-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-tr='$num_tabla-vot' data-id='$id_votacion_tabla' data-descr='$descripcion_votacion_tabla'><span class='glyphicon glyphicon-remove'></span></a></th></tr></thead><tbody>";
        
        if($id_votacion_opcion > 0){
            $tablas_html.="<tr id='$num_opcion'><td><a class='remover-voto-opc' href='#elim-voto-mod' data-target='#elim-voto-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-tr='$num_opcion' data-id='$id_votacion_opcion_encr' data-descr='$descripcion_votacion_opcion'><span class='glyphicon glyphicon-remove'></span></a></td><td>$descripcion_votacion_opcion</td></tr>";
        }else{
            
        }

        
        $temp_vot_tabla = $descripcion_votacion_tabla;
        
        
    }else{
        $num_opcion++;
        
        $tablas_html.="<tr id='$num_opcion'><td><a class='remover-voto-opc' href='#elim-voto-mod' data-target='#elim-voto-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-tr='$num_opcion' data-id='$id_votacion_opcion_encr' data-descr='$descripcion_votacion_opcion'><span class='glyphicon glyphicon-remove'></span></a></td><td>$descripcion_votacion_opcion</td></tr>";;
    }
    $id_votacion_tabla_temp =$id_votacion_tabla;
}

$num_opcion++;//Para agregar más filas
$num_tabla++;//Para agregar más tablas

if($num_tabla>1){//Para cuando no hay nada aún, sino se mostraría agregar opcion
    $tablas_html.="<tr id='agtr-$num_opcion'><td><a class='agregar-votacion-adm' data-toggle='modal' data-target='#agregar-voto-mod' data-table='$id_votacion_tabla_temp' data-id='agtr-$num_opcion'><span class='glyphicon glyphicon-plus'></span></a></span></a></td><td>Agregar opción</td></tr>";
    $tablas_html.="</tbody></table>";
}

$tablas_html.="<a class='agregar-votacion-adm' id='agregar-tabla-btn' data-toggle='modal' data-target='#agregar-tabla-mod'><span class='glyphicon glyphicon-plus'></span> Agregar Tabla</a>";
mysqli_free_result($lista_votacion);
desconectar();



?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Eventos</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body style="min-width:450px;height:100%">
<script type="text/javascript" src="js/jquery.min.js"></script>
    
<div id="header"></div>

<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Votación</span>
    </div>
</div>
<div class="container cont-votacion" style="min-height: 100%;margin: -472px auto 0;">
    <div class="row row-votacion" style="padding-top: 472px;">
        <div class="vot-tit-wrapp">
            <div class="vot-tit-princ">
                Votación: <?php echo $votacion_descripcion; ?>
            </div>
        </div>
        
        <div class="vot-tab-wrapp">

            <?php echo $tablas_html; ?>
            
        </div>
        <div>
            <a class="btn btn-default" href="adm_votacion_lista.php" style="float:right">Regresar</a>
        </div>        
    </div>
</div>

<div id="footer"></div>
    
<div id="elim-voto-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Eliminar opción</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea eliminar la opción: " <span id="opc-dscr"></span>"?</div>
                <div class="mensaje-opc-elim"></div>
                <div class="vota-modbod-btn">
                    <button type="button" class="btn btn-sm btn-warning elim-conf-btn" data-id="0" data-tr="0">Si</button>
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                </div>
            </div>

        </div>
    </div>
</div> 
    
<div id="elim-vototab-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Eliminar tabla</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea eliminar la tabla: " <span id="tab-dscr"></span>"?</div>
                <div class="mensaje-tab-elim"></div>
                <div class="vota-modbod-btn">
                    <button type="button" class="btn btn-sm btn-warning elim-tab-btn" data-id="0" data-tr="0">Si</button>
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                </div>
            </div>

        </div>
    </div>
</div> 
    
<div id="agregar-voto-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Agregar opción</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="form-group row">
                  <div class="col-sm-12">
                    <input class="form-control" type="text" placeholder="Ingrese la nueva opción de la votación" name="opc-votacion" id="opc-votacion">
                  </div>
                </div>
                <div class="mensaje-opc-agre"></div>
                <div class="vota-modbod-btn">
                    <button type="button" class="btn btn-sm btn-warning agre-conf-btn" data-table="0" data-idagre="">Agregar</button>
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                </div>
            </div>

        </div>
    </div>
</div>
    
<div id="agregar-tabla-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Agregar tabla</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="form-group row">
                  <div class="col-sm-12">
                    <input class="form-control" type="text" placeholder="Ingrese la nueva tabla de la votación" name="tabla-votacion" id="tabla-votacion">
                  </div>
                </div>
                <div class="mensaje-tabla-agre"></div>
                <div class="vota-modbod-btn">
                    <button type="button" class="btn btn-sm btn-warning agre-tabla-btn" data-table="0" data-idagre="">Agregar</button>
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                </div>
            </div>

        </div>
    </div>
</div>
    
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
    
<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 4, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<script>
var id_vot = "<?php echo $id_votacion_encriptado; ?>";
var num_tablas = <?php echo $num_tabla; ?>;
var ultima_opc= <?php echo $num_opcion; ?>;
    
$(document).ready(function(){
    
    $('[data-toggle="popover"]').popover();
    $('#preg-edit-mod').on('hidden.bs.modal', function () {
        var myNode = document.getElementById("mod-cont");
        while (myNode.firstChild) {
            myNode.removeChild(myNode.firstChild);
        }
    });
    $(document).ajaxSend(function(event, request, settings) {
      $('#loading-indicator').show();
    });

    $(document).ajaxComplete(function(event, request, settings) {
      $('#loading-indicator').hide();
    });
});
    
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
    
$(document).on("click", '.elim-conf-btn',function(){
    
    var i1_1 = $(this).attr('data-id');
    var i1_2 = $(this).attr('data-tr');
    //console.log(i1_1);
    //console.log(i1_2);
    
    $(".mensaje-opc-elim").html("Eliminando...");
    
    $.ajax({
        url:'apost_elimvoto_opcion.php',
        type:'post',
        data:{
            'x_i1': i1_1
        },
        success: function (result) {
            $('#elim-voto-mod').modal('hide');
            if(result == 1){
                //console.log(result);
                alert("Ocurrió algún problema al eliminar la opción. Si este inconveniente persiste, comuníquese con servicio técnico.");
            }else if(result == 2){
                alert("Se eliminó la opción con éxito.");
                $( "#"+i1_2).remove();
            }else if(result == 3){
                alert("El sistema de votación no ha identificado el dominio como Aneic. Por favor, contáctese con servicio técnico.");
            }else{

            }
            $(".mensaje-opc-elim").html("");
            
        }
    });


});
    
$(document).on("click", '.elim-tab-btn',function(){
    
    var i1_1 = $(this).attr('data-id');
    var i1_2 = $(this).attr('data-tr');
    //console.log(i1_1);
    //console.log(i1_2);
    
    $(".mensaje-tab-elim").html("Eliminando...");
    
    $.ajax({
        url:'apost_elimvoto_tabla.php',
        type:'post',
        data:{
            'x_i1': i1_1
        },
        success: function (result) {
            $('#elim-vototab-mod').modal('hide');
            if(result == 1){
                //console.log(result);
                alert("Ocurrió algún problema al eliminar la tabla. Si este inconveniente persiste, comuníquese con servicio técnico.");
            }else if(result == 2){
                alert("Se eliminó la tabla con éxito.");
                $( "#"+i1_2).remove();
                num_tablas--;
                
                $( ".tabla-vot-tit" ).each(function( index, element ) {
                    var nuevo_num_tabla = index+1;
                    $elem = $( this ).find("th:first");
                    
                    $elem.html(nuevo_num_tabla+".");
                });
                
            }else if(result == 3){
                alert("El sistema de votación no ha identificado el dominio como Aneic. Por favor, contáctese con servicio técnico.");
            }else{

            }
            $(".mensaje-tab-elim").html("");
            console.log(result+"#"+i1_2);
        }
    });


});
    
$(document).on("click", '.agre-conf-btn',function(){
    
    var i1_1 = $(this).attr('data-table');
    var i1_2 = $('#opc-votacion').val();
    
    var i1_3 = $(this).attr('data-idagre');
    console.log(i1_1);
    console.log(i1_2);
    console.log(i1_3);
    
    $(".mensaje-opc-agre").html("Agregando...");
    
    $.ajax({
        url:'apost_agrevoto_opcion.php',
        type:'post',
        data:{
            'x_i1': i1_1,
            'x_i2': i1_2
        },
        success: function (result) {
            $('#agregar-voto-mod').modal('hide');
            if(result == 1){
                //console.log(result);
                alert("Ocurrió algún problema al agregar la opción. Si este inconveniente persiste, comuníquese con servicio técnico.");
            }else if(result == 2){
                alert("El sistema de votación no ha identificado el dominio como Aneic. Por favor, contáctese con servicio técnico.");
            }else{
                var insrt_tr="<tr id='"+ultima_opc+"'><td><a class='remover-voto-opc' href='#elim-voto-mod' data-target='#elim-voto-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-tr='"+ultima_opc+"' data-id='"+result+"' data-descr='"+i1_2+"'><span class='glyphicon glyphicon-remove'></span></a></td><td>"+i1_2+"</td></tr>";
                $(insrt_tr).insertBefore( "#"+i1_3 );
                ultima_opc++;
            }
            $(".mensaje-opc-agre").html("");
            $('#opc-votacion').val("");
            
        }
    });


});
    
$(document).on("click", '.agre-tabla-btn',function(){
    
    var i1_2 = $('#tabla-votacion').val();
    
    $(".mensaje-tabla-agre").html("Agregando...");
    
    $.ajax({
        url:'apost_agrevoto_tabla.php',
        type:'post',
        data:{
            'x_i1': id_vot,
            'x_i2': i1_2
        },
        success: function (result) {
            $('#agregar-tabla-mod').modal('hide');
            if(result == 1){
                //console.log(result);
                alert("Ocurrió algún problema al agregar la tabla. Si este inconveniente persiste, comuníquese con servicio técnico.");
            }else if(result == 2){
                alert("El sistema de votación no ha identificado el dominio como Aneic. Por favor, contáctese con servicio técnico.");
            }else{
                var insrt_tr="<table class='table table-striped tabla-vot-tit' id='"+num_tablas+"-vot' data-id='"+result+"'><thead><tr><th>"+num_tablas+".</th><th>"+i1_2+" <a class='btn btn-xs btn-danger remover-voto-tabla' href='#elim-vototab-mod' data-target='#elim-vototab-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-tr='"+num_tablas+"-vot' data-id='"+result+"' data-descr='"+i1_2+"'><span class='glyphicon glyphicon-remove'></span></a></th></tr></thead><tbody><tr id='agtr-"+ultima_opc+"'><td><a class='agregar-votacion-adm' data-toggle='modal' data-target='#agregar-voto-mod' data-table='"+result+"' data-id='agtr-"+ultima_opc+"'><span class='glyphicon glyphicon-plus'></span></a></td><td>Agregar opción</td></tr></tbody></table>";
                
                $(insrt_tr).insertBefore( "#agregar-tabla-btn" );
                num_tablas++;
                ultima_opc++;
            }
            $(".mensaje-tabla-agre").html("");
            $('#tabla-votacion').val("");
            
        }
    });


});
    
$(document).on("click", '.agregar-votacion-adm',function(){
    
    var id_table=$(this).attr('data-table');    
    var id_agre=$(this).attr('data-id');    
    
    $(".agre-conf-btn").attr('data-table',id_table);
    $(".agre-conf-btn").attr('data-idagre',id_agre);

});
    
$(document).on("click", '.remover-voto-opc',function(){
    
    var id_opc=$(this).attr('data-id');
    var tr_id=$(this).attr('data-tr');
    var dscr_opc=$(this).attr('data-descr');
    
    
    $("#opc-dscr").html(dscr_opc);
    $(".elim-conf-btn").attr('data-id',id_opc);
    $(".elim-conf-btn").attr('data-tr',tr_id);

});
    
$(document).on("click", '.remover-voto-tabla',function(){
    
    var dscr_tabla=$(this).attr('data-descr');
    var id_tabla=$(this).attr('data-id');
    var tr_id=$(this).attr('data-tr');
    
    $("#tab-dscr").html(dscr_tabla);
    $(".elim-tab-btn").attr('data-id',id_tabla);
    $(".elim-tab-btn").attr('data-tr',tr_id);

});

</script>
    
<?php

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}    
    
?>
</body>
</html>