<?php
include 'funciones.php';
$puesto_comite=$_GET['xpuesto'];
$ano_comite=$_GET['xano'];

$cmdsql="select * FROM 	adm_comite WHERE puesto_adm_comite = ".$puesto_comite." AND ano_comite=".$ano_comite;
$resultado=mysqli_query(conectar(), $cmdsql);
if (!$resultado) {
    echo "<br>Error de procedimiento";
}

while ($row=mysqli_fetch_assoc($resultado)) {
    $id_adm_comite =$row['id_adm_comite'];
    $puesto_adm_comite =$row['puesto_adm_comite'];
    $nombres_adm_comite =$row['nombres_adm_comite'];
    $apellidos_adm_comite =$row['apellidos_adm_comite'];
    $foto_adm_comite =$row['foto_adm_comite'];
    $descripcion_adm_comite =$row['descripcion_adm_comite'];
    $fb_adm_comite =$row['fb_adm_comite'];
    $li_adm_comite = $row['li_adm_comite'];
    
?>

    <div class="modal-header">
        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
        <h4 class="modal-title">Modificar Comité</h4>
    </div>
    <div class="modal-body modbod-exp">
         <form class="form-horizontal form-datoper" role="form" action="" method="post" enctype="multipart/form-data">
             <div class="row form-datper-left">
                <div class="col-xs-12">
                  <div class="form-group" style="display:none">
                    <label class="control-label col-sm-4" for="">puesto de comite:</label>
                    <div class="col-sm-8">
                      <input type="number" class="form-control" id="puesto-comite" name="puesto-comite" value="<?php echo $puesto_adm_comite; ?>">
                    </div>
                  </div>
                  <div class="form-group" style="display:none">
                    <label class="control-label col-sm-4" for="">año de comite:</label>
                    <div class="col-sm-8">
                      <input type="number" class="form-control" id="ano-comite" name="ano-comite" value="<?php echo $ano_comite; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="nombre-miembro">Nombres de miembro:</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="nombre-miembro" name="nombre-miembro" placeholder="Ingrese los nombres del miembro" value="<?php echo $nombres_adm_comite; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="apellidos-miembro">Apellidos de miembro:</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="apellidos-miembro" name="apellidos-miembro" placeholder="Ingrese los apellidos del miembro" value="<?php echo $apellidos_adm_comite; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="dscr-miembro">Descripción de miembro:</label>
                    <div class="col-sm-8">
                        <textarea rows="3" style="width:100%" class="form-control" name="dscr-miembro" id="dscr-miembro"><?php echo $descripcion_adm_comite; ?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="link-fb">Link de Facebook:</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="link-fb" name="link-fb" placeholder="Ingrese el link del facebook del miembro" value="<?php echo $fb_adm_comite; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="link-li">Link de LinkedIn:</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="link-li" name="link-li" placeholder="Ingrese el link de linkedin del miembro" value="<?php echo $li_adm_comite; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-4" for="link-evento">Imagen de miembro:</label>
                    <div class="col-sm-8">
                          <label for="foto-miembro"><a class="btn btn-md form-control input-sm selec-arch-mod">Selecciona archivo</a></label>
                        <div id="nombrefoto"></div>
                          <input type="file" class="archivo-subir" id="foto-miembro" name="foto-miembro" style="display:none" data-id="1">
                        <span class="span-foto-estado" id="estado-cambio-1"></span>
                    </div>
                  </div>

                </div>
             </div>
             <div class="row">
                 <div class="col-xs-12 datpersbut-wrap">
                      <div class="datpersbut-mod">
                        <button type="submit" class="btn btn-datper-mod btn-sm" name="miembro-adm-sub" >Guardar</button>
                        <button type="button" class="btn btn-datper-canc btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                     </div>
                 </div>
             </div>
        </form>
    </div>
<?php
    }
?>