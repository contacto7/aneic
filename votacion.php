<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    //echo "<script> window.location.href='index.php';</script>";
}

$id_delegado_encriptado = $_GET['xi_1'];
$id_votacion_encriptado = $_GET['xi_2'];
$votacion_descripcion_encriptado = $_GET['xi_3'];
$id_votacion_2_encriptado = $_GET['xi_4'];

$id_delegado = (int)doDecrypt($id_delegado_encriptado);
$id_votacion= (int)doDecrypt($id_votacion_encriptado);
$id_votacion_2= (int)doDecrypt($id_votacion_2_encriptado);
$id_votacion_2= $id_votacion_2 / 37;

$votacion_descripcion = doDecrypt($votacion_descripcion_encriptado);

//VALIDAMOS QUE NO ENTRE UN USUARIO CON OTRA SESION
//NO SE MODIFQUE EL LINK
if (!isset($_SESSION['id_delegado']) || $id_delegado != $_SESSION['id_delegado'] || $id_votacion_2 != $id_votacion){
    echo "<script> alert('Por favor, ingrese nuevamente al sistema de votaciones.');</script>";
 //   echo "<script> window.location.href='index.php';</script>";
}


//Inicio variables de header
$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();
//Fin variables de header

$lista_votacion = listarvotacion($id_votacion);
$temp_vot_tabla= "";
$num_tabla = 0;
$num_opcion = 1;

$tablas_html = "";

while ($row=mysqli_fetch_assoc($lista_votacion)) {
    $id_votacion_tabla =$row['id_votacion_tabla'];
    $descripcion_votacion_tabla =$row['descripcion_votacion_tabla'];
    $id_votacion_opcion =$row['id_votacion_opcion'];
    $descripcion_votacion_opcion =$row['descripcion_votacion_opcion'];
    
    if($temp_vot_tabla != $descripcion_votacion_tabla){        
        $num_tabla++;
        
        if($num_tabla>1){
            $tablas_html.="</tbody></table>";
        }
        
        $tablas_html.="<table class='table table-striped tabla-vot-tit' id='$num_tabla-vot' data-id='$id_votacion_tabla'><thead><tr><th>$num_tabla.</th><th>$descripcion_votacion_tabla</th></tr></thead><tbody>";
        
        $tablas_html.="<tr><td><input type='radio' name='$num_tabla-vot' id='$num_tabla-vot-$num_opcion' data-id='$id_votacion_opcion'></td><td><label for='$num_tabla-vot-$num_opcion' id='l-$num_tabla-vot-$num_opcion'>$descripcion_votacion_opcion</label></td></tr>";
        
        $temp_vot_tabla = $descripcion_votacion_tabla;
        
        
    }else{
        $num_opcion++;
        
        $tablas_html.="<tr><td><input type='radio' name='$num_tabla-vot' id='$num_tabla-vot-$num_opcion' data-id='$id_votacion_opcion'></td><td><label for='$num_tabla-vot-$num_opcion' id='l-$num_tabla-vot-$num_opcion'>$descripcion_votacion_opcion</label></td></tr>";
    }
}

$tablas_html.="</tbody></table>";
mysqli_free_result($lista_votacion);
desconectar();


?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Eventos</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body style="min-width:450px;">
<script type="text/javascript" src="js/jquery.min.js"></script>

<div id="header"></div>

<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Votación</span>
    </div>
</div>
<div class="container cont-votacion">
    <div class="row row-votacion">
        <div class="vot-tit-wrapp">
            <div class="vot-tit-princ">
                Votación: <?php echo $votacion_descripcion; ?>
            </div>
            <div class="vot-tit-sec">
                Seleccione el círculo al costado de su elección
            </div>
        </div>
        
        <div class="vot-tab-wrapp">

            <?php echo $tablas_html; ?>
            
        </div>
        
        <div class="vot-btn-wrapp">
            <button class="vot-btn" href="#mensaje-vot-mod" data-target="#mensaje-vot-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" onclick="anadirvoto()">
                Votar
            </button>
        </div>
        
    </div>
</div>

<div id="footer"></div>
    
<div id="mensaje-vot-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Estado de voto</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">Voto no contabilizado: ¡Debe seleccionar todas las opciones!</div>
                <div class="vota-modbod-btn">
                    <button type="button" class="btn btn-datper-canc btn-sm" data-dismiss="modal" aria-label="Close">Aceptar</button>
                </div>
            </div>

        </div>
    </div>
</div>
    
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
    
<script>
$(function(){
    
   $("#header").load("header.php", {
       xph: 1, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<script>
var num_votos = <?php echo $num_tabla; ?>;
var votos_rea = 0;
var arr_votos = [];
var estado_voto= 0;
    
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
    
$(function() {
   $('input[type="radio"]').on('click', function(e) {
       var name = e.currentTarget.name;
       var id = e.currentTarget.id;
        //console.log(e);
        //console.log(name);
        //console.log(id);
       $("#"+name+" tbody label").removeClass("vot-tab-tit-az");
       $("#"+name+" thead").addClass("vot-tab-tit-az");
       $("#l-"+id).addClass("vot-tab-tit-az");
       
       if($.inArray(name, arr_votos)<0) {
           votos_rea++;
            //add to array
            arr_votos.push(name); // <- basic JS see Array.push
           //console.log(votos_rea);
        } 
       
       if( num_votos == votos_rea ){
           //Ajax acá
           //console.log("presionado");
           estado_voto = 1;
           
       }
       
    });
});

function anadirvoto(){
    
    if(estado_voto == 1){
        $(".vota-modbod-msg").html("Esperando respuesta de servidor...");
    
        var ids_opciones= new Array();

        for (i = 1; i <= num_votos; i++) { 

            var opc_selec_id = $('input[name='+i+'-vot]:checked').attr("data-id");

            if(opc_selec_id==""){
                //console.log(dia);
            }else{
                //console.log(opc_selec_id);
                ids_opciones.push(opc_selec_id);
            }
        }


        $.ajax({
            url:'apost_voto_persona.php',
            type:'post',
            data:{
                'x_i1': "<?php echo $id_delegado_encriptado; ?>",
                'x_i2': "<?php echo $id_votacion_encriptado; ?>",
                'x_i3': ids_opciones
            },
            success: function (result) {
                if(result == 1){
                    //console.log(result);
                    $(".vota-modbod-msg").html("Ya se ha registrado un voto con su usuario.");
                }else if(result == 2){
                    //console.log(result);
                    $(".vota-modbod-msg").html("Ocurrió algún problema. Si este inconveniente persiste, comuníquese con el encargado de los votos.");
                }else if(result == 3){
                    $(".vota-modbod-msg").html("¡Voto realizado con éxito!");
                    console.log(result);
                    //window.location.reload();
                }else if(result == 4){
                    $(".vota-modbod-msg").html("¡El sistema de votación no ha identificado el dominio como Aneic. Por favor, contáctese con servicio técnico!");
                    console.log(result);
                    //window.location.reload();
                }else{
                    
                }

            }
        });
       
       
    }else{
       
    }
    
}
    
</script>
    
<?php



if(isset($_POST['ver-votdel-btn'])){

    if(!empty($_POST['dni-votar']) && !empty($_POST['cod-votar'])){
        
        $dni_delegado_votar = $_POST['dni-votar'];
        $cod_delegado_votar = $_POST['cod-votar'];

        $info_delegado = listarinfodelegadodni($dni_delegado_votar);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_delegado)) {
            $id_delegado =$row['id_delegado'];
            $puesto_delegado =$row['puesto_delegado'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){
            
            $codigo_delegado_tmp=$dni_delegado_votar*7*13*37;//para codificar el código

            $codigo_delegado_tmp = substr($codigo_delegado_tmp, -4);

            if($cod_delegado_votar == $codigo_delegado_tmp){
                
                unset($_SESSION['id_delegado']);
                unset($_SESSION['puesto_delegado']);
                
                $_SESSION["id_delegado"]=$id_delegado;
                $_SESSION["puesto_delegado"]=$puesto_delegado;
                
                $id_delegado_encriptado = doEncrypt($id_delegado);
                $puesto_delegado_encriptado = doEncrypt($puesto_delegado);
                                
                //echo "<script>window.location.href='lista_delegado_votacion.php?xi_1=$id_delegado_encriptado&xi_2=$puesto_delegado_encriptado';</script>";
                echo "<script>window.location.href='lista_delegado_votacion.php';</script>";

            }else{
                echo "<script> alert('Código de votación incorrecto.');</script>";
            }
            
        }else{
            echo "<script> alert('No se encuentra un delegado con el DNI brindado.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}    
    
?>
</body>
</html>