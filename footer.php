<?php 

$img_logo = "";
$tlf_contacto = 0;

$img_logo  = $_POST['xdf'] ;
$tlf_contacto  = $_POST['xdt'] ;

?>
<footer class="foot-wrap">
    <div class="foot-cont-wrapp">
        <div class="foot-wrapp-inn row">
            <div class="col-sm-5 col-footer-izquierda">
                <div class="foot-cont-izq">
                    <div class="foot-izq-rowlogo">
                        <img src="<?php echo $img_logo; ?>" class="foot-img-logo" alt="logo">
                    </div>
                    <div class="foot-izq-rowwrp">
                        <div class="foot-izq-row">
                            <a class="foot-izqrow-inn" href="tel:+51<?php echo $tlf_contacto; ?>">
                                <span class="glyphicon glyphicon-earphone"></span> 
                                <span class="info-izqfoot-row"><?php echo $tlf_contacto; ?></span>
                            </a>
                        </div><br>
                        <div class="foot-izq-row">
                            <a class="foot-izqrow-inn" href="mailto:presidencia@aneicperu.com" target="_blank">
                                <span class="glyphicon glyphicon-envelope"></span> 
                                <span class="info-izqfoot-row">presidencia@aneicperu.com</span>
                            </a>
                        </div><br>
                        <div class="foot-izq-row">
                            <a class="foot-izqrow-inn" href="mailto:secretaria.general@aneicperu.com" target="_blank">
                                <span class="glyphicon glyphicon-envelope"></span> 
                                <span class="info-izqfoot-row">secretaria.general@aneicperu.com</span>
                            </a>
                        </div><br>
                        <div class="foot-izq-row">
                            <a class="foot-izqrow-inn" href="https://www.facebook.com/ANEICPeru/" target="_blank">
                                <span class="facebook-gly">f</span> 
                                <span class="info-izqfoot-row">/AneicPeru</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-7 col-footer-derecha">
                <div class="foot-cont-der">
                    <div class="foot-contdertit-wrapp">
                        <div class="foot-contdertit"><span class="glyphicon glyphicon-play"></span> Contacto</div>
                    </div>
                    <div class="foot-continpt-wrapp">


                         <form class="form form-registdel form-contacto row" role="form" action="" method="post">
                            <div class="form-group col-sm-6">
                              <div class="">
                                <input class="form-control" type="text" placeholder="Nombres" name="nombres-contacto" id="nombres-contacto">
                              </div>
                            </div>
                            <div class="form-group col-sm-6">
                              <div class="">
                                <input class="form-control" type="text" placeholder="Apellidos" name="apellidos-contacto" id="apellidos-contacto">
                              </div>
                            </div>
                            <div class="form-group col-sm-6">
                              <div class="">
                                <input class="form-control" type="number" placeholder="Teléfono" name="tlf-contacto" id="tlf-contacto">
                              </div>
                            </div>
                            <div class="form-group col-sm-6">
                              <div class="">
                                <input class="form-control" type="email" placeholder="Correo" name="correo-contacto" id="correo-contacto">
                              </div>
                            </div>
                            <div class="form-group col-sm-12">
                              <div class="">
                                  <textarea class="form-control" placeholder="Describir consulta" name="consulta-contacto" id="consulta-contacto"></textarea>
                              </div>
                            </div>
                             <div class="form-group col-sm-12">
                                  <div class="enviar-contacto-wrapp">
                                    <button type="submit" class="btn btn-sub-cont btn-md" name="cont-sub" >Enviar</button>
                                  </div>
                             </div>
                        </form>
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-inner">
        <div class="footer-izq">
            <div class="aneic-footer">&copy; Aneic Perú 2018</div>
        </div>
        <div class="footer-der">
            <div class="copr-aqm"><span>Desarrollado por Aneic</span>
                <a href="" target="_blank" class="logo-foot-wrap">    <img class="logo-foot" src=""> 
                </a>
            </div>
        </div>
        <div class="footr-dwn">
            <a class="busq-cert-wrap" href="buscarcert.php" target="_blank">
                <div class="busq-cer-icon"><span class="glyphicon glyphicon-search"></span></div>
                <div class="busq-cer-lin">Buscar</div>
                <div class="busq-cer-lin">Certificado</div>
            </a>
        </div>
    </div>
</footer>
