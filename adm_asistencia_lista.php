<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    echo "<script> window.location.href='index.php';</script>";
}

$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();

//Tabla de asistencias
$lista_votaciones = listarasistenciasclompleto();

$tablas_html="";
$tablas_html.="</tbody></table>";
$tablas_html.="<table class='table table-striped tabla-vot-list'><thead><tr><th></th><th>Nombre</th><th>Fecha de Publicación</th><th>Acción</th></tr></thead><tbody>";
$estado_texto = "Cerrada";
$btn_accion = "";
$temp_tr= 0;
while ($row=mysqli_fetch_assoc($lista_votaciones)) {
    $id_asistencia = $row['id_asistencia'];
    $descripcion_asistencia = $row['descripcion_asistencia'];
    $fecha_asistencia = $row['fecha_asistencia'];
    $estado_asistencia = $row['estado_asistencia'];
    $tipo_asistencia = $row['tipo_asistencia'];
    $codigo_asistencia = $row['codigo_asistencia'];
    
   
    //Encriptando votacion
    $id_asistencia_encriptado= doEncrypt($id_asistencia);
    $descripcion_asistencia_encriptado= doEncrypt($descripcion_asistencia);

    $btn_eliminar = "<a class='remover-asis' href='#elim-asis-mod' data-target='#elim-asis-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-tr='$temp_tr' data-id='$id_asistencia_encriptado' data-descr='$descripcion_asistencia'><span class='glyphicon glyphicon-remove'></span></a>";
    
    
    if($estado_asistencia == 0){
        $estado_texto = "Cerrada";
        $btn_accion = "<a class='btn btn-sm btn-info' href='adm_asistencia_ver.php?x_i_1=$descripcion_asistencia_encriptado&x_i_2=$id_asistencia_encriptado' style='font-size:14px;color: #fff'>Ver asistencia</a>";
    }elseif($estado_asistencia == 1){
        
        if($tipo_asistencia == 1){
            $estado_texto = "Cerrada";
            $btn_accion = "<a class='btn btn-sm btn-success link-edit-vota' href='adm_asistencia_tomar.php?x_i_1=$descripcion_asistencia_encriptado&x_i_2=$id_asistencia_encriptado' style='font-size:14px;color: #fff'>Tomar asistencia</a>";
            $estado_texto = "Abierta";
        }elseif($tipo_asistencia == 2){

            $btn_accion = "<button class='btn btn-sm btn-danger cerrar-asis' href='#cerrar-asis-mod' data-target='#cerrar-asis-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id='$id_asistencia_encriptado' data-dscr='$descripcion_asistencia'>Cerrar</button>";
            $estado_texto = "Abierta";

            $descripcion_asistencia .= "<br><b>Código</b>: $codigo_asistencia";
        } 
        
    }    
    
    $fecha_asistencia = date_create($fecha_asistencia);
    $fecha_asistencia = date_format($fecha_asistencia,"H:i:s d/m/Y");
        
        
    $tablas_html.="<tr id='$temp_tr'><td>$btn_eliminar</td><td>$descripcion_asistencia</td><td>$fecha_asistencia</td><td>$btn_accion</td></tr>";

    $temp_tr++;
}

$tablas_html.="</tbody></table>";
mysqli_free_result($lista_votaciones);
desconectar();
?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Eventos</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body style="min-width:450px;height:100%">
<script type="text/javascript" src="js/jquery.min.js"></script>
    
<div id="header"></div>
    
<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Asistencia</span>
    </div>
</div>

<div class="container cont-eventos">
    <div class="row">
        <?php echo $tablas_html; ?>
    </div>
</div>

<div class="container cont-eventos" style="min-height: 100%;margin: -472px auto 0;">
    <div class="row" style="padding-top: 472px;">
        <div class="col-xs-12">
            <div class="agregar-vot-wrap">
                <a class="agregar-votacion-adm" data-toggle="modal" data-target="#agregar-asistencia"><span class="glyphicon glyphicon-plus"></span> Agregar asistencia</a>
            </div>
        </div>
    </div>
</div>
    
<div id="footer"></div>

<div id="agregar-asistencia" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Agregar Asistencia</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-registdel" role="form" action="" method="post">
            <div class="form-group row">
              <label for="descr-votacion" class="col-sm-2 col-form-label">Descripcion</label>
              <div class="col-sm-10">
                <input class="form-control" type="text" placeholder="Ingrese el nombre de la asistencia" name="descr-asistencia" id="descr-asistencia">
              </div>
            </div>
            <div class="form-group row">
              <label for="descr-votacion" class="col-sm-2 col-form-label">Tipo</label>
              <div class="col-sm-10">
                <select class="form-control sel-unibydel" name="tipo-asistencia" id="tipo-asistencia">
                    <option class="sel-prov" value="1">Administrable</option>
                    <option class="sel-prov" value="2">Auto-asistencia</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-12 col-form-label">Se toma asistencia de:</label>
            </div>            
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="del-asiste" id="del-asiste">
              </div>
              <label for="del-asiste" class="col-xs-11 col-form-label">Delegados</label>
            </div>
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="subdel-asiste" id="subdel-asiste">
              </div>
              <label for="subdel-asiste" class="col-xs-11 col-form-label">Sub-delegados</label>
            </div>
            
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="acce1-asiste" id="acce1-asiste">
              </div>
              <label for="acce1-asiste" class="col-xs-11 col-form-label">Accesitario 1</label>
            </div>
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="acce2-asiste" id="acce2-asiste">
              </div>
              <label for="acce2-asiste" class="col-xs-11 col-form-label">Accesitario 2</label>
            </div>
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="miembhon-asiste" id="miembhon-asiste">
              </div>
              <label for="miembhon-asiste" class="col-xs-11 col-form-label">Miembros Honorarios</label>
            </div>
            
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="secreta-asiste" id="secreta-asiste">
              </div>
              <label for="secreta-asiste" class="col-xs-11 col-form-label">Secretario Coredes</label>
            </div>
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="consejo-asiste" id="consejo-asiste">
              </div>
              <label for="consejo-asiste" class="col-xs-11 col-form-label">Consejo Directivo</label>
            </div>
			
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="subsecreta-asiste" id="subsecreta-asiste">
              </div>
              <label for="subsecreta-asiste" class="col-xs-11 col-form-label">Sub Secretario de Corede</label>
            </div>
			
            <div class="form-group row">
              <div class="col-xs-1">
                <input type="checkbox" value="1" name="embajador-asiste" id="embajador-asiste">
              </div>
              <label for="embajador-asiste" class="col-xs-11 col-form-label">Embajador Internacional</label>
            </div>
			
             <div class="form-group row">
                  <div class="col-xs-12 col-mod-btn">
                    <button type="submit" class="btn btn-ag-vot btn-sm" name="agre-asis-sub">Agregar</button>
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal">Cerrar</button>
                  </div>
             </div>
          </form>
          
          
      </div>
    </div>

  </div>
</div>
    
<div id="cerrar-asis-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Cerrar asistencia</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea cerrar la asistencia: " <span class="vota-dscr"></span>"?</div>
                <div class="mensaje-asis-cerrar"></div>
                <div class="vota-modbod-btn">
                    <button type="button" class="btn btn-sm btn-warning cerrar-asis-btn" data-id="0">Si</button>
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                </div>
            </div>

        </div>
    </div>
</div> 
    
<div id="elim-asis-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Eliminar asistencia</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea eliminar la asistencia: " <span id="opc-dscr"></span>"?</div>
                <div class=""><b>IMPORTANTE: Si elimina una asistencia asociada a una votación, generará problemas al momento de efectuar la votación.</b></div>
                <div class="mensaje-opc-elim"></div>
                <div class="vota-modbod-btn">
                    <button type="button" class="btn btn-sm btn-warning elim-asis-btn" data-id="0" data-tr="0">Si</button>
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                </div>
            </div>

        </div>
    </div>
</div> 
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
    
<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 3, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
    
    
$(document).on("click", '.cerrar-asis',function(){
    var dscr_vota = $(this).attr('data-dscr');
    var id_vota = $(this).attr('data-id');
    
    $(".vota-dscr").html(dscr_vota);
    $(".cerrar-asis-btn").attr('data-id',id_vota);

});    
     
$(document).on("click", '.remover-asis',function(){
    
    var id_opc=$(this).attr('data-id');
    var tr_id=$(this).attr('data-tr');
    var dscr_opc=$(this).attr('data-descr');
    
    
    $("#opc-dscr").html(dscr_opc);
    $(".elim-asis-btn").attr('data-id',id_opc);
    $(".elim-asis-btn").attr('data-tr',tr_id);

});
    
</script>
    
<script>
    
$(document).on("click", '.cerrar-asis-btn',function(){
    
    var i1_1 = $(this).attr('data-id');
    
    $(".mensaje-asis-cerrar").html("Cerrando...");
    
    $.ajax({
        url:'apost_cerrar_asistencia.php',
        type:'post',
        data:{
            'x_i1': i1_1
        },
        success: function (result) {
            $('#cerrar-asis-mod').modal('hide');
            if(result == 1){
                //console.log(result);
                alert("Ocurrió algún problema al cerrar la asistencia. Si este inconveniente persiste, comuníquese con servicio técnico.");
            }else if(result == 2){
                alert("Se cerró la asistencia con éxito.");
                location.reload();
            }else if(result == 3){
                alert("El sistema de asistencia no ha identificado el dominio como Aneic. Por favor, contáctese con servicio técnico.");
            }else{

            }
            $(".mensaje-asis-cerrar").html("");
            
        }
    });


});

$(document).on("click", '.elim-asis-btn',function(){
    
    var i1_1 = $(this).attr('data-id');
    var i1_2 = $(this).attr('data-tr');
    //console.log(i1_1);
    //console.log(i1_2);
    
    $(".mensaje-opc-elim").html("Eliminando...");
    
    $.ajax({
        url:'apost_eliminar_asistencia.php',
        type:'post',
        data:{
            'x_i1': i1_1
        },
        success: function (result) {
            $('#elim-asis-mod').modal('hide');
            if(result == 1){
                //console.log(result);
                alert("Ocurrió algún problema al eliminar la asistencia. Si este inconveniente persiste, comuníquese con servicio técnico.");
            }else if(result == 2){
                alert("Se eliminó la asistencia con éxito.");
                $( "#"+i1_2).remove();
            }else if(result == 3){
                alert("El sistema de votación no ha identificado el dominio como Aneic. Por favor, contáctese con servicio técnico.");
            }else{

            }
            $(".mensaje-opc-elim").html("");
            
        }
    });


});
    
</script>
<?php

if (isset($_POST['agre-asis-sub'])){

    if(!empty($_POST['descr-asistencia'])){
        
        $asistencia_descripcion_nue = $_POST['descr-asistencia'];
        $asistencia_tipo_nue = $_POST['tipo-asistencia'];
        
        $asisten_delegados = 0;
        $asisten_subdelegados = 0;
        $asisten_accesitario1 = 0;
        $asisten_accesitario2 = 0;
        $asisten_miembros_hon = 0;
        $asisten_secretario_corede = 0;
        $asisten_consejo_direct = 0;
        $asisten_subsecreta_asiste = 0;
        $asisten_embajador_asiste = 0;
		
		
        if(isset($_POST['del-asiste'])){
            $asisten_delegados = 1;
        }
        if(isset($_POST['subdel-asiste'])){
            $asisten_subdelegados = 1;
        }
        if(isset($_POST['acce1-asiste'])){
            $asisten_accesitario1 = 1;
        }
        if(isset($_POST['acce2-asiste'])){
            $asisten_accesitario2 = 1;
        }
        if(isset($_POST['miembhon-asiste'])){
            $asisten_miembros_hon = 1;
        }
        if(isset($_POST['secreta-asiste'])){
            $asisten_secretario_corede = 1;
        }
        if(isset($_POST['consejo-asiste'])){
            $asisten_consejo_direct = 1;
        }
		if(isset($_POST['subsecreta-asiste'])){
            $asisten_subsecreta_asiste = 1;
        }
		if(isset($_POST['embajador-asiste'])){
            $asisten_embajador_asiste = 1;
        }
        
        
        $id_asistencia_agr = anadirasistencia($asistencia_descripcion_nue, $asistencia_tipo_nue, $asisten_delegados, $asisten_subdelegados, $asisten_accesitario1, $asisten_accesitario2, $asisten_miembros_hon, $asisten_secretario_corede, $asisten_consejo_direct,$asisten_subsecreta_asiste,$asisten_embajador_asiste);
        
        if($id_asistencia_agr =="Error"){
            echo "<script>alert('Hubo un error al añadir la asistencia. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script> window.location.href='adm_asistencia_lista.php';</script>";
        }
    }else{

        echo "<script>alert('Ingrese la descripción de la asistencia.')</script>";
    }

}

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}


if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}   
?>
</body>
</html>