<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    echo "<script> window.location.href='index.php';</script>";
}


$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();


$nombre_puesto_delegado = "";
$filas_tabla_delegados = "";
$info_del= listardelegadosadm();
while ($row=mysqli_fetch_assoc($info_del)) {
    $apellidos_delegado=$row['apellidos_delegado'];
    $nombres_delegado=$row['nombres_delegado'];
    $dni_delegado=$row['dni_delegado'];
    $correo_delegado=$row['correo_delegado'];
    $celular_delegado=$row['celular_delegado'];
    $puesto_delegado=$row['puesto_delegado'];
    $departamento=$row['departamento'];
    $nombre_universidad=$row['nombre_universidad'];
    
    
    switch ($puesto_delegado) {
        case 1:
            $nombre_puesto_delegado = "Delegado";
            break;
        case 2:
            $nombre_puesto_delegado = "Sub-delegado";
            break;
        case 3:
            $nombre_puesto_delegado = "Accesitario 1";
            break;
        case 4:
            $nombre_puesto_delegado = "Accesitario 2";
            break;
        case 5:
            $nombre_puesto_delegado = "Miembro honorario";
            break;
        case 6:
            $nombre_puesto_delegado = "Secretario de COREDE";
            break;
        case 7:
            $nombre_puesto_delegado = "Consejo directivo";
            break;
		
		case 8:
			$nombre_puesto_delegado = "Sub Secretario de Corede";
			break;
			
		case 9:
			$nombre_puesto_delegado = "Embajador Internacional";
			break;
			
        default:
            echo "";
            break;
    }
    
    
    $filas_tabla_delegados .= "<tr>";
    $filas_tabla_delegados .= "<td>$apellidos_delegado</td>";
    $filas_tabla_delegados .= "<td>$nombres_delegado</td>";
    $filas_tabla_delegados .= "<td><center>$dni_delegado</center></td>";
    $filas_tabla_delegados .= "<td>$correo_delegado</td>";
    $filas_tabla_delegados .= "<td>$celular_delegado</td>";
    $filas_tabla_delegados .= "<td>$nombre_puesto_delegado</td>";
    $filas_tabla_delegados .= "<td>$departamento</td>";
    $filas_tabla_delegados .= "<td>$nombre_universidad</td>";
    $filas_tabla_delegados .= "</tr>";
    
    
}
mysqli_free_result($info_del);
desconectar();

?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Eventos</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body style="min-width:450px;">
<script type="text/javascript" src="js/jquery.min.js"></script>

<div id="header"></div>
    
<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Generar lista de delegados</span>
    </div>
</div>
<div class="container cont-eventos">
    <div class="row">
        <div class="col-xs-12">
            <form action="exporttoexcel.php" method="post" 
            onsubmit='$("#datatodisplay").val( $("<div>").append( $("#ReportTable").eq(0).clone() ).html() )'>
                <div class="expsub-wrapp">
                    <input class="btn btn-md btn-info" type="submit" value="Exportar a Excel">                
                </div>
              <table id="ReportTable" width="600" cellpadding="2" cellspacing="2" class="table table-bordered tabla-lista-delegados">
                <tr>
                  <th>Apellidos</th>
                  <th>Nombres</th>
                  <th>DNI</th>
                  <th>Correo</th>
                  <th>Celular</th>
                  <th>Cargo</th>
                  <th>Departamento</th>
                  <th>Universidad</th>
                </tr>
                  <?php echo $filas_tabla_delegados; ?>
              </table>
                <div class="expsub-wrappbot">
                    <input class="btn btn-md btn-info" type="submit" value="Exportar a Excel">                
                </div>
                <input type="hidden" id="datatodisplay" name="datatodisplay">
            </form>
        </div>
    </div>
</div>

<div id="footer"></div>
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 1, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
<?php

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}   
?>

</body>
</html>