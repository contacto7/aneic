/*
function downloadCanvas(link, canvasId, filename) {
    //alert($("#demo").children()[0]);
    //alert(document.getElementsByTagName("canvas")[0]);
    link.href = $("#demo").children()[0].toDataURL();
    link.download = filename;
}

document.getElementById('download').addEventListener('click', function() {
    var nombre= $(this).attr("data-nombre");
    var idpadre= $(this).attr("data-padreid");
    downloadCanvas(this, idpadre, nombre);
}, false);

*/


function dataqr(){
    $(".canvas-ant").hide();
    if($("#demo").children()[0]){
        $("#demo").children()[0].remove();
        $(".qr-canvas").children().remove();
    }
    
    var altopp=$("#altpapel").val();
    var anchopp=$("#anchpapel").val();
    var tamqr=$("#tamqr").val();
    var tamln=$("#grosorln").val();
    var esp=$("#espaciadoqr").val();
    var distarr=$("#distarr").val();
    var distizq=$("#distizq").val();
    generarqr(altopp,anchopp,tamqr,tamln,esp,distarr,distizq);
}

function generarqrejemplo(altopp,anchopp,tamqr,tamln,esp,distarr,distizq){

    var linkarch="http://www.aneicperu.com/certificado.php?xid=0";
    
    jQuery("#demo").qrcode({
        //render:"table"
        width: tamqr,
        height: tamqr,
        text: linkarch
    });



    // PONERLE EL RECUADRO AL QR
    var c=$("#demo").children()[0];

    var ctx=c.getContext("2d");
    var imgcanvas= ctx.getImageData(0,0,tamqr,tamqr);


    c.width = tamqr+(esp+tamln)*2;
    c.height = tamqr+(esp+tamln)*2;
    ctx.lineWidth=tamln;
    ctx.strokeStyle="#000";

    ctx.fillStyle = "#fff";
    ctx.fillRect(0, 0, c.width , c.height)

    ctx.strokeRect(2,2,tamqr+(esp+tamln)*2-4,tamqr+(esp+tamln)*2-4);
    ctx.putImageData(imgcanvas, esp+tamln, esp+tamln);
    ctx.stroke();

    //PONER EL QR EN UN RECTÁNGULO

    var nuevotam= c.width;
    var imgcanvasnueva= ctx.getImageData(0,0,nuevotam,nuevotam);

    c.width=anchopp;
    c.height = altopp;

    ctx.fillStyle = "#fff";
    ctx.fillRect(0, 0, c.width, c.height);
    ctx.putImageData(imgcanvasnueva, distizq, distarr);
    

}

function generarqr(altopp,anchopp,tamqr,tamln,esp,distarr,distizq){
    altopp = parseInt(altopp);
    anchopp = parseInt(anchopp);
    tamqr = parseInt(tamqr);
    tamln = parseInt(tamln);
    esp = parseInt(esp);
    distarr = parseInt(distarr);
    distizq = parseInt(distizq);
    console.log(tamqr+esp+tamln+distarr+distizq);
    
    generarqrejemplo(altopp,anchopp,tamqr,tamln,esp,distarr,distizq);    
    
    $('.qr-canvas').each(function(i, obj) {

        var id_evento=$(obj).attr("data-idevento");
        var id_certificado=$(obj).attr("data-idcert");
        var id_certificado_basedatos=$(obj).attr("data-id");
        var calidad=$(obj).attr("data-nombre");
        var nombre=$(obj).attr("data-calidad");

        var linkarch="http://www.aneicperu.com/certificado.php?xid="+id_certificado_basedatos;
        var nombre_arch_ver=nombre+"_"+calidad;
        //var nombre_arch_ver=id_evento+"_"+id_certificado;
        
        jQuery("#demo-"+i).qrcode({
            //render:"table"
            width: tamqr,
            height: tamqr,
            text: linkarch
        });



        // PONERLE EL RECUADRO AL QR
        var c=$("#demo-"+i).children()[0];

        var ctx=c.getContext("2d");
        var imgcanvas= ctx.getImageData(0,0,tamqr,tamqr);


        c.width = tamqr+(esp+tamln)*2;
        c.height = tamqr+(esp+tamln)*2;
        ctx.lineWidth=tamln;
        ctx.strokeStyle="#000";

        ctx.fillStyle = "#fff";
        ctx.fillRect(0, 0, c.width , c.height)

        ctx.strokeRect(2,2,tamqr+(esp+tamln)*2-4,tamqr+(esp+tamln)*2-4);
        ctx.putImageData(imgcanvas, esp+tamln, esp+tamln);
        ctx.stroke();

        //PONER EL QR EN UN RECTÁNGULO

        var nuevotam= c.width;
        var imgcanvasnueva= ctx.getImageData(0,0,nuevotam,nuevotam);

        c.width=anchopp;
        c.height = altopp;

        ctx.fillStyle = "#fff";
        ctx.fillRect(0, 0, c.width, c.height);
        ctx.putImageData(imgcanvasnueva, distizq, distarr);
    
        /*
        var canvashref = $("#demo-"+i).children()[0].toDataURL('image/png');
        $( '#download' ).attr( {
            href: canvashref,
            download: nombre_arch_ver + ".png"
        } )[0].click();
    
        */
    });
   
}

function descargarqrs(){
    $('.qr-canvas').each(function(j, obj2) {

        var id_evento=$(obj2).attr("data-idevento");
        var id_certificado=$(obj2).attr("data-idcert");
        var id_certificado_basedatos=$(obj2).attr("data-id");
        var nombre=$(obj2).attr("data-nombre");
        var calidad=$(obj2).attr("data-calidad");

        var linkarch="http://www.aneicperu.com/certificado.php?xid="+id_certificado_basedatos;
        //var nombre_arch_ver=id_evento+"_"+id_certificado;
        //var nombre_arch_ver=nombre+"_"+calidad;
        var nombre_arch_ver=id_certificado+"_"+nombre;
    

        var canvashref = $("#demo-"+j).children()[0].toDataURL('image/png');
        $( '#download-verd' ).attr( {
            href: canvashref,
            download: nombre_arch_ver + ".png"
        } )[0].click();
    
    });
}

$('#nuevau-delegado').change(function() {
    if($(this).is(":checked")){
        $("#univ-dep").val("0");        
        $("#row-univ-nue").show();
        $("#row-univ").hide();
    }else{
        $('#univag-dep').val('');
        $("#row-univ").show();
        $("#row-univ-nue").hide();
    }
});

$('#cargo-delegado').change(function() {
    if($(this).val()==1){
        $(".row-delegado").show();
    }else{
        if($("#nuevau-delegado").is(':checked')){
            console.log("a");
            $("#nuevau-delegado").click();
        }
    
        $(".row-delegado").hide();
    }
});

$('.archivo-subir').change(function() {
    var cambiofile = $(this).attr("data-id");
    $("#estado-cambio-"+cambiofile).html("Se seleccionó archivo.");
});

$( ".acept-del" ).on( "click", function() {
    var tipo= $(this).attr("data-tipo");
    var id_accion= $(this).attr("data-id");

    console.log(tipo);  
    console.log(id_accion);  


    if(tipo == 1){
        var dni= $(this).attr("data-dni");

        $("#td-"+id_accion).css({
            'opacity':"0.5",
            "pointer-events":"none"
        });
        
        
        var puesto= $(this).attr("data-puesto");
        $.ajax({
        url:'apost_aceptdel_post.php',
        type:'get',
        data:{
            'xid_accion':id_accion,
            'xid_universidad':id_universidad,
            'xpuesto':puesto,
            'xdni':dni
        },
        success: function (result) {
                $("#td-"+id_accion).css({
                    'opacity':"1",
                    "pointer-events":"inherit"
                }); 
            
                if(result.includes("Error")){
                    console.log(result);
                    alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                }else{
                    console.log(result);
                    $("#td-"+id_accion).html('<div class="tbl-acep"><span class="glyphicon glyphicon-ok"></span> Aceptado</div>');
                }
            
                var cuenta_acept=0;

                $(".acept-del").each( function( ){

                    cuenta_acept++;
                });
                console.log(cuenta_acept); 

                if(cuenta_acept == 0){
                    window.location.href='adm-regdel.php';
                }

            }
        });
        
    }else if(tipo == 2){
        if(confirm("¿Está seguro de rechazar al delegado?")){
            
            $("#td-"+id_accion).css({
                'opacity':"0.5",
                "pointer-events":"none"
            });


            var puesto= $(this).attr("data-puesto");
            $.ajax({
            url:'apost_rechadel_post.php',
            type:'get',
            data:{'xid_accion':id_accion,'xid_universidad':id_universidad,'xpuesto':puesto},
            success: function (result) {
                    $("#td-"+id_accion).css({
                        'opacity':"1",
                        "pointer-events":"inherit"
                    }); 

                    if(result.includes("Error")){
                        console.log(result);
                        alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                    }else{
                        console.log(result);
                        $("#td-"+id_accion).html('<div class="tbl-rech"><span class="glyphicon glyphicon-remove"></span> Rechazado</div>');
                    }

                    var cuenta_acept=0;

                    $(".acept-del").each( function( ){

                        cuenta_acept++;
                    });
                    console.log(cuenta_acept); 
                    
                    if(cuenta_acept == 0){
                        window.location.href='adm-regdel.php';
                    }

                }
            });
            
        }

        
    }else if(tipo == 3){

        $(".univ-href-acep").css({
            'opacity':"0.5",
            "pointer-events":"none"
        });
        
        $.ajax({
        url:'apost_aceptuniv_post.php',
        type:'get',
        data:{'xid_universidad':id_universidad},
        success: function (result) {
                $(".univ-href-acep").css({
                    'opacity':"1",
                    "pointer-events":"inherit"
                }); 
            
                if(result.includes("Error")){
                    console.log(result);
                    alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                }else{
                    console.log(result);
                    $(".univ-href-acep").html('<div class="tbl-acep"><span class="glyphicon glyphicon-ok"></span> Aceptada</div>');
                }
            
                var cuenta_acept=0;

                $(".acept-del").each( function( ){

                    cuenta_acept++;
                });
                console.log(cuenta_acept); 

                if(cuenta_acept == 0){
                    window.location.href='adm-regdel.php';
                }

            }
        });
        
    }else if(tipo == 4){
        
        if(confirm("Al rechazar la universidad también rechazará todos los delegados. ¿Está seguro de rechazar la universidad?")){
            
            $(".univ-href-acep").css({
                'opacity':"0.5",
                "pointer-events":"none"
            });
            
            $.ajax({
            url:'apost_rechauniv_post.php',
            type:'get',
            data:{'xid_universidad':id_universidad},
            success: function (result) {
                    $(".univ-href-acep").css({
                        'opacity':"1",
                        "pointer-events":"inherit"
                    }); 

                    if(result.includes("Error")){
                        console.log(result);
                        alert("Ocurrió algún problema. Inténtelo de nuevo más tarde.");
                    }else{
                        console.log(result);
                        $("td[id^='td-']").html('<div class="tbl-rech"><span class="glyphicon glyphicon-remove"></span> Rechazado</div>');
                        $(".univ-href-acep").html('<div class="tbl-rech"><span class="glyphicon glyphicon-remove"></span> Rechazada</div>');
                        
                    }

                    var cuenta_acept=0;

                    $(".acept-del").each( function( ){

                        cuenta_acept++;
                    });
                    console.log(cuenta_acept); 
                    
                    if(cuenta_acept == 0){
                        window.location.href='adm-regdel.php';
                    }
                
                }
            });
            
        }

        
    }
    
});

function cambtipo(tipo_foto){
    console.log(tipo_foto);
    $("#tipo-foto").attr('value', tipo_foto);
}

$( "#inicio-evento" ).unbind().on( "focus", function() {
  $('#btn-dp-1').click();
});

function getId(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }
}

$('.sel-comithist').change(function() {
    var ano_valor = $(this).val();
    var url = "comite.php?xano="+ano_valor; // get selected value
    if (url) { // require a URL
      window.location = url; // redirect
    }
    return false;
});

function recuperarcod(){
    $("#del-votar-mod").modal('hide');
    $("#olvide-mod").modal('show');

}

$( ".punto-dep-link" ).on( "click", function() {
    $( ".punto-dep-link circle" ).css({ fill: "#fff" });
    
    var departamento_val = $(this).attr("data-dep");
    $("#departamento").val(departamento_val);
    
    var color = $(this).children('svg').children('circle').css("stroke");
    $( ".punto-dep-link[data-dep='"+departamento_val+"'] circle" ).css({ fill: color});
    
    $('#departamento').trigger('change');
});

function mostrarcert(e){
    $('body').css({"min-width":"980px"});
    $("#canvas-"+e).css({
        "position": "relative",
        "top": "0",
        "left":"0"
    });
}


$('#cont-adm-nue2').on('input',function(e){
     if($('#cont-adm-nue').val() == $('#cont-adm-nue2').val()){
         $('#cabm-clave-btn').prop("disabled", false);
         $('#contra-repetir').removeClass('has-error');
         $('#contra-repetir .help-block').hide();
     }else{
         $('#contra-repetir').addClass('has-error');
         $('#contra-repetir .help-block').show();
         $('#cabm-clave-btn').prop("disabled", true);
     }
});
$('#cont-adm-nue').on('input',function(e){
     if($('#cont-adm-nue').val() == $('#cont-adm-nue2').val()){
         $('#cabm-clave-btn').prop("disabled", false);
         $('#contra-repetir').removeClass('has-error');
         $('#contra-repetir .help-block').hide();
     }else{
         $('#contra-repetir').addClass('has-error');
         $('#contra-repetir .help-block').show();
         $('#cabm-clave-btn').prop("disabled", true);
     }
});

$( ".camb-pos-fotin" ).on( "click", function() {
    //$( ".punto-dep-link circle" ).css({ fill: "#fff" });
    
    var posicion = $("#dist-foto-adm").attr("data-attr");
    var accion = $(this).attr("data-tipo");
    var nueva_posicion = 0;
    
    if(posicion == "-"){
        if(accion == 1){
            nueva_posicion= 1;
           }else{
            nueva_posicion= -1;
           }
    }else{
        
        if(accion == 1){
            nueva_posicion= parseInt(posicion)+1;
           }else{
            nueva_posicion= parseInt(posicion)-1;
           }
        
    }
    $( ".cont-index" ).css({     
        'background-position-y': nueva_posicion+"px",
    });
    $("#dist-foto-adm").attr("data-attr", nueva_posicion)
    $("#dist-foto-adm").val(nueva_posicion)
    
});
