<?php 
session_start();
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {

    $sesioninic=1;

}else{
    echo "<script> window.location.href='index.php';</script>";
}


$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();

?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Eventos</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
</head>
<body style="min-width:450px;height:100%">
<script type="text/javascript" src="js/jquery.min.js"></script>
    
<div id="header"></div>

<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Eliminar certificados</span>
    </div>
</div>
<div class="container cont-eventos" style="min-height: 100%;margin: -472px auto 0;">
    <div class="row" style="padding-top: 472px;">
        <div class="col-xs-12">
          <table class="table">
            <tbody>
              <tr>
                <td>Por DNI</td>
                <td><input class="form-control" type="text" placeholder="Ingrese el DNI del delegado a eliminar" name="dnielim-del" id="dnielim-del"></td>
                <td><button class='btn btn-sm btn-danger dnielim-del-btnmod'   href='#elimasis-dni-mod' data-target='#elimasis-dni-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin'>Eliminar</button></td>
              </tr>
              <tr>
                <td>Por evento</td>
                <td>
                    <select class="form-control sel-unibydel" name="id-evento" id="id-evento">
                      <option class="sel-prov" value="0">Seleccione el evento</option>

        <?php 
            $eventos_res=listareventosadm();

            $nombre_adm_eventos="";
            $inicio_adm_eventos="";
            $dscr_adm_eventos="";
            $link_adm_eventos="";
            $foto_adm_eventos="";


            while ($row=mysqli_fetch_assoc($eventos_res)){

                $id_adm_eventos=$row['id_evento'];
                $nombre_adm_eventos=$row['nombre_evento'];
                $inicio_adm_eventos=$row['fechainic_evento'];
                $dscr_adm_eventos=$row['descripcion_evento'];
                $link_adm_eventos=$row['link_evento'];
                $foto_adm_eventos=$row['foto_adm_eventos'];

                setlocale(LC_TIME, "");
                setlocale(LC_ALL,"es_ES.UTF8");

                //$fechain= date('m/Y', strtotime($fechain));
                $inicio_adm_eventos = DateTime::createFromFormat("Y-m-d", $inicio_adm_eventos);
                $inicio_adm_eventos = strftime("%d de %B",$inicio_adm_eventos->getTimestamp());

        ?>
                      <option class="sel-prov" value="<?php echo $id_adm_eventos ?>"><?php echo $nombre_adm_eventos ?></option>

        <?php 

            };
        ?>
                    </select>                  
                </td>
                <td>
                    <button class='btn btn-sm btn-danger cerrar-vota'   href='#elimasi-eve-mod' data-target='#elimasi-eve-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin'>Eliminar</button>
                </td>
              </tr>
              <tr>
                <td>Por fecha de subida</td>
                <td>
                    <select class="form-control sel-unibydel" name="id-eventofec" id="id-eventofec">
                      <option class="sel-prov" value="0">Seleccione el evento</option>

        <?php 
            $eventos_res=listareventosfechaadm();

            $nombre_adm_eventos="";
            $inicio_adm_eventos="";
            $dscr_adm_eventos="";
            $link_adm_eventos="";
            $foto_adm_eventos="";
            $fecemi_participante_certificadofor="";

            while ($row=mysqli_fetch_assoc($eventos_res)){
                
                //EVENTO
                $id_adm_eventos=$row['id_evento'];
                $nombre_adm_eventos=$row['nombre_evento'];
                $inicio_adm_eventos=$row['fechainic_evento'];
                $dscr_adm_eventos=$row['descripcion_evento'];
                $link_adm_eventos=$row['link_evento'];
                $foto_adm_eventos=$row['foto_adm_eventos'];
                
                setlocale(LC_TIME, "");
                setlocale(LC_ALL,"es_ES.UTF8");

                //$fechain= date('m/Y', strtotime($fechain));
                $inicio_adm_eventos = DateTime::createFromFormat("Y-m-d", $inicio_adm_eventos);
                $inicio_adm_eventos = strftime("%d de %B",$inicio_adm_eventos->getTimestamp());
                
                //CERTIICADOS
                $fecemi_participante_certificado=$row['fecemi_participante_certificado'];
                
                $fecemi_participante_certificadofor = DateTime::createFromFormat("Y-m-d", $fecemi_participante_certificado);
                $fecemi_participante_certificadofor = strftime("%d de %B",$fecemi_participante_certificadofor->getTimestamp());

        ?>
                      <option class="sel-prov" value="<?php echo $id_adm_eventos ?>" data-fecha="<?php echo $fecemi_participante_certificado ?>"><?php echo $nombre_adm_eventos ?>, subidos el <?php echo $fecemi_participante_certificadofor ?></option>

        <?php 

            };
        ?>
                    </select>         
                </td>
                <td>
                    <button class='btn btn-sm btn-danger cerrar-vota'   href='#elimfec-eve-mod' data-target='#elimfec-eve-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin'>Eliminar</button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
    </div>
</div>

<div id="footer"></div>
    
<div id="elimasis-dni-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Eliminar certificado</h4>
            </div>
            <div class="modal-body elimdeldni-mod-bod">
                
            </div>

        </div>
    </div>
</div> 
    
    
<div id="elimasi-eve-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Eliminar certificados</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea eliminar los certificados del evento: <span class="elimevento-dscr"></span>?</div>
                <div class="mensaje-deleve-elim"></div>
                <div class="vota-modbod-btn">
                    <form class="form-inline" role="form"  action="" method="post">
                        <input type="hidden" name="elim-eve-inp" id="elim-eve-inp" value="">
                        <button type="submit" class="btn btn-sm btn-warning elimdel-todos-btn" name="elim-evento-btn">Si</button>
                        <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> 
    
<div id="elimfec-eve-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Eliminar delegados</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea eliminar los certificados del evento <span class="elimeventofec-dscr"></span>?</div>
                <div class="vota-modbod-btn">
                    <form class="form-inline" role="form"  action="" method="post">
                        <input type="hidden" name="elimfec-eve-inp" id="elimfec-eve-inp" value="">
                        <input type="hidden" name="elimfec-fec-inp" id="elimfec-fec-inp" value="">
                        <button type="submit" class="btn btn-sm btn-warning elimdel-todos-btn" name="elim-eventofec-btn">Si</button>
                        <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div> 
    
    
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script>
$(function(){
    
   $("#header").load("header-adm.php", {
       xph: 2, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
<script>
    
$("#id-evento").change(function() {
    var id_evento=this.value;
    var nombre_evento = $(this).find("option:selected").text();
    
    $(".elimevento-dscr").html(nombre_evento);
    $("#elim-eve-inp").val(id_evento);

});
    
$("#id-eventofec").change(function() {
    var id_evento=this.value;
    var nombre_evento = $(this).find("option:selected").text();
    var fecha_emision= $(this).find("option:selected").attr("data-fecha");
    
    $(".elimeventofec-dscr").html(nombre_evento);
    $("#elimfec-eve-inp").val(id_evento);
    $("#elimfec-fec-inp").val(fecha_emision);

});
    
/*Funciones AJAX*/
    
    
$(document).on("click", '.elimdel-univ-btn',function(){
    
    var i1_1 = $(this).attr('data-id');
    
    $(".mensaje-deleuniv-elim").html("Eliminando...");
    
    $.ajax({
        url:'apost_eliminardel_universidad.php',
        type:'post',
        data:{
            'x_i1': i1_1
        },
        success: function (result) {
            $('#elimdel-univ-mod').modal('hide');
            if(result == 1){
                //console.log(result);
                alert("Ocurrió algún problema al eliminar los delegados. Si este inconveniente persiste, comuníquese con servicio técnico.");
            }else if(result == 2){
                alert("Se eliminaron los delegados correctamente.");
            }else if(result == 3){
                alert("El sistema de votación no ha identificado el dominio como Aneic. Por favor, contáctese con servicio técnico.");
            }else{

            }
            $(".mensaje-deleuniv-elim").html("");
            
        }
    });


});
    
    
$(document).on("click", '.dnielim-del-btnmod',function(){
    
    var i1_1 = $("#dnielim-del").val();
    
    $(".elimdeldni-mod-bod").html("Cargando...");
    
    $.ajax({
        url:'apost_eliminarasi_dni.php',
        type:'post',
        data:{
            'x_i1': i1_1
        },
        success: function (result) {
            $(".elimdeldni-mod-bod").html(result);
        }
    });


});
    
    
</script>
    
    
<?php

if (isset($_POST['elimasi-dni-btn'])){

    if(!empty($_POST['idcert-elim-inp'])){
        
        $id_certificado_eliminar = $_POST['idcert-elim-inp'];

        $id_cert_res = eliminarcertificadoasis($id_certificado_eliminar);
        
        if($id_cert_res =="Error"){
            echo "<script>alert('Hubo un error al eliminar el certificado. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Se eliminó el certificado con éxito.')</script>";
            echo "<script> window.location.href='adm_elim_certificados.php';</script>";
        }
    }else{

        echo "<script>alert('Hubo un error en el formulario. Si el problema persiste comuníquese con servicio técnico.')</script>";
    }

}
  
if (isset($_POST['elim-evento-btn'])){

    if(!empty($_POST['elim-eve-inp'])){
        
        $id_evento_elimcert = $_POST['elim-eve-inp'];

        $id_certev_res = eliminarcertificadosev($id_evento_elimcert);
        
        if($id_certev_res =="Error"){
            echo "<script>alert('Hubo un error al eliminar los certificados. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Se eliminaron los certificados con éxito.')</script>";
            echo "<script> window.location.href='adm_elim_certificados.php';</script>";
        }
    }else{

        echo "<script>alert('Hubo un error en el formulario. Si el problema persiste comuníquese con servicio técnico.')</script>";
    }

}
  
  
if (isset($_POST['elim-eventofec-btn'])){

    if(!empty($_POST['elimfec-eve-inp']) && !empty($_POST['elimfec-fec-inp'])){
        
        $id_eventofec_elimcert = $_POST['elimfec-eve-inp'];
        $fecha_evento_elimcert = $_POST['elimfec-fec-inp'];

        $id_certfecev_res = eliminarcertificadosfecev($id_eventofec_elimcert, $fecha_evento_elimcert);
        
        if($id_certfecev_res =="Error"){
            echo "<script>alert('Hubo un error al eliminar los certificados. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Se eliminaron los certificados con éxito.')</script>";
            echo "<script> window.location.href='adm_elim_certificados.php';</script>";
        }
    }else{

        echo "<script>alert('Hubo un error en el formulario. Si el problema persiste comuníquese con servicio técnico.')</script>";
    }

}
  
if (isset($_POST['elimdel-todos-btn'])){

    $estado_elim = eliminartodosdelegados();

    if($estado_elim =="Error"){
        echo "<script>alert('Hubo un error al eliminar los delegados. Por favor intenta más tarde.')</script>";
        exit();
    }else{
        echo "<script>alert('Se eliminaron los delegados con éxito.')</script>";
        echo "<script> window.location.href='adm_elim_delegados.php';</script>";
    }

}

if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}

    
if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";        
}   
?>

</body>
</html>