<?php 
session_start();
$nombres="";
$sesioninic=0;

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_adm_user'])) {
    $sesioninic=1;
}


$foto_sup_res= listarfotoadm();

$url_fotos_arr = array();
while ($row=mysqli_fetch_assoc($foto_sup_res)) {
    $url_adm_fotos =$row['url_adm_fotos'];
    $url_fotos_arr[] = $url_adm_fotos;
}
mysqli_free_result($foto_sup_res);
desconectar();

$info_res= listarinfoadm();
while ($row=mysqli_fetch_assoc($info_res)) {
    $horainic_adm_info =$row['horainic_adm_info'];
    $horafin_adm_info =$row['horafin_adm_info'];
    $contacto_adm_info =$row['contacto_adm_info'];
}
mysqli_free_result($info_res);
desconectar();

$universidadeslista = listaruniversidadesdelegados();

$amazonas   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ancash     = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$apurimac   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$arequipa   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ayacucho   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$cajamarca  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$callao     = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$cusco      = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$huancavelica = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$huanuco = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ica = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$junin = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$lalibertad = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$lambayeque = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$lima  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$loreto  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$madrededios = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$moquegua   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$pasco   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$piura  = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$puno   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$sanmartin   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$tacna   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$tumbes   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";
$ucayali   = "<option class='sel-prov' value='0'>Seleccione la universidad</option>";

$universidadesarr = array();

while ($row=mysqli_fetch_assoc($universidadeslista)) {
    $id_universidad=$row['id_universidad'];
    $nombre_universidad=$row['nombre_universidad'];
    $departamento=$row['departamento'];

    switch ($departamento) {
        case "Amazonas":
            $amazonas   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Áncash":
            $ancash     .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Apurímac":
            $apurimac   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Arequipa":
            $arequipa   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Ayacucho":
            $ayacucho   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Cajamarca":
            $cajamarca  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Callao":
            $callao     .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Cusco":
            $cusco      .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Huancavelica":
            $huancavelica .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Huánuco":
            $huanuco .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Ica":
            $ica .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Junín":
            $junin .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "La Libertad":
            $lalibertad .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Lambayeque":
            $lambayeque .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Lima":
            $lima  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Loreto":
            $loreto  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Madre de Dios":
            $madrededios .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Moquegua":
            $moquegua   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Pasco":
            $pasco   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Piura":
            $piura  .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Puno":
            $puno   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "San Martín":
            $sanmartin   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Tacna":
            $tacna   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Tumbes":
            $tumbes   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
        case "Ucayali":
            $ucayali   .= "<option class='sel-prov' value='$id_universidad'>".$nombre_universidad." </option>";
            break;
    }


}
mysqli_free_result($universidadeslista);

?>
<!DOCTYPE html>
<html ng-app="">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aneic Perú - Universidades y delegados</title>
    <link rel="icon" href="img/logo-aneic.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link href="css/princ.css" rel="stylesheet">
</head>
<body>
<script type="text/javascript" src="js/jquery.min.js"></script>

<div id="header"></div>
    
<div class="unidele-etiq-wrapp">
    <div class="unidele-etiq-inn">
        <span>Registro de delegados</span>
    </div>
</div>
<?php
    $msg="";
  if(isset($_GET['xstate']))  {
      
    if($_GET['xstate'] == 1){
        
    $msg="Se añadió el delegado con éxito.";
    }
      
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <b><?php echo $msg; ?></b>
        </div>
    </div>
</div>
<?php
    
  }
?>
<div class="container cont-unidele">
    <div class="row regdel-row">
        <div class="univdel-card-wrapp">
            <div class="nodelegado-registrado">
                 <form class="form-horizontal form-registdel" role="form" action="" method="post"  enctype="multipart/form-data">
                    <div class="form-group row">
                      <label for="nombres-delegado" class="col-sm-2 col-form-label">Nombres</label>
                      <div class="col-sm-10">
                        <input class="form-control" type="text" placeholder="Ingrese sus nombres" name="nombres-delegado" id="nombres-delegado">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="apellidos-delegado" class="col-sm-2 col-form-label">Apellidos</label>
                      <div class="col-sm-10">
                        <input class="form-control" type="text" placeholder="Ingrese sus apellidos" name="apellidos-delegado" id="apellidos-delegado">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="dni-delegado" class="col-sm-2 col-form-label">DNI</label>
                      <div class="col-sm-10">
                        <input class="form-control" type="number" placeholder="Ingrese su DNI" name="dni-delegado" id="dni-delegado">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="correo-delegado" class="col-sm-2 col-form-label">Correo</label>
                      <div class="col-sm-10">
                        <input class="form-control" type="email" placeholder="Ingrese su correo" name="correo-delegado" id="correo-delegado">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="numero-delegado" class="col-sm-2 col-form-label">Teléfono</label>
                      <div class="col-sm-10">
                        <input class="form-control" type="number" placeholder="Ingrese su número celular" name="numero-delegado" id="numero-delegado">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="cargo-delegado" class="col-sm-2 col-form-label">Cargo</label>
                      <div class="col-sm-10">
                        <select class="form-control sel-unibydel" name="cargo-delegado" id="cargo-delegado">
                          <option class="sel-prov" value="1">Delegado</option>
                          <option class="sel-prov" value="2">Sub-Delegado</option>
                          <option class="sel-prov" value="3">Primer Accesitario</option>
                          <option class="sel-prov" value="4">Segundo Accesitario</option>
                          <option class="sel-prov" value="5">Miembro honorario</option>
                          <option class="sel-prov" value="6">Secretario Corede</option>
                          <option class="sel-prov" value="7">Consejo directivo</option>
						  <option class="sel-prov" value="8">Sub Secretario de Corede</option>
						  <option class="sel-prov" value="9">Embajador Internacional</option>
                        </select>
                      </div>
                    </div>
                     
                    <div class="form-group row">
                      <label for="Departamento" class="col-sm-2 col-form-label">Departamento</label>
                      <div class="col-sm-10">
                        <select class="form-control sel-unibydel" name="departamento" id="departamento">
                            <option class="sel-dep" value="Departamento">Departamento</option>
                            <option class="sel-dep" value="Amazonas">Amazonas</option>
                            <option class="sel-dep" value="Áncash">Ancash</option>
                            <option class="sel-dep" value="Apurímac">Apurímac</option>
                            <option class="sel-dep" value="Arequipa">Arequipa</option>
                            <option class="sel-dep" value="Ayacucho">Ayacucho</option>
                            <option class="sel-dep" value="Cajamarca">Cajamarca</option>
                            <option class="sel-dep" value="Callao">Callao</option>
                            <option class="sel-dep" value="Cusco">Cusco</option>
                            <option class="sel-dep" value="Huancavelica">Huancavelica</option>
                            <option class="sel-dep" value="Huánuco">Huánuco</option>
                            <option class="sel-dep" value="Ica">Ica</option>
                            <option class="sel-dep" value="Junín">Junín</option>
                            <option class="sel-dep" value="La Libertad">La Libertad</option>
                            <option class="sel-dep" value="Lambayeque">Lambayeque</option>
                            <option class="sel-dep" value="Lima">Lima</option>
                            <option class="sel-dep" value="Loreto">Loreto</option>
                            <option class="sel-dep" value="Madre de Dios">Madre de Dios</option>
                            <option class="sel-dep" value="Moquegua">Moquegua</option>
                            <option class="sel-dep" value="Pasco">Pasco</option>
                            <option class="sel-dep" value="Piura">Piura</option>
                            <option class="sel-dep" value="Puno">Puno</option>
                            <option class="sel-dep" value="San Martín">San Martín</option>
                            <option class="sel-dep" value="Tacna">Tacna</option>
                            <option class="sel-dep" value="Tumbes">Tumbes</option>
                            <option class="sel-dep" value="Ucayali">Ucayali</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row" id="row-univ">
                      <label for="univ-dep" class="col-sm-2 col-form-label">Universidad</label>
                      <div class="col-sm-10">
                        <select class="form-control sel-unibydel" name="univ-dep" id="univ-dep">
                          <option class="sel-prov" value="0">Seleccione el departamento</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row" id="row-univ-nue" style="display:none">
                      <label for="univag-dep" class="col-sm-2 col-form-label">Agregar universidad</label>
                      <div class="col-sm-10">
                        <input class="form-control" type="text" placeholder="Ingrese el nombre de la universidad a agregar." name="univag-dep" id="univag-dep">
                        <span>No se olvide de también seleccionar el departamento donde se encuentra la universidad.<br> Al ingresar el nombre especifique si es sede. Por por ejemplo: "Universidad del Perú - Sede Lima"</span>
                      </div>
                    </div>
                    <div class="form-group row row-delegado">
                      <label for="nuevau-delegado" class="col-sm-12 col-form-label">No encuentro mi universidad <input type="checkbox" class="archivo-subir" id="nuevau-delegado" name="nuevau-delegado" value="noenc"></label>
                    </div>
                    <div class="form-group row">
                      <label for="" class="col-sm-12 col-form-label">Importante: Los archivos a subir no deben pesar más de 8 mb. </label>
                    </div>
                    <div class="form-group row">
                      <label for="" class="col-sm-2 col-form-label">Foto</label>
                      <div class="col-sm-10">
                        <label for="foto-delegado"><a class="btn btn-md form-control input-sm selec-arch">Seleccionar foto</a></label>
                        <div id="nombrefoto"></div>
                          <input type="file" class="archivo-subir" id="foto-delegado" name="foto-delegado" style="display:none" data-id="1">
                        <span class="span-foto-estado" id="estado-cambio-1"></span>
                      </div>
                    </div>
                    <div class="form-group row row-delegado">
                      <label for="" class="col-sm-2 col-form-label">Documento de acreditación</label>
                      <div class="col-sm-10">
                        <label for="acredita-delegado"><a class="btn btn-md form-control input-sm selec-arch">Seleccionar archivo</a></label>
                        <div id="nombrefoto"></div>
                          <input type="file" class="archivo-subir" id="acredita-delegado" name="acredita-delegado" style="display:none" data-id="2">
                        <span class="span-foto-estado" id="estado-cambio-2"></span>
                      </div>
                    </div>
                    <div class="form-group row row-delegado">
                      <label for="" class="col-sm-2 col-form-label"></label>
                      <div class="col-sm-10">
                          <span>Decargue este <a href="documentoej/registro.xlsx" class="descarg-exc" download="registrodedelegados"><span class="glyphicon glyphicon-download"></span> archivo</a>, complételo con la información requerida y luego súbalo con el botón "Subir Excel"</span>
                      </div>
                    </div>
                    <div class="form-group row row-delegado">
                      <label for="" class="col-sm-2 col-form-label">Excel con registro</label>
                      <div class="col-sm-10">
                        <label for="excel-delegado"><a class="btn btn-md form-control input-sm selec-arch">Subir Excel</a></label>
                        <div id="nombrefoto"></div>
                          <input type="file" class="archivo-subir" id="excel-delegado" name="excel-delegado" style="display:none" data-id="3">
                        <span class="span-foto-estado" id="estado-cambio-3"></span>
                      </div>
                    </div>
                     <div class="form-group row">
                          <label for="" class="col-sm-2 col-form-label"></label>
                          <div class="col-sm-10">
                            <button type="submit" class="btn btn-sub-regdel btn-sm" name="reg-del-sub" onclick="$('#cargando-reg').show()">Guardar</button>
                          </div>
                     </div>
                     <div class="form-group row">
                         <span id="cargando-reg" style="display:none">Cargando...</span>
                     </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="footer"></div>

<div id="login-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                <h4 class="modal-title">Login</h4>
            </div>
            <div class="modal-body">
                 <form class="form-inline" role="form" action="" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="correo">Correo</label>
                        <input type="email" class="form-control input-sm" name="correo" id="user_mail" placeholder="Correo" required>
                    </div>     
                    <div class="form-group">
                        <label class="sr-only" for="contrasena">Contraseña</label>
                        <input type="password" class="form-control input-sm" name="contrasena" id="password" placeholder="Contraseña" required>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox"><small>Recordarme</small></label>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm" name="ingresar" >Entrar</button>
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/princ.js"></script>
<script src="js/jquery.carouFredSel.js"></script>
<script>

var departamentos ={

    "Departamento":'<option class="sel-prov" value="universidad">Seleccione el departamento</option>',
    "Amazonas": "<?php echo $amazonas; ?>",
    "Áncash": "<?php echo $ancash; ?>",
    "Apurímac": "<?php echo $apurimac; ?>",
    "Arequipa": "<?php echo $arequipa; ?>",
    "Ayacucho": "<?php echo $ayacucho; ?>",
    "Cajamarca": "<?php echo $cajamarca; ?>",
    "Callao": "<?php echo $callao; ?>",
    "Cusco": "<?php echo $cusco; ?>",
    "Huancavelica": "<?php echo $huancavelica; ?>",
    "Huánuco": "<?php echo $huanuco; ?>",
    "Ica": "<?php echo $ica; ?>",
    "Junín": "<?php echo $junin; ?>",
    "La Libertad": "<?php echo $lalibertad; ?>",
    "Lambayeque": "<?php echo $lambayeque; ?>",
    "Lima": "<?php echo $lima; ?>",
    "Loreto": "<?php echo $loreto; ?>",
    "Madre de Dios": "<?php echo $madrededios; ?>",
    "Moquegua": "<?php echo $moquegua; ?>",
    "Pasco": "<?php echo $pasco; ?>",
    "Piura": "<?php echo $piura; ?>",
    "Puno": "<?php echo $puno; ?>",
    "San Martín": "<?php echo $sanmartin; ?>",
    "Tacna": "<?php echo $tacna; ?>",
    "Tumbes": "<?php echo $tumbes; ?>",
    "Ucayali": "<?php echo $ucayali; ?>",
    
};
/*
$("#departamento").change(function() {
    var temp=0;
    var departamento= this.value;
    var prov=departamentos[departamento];
    var provarr = prov.split(",");
    var startlista="<ul>";

    var midlista="";
    for(provinc in provarr){
        midlista+="<option class='sel-prov' value='"+provarr[temp]+"'>"+provarr[temp]+"</option>";
        temp++;
    }
    var listaprov=startlista+midlista;
    $("#univ-dep").html(listaprov);
});
*/
$("#departamento").change(function() {
    var temp=0;
    var departamento= this.value;
    var prov=departamentos[departamento];
    $("#univ-dep").html(prov);
    //console.log(prov);
});
    
/*
    
$("#univ-dep").change(function() {
    console.log("asd");
    var id_universidad=this.value;

    $(".cont-unidele").css({
        "opacity": "0.5",
        "pointer-events":"none"
    });
    $(".univdel-card-wrapp").html("Cargando...");

    $.ajax({url:"apost_append_delegados.php?xid_universidad="+id_universidad,cache:false,success:function(result){
        $(".univdel-card-wrapp").html(result);
                $(".cont-unidele").css({
                    "opacity": "1",
                    "pointer-events":"inherit"
                });
    }});
    
});
*/
</script>
    
<script>
$(function(){
    
   $("#header").load("header.php", {
       xph: 2, 
       xhs: <?php echo $sesioninic ?>,
       xdf1: "<?php echo $url_fotos_arr[0]; ?>", 
       xdf2: "<?php echo $url_fotos_arr[1]; ?>", 
   });
   $("#footer").load("footer.php", {
       xdf: "<?php echo $url_fotos_arr[1]; ?>", 
       xdt: "<?php echo $contacto_adm_info; ?>", 
   });
});
</script>
    
<?php
  if (isset($_POST['reg-del-sub'])){
      
    if($image_size=$_FILES['foto-delegado']['size']>0 && !empty($_POST['nombres-delegado']) && !empty($_POST['apellidos-delegado']) && !empty($_POST['dni-delegado']) && !empty($_POST['correo-delegado']) && !empty($_POST['numero-delegado'])  && !empty($_POST['departamento'])){
        $csvMimes = array('image/gif','image/png' ,'image/jpg','image/jpeg','image/bmp','image/ico','image/apng','image/xbm','image/webp','image/svg','image/GIF','image/PNG' ,'image/JPG', 'image/JPEG','image/BMP','image/ICO','image/APNG','image/XBM','image/WEBP','image/SVG');
        
        $filename = $_FILES['foto-delegado']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        
        if(!in_array($_FILES['foto-delegado']['type'],$csvMimes)) {
            echo "<script>alert('Por favor selecciona una imagen')</script>";
            exit();
        }else{
            
            
            $nuevo_nombre=randomalfanume(25);
            $nuevo_nombre= $nuevo_nombre.".".$ext;
            $url_foto= "fotos_delegado/".$nuevo_nombre;

            $image_temp_name=$_FILES['foto-delegado']['tmp_name'];
            if(move_uploaded_file($image_temp_name,$url_foto)){

                //Datos generales
                $nombres_delegado=$_POST['nombres-delegado'];
                $apellidos_delegado=$_POST['apellidos-delegado'];
                $dni_delegado=$_POST['dni-delegado'];
                $correo_delegado=$_POST['correo-delegado'];
                $celular_delegado=$_POST['numero-delegado'];
                $puesto_delegado=$_POST['cargo-delegado'];
                $departamento=$_POST['departamento'];
                
                
                if(isset($_POST['nuevau-delegado'])){
                    $nombre_universidad=$_POST['univag-dep'];
                    $estado_universidad = 0;
                    $id_universidad= anadiruniversidad($nombre_universidad, $departamento, $estado_universidad);
                    if($id_universidad =="Error"){
                        echo "<script>alert('Hubo un error al añadir la universidad. Por favor intenta más tarde.')</script>";
                        exit();
                    }
                }else{
                    $id_universidad=$_POST['univ-dep'];
                }
                
                $id_delegado=0;
                if($puesto_delegado !=1){
                    $id_delegado=anadirdelegado($id_universidad, $puesto_delegado, $nombres_delegado, $apellidos_delegado, $dni_delegado, $correo_delegado, $celular_delegado, $url_foto);
                }
                

                    if(strpos($id_delegado, 'Error') !== false){
                        if(strpos($id_delegado, 'Duplicate') !== false){
                            echo "<script>alert('Ya se ha registrado un delegado en la universidad seleccionada, comuníquese con presidencia.aneicperu@gmail.com para solucionar su problema.')</script>";
                            exit();
                        }else{
                            echo '<script>alert("Hubo un error al añadir al delegado. Por favor intenta más tarde. '.$id_delegado.'")</script>';
                            exit();
                        }
                    }else{
                        
                        if($puesto_delegado !=1){
                            echo "<script>alert('Se ha grabado con éxito el delegado.')</script>";
                            echo "<script> window.location.href='registrodelegados.php?xstate=1';</script>";
                            exit();
                        }else{
                            
                            $id_delegado=anadirdelegado($id_universidad, $puesto_delegado, $nombres_delegado, $apellidos_delegado, $dni_delegado, $correo_delegado, $celular_delegado, $url_foto);
                            
                            if(strpos($id_delegado, 'Error') !== false){
                                if(strpos($id_delegado, 'Duplicate') !== false){
                                    echo "<script>alert('Ya se ha registrado un delegado en la universidad seleccionada, comuníquese con presidencia.aneicperu@gmail.com para solucionar su problema.')</script>";
                                    exit();
                                }else{
                                    echo '<script>alert("Hubo un error al añadir al delegado. Por favor intenta más tarde. '.$id_delegado.'")</script>';
                                    exit();
                                }
                            }else{
                                if($_FILES['acredita-delegado']['size']>0 && $_FILES['excel-delegado']['size']>0 ){

                                    $filename1 = $_FILES['acredita-delegado']['name'];
                                    $filename2 = $_FILES['excel-delegado']['name'];

                                    $ext1 = pathinfo($filename1, PATHINFO_EXTENSION);
                                    $ext2 = pathinfo($filename2, PATHINFO_EXTENSION);

                                    $nuevo_nombre1=randomalfanume(25);
                                    $nuevo_nombre1= $nuevo_nombre1.".".$ext1;
                                    $url_documento_delegado1= "documentos_delegados/".$nuevo_nombre1;

                                    $nuevo_nombre2=randomalfanume(25);
                                    $nuevo_nombre2= $nuevo_nombre2.".".$ext2;
                                    $url_documento_delegado2= "documentos_delegados/".$nuevo_nombre2;

                                    $image_temp_name1=$_FILES['acredita-delegado']['tmp_name'];
                                    $image_temp_name2=$_FILES['excel-delegado']['tmp_name'];


                                    if(move_uploaded_file($image_temp_name1,$url_documento_delegado1) && move_uploaded_file($image_temp_name2,$url_documento_delegado2)){

                                        $resultado=anadirarchivosdelegado($id_universidad, $url_documento_delegado1, $url_documento_delegado2);
                                        if($id_delegado =="Error"){
                                            echo "<script>alert('Hubo un error al cargar los archivos. Por favor intenta más tarde.')</script>";
                                            exit();
                                        }else{
                                            echo "<script>alert('Se añadió al delegado y los archivos con éxito.')</script>";
                                            echo "<script> window.location.href='registrodelegados.php?xstate=1';</script>";
                                        }

                                    }else{
                                        echo "<script>alert('Hubo un error al mover los archivos. Por favor intenta más tarde.')</script>";
                                        exit();
                                    }



                                }else{
                                    echo "<script>alert('Debe subir ambos archivos para completar la acreditación.')</script>";
                                    exit();
                                }                  
                            
                            
                            }

                        
                                                
                        }
                        
                        
                    }
                
            }else{
                echo "<script>alert('Hubo un error al cargar la imagen. Por favor intenta más tarde.')</script>";
                exit();
            }


        }
        
        
    }else{
        echo "<script>alert('Por favor llena todos los datos.')</script>";
    }

}

if(isset($_POST['ver-votdel-btn'])){

    if(!empty($_POST['dni-votar']) && !empty($_POST['cod-votar'])){
        
        $dni_delegado_votar = $_POST['dni-votar'];
        $cod_delegado_votar = $_POST['cod-votar'];

        $info_delegado = listarinfodelegadodni($dni_delegado_votar);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_delegado)) {
            $id_delegado =$row['id_delegado'];
            $puesto_delegado =$row['puesto_delegado'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){
            
            $codigo_delegado_tmp=$dni_delegado_votar*7*13*37;//para codificar el código

            $codigo_delegado_tmp = substr($codigo_delegado_tmp, -4);

            if($cod_delegado_votar == $codigo_delegado_tmp){
                
                unset($_SESSION['id_delegado']);
                unset($_SESSION['puesto_delegado']);
                
                $_SESSION["id_delegado"]=$id_delegado;
                $_SESSION["puesto_delegado"]=$puesto_delegado;
                
                $id_delegado_encriptado = doEncrypt($id_delegado);
                $puesto_delegado_encriptado = doEncrypt($puesto_delegado);
                                
                //echo "<script>window.location.href='lista_delegado_votacion.php?xi_1=$id_delegado_encriptado&xi_2=$puesto_delegado_encriptado';</script>";
                echo "<script>window.location.href='lista_delegado_votacion.php';</script>";

            }else{
                echo "<script> alert('Código de votación incorrecto.');</script>";
            }
            
        }else{
            echo "<script> alert('No se encuentra un delegado con el DNI brindado.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}

if(isset($_POST['ver-asisdel-btn'])){

    if(!empty($_POST['dni-asist']) && !empty($_POST['cod-asist'])){
        
        $dni_delegado_asist = $_POST['dni-asist'];
        $cod_delegado_asist = $_POST['cod-asist'];

        $info_asistencia = listarinfoasis($dni_delegado_asist, $cod_delegado_asist);
        $cuenta_tmp = 0;

        while ($row=mysqli_fetch_assoc($info_asistencia)) {
            $id_delegado_asis =$row['id_delegado'];
            $id_asistencia_asis =$row['id_asistencia'];
            $estado_asistencia_asis =$row['estado_asistencia'];
            $cuenta_tmp++;
        }

        if($cuenta_tmp > 0){

            if($estado_asistencia_asis > 0){

                $resultado_asi_agr = registrarasistenciadel($id_delegado_asis, $id_asistencia_asis);
                $cuenta_tmp = 0;

                if($resultado_asi_agr){
                    echo "<script> alert('Se registró la asistencia.');</script>";
                }else{
                    echo "<script> alert('Ha ocurrido un problema al registrar la asistencia. Inténtelo de nuevo, o comuníquese con algún representante de Aneic para solucionar el problema.');</script>";
                }

            }else{
                echo "<script> alert('La asistencia ya se ha cerrado.');</script>";
            }
            
        }else{
            echo "<script> alert('La asistencia no se ha abierto para usted, o el código brindado es incorrecto.');</script>";
        }
        
        
    }else{
        echo "<script>alert('Ingrese ambos datos.');</script>";
        exit();
    }

}


if(isset($_POST['cont-sub'])){

    if(!empty($_POST['nombres-contacto']) && !empty($_POST['apellidos-contacto']) && !empty($_POST['consulta-contacto']) && (!empty($_POST['tlf-contacto']) || !empty($_POST['correo-contacto']))){
        
        $nombres_contacto = $_POST['nombres-contacto'];
        $apellidos_contacto = $_POST['apellidos-contacto'];
        $consulta_contacto = $_POST['consulta-contacto'];
        $tlf_contacto = $_POST['tlf-contacto'];
        $correo_contacto = $_POST['correo-contacto'];

        $mensaje_contacto = enviarmensajecontacto($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        $mensaje_contacto2 = enviarmensajecontactoconf($nombres_contacto, $apellidos_contacto, $consulta_contacto, $tlf_contacto, $correo_contacto);
        
        if($mensaje_contacto =="Error" || $mensaje_contacto2 =="Error"){
            echo "<script>alert('Hubo un error al enviar el mensaje. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Su mensaje ha sido enviado.')</script>";
        }
        
    }else{
        echo "<script>alert('Ingrese al menos un dato de contacto y la consulta.');</script>";
        exit();
    }

}


    
if (isset($_REQUEST['cerrar-sesion'])){
    session_destroy();
    echo "<script>window.location.href='index.php';</script>";
}    

if (isset($_POST['codigo-recu'])){
	if(!empty($_POST['dni-recu'])){
		$dni_delegado_rec=$_POST['dni-recu'];

		$result=recuperarcontradel($dni_delegado_rec);

	}else{
        echo "<script> alert('Ingrese un DNI.');</script>";
    }
}


?>
</body>
</html>